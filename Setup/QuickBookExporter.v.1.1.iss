#include "scripts\products.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#include "scripts\products\dotnetfx40.iss"

#define Company = 'IPC'
#define ShortCompany = 'IPC'
#define MadeBy = 'EZUniverse '

// not change RegCompany - always have to be EZuniverse Inc.
//  #define RegCompany = 'EZUniverse Inc.'
//  #define AppID = '{4B40DA48-B22B-4946-95E3-08816CA895C7}'
//  #define AppName = 'QuickBookExporter'
//  #define AppNameOnDesktop = 'QuickBook Exporter'
//  #define SetupName = 'QuickBookExporter'
//  #define Versiu = '1.1'
//  #define Rev = '.33.33'

#define InstallVideo = 'false'
#define InstallUpdateCenter = 'true'
#define RunSurvey = 'false'
// setups
#define EZVideoPlayerReg = 'SubwayVideoPlayer.1.1'
#define EZVideoPlayerSetup = 'SubwayVideoPlayer.Setup.v.1.1.0.16.exe'
#define EZVideoPlayerVersion = '1.1.0.16'
#define EZUpdateCenterReg = 'SubwayUpdateCenter'
#define EZUpdateCenterSetup = 'SubwayUpdateCenter.Setup.v.1.0.0.24.exe'
#define EZUpdateCenterVersion = '1.0.0.24'
#define QBSDK = 'QBFC12_0Installer.exe'
#define QBSDKReg = 'QBSDK'
#define QBSDKVersion = '12'
#define MSSQL = 'SQLEXPR_x86_ENU.exe'
#define WinInst = 'WindowsXP-KB942288-v3-x86.exe'

// other
#define CommonFolder = '_CommonSetup'

[CustomMessages]
win2000sp3_title=Windows 2000 Service Pack 3
winxpsp2_title=Windows XP Service Pack 2
PostInstallTasks=Post install tasks.
PleaseWait=Please wait...
InstallingVisualCRuntimes=Installing Visual C++ 2005 SP1 Runtimes... This might take a few minutes.
InstallingCodecs=Installing codecs... This might take a few minutes.
InstallingEZVideoPlayer=Installing SubwayVideoPlayer... This might take a few minutes.
InstallingEZUpdateCenter=Installing SubwayUpdateCenter... This might take a few minutes.
InstallingQBSDK=Installing QBSDK... This might take a few minutes.
InstallingMSSQL=Installing database server and instance.
CompilingApplication=Compiling application... This might take a few minutes.
OpenReleaseNotes=Open Release Notes
OpenHelpFile=Open Help File
;AddExceptionRule=Add exception rule
;InstallingDVRDrivers=Installing HEM DVR Drivers... This might take a few minutes.
; cz
CZ.PostInstallTasks=Po instalaci �koly.
CZ.PleaseWait=Pros�m, vy�kejte...
CZ.InstallingVisualCRuntimes=Instalace Visual C + + 2005 SP1 Runtime ... To m��e trvat n�kolik minut.
CZ.InstallingCodecs=Instalace kodek� ... To m��e trvat n�kolik minut.
CZ.InstallingEZVideoPlayer=Instalace SubwayVideoPlayer... To m��e trvat n�kolik minut.
CZ.InstallingEZUpdateCenter=Instalace SubwayUpdateCenter... To m��e trvat n�kolik minut.
CZ.CompilingApplication=Kompilace aplikace ... To m��e trvat n�kolik minut.
CZ.OpenReleaseNotes=Open Pozn�mky k vyd�n�
CZ.OpenHelpFile=Otev��t soubor n�pov�dy
;CZ.AddExceptionRule=Add exception rule
;CZ.InstallingDVRDrivers=Instalace ovlada�� HEM DVR ... To m��e trvat n�kolik minut.
; PL
PL.PostInstallTasks=Zadania po instalacji.
PL.PleaseWait=Prosz� czeka�...
PL.InstallingVisualCRuntimes=Instalacja Visual C++ 2005 SP1 Runtimes... To mo�e zaj�� kilka minut.
PL.InstallingCodecs=Instalacja kodek�w... To mo�e zaj�� kilka minut.
PL.InstallingEZVideoPlayer=Instalacja SubwayVideoPlayer... To mo�e zaj�� kilka minut.
PL.InstallingEZUpdateCenter=Instalacja SubwayUpdateCenter... To mo�e zaj�� kilka minut.
PL.CompilingApplication=Kompilowanie aplikacji... To mo�e zaj�� kilka minut.
PL.OpenReleaseNotes=Otwarcie Release manifestu
PL.OpenHelpFile=Otwarcie pliku pomocy
;PL.AddExceptionRule=Dodaj wyj�tek
;PL.InstallingDVRDrivers=Instalacja HEM DVR Drivers... To mo�e zaj�� kilka minut.

[Languages]
Name: en; MessagesFile: compiler:Default.isl
Name: PL; MessagesFile: compiler:Languages\Polish.isl
Name: CZ; MessagesFile: compiler:Languages\Polish.isl; 

[Setup]
CloseApplications=false
OutputDir={#AppName}.v.{#Versiu}
OutputBaseFilename={#SetupName}.Setup.{#SqlBit}.v.{#Versiu}{#Rev}
AppCopyright=Copyright � 2005-2014 by EZUniverse� Inc.
AppName={#AppName}
AppVerName={#AppName}.v.{#Versiu}{#Rev}
VersionInfoVersion={#Versiu}{#Rev}
LanguageDetectionMethod=locale
ShowLanguageDialog=no
DefaultDirName={pf}\{#ShortCompany}\{#AppName}.v.{#Versiu}
DefaultGroupName={#ShortCompany}\{#AppName}.v.{#Versiu}
AlwaysUsePersonalGroup=false
DisableProgramGroupPage=false
DisableDirPage=false
AppID={{#AppID}
SetupIconFile={#CommonFolder}\SetupIcons\inst.ico
UninstallLogMode=overwrite
AppPublisher={#Company}
AppPublisherURL=www.{#ShortCompany}oop.com
AppVersion={#Versiu}{#Rev}
AppSupportURL=www.{#ShortCompany}oop.com
AppUpdatesURL=www.{#ShortCompany}oop.com
UninstallDisplayIcon=
UninstallDisplayName={#AppNameOnDesktop}.v.{#Versiu}
WizardImageFile={#CommonFolder}\SetupImages\{#AppName}_Wizard.bmp
WizardSmallImageFile={#CommonFolder}\SetupImages\{#AppName}_WizardSmall.bmp
PrivilegesRequired=admin
LicenseFile=..\Application\Docs\{#AppName}(TM) License Agreement.rtf
SignedUninstaller=false
Compression=lzma
InternalCompressLevel=normal
VersionInfoCompany={#Company}
SetupLogging=yes

[Files]
Source: ..\Application\Bin\*.*; DestDir: {app}\Bin; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly onlyifdoesntexist; Permissions: users-full; Tasks: 
Source: ..\Application\App\*.*; DestDir: {app}\App; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly; Permissions: users-full
Source: ..\Application\Debug\*.*; DestDir: {app}\Debug; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly; Permissions: users-full
Source: ..\Application\Docs\*.*; DestDir: {app}\Docs; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly; Permissions: users-full
;Source: ..\Application\Plugin\*.*; DestDir: {app}\Plugin; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly; Permissions: users-full
Source: ..\Application\Bin\XmlRuntime.exe; DestDir: {app}\Bin; Flags: overwritereadonly replacesameversion; Permissions: users-full
Source: {#CommonFolder}\SetupIcons\*.*; DestDir: {app}\App\Icons; Flags: onlyifdoesntexist; Permissions: users-full
Source: {#CommonFolder}\WebSite\WebSite {#ShortCompany}.url; DestDir: {app}; Flags: overwritereadonly replacesameversion; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\ffdshow-rev2547_20081228.exe; DestDir: {tmp}; DestName: ffdshow-rev2547_20081228.exe; Flags: onlyifdoesntexist; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\ffdshow.reg; DestDir: {app}\Bin; DestName: ffdshow.reg; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\{#EZUpdateCenterSetup}; DestDir: {tmp}; DestName: {#EZUpdateCenterSetup}; Flags: onlyifdoesntexist; Permissions: users-full
Source: {#CommonFolder}\{#QBSDK}; DestDir: {tmp}; DestName: {#QBSDK}; Flags: onlyifdoesntexist; Permissions: users-full
Source: {#CommonFolder}\{#MSSQL}; DestDir: {tmp}; DestName: {#MSSQL}; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\{#WinInst}; DestDir: {tmp}; DestName: {#WinInst}; Flags: overwritereadonly; Permissions: users-full
; DEPLOY DACPAC
Source: {#CommonFolder}\ToDeploy\*.*; DestDir: {tmp}; Flags: overwritereadonly; Permissions: users-full
; DEPLOY DACPAC
Source: {#CommonFolder}\SetupVideo\l3codec\*; DestDir: {app}\Bin; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\vcredis1.cab; DestDir: {tmp}; DestName: vcredis1.cab; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\vcredist.msi; DestDir: {tmp}; DestName: vcredist.msi; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\vcredist_x86_90\*.*; DestDir: {tmp}\vcredist_x86_90; Permissions: users-full; Flags: deleteafterinstall overwritereadonly ignoreversion uninsremovereadonly replacesameversion
Source: {#CommonFolder}\Dll\Skype4COM.dll; DestDir: {cf}\Skype; Flags: overwritereadonly; Permissions: users-full
;Source: {#CommonFolder}\SetupLanguages\AppSettings\{#AppName}*.*; DestDir: {tmp}\AppSettings; Permissions: users-full; Flags: deleteafterinstall overwritereadonly ignoreversion uninsremovereadonly replacesameversion
;dll for DirectDrawVerification
Source: {#CommonFolder}\SetupVideo\DirectDraw\*.*; DestDir: {tmp}; Flags: dontcopy; Permissions: users-full

[Dirs]
Name: {app}\Bin; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {app}\App; Flags: uninsalwaysuninstall; Permissions: users-full
;Name: {app}\Update; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {app}\Debug; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {app}\Docs; Flags: uninsalwaysuninstall; Permissions: users-full
;Name: {app}\Plugin; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {pf}\{#ShortCompany}\Licenses; Attribs: hidden system

; user documents
Name: {userdocs}\{#SetupName}\SnapShots; Permissions: users-full; Flags: uninsneveruninstall
Name: {userdocs}\{#SetupName}\VideoFiles; Permissions: users-full; Flags: uninsneveruninstall

; temp direcotry for logs
Name: {localappdata}\{#ShortCompany}\{#SetupName}\Download; Permissions: users-full
Name: {localappdata}\{#ShortCompany}\{#SetupName}\Export; Permissions: users-full
Name: {localappdata}\{#ShortCompany}\{#SetupName}\Logs; Permissions: users-full; Flags: uninsalwaysuninstall
Name: {localappdata}\{#ShortCompany}\{#SetupName}\Logs\CSV; Permissions: users-full; Flags: uninsalwaysuninstall
Name: {localappdata}\{#ShortCompany}\{#SetupName}\Settings; Permissions: users-full
Name: {app}\Bin\logs; Flags: uninsalwaysuninstall; Tasks: ; Languages: ; Permissions: users-full

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Check: MyFunction()
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Check: ShowQuickLunch()
;Name: addruletofirewall; Description: {cm:AddExceptionRule}; GroupDescription: Firewall:; Check: MyFunction()

[Icons]
Name: {commondesktop}\{#AppNameOnDesktop} v.{#Versiu}{#Rev}; Filename: {app}\Bin\XmlRuntime.exe; Tasks: desktopicon; Parameters: """{app}\App\{#AppName}.v.{#Versiu}.apppc"" "; WorkingDir: {app}\Bin; IconFilename: {app}\App\Icons\{#AppName}.ico; Languages: 
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppNameOnDesktop}.v.{#Versiu}{#Rev}; Filename: {app}\Bin\XmlRuntime.exe; Tasks: quicklaunchicon; Parameters: """{app}\App\{#AppName}.v.{#Versiu}.apppc"" "; WorkingDir: {app}\Bin; IconFilename: {app}\App\Icons\{#AppName}.ico; Check: ShowQuickLunch()
; folders
Name: {group}\{#AppNameOnDesktop}.v.{#Versiu} Documents; Filename: {userdocs}\{#SetupName}; IconFilename: {app}\App\Icons\foldergreen.ico
;Name: {group}\{#AppName}.v.{#Versiu} Logs; Filename: {userappdata}\{#ShortCompany}\{#AppName}.v.{#Versiu}\Debug; IconFilename: {app}\App\Icons\Folder_stuffed.ico
; shortcuts
Name: {group}\{#AppNameOnDesktop}.v.{#Versiu}{#Rev}; Filename: {app}\Bin\XmlRuntime.exe; WorkingDir: {app}\Bin; IconFilename: {app}\App\Icons\{#AppName}.ico; IconIndex: 0; Parameters: """{app}\App\{#AppName}.v.{#Versiu}.apppc"" "
Name: {group}\{#ShortCompany} Website; Filename: {app}\WebSite {#ShortCompany}.url; IconFilename: {app}\App\Icons\WWW.ico
Name: {group}\{cm:UninstallProgram,{#AppNameOnDesktop}.v.{#Versiu}{#Rev}}; Filename: {uninstallexe}

[Run]
Filename: msiexec.exe; Parameters: /i {tmp}\vcredist.msi /quiet; WorkingDir: {tmp}; Description: Register C++ Library
Filename: regedit.exe; Parameters: /S ffdshow.reg; WorkingDir: {app}\Bin; Flags: shellexec; Description: Install codec config file
Filename: regsvr32.exe; Parameters: /S LMVYUVxf.dll; WorkingDir: {app}\Bin; Description: Register video library
Filename: regsvr32.exe; Parameters: /S OvTool.dll; WorkingDir: {app}\Bin; Description: Register text overlay library
Filename: regsvr32.exe; Parameters: /S GSSF.ax; WorkingDir: {app}\Bin; Description: Register video library
Filename: regsvr32.exe; Parameters: /S AxisMediaViewer.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S AxH264Dec.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S AxMP4Dec.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S AxMJPGDec.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S Audio.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S l3codecx.ax; WorkingDir: {app}\Bin; Description: Register mpeg codecs
Filename: regsvr32.exe; Parameters: /S Skype4COM.dll; WorkingDir: {cf}\Skype; Description: Register skype4COMLib
Filename: regsvr32.exe; Parameters: /S TextOverlayInPlaceTransFilter.dll; WorkingDir: {app}\Bin; Description: Register video library
;Filename: notepad.exe; Description: {cm:OpenReleaseNotes}; Parameters: {app}\Docs\ReleaseManifest.txt; Flags: nowait postinstall skipifsilent; WorkingDir: {app}\Docs; Languages:
;Filename: {app}\App\Help\{#AppName}_30.chm; Description: {cm:OpenHelpFile}; Flags: nowait postinstall skipifsilent shellexec unchecked
Filename: {app}\Bin\XmlRuntime.exe; Description: {cm:LaunchProgram,{#AppName}.v.{#Versiu}{#Rev}}; Flags: nowait postinstall skipifsilent; Parameters: """{app}\App\{#AppName}.v.{#Versiu}.apppc"" "; WorkingDir: {app}\Bin; Languages: 
;Filename: {sys}\netsh.exe; Tasks: addruletofirewall; Parameters: "firewall add allowedprogram program=""{app}\Bin\XmlRuntime.exe"" name=""{#AppName}.v.{#Versiu}{#Rev}"" ENABLE ALL"; StatusMsg: {#AppName}.v.{#Versiu}{#Rev} add exception rule to firewall; Flags: runhidden; MinVersion: 0,5.01.2600sp2

[UninstallDelete]
Name: {app}\{#AppName}; Type: filesandordirs; Tasks: ; Languages: 
Name: {app}\Bin; Type: filesandordirs; Tasks: ; Languages: 
Name: {app}\Debug; Type: filesandordirs; Tasks: ; Languages: 

[InstallDelete]
Name: {pf}\{#ShortCompany}\{#AppName}.v.{#Versiu}; Type: filesandordirs; Tasks: ; Languages: 
Name: {pf}\{#ShortCompany}\{#AppName}.v.{#Versiu}; Type: dirifempty; Tasks: ; Languages: 

[UninstallRun]
Filename: {sys}\netsh.exe; Parameters: "firewall delete allowedprogram program=""{app}\Bin\XmlRuntime.exe"" profile=ALL"; StatusMsg: {#AppName}.v.{#Versiu}{#Rev} removing exception rule to firewall; Flags: runhidden

[Registry]
Root: HKLM; Subkey: Software\{#RegCompany}
Root: HKLM; Subkey: Software\{#RegCompany}\{#AppName}.{#Versiu}
Root: HKLM; Subkey: Software\{#RegCompany}\{#AppName}.{#Versiu}; ValueType: string; ValueName: AppID; ValueData: {{#AppID}
Root: HKLM; Subkey: Software\{#RegCompany}\{#AppName}.{#Versiu}; ValueType: string; ValueName: Version; ValueData: {#Versiu}{#Rev}

[Code]
// DirectDrawVerification
  function fnDirectDrawInfo(): Integer;
  external 'fnDirectDrawInfo@files:DirectDrawInfo.dll stdcall setuponly delayload';


const
	Netfx35RegKeyName = 'Software\Microsoft\NET Framework Setup\NDP\v3.5';
	Netfx40RegKeyName = 'Software\Microsoft\NET Framework Setup\NDP\v4\Full';
 
  DB_PAGE_CAPTION='Select Application Database Folder';
  DB_PAGE_DESCRIPTION='Where should application database files be installed or where     your database files already are?';
  DB_PAGE_SUBCAPTION='In case of new installation please create your unique username and password to application.';
  adCmdText = $00000001;

	NetfxStandardRegValueName = 'Install';
	NetfxStandardSPxRegValueName = 'SP';
	NetfxStandardVersionRegValueName = 'Version';

	// Version information for final release of .NET Framework 3.0
	Netfx30VersionMajor = 3;
	Netfx30VersionMinor = 0;
	Netfx30VersionBuild = 4506;
	Netfx30VersionRevision = 26;

	// Version information for final release of .NET Framework 3.5
	Netfx35VersionMajor = 3;
	Netfx35VersionMinor = 5;
	Netfx35VersionBuild = 21022;
	Netfx35VersionRevision = 8;

	// Version information for final release of .NET Framework 4.0
	Netfx40VersionMajor = 4;
	Netfx40VersionMinor = 0;
	Netfx40VersionBuild = 30319;

	// network path for .Net 3.5
	Netfx35InstallPath = 'http://download.microsoft.com/download/6/0/f/60fc5854-3cb8-4892-b6db-bd4f42510f28/dotnetfx35.exe';
	Netfx40InstallPath = 'http://www.microsoft.com/downloads/info.aspx?na=41&srcfamilyid=0a391abd-25c1-4fc0-919f-b21f31ab88b7&srcdisplaylang=en&u=http%3a%2f%2fdownload.microsoft.com%2fdownload%2f9%2f5%2fA%2f95A9616B-7A37-4AF6-BC36-D6EA96C8DAAE%2fdotNetFx40_Full_x86_x64.exe';

	// ffdshow
	ffdshowRegKeyName = 'SOFTWARE\GNU\ffdshow';
 	ffdShowStandardVersionRegValueName = 'revision';
	ffdshowRevision = 2547;
	ffdshowDirectory = '{pf32}\{#ShortCompany}\ffdshow';

	// vcredist
	vcredistRegKeyName = 'SOFTWARE\Classes\Installer\Products\6F9E66FF7E38E3A3FA41D89E8A906A4A';
 	vcredistStandardVersionRegValueName = 'Version';
	vcredistRevision = 151015966;

	// EZVideoPlayer
	EZVideoPlayerRegKeyName = 'Software\{#RegCompany}\{#EZVideoPlayerReg}';
 	EZVideoPlayerVersionRegValueName = 'Version';

	// EZUpdateCenter
	EZUpdateCenterRegKeyName = 'Software\{#RegCompany}\{#EZUpdateCenterReg}';
 	EZUpdateCenterVersionRegValueName = 'Version';

  //QBSDK
  EZQBSDKRegKeyName = 'Software\{#RegCompany}\{#QBSDKReg}';
 	EZQBSDKVersionRegValueName = 'Version';

	// HEM drivers path
	//UnInstRegPathHEM = 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{8629A6E3-E2B6-4EDC-8BBB-826EF9369E67}';

	// path in registry to unintall data
	UnInstRegPath = 'Software\Microsoft\Windows\CurrentVersion\Uninstall\{#AppID}_is1';


	// path in registry to unintall data
	UnInstRegValueName = 'UninstallString';

	// path to install location
	InstLocRegValueName = 'InstallLocation';

	// Address of survey webpage
	SurveyWebPage = 'http://survey.ezconnectsystem.com';

  // The constants for CreateFile ().
  GENERIC_READ        = $80000000;
  GENERIC_WRITE       = $40000000;
  GENERIC_EXECUTE     = $20000000;
  GENERIC_ALL         = $10000000;
  FILE_SHARE_READ     = 1;
  FILE_SHARE_WRITE    = 2;
  FILE_SHARE_DELETE   = 4;
  CREATE_NEW          = 1;
  CREATE_ALWAYS       = 2;
  OPEN_EXISTING       = 3;
  OPEN_ALWAYS         = 4;
  TRUNCATE_EXISTING   = 5;

  // General Win32.
  INVALID_HANDLE_VALUE = -1;

var
  autostartParameter: Boolean;
  helpParameter: Boolean;
  CustomPage: TWizardPage;
  lblPasswordConfirm: TLabel;
  lblUserName: TLabel;
  lblPassword: TLabel;
  lblSecureQuest: TLabel; 
  lblSecureAns: TLabel; 
  userAdded: Boolean; 
  
  txtUserName: TEdit;
  txtUserPassword: TPasswordEdit;
  txtUserPasswordConfirm: TPasswordEdit;
  txtSecureQuest: TEdit;
  txtSecureAns: TEdit;

  
  supressMessageBoxesParameter: Boolean;
  verySilentParameter: Boolean;
  logParameter: Boolean;
  rebootRequired: Boolean;
  language : String;
  UnInstallMessage : String;
  ffdshowParameter: Boolean;
  // personal information page
  FirstName, LastName, StoreNumber, Email, PhoneNumber, UserType: String;
  UserInformationPage: TWizardPage;
  Page: TInputQueryWizardPage;
  EditFirstName, EditLastName, EditStoreNumber, EditEmail, EditPhoneNumber: TEdit;
  RbFranchisee, RbAccountant: TRadioButton;

//inno defined event -
//returns 'true' if dotnet was installed for first time
function NeedRestart: Boolean;
begin
	Result := rebootRequired
end;


// The required Win32 functions.
function CreateFile (
	 lpFileName: String; // Could be PChar, but String is good enough for us here.
	 dwDesiredAccess: Cardinal;
	 dwShareMode: Cardinal;
	 lpSecurityAttributes: Cardinal;
	 dwCreationDisposition: Cardinal;
	 dwFlagsAndAttributes: Cardinal;
	 hTemplateFile:Integer
): Integer;
external 'CreateFileA@kernel32.dll stdcall';

function WriteFile (
	 hFile : THandle;
	 lpBuffer : String;
	 nNumberOfBytesToWrite : LongInt;
	 var lpNumberOfBytesWritten: LongInt;
	 lpOverlapped : LongInt
) : Boolean;
external 'WriteFile@kernel32.dll stdcall';

function CloseHandle (hHandle: Integer): Integer;
external 'CloseHandle@kernel32.dll stdcall';

function CompareVersionNumber( versionNumber: string; verMajor, verMinor, verBuild, verRevision: Integer ): Boolean;
var
	Major, Minor, Build, Revision: Integer;
	num, i, len, p: Integer;
	ToConvert, Rest: string;
begin
	Major := 0;
	Minor := 0;
	Build := 0;
	Revision := 0;
	i := 0;
	Rest := versionNumber;
	while i < 4 do
	begin
		p := Pos( '.', Rest );
		len := Length( Rest );

		if p = 0 then
		begin
			// try converting the last number available
			ToConvert := Copy( Rest, 1, len );
			Rest := '';
		end
		else
		begin
			ToConvert := Copy( Rest, 1, p - 1 );
			Rest := Copy( Rest, p + 1, len - p );
		end

		num := StrToIntDef( ToConvert, 0 );

		case i of
			0 : Major := num;
			1 : Minor := num;
			2 : Build := num;
			3 : Revision := num;
		end;

		if p = 0 then
		begin
			break;
		end;

		i := i + 1;
	end;

	Result := False;
	// compare numbers
	if Major > verMajor then
	begin
		Result := True;
	end
	else if Major = verMajor then
	begin
		if Minor > verMinor then
		begin
			Result := True;
		end
		else if Minor = verMinor then
		begin
			if Build > verBuild then
			begin
				Result := True;
			end
			else if Build = verBuild then
			begin
				if Revision >= verRevision then
				begin
					Result := True;
				end;
			end;
		end;
	end;
end;

// compare version by string
function GetNumber(var temp: String): Integer;
var
	part: String;
	pos1: Integer;
begin
	if Length(temp) = 0 then
	begin
		Result := -1;
		Exit;
	end;
    pos1 := Pos('.', temp);
    if (pos1 = 0) then
    begin
      Result := StrToInt(temp);
    temp := '';
    end
    else
    begin
    part := Copy(temp, 1, pos1 - 1);
      temp := Copy(temp, pos1 + 1, Length(temp));
      Result := StrToInt(part);
    end;
end;

function CompareInner(var temp1, temp2: String): Integer;
var
	num1, num2: Integer;
begin
    num1 := GetNumber(temp1);
	num2 := GetNumber(temp2);
	if (num1 = -1) or (num2 = -1) then
	begin
		Result := 0;
		Exit;
	end;

    if (num1 > num2) then
    begin
		Result := 1;
    end
    else if (num1 < num2) then
    begin
		Result := -1;
    end
    else
    begin
		Result := CompareInner(temp1, temp2);
	end;
end;

function CompareVersion(str1, str2: String): Integer;
var
  temp1, temp2: String;
begin
    temp1 := str1;
    temp2 := str2;
    Result := CompareInner(temp1, temp2);
end;

function IsNetfx35Installed( var version: string ): Boolean;
var
	ValDword: Cardinal;
	ValString: string;
begin
	Result := False;

	// Check that the Install registry value exists and equals 1
	if RegQueryDWordValue( HKEY_LOCAL_MACHINE, Netfx35RegKeyName, NetfxStandardRegValueName, ValDword ) then
	begin
		if ValDword = 1 then
		begin
			// A system with a pre-release version of the .NET Framework 3.5 can
			// have the Install value.  As an added verification, check the
			// version number listed in the registry
			// Get version number
			if RegQueryStringValue( HKEY_LOCAL_MACHINE, Netfx35RegKeyName, NetfxStandardVersionRegValueName, ValString ) then
			begin
				version := ValString;
				if CompareVersionNumber( ValString
					, Netfx35VersionMajor
					, Netfx35VersionMinor
					, Netfx35VersionBuild
					, Netfx35VersionRevision ) then
				begin
					Result := True;
				end
			end;
		end
	end;
end;

function IsNetfx40Installed(): Boolean;
var
	ValDword: Cardinal;
	ValString: string;
begin
	Result := False;

	// Check that the Install registry value exists and equals 1
	if RegQueryDWordValue( HKEY_LOCAL_MACHINE, Netfx40RegKeyName, NetfxStandardRegValueName, ValDword ) then
	begin
		if ValDword = 1 then
		begin
			Result := True;
		end
	end;
end;

function IsFfdshowInstalled(): Boolean;
var
	ValDword: Cardinal;
	ValRevision: DWord;
begin
	Result := False;
	ValRevision := 0;

    // Check that the ffdshow was installed or not
    if RegKeyExists( HKLM32, ffdshowRegKeyName ) then
    begin
      // Get revision number
      if RegQueryDWordValue( HKLM32, ffdshowRegKeyName, ffdShowStandardVersionRegValueName, ValRevision ) then
      begin
        if ValRevision = ffdshowRevision then
        begin
            if FileOrDirExists(ExpandConstant(ffdshowDirectory)) then
              begin
                Result := True;
              end;
         end;
      end;
    end;
end;

function IsVcredistInstalled(): Boolean;
var
	ValDword: Cardinal;
	ValVersion: DWord;
begin
	Result := False;
	ValVersion := 0;

	// Check that the vcredist was installed or not
	if RegKeyExists( HKEY_LOCAL_MACHINE, vcredistRegKeyName ) then
	begin
		// Get version number
		if RegQueryDWordValue( HKEY_LOCAL_MACHINE, vcredistRegKeyName, vcredistStandardVersionRegValueName, ValVersion ) then
		begin
			if ValVersion >= vcredistRevision then
			begin
				Result := True;
			end
		end;
	end;
end;


function IsEZVideoPlayerInstalled(): Boolean;
var
	ValString: string;
  RootKey: Integer;
  CompreResult: Integer;
begin
	Result := False;

	if IsWin64 then
	begin
		RootKey:= HKLM64;
	end
	else
	begin
		RootKey:= HKEY_LOCAL_MACHINE;
	end;

	// Check that the EZVideoPlayer was installed or not
	if RegKeyExists( RootKey, EZVideoPlayerRegKeyName ) then
	begin
		if RegQueryStringValue( RootKey, EZVideoPlayerRegKeyName, EZVideoPlayerVersionRegValueName, ValString ) then
		begin
			if ValString <> '' then
			begin
				CompreResult := CompareVersion(ExpandConstant('{#EZVideoPlayerVersion}'), ValString);
				if CompreResult < 1 then
				begin
					Result := True;
				end
			end;
		end;
	end;
end;

function IsEZUpdateCenterInstalled(): Boolean;
var
	ValString: string;
  AppNameString: string;
  CompreResult: Integer;
begin
	Result := False;

	// Check that the EZUpdateCenter was installed or not
	if RegKeyExists( HKEY_LOCAL_MACHINE, EZUpdateCenterRegKeyName ) then
	begin
		if RegQueryStringValue( HKEY_LOCAL_MACHINE, EZUpdateCenterRegKeyName, EZUpdateCenterVersionRegValueName, ValString ) then
		begin
			if ValString <> '' then
			begin
				CompreResult := CompareVersion(ExpandConstant('{#EZUpdateCenterVersion}'), ValString);
				if CompreResult < 1 then
				begin
          Result := True;
				end
			end;
		end;
	end;
end;

function IsQBSDKInstalled(): Boolean;
var
	ValString: string;
  AppNameString: string;
  CompreResult: Integer;
begin
	Result := False;

	// Check that the EZUpdateCenter was installed or not
	if RegKeyExists( HKEY_LOCAL_MACHINE, EZQBSDKRegKeyName ) then
	begin
		if RegQueryStringValue( HKEY_LOCAL_MACHINE, EZQBSDKRegKeyName, EZQBSDKVersionRegValueName, ValString ) then
		begin
			if ValString <> '' then
			begin
				CompreResult := CompareVersion(ExpandConstant('{#QBSDKVersion}'), ValString);
				if CompreResult < 1 then
				begin
          Result := True;
				end
			end;
		end;
	end;
end;


function IsWindowsXP(): Boolean;
var
	ValString: string;
  AppNameString: string;
  CompreResult: Integer;
begin
	Result := False;

	// Check that the EZUpdateCenter was installed or not
	if RegKeyExists( HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Windows NT\CurrentVersion' ) then
	begin
		if RegQueryStringValue( HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Windows NT\CurrentVersion', 'ProductName', ValString ) then
		begin
			if POS( 'Windows XP', ValString )>0 then
			begin
				Result := True;
				
			end;
		end;
	end;
end;


function IsMSSQLInstalled(): Boolean;
var
  ValString: string;
  ADOConnection: Variant;  
begin
	   try
	  
	    Result := False;
	  
      ADOConnection := CreateOleObject('ADODB.Connection');
      ADOConnection.ConnectionString := 
       'Provider=SQLOLEDB;' +                        // provider
       'Data Source=localhost\QBUniverse;' +  // server name
       'Initial Catalog=QBExporterDB;' +                // default database
       'User Id=sa;' +                               // user name
       'Password=asSA1#9$;';                          // password
     
      ADOConnection.Open;
      ADOConnection.Close;

      LOG('QBUniverse instance was in database');

      Result := True;
     except 
   
      LOG('QBUniverse instance was not in database');

      Result := False;
     
     end;
end;



function IsUserInDatabase(): Boolean;
var
  ValString: string;
  ADOConnection: Variant;  
  ADOCommand: Variant; 
  ADORecordset: Variant;
  Name: string; 
 begin
	   try
	  
	    Result := False;
	  
      ADOConnection := CreateOleObject('ADODB.Connection');
      ADOConnection.ConnectionString := 
       'Provider=SQLOLEDB;' +                        // provider
       'Data Source=localhost\QBUniverse;' +  // server name
       'Initial Catalog=QBExporterDB;' +                // default database
       'User Id=sa;' +                               // user name
       'Password=asSA1#9$;';                          // password
     
      ADOConnection.Open;

      ADOCommand := CreateOleObject('ADODB.Command');
   
      ADOCommand.ActiveConnection := ADOConnection;

      ADOCommand.CommandText := 'SELECT Username FROM QBExporterDB.dbo.Logins';

      ADOCommand.CommandType := adCmdText;

      ADORecordset := ADOCommand.Execute;
      
      Name := ADORecordset.Fields(0);
      
      ADOConnection.Close;

      LOG('User was in database - not adding');

      Result := True;
     except 
      
      LOG('User was not in database - adding');

      Result := False;
     
     end;
end;

function IsUnInstallRequired( var unInstCmd: string ): Boolean;
var
	ValString: string;
begin
	Result := False;

	if (RegQueryStringValue( HKEY_LOCAL_MACHINE, UnInstRegPath, UnInstRegValueName, ValString )) then
	begin
		if ValString <> '' then
		begin
			unInstCmd := RemoveQuotes( ValString );
			Result := True;
		end;
	end
end;

function GetInstallLocation( var instLocCmd: string ): Boolean;
var
	ValString: string;
begin
	Result := False;
	if RegQueryStringValue( HKEY_LOCAL_MACHINE, UnInstRegPath, InstLocRegValueName, ValString ) then
	begin
		if ValString <> '' then
		begin
			instLocCmd := RemoveQuotes( ValString );
			Result := True;
		end;
	end
end;

function PerformUnInstall( unInstCmd: string ): Boolean;
var
	ErrorCode: Integer;
	ResultCode: Integer;
 	InstLocCmd: String;
 	UninstallResult: Boolean;
begin
	if GetInstallLocation( InstLocCmd ) then
	begin
		//Exec(ExpandConstant('{win}\Microsoft.NET\Framework\v2.0.50727\ngen.exe'), 'uninstall "' + InstLocCmd + 'Bin\XmlRuntime.exe"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
		Exec(ExpandConstant('{win}\Microsoft.NET\Framework\v4.0.30319\ngen.exe'), 'uninstall "' + InstLocCmd + 'Bin\XmlRuntime.exe"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
	end

	if verySilentParameter then
	begin
		UninstallResult := Exec( unInstCmd, '/VERYSILENT /NORESTART', '', SW_HIDE, ewWaitUntilTerminated, ErrorCode );
    // not delete Sleep as soon as Inno Setup not fixed bug: in silent mode installation not waiting untill the uninstallation will finish.
    Sleep(3000)
	end
	else
	begin
		UninstallResult := Exec( unInstCmd, '/SILENT /NORESTART', '', SW_HIDE, ewWaitUntilTerminated, ErrorCode );
	end

	if UninstallResult then
	begin
		Result := True;
	end
	else
	begin
		Result := False;
	end;
end;

function UnInstallOldVersion(): Integer;
var
	sUnInstPath: string;
	sUnInstallString: string;
	iResultCode: integer;
	ResultCode: integer;
 	instLocCmd: string;
 	UninstallResult: Boolean;
begin
	// Return Values:
	// 0 - no idea
	// 1 - can't find the registry key (probably no previous version installed)
	// 2 - uninstall string is empty
	// 3 - error executing the UnInstallString
	// 4 - successfully executed the UnInstallString

	// default return value
  Result := 0;

	sUnInstPath :='Software\Microsoft\Windows\CurrentVersion\Uninstall\{#AppID}_is1';
	sUnInstallString := '';

	// get the uninstall string of the old app
	if RegQueryStringValue( HKLM, sUnInstPath, 'UninstallString', sUnInstallString ) then
	begin
		if sUnInstallString <> '' then
		begin

			if GetInstallLocation( instLocCmd ) then
			begin
				Exec(ExpandConstant('{win}\Microsoft.NET\Framework\v2.0.50727\ngen.exe'), 'uninstall "' + instLocCmd + 'Bin\XmlRuntime.exe"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
			end

			sUnInstallString := RemoveQuotes(sUnInstallString);

			if verySilentParameter then
			begin
				UninstallResult := Exec( sUnInstallString, '/VERYSILENT /NORESTART','', SW_HIDE, ewWaitUntilTerminated, iResultCode )
        // not delete Sleep as soon as Inno Setup not fixed bug: in silent mode installation not waiting untill the uninstallation will finish.
        Sleep(3000)
			end
			else
			begin
				UninstallResult := Exec( sUnInstallString, '/SILENT /NORESTART','', SW_HIDE, ewWaitUntilTerminated, iResultCode )
			end

			if UninstallResult then
			begin
				Result := 4;
			end
			else
			begin
				Result := 3;
			end
		end
		else
		begin
			Result := 2;
		end
	end
	else
	begin
		Result := 1;
	end
end;

function UnInstallEZU35Version(): Integer;
var
	sUnInstPath: string;
	sUnInstallString: string;
	iResultCode: integer;
	ResultCode: integer;
 	instLocCmd: string;
 	UninstallResult: Boolean;
begin
	// Return Values:
	// 0 - no idea
	// 1 - can't find the registry key (probably no previous version installed)
	// 2 - uninstall string is empty
	// 3 - error executing the UnInstallString
	// 4 - successfully executed the UnInstallString

	// default return value
    Result := 0;


	sUnInstallString := '';

	// get the uninstall string of the old app
	if RegQueryStringValue( HKLM, sUnInstPath, 'UninstallString', sUnInstallString ) then
	begin
		if sUnInstallString <> '' then
		begin

			if GetInstallLocation( instLocCmd ) then
			begin
				Exec(ExpandConstant('{win}\Microsoft.NET\Framework\v2.0.50727\ngen.exe'), 'uninstall "' + instLocCmd + 'Bin\XmlRuntime.exe"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
			end

			sUnInstallString := RemoveQuotes(sUnInstallString);

			if verySilentParameter then
			begin
				UninstallResult := Exec( sUnInstallString, '/VERYSILENT /NORESTART','', SW_HIDE, ewWaitUntilTerminated, iResultCode )
			end
			else
			begin
				UninstallResult := Exec( sUnInstallString, '/SILENT /NORESTART','', SW_HIDE, ewWaitUntilTerminated, iResultCode )
			end

			if UninstallResult then
			begin
				Result := 4;
			end
			else
			begin
				Result := 3;
			end
		end
		else
		begin
			Result := 2;
		end
	end
	else
	begin
		Result := 1;
	end
end;

function MyFunction(): Boolean;
begin
	Result:=true;
end;

// check Windows version
function ShowQuickLunch(): Boolean;
var
  Version: TWindowsVersion;
begin
  GetWindowsVersionEx(Version);

  if Version.NTPlatform and
     (Version.Major = 6) and
     (Version.Minor = 1) then
  begin
	Result:=false;
  end
  else
  begin
    Result:=true;
  end;
end;

procedure WriteAgreementLog();
var
	hFile: THandle;
	bytesWritten: LongInt;
	fileName, agreement : String;
begin

	fileName := ExpandConstant('{pf}\{#ShortCompany}\Licenses\{#AppName}.v.{#Versiu}{#Rev}') + '.' + GetDateTimeString('(yyyy.mm.dd)(hh.nn.ss)', #0, #0 ) + '.log'
	hFile := CreateFile (fileName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY, 0);

	if ( hFile <> INVALID_HANDLE_VALUE ) then
	begin
		agreement := 'User accepted license agreement for ' + ExpandConstant('{#AppName}.v.{#Versiu}{#Rev}') + ' at ' + GetDateTimeString('yyyy.mm.dd hh:nn:ss', #0, #0 ) + '.'
		WriteFile(hFile, agreement, length(agreement), bytesWritten, 0);
		CloseHandle(hFile);
	end;
end;

procedure CommandLineInit();
var
	i: Integer;
	arg: String;
begin
	autostartParameter := true;
	helpParameter := false;
	supressMessageBoxesParameter := false;
	verySilentParameter := false;
	logParameter := false;
	ffdshowParameter := true

	i:=0;
	while (i <= ParamCount) do
	begin
		arg := lowercase(ParamStr(i));

		if arg = '/?' then helpParameter := true
		else if arg = '/help' then helpParameter := true
		else if arg = '/noautostart' then autostartParameter := false
		else if arg = '/verysilent' then verySilentParameter := true
		else if arg = '/suppressmsgboxes' then supressMessageBoxesParameter := true
		else if arg = '/log' then logParameter := true
		else if arg = '/noffdshow' then ffdshowParameter := false
		i := i + 1;
	end
end;

procedure SetLanguageMessages();
var
	i: Integer;
	arg: String;
begin
	language := ExpandConstant('{language}');

	if language = 'PL' then
	begin
		UnInstallMessage := 'Odinstalowanie aktualnie zainstalowanej wersji jest wymagane' + Chr( 13 ) + Chr( 10 )
			+ 'Czy chcesz odinstalowac program?';
	end
	else if language = 'CZ' then
	begin
		UnInstallMessage := 'Odinstalov�n� aktu�ln� nainstalovanou verzi je nutn�' + Chr( 13 ) + Chr( 10 )
			+ 'Cht�li byste prov�st odinstalaci?';
	end
	else
	begin
		UnInstallMessage :=  'Uninstalling of currently installed version is required' + Chr( 13 ) + Chr( 10 )
			+ 'Would you like to proceed with uninstall?';
	end;
end;

function CheckRunApplication() : Boolean;
begin
	Result := False;

	Result := CheckForMutexes( ExpandConstant('{#AppNameOnDesktop} v.{#Versiu}') );

 	if not Result then
	begin
		Result := CheckForMutexes( ExpandConstant('{#AppNameOnDesktop} v.1.0') );
	end

  if not Result then
	begin
		Result := CheckForMutexes( ExpandConstant('{#AppNameOnDesktop} v.3.6') );
	end

  if not Result then
	begin
		Result := CheckForMutexes( ExpandConstant('{#AppName} v.1.1') );
	end

  if not Result then
	begin
		Result := CheckForMutexes( ExpandConstant('EZQuickBooksExporter v.1.1') );
	end

  if not Result then
	begin
		Result := CheckForMutexes( ExpandConstant('QuickBookExporter v.1.1') );
	end


  if not Result then
	begin
		Result := CheckForMutexes( ExpandConstant('{#AppName} v.3.6') );
	end

end;

function InitializeSetup(): Boolean;
var
    ErrorCode: Integer;
    Net35, Inst35, UnInst, DelDatabase, Install: Boolean;
    Net40, Inst40: Boolean;
   	UnInstCmd, Message, Ver: string;
   	ResultCode: integer;
   	version: cardinal;
   	rebootR: Boolean;
   	AppBetaGUID: string;
    ezAppRun: Boolean;
    ADOConnection: variant;
    ADOCommand: variant;
    
begin
	Result := False;
	ezAppRun := false;

	CommandLineInit();
	SetLanguageMessages();

  if not verySilentParameter then
  begin
    try
      // 1: ok, -1: not support directDraw
      if (fnDirectDrawInfo() = -1) then
      begin
        MsgBox( 'Your computer doesn�t meet the minimum requirements for DirectDraw. You might not be able to see video from stores that based on Stretch Capture card. Please contact Support for more information.', mbError, MB_OK );
      end;
    except
      //handle missing dll here
    end;
  end;

	if IsUnInstallRequired( UnInstCmd ) then
	begin
		Message := UnInstallMessage;

		if not verySilentParameter then
		begin
			UnInst := MsgBox( Message, mbConfirmation, MB_YESNO ) = idYes;
		end
		else
		begin
			UnInst := true;
		end

		if UnInst then
		begin
			//check if application is running
			if CheckRunApplication() then
			begin
				if not verySilentParameter then
				begin
					MsgBox( ExpandConstant('{#AppNameOnDesktop}') + ' is running, please close it and run again setup.', mbError, MB_OK );
					Install := False;
					ezAppRun := true;
				end
				else
				begin
					// kill app
					Exec(ExpandConstant('taskkill.exe'), ExpandConstant('/FI "WINDOWTITLE eq {#AppName}*" /FI "WINDOWTITLE ne {#AppName} for GCB*" /IM XMLRuntime.exe'), '', SW_HIDE , ewWaitUntilTerminated, ResultCode);
					Sleep(2000);
				end;
			end

			if not ezAppRun then
			begin

        RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'UpdateDate', GetDateTimeString('yyyy.mm.dd hh:nn:ss', '-', ':') );
          // Do not show survey
        RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'ShowSurvey', 'false' );

        PerformUnInstall( UnInstCmd );
				Install := True;
      

			end
		end
		else
		begin
			Install := False;
		end;
	end
	else
	begin
		RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'InstallDate', GetDateTimeString('yyyy.mm.dd hh:nn:ss', '-', ':') );
		Install := True;
	end;

	if Install then
	begin
		//uninstall EZU version
		RegQueryStringValue(HKLM, ExpandConstant('Software\{#RegCompany}\{#AppName}.3.5'), 'AppID', AppBetaGUID);
		if AppBetaGUID = '{07279EBE-96CE-42FC-ADFF-D9F720111118A}' then
		begin
			UnInstallEZU35Version;
		end;

		//uninstall old version

		if RegValueExists(HKLM, ExpandConstant('Software\{#RegCompany}\{#AppName}'), 'Version') then
		begin
			RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}' ), 'ShowSurvey', 'false' );
		end;

		UnInstallOldVersion;

		//init windows version
		//initwinversion();

		RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v4\Full', 'Install', version);
		if version <> 1 then
		begin
			rebootR := True;
		end
		else
		begin
			rebootR := False;
		end;

		dotnetfx40();
		rebootRequired := rebootR;
		Result := True;
	end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
var 
   DelDatabase:bool; 
    ADOConnection:variant;
    ADOCommand:variant; 
    onlyTypicall:string; 

begin
  if CurUninstallStep = usUninstall then 
  begin

        DelDatabase := MsgBox( 'Would you like to preserve the current data and configurations within the exporter tool? Selecting No WILL DESTROY all current data', mbConfirmation, MB_YESNO ) = idYes;

        if not DelDatabase then
        begin

        DelDatabase := MsgBox( 'Are you sure you want to remove the database files? If you select Yes, the data within the exporter tool will be deleted and permanently lost. Click Yes to continue, No to abort the uninstall process. ', mbConfirmation, MB_YESNO ) = idYes;
            if DelDatabase then 
            begin 

                  ADOConnection := CreateOleObject('ADODB.Connection');
                  ADOConnection.ConnectionString := 
                    'Provider=SQLOLEDB;' +                         // provider
                    'Data Source=localhost\QBUniverse;' +          // server name
                    'Initial Catalog=QBExporterDB;' +              // default database
                    'User Id=sa;' +                                // user name
                    'Password=asSA1#9$;';                          // password
         
                  ADOConnection.Open;

                  ADOCommand := CreateOleObject('ADODB.Command');
       
                  ADOCommand.ActiveConnection := ADOConnection;

                  ADOCommand.CommandText :=  'DELETE FROM [QBExporterDB].[Archive].[Configuration] ' +
                                             'DELETE FROM [QBExporterDB].[Archive].[DetailsData] ' +
                                             'DELETE FROM [QBExporterDB].[Archive].[EmployeeMatch] ' +
                                             'DELETE FROM [QBExporterDB].[Archive].[StoreMatch] ' +
                                             'DELETE FROM [QBExporterDB].[Data].[DetailsData] ' +
                                             'DELETE FROM [QBExporterDB].[Data].[Employee] ' +
                                             'DELETE FROM [QBExporterDB].[Data].[Stores] ' +
                                             'DELETE FROM [QBExporterDB].[Data].[UsersPermissions] ' +
                                             'DELETE FROM [QBExporterDB].[dbo].[Sessions] ' +
                                             'DELETE FROM [QBExporterDB].[dbo].[Logins] ' +
                                             'DELETE FROM [QBExporterDB].[dbo].[TraceActivity] ' +
                                             'DELETE FROM [QBExporterDB].[Dict].[ConfigFields] ' +
                                             'DELETE FROM [QBExporterDB].[Export].[Configuration] ' +
                                             'DELETE FROM [QBExporterDB].[Export].[EmployeeMatch] ' +
                                             'DELETE FROM [QBExporterDB].[Export].[Log] ' +
                                             'DELETE FROM [QBExporterDB].[Export].[StoreMatch] ' +
                                             'DELETE FROM [QBExporterDB].[Service].[Logs]';

                  ADOCommand.Execute;
              
                  ADOConnection.Close;
			end
			else 
			begin
				Abort();
			end;
        end;   
  end;
end;

procedure DeinitializeSetup();
var
	sPath: String;
    remove: Boolean;
begin

	WriteAgreementLog();

	RegDeleteValue(HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'ShowSurvey');

	sPath := 'C:\'
	remove := True;
	// removing files
	while (remove) do
	begin
		if FileExists(sPath + 'eula.1028.txt') then
		begin
			DeleteFile(sPath + 'eula.1028.txt')
			DeleteFile(sPath + 'eula.1031.txt')
			DeleteFile(sPath + 'eula.1033.txt')
			DeleteFile(sPath + 'eula.1036.txt')
			DeleteFile(sPath + 'eula.1040.txt')
			DeleteFile(sPath + 'eula.1041.txt')
			DeleteFile(sPath + 'eula.1042.txt')
			DeleteFile(sPath + 'eula.2052.txt')
			DeleteFile(sPath + 'eula.3082.txt')
			DeleteFile(sPath + 'globdata.ini')
			DeleteFile(sPath + 'install.exe')
			DeleteFile(sPath + 'install.ini')
			DeleteFile(sPath + 'install.res.1028.dll')
			DeleteFile(sPath + 'install.res.1031.dll')
			DeleteFile(sPath + 'install.res.1033.dll')
			DeleteFile(sPath + 'install.res.1036.dll')
			DeleteFile(sPath + 'install.res.1040.dll')
			DeleteFile(sPath + 'install.res.1041.dll')
			DeleteFile(sPath + 'install.res.1042.dll')
			DeleteFile(sPath + 'install.res.2052.dll')
			DeleteFile(sPath + 'install.res.3082.dll')
			DeleteFile(sPath + 'VC_RED.cab')
			DeleteFile(sPath + 'VC_RED.MSI')
			DeleteFile(sPath + 'vcredist.bmp')
		end

		if (sPath = 'C:\') then
			sPath := 'D:\'
		else if (sPath = 'D:\') then
			sPath := 'E:\'
		else
			remove := False
	end
end;

function InitializeUninstall(): Boolean;
var
	showSurvay : string;
	ResultCode: integer;
begin
	if CheckRunApplication() then
	begin
		if not verySilentParameter then
		begin
			MsgBox( ExpandConstant('{#AppNameOnDesktop}') + ' is running, please close it and run again uninstall setup.', mbError, MB_OK );
			RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'ShowSurvey', 'false' );
			Result := false;
		end
		else
		begin
			// kill app
			Exec(ExpandConstant('taskkill.exe'), ExpandConstant('/FI "WINDOWTITLE eq {#AppName}*" /FI "WINDOWTITLE ne {#AppName} for GCB*" /IM XMLRuntime.exe'), '', SW_HIDE , ewWaitUntilTerminated, ResultCode);
			Sleep(2000);
			Result := true;
		end;
	end
	else
	begin
		RegQueryStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'ShowSurvey', showSurvay );

		if showSurvay = '' then
		begin
			RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'ShowSurvey', 'true' );
		end

		Result := true;
	end
end;

procedure DeinitializeUninstall();
var
	ErrorCode : Integer;
	showSurvay : string;
begin

	RegQueryStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'ShowSurvey', showSurvay );

	if (showSurvay <> 'false') and ({#RunSurvey}) then
	begin
		RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'RemoveDate', GetDateTimeString('yyyy.mm.dd hh:nn:ss', '-', ':') );
		ShellExec('open', SurveyWebPage, '', '', SW_SHOWNORMAL, ewNoWait, ErrorCode);
	end

	if RegValueExists(HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'ShowSurvey') then
	begin
		RegDeleteValue(HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'ShowSurvey');
	end
end;

procedure CopyAppSettingsFile();
var
	srcPath: String;
	destPath: String;
begin
	// copy settings file if not exists in destination
	destPath := ExpandConstant('{app}\App\{#AppName}.v.{#Versiu}.settings')
	srcPath := ExpandConstant('{tmp}\AppSettings\{#AppName}.v.{#Versiu}{language}.settings');
	FileCopy(srcPath, destPath, True);
end;

procedure CreateUserInformationPage;
var
  LblFirstName, LblLastName, LblStoreNumber, LblEmail, LblPhoneNumber, LblUserType: TLabel;
begin
  UserInformationPage := CreateCustomPage(wpWelcome, 'User Information', 'Please fill out personal information');

  // First Name
  LblFirstName := TLabel.Create(UserInformationPage);
  LblFirstName.Caption := 'First Name:';
  LblFirstName.AutoSize := True;
  LblFirstName.Parent := UserInformationPage.Surface;

  EditFirstName := TEdit.Create(Page);
  EditFirstName.Top := LblFirstName.Top + LblFirstName.Height + ScaleY(2);
  EditFirstName.Width := UserInformationPage.SurfaceWidth - ScaleX(8);
  EditFirstName.Parent := UserInformationPage.Surface;

  // Last Name
  LblLastName := TLabel.Create(Page);
  LblLastName.Top := EditFirstName.Top + EditFirstName.Height + ScaleY(4);
  LblLastName.Caption := 'Last Name:';
  LblLastName.AutoSize := True;
  LblLastName.Parent := UserInformationPage.Surface;

  EditLastName := TEdit.Create(Page);
  EditLastName.Top := LblLastName.Top + LblLastName.Height + ScaleY(2);
  EditLastName.Width := UserInformationPage.SurfaceWidth - ScaleX(8);
  EditLastName.Parent := UserInformationPage.Surface;

  // Store Number
  LblStoreNumber := TLabel.Create(Page);
  LblStoreNumber.Top := EditLastName.Top + EditLastName.Height + ScaleY(4);
  LblStoreNumber.Caption := 'Store Number (one store # if you have multiple locations):';
  LblStoreNumber.AutoSize := True;
  LblStoreNumber.Parent := UserInformationPage.Surface;

  EditStoreNumber := TEdit.Create(Page);
  EditStoreNumber.Top := LblStoreNumber.Top + LblStoreNumber.Height + ScaleY(2);
  EditStoreNumber.Width := UserInformationPage.SurfaceWidth - ScaleX(8);
  EditStoreNumber.Parent := UserInformationPage.Surface;

  // type
  LblUserType := TLabel.Create(Page);
  LblUserType.Top := EditStoreNumber.Top + EditStoreNumber.Height + ScaleY(4);
  LblUserType.Caption := 'Are you the:';
  LblUserType.AutoSize := True;
  LblUserType.Parent := UserInformationPage.Surface;
  
  RbFranchisee := TRadioButton.Create(WizardForm);
  RbFranchisee.Top := LblUserType.Top + LblUserType.Height + ScaleY(2);
  RbFranchisee.Width := UserInformationPage.SurfaceWidth / 2 - ScaleX(8);
  RbFranchisee.Caption := 'Franchisee'
  RbFranchisee.Parent := UserInformationPage.Surface;
  RbFranchisee.Checked := True;
  
  RbAccountant := TRadioButton.Create(WizardForm);
  RbAccountant.Top := LblUserType.Top + LblUserType.Height + ScaleY(2);
  RbAccountant.Left := UserInformationPage.SurfaceWidth / 2;
  RbAccountant.Width := UserInformationPage.SurfaceWidth / 2 - ScaleX(8);
  RbAccountant.Caption := 'Accountant'
  RbAccountant.Parent := UserInformationPage.Surface;
  RbAccountant.Checked := False;
  
  // E-mail
  LblEmail := TLabel.Create(Page);
  LblEmail.Top := RbAccountant.Top + RbAccountant.Height + ScaleY(6);
  LblEmail.Caption := 'E-mail:';
  LblEmail.AutoSize := True;
  LblEmail.Parent := UserInformationPage.Surface;

  EditEmail := TEdit.Create(Page);
  EditEmail.Top := LblEmail.Top + LblEmail.Height + ScaleY(2);
  EditEmail.Width := UserInformationPage.SurfaceWidth - ScaleX(8);
  EditEmail.Parent := UserInformationPage.Surface;

  // Phone Number
  LblPhoneNumber := TLabel.Create(Page);
  LblPhoneNumber.Top := EditEmail.Top + EditEmail.Height + ScaleY(4);
  LblPhoneNumber.Caption := 'Phone Number:';
  LblPhoneNumber.AutoSize := True;
  LblPhoneNumber.Parent := UserInformationPage.Surface;

  EditPhoneNumber := TEdit.Create(Page);
  EditPhoneNumber.Top := LblPhoneNumber.Top + LblPhoneNumber.Height + ScaleY(2);
  EditPhoneNumber.Width := UserInformationPage.SurfaceWidth - ScaleX(8);
  EditPhoneNumber.Parent := UserInformationPage.Surface;
end;

procedure InitializeWizard;
var
  userInfoValue: String;
begin
 IF (not (IsMSSQLInstalled())) then 
  BEGIN
    CreateUserInformationPage;
  END ELSE BEGIN IF ( not IsUserInDatabase()) THEN
    BEGIN
      CreateUserInformationPage;
    END ELSE BEGIN  IF (not RegQueryStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'UserInfo', userInfoValue )) THEN
    BEGIN 
        CreateUserInformationPage;
    END
    END
  END
end;

function InstallPage (CurPageID: Integer): Boolean;
var
  userInfoValue: String;
begin
	
  if ( ( CurPageID = UserInformationPage.ID ) AND ( verySilentParameter = False ) ) then
  begin
    if ( ( EditFirstName.Text = '' ) OR ( EditLastName.Text = '' ) OR ( EditStoreNumber.Text = '' ) 
        OR ( EditEmail.Text = '' ) OR ( EditPhoneNumber.Text = '' )
       ) then
    begin
      MsgBox('You must enter all data.', mbError, MB_OK);
      Result := False;
    end
    else
    begin
      FirstName := EditFirstName.Text;
      LastName := EditLastName.Text;
      StoreNumber := EditStoreNumber.Text;
      if RbAccountant.Checked = True then
      begin
        userType := 'Accountant';
      end
      else
      begin
        userType := 'Franchisee';
      end;
      Email := EditEmail.Text;
      PhoneNumber := EditPhoneNumber.Text;

      userInfoValue := ' "'+StoreNumber + '" "' +  FirstName + '" "' + LastName + '" "' + Email + '" "' + PhoneNumber + '" "' + UserType + '"';

      RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'UserInfo', userInfoValue );

      Result := True;
    end;
	end else 
  BEGIN
	Result:=True;
  END;
end;


function NextButtonClick(CurPageID: Integer): Boolean;
var
  userInfoValue: String;
begin
  if CurPageID = wpReady then 
  begin

		if downloadMemo <> '' then 
		begin
			//change isxdl language only if it is not english because isxdl default language is already english
			if ActiveLanguage() <> 'en' then 
			begin
				ExtractTemporaryFile(CustomMessage('isxdl_langfile'));
				isxdl_SetOption('language', ExpandConstant('{tmp}{\}') + CustomMessage('isxdl_langfile'));
			end;
			
			if SuppressibleMsgBox(FmtMessage(CustomMessage('depdownload_msg'), [downloadMessage]), mbConfirmation, MB_YESNO, IDYES) = IDNO then
				Result := false
			else if isxdl_DownloadFiles(StrToInt(ExpandConstant('{wizardhwnd}'))) = 0 then
				Result := false;
				end else 
						begin
							Result:=true; 
						end;
		

  end
  else
  begin 
		IF (not (IsMSSQLInstalled())) then 
		  BEGIN
			Result:=InstallPage(CurPageID);
		  END ELSE BEGIN IF ( not IsUserInDatabase()) THEN
			BEGIN
			  Result:=InstallPage(CurPageID);
			END ELSE BEGIN  IF (not RegQueryStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'UserInfo', userInfoValue )) THEN
			BEGIN 
				Result:=InstallPage(CurPageID);
			END ELSE BEGIN 
				 Result:=true;
				END
			END
		  END
		  end
end;

procedure frmDomainReg_Activate(Page: TWizardPage);
begin
end;

function frmDomainReg_ShouldSkipPage(Page: TWizardPage): Boolean;
begin
Result := userAdded;
end;

function frmDomainReg_BackButtonClick(Page: TWizardPage): Boolean;
begin
Result := True;
end;

function ValidateEmail(strEmail : String) : boolean;
var
    strTemp  : String;
    nSpace   : Integer;
    nAt      : Integer;
    nDot     : Integer;
begin
    strEmail := Trim(strEmail);
    nSpace := Pos(' ', strEmail);
    nAt := Pos('@', strEmail);
    strTemp := Copy(strEmail, nAt + 1, Length(strEmail) - nAt + 1);
    nDot := Pos('.', strTemp) + nAt;
    Result := ((nSpace = 0) and (1 < nAt) and (nAt + 1 < nDot) and (nDot < Length(strEmail)));
end;

function frmDomainReg_NextButtonClick(Page: TWizardPage): Boolean;
var
     ADOConnection: variant; 
     ADOCommand: variant;

begin

    if ( txtUserName.Text <> '' ) THEN
      
      if ( txtUserPassword.Text = txtUserPasswordConfirm.Text ) THEN
		begin
		if ( ValidateEmail(txtSecureQuest.Text )) THEN
		begin


        try
          
          Result := False;
        
          ADOConnection := CreateOleObject('ADODB.Connection');
          ADOConnection.ConnectionString := 
           'Provider=SQLOLEDB;' +                         // provider
           'Data Source=localhost\QBUniverse;' +          // server name
           'Initial Catalog=QBExporterDB;' +              // default database
           'User Id=sa;' +                                // user name
           'Password=asSA1#9$;';                          // password
         
          ADOConnection.Open;

          ADOCommand := CreateOleObject('ADODB.Command');
       
          ADOCommand.ActiveConnection := ADOConnection;

          ADOCommand.CommandText := 'INSERT INTO [dbo].[Logins]( [Shortname], [Username],[Password], [UserEmail] ) VALUES ( ''' + txtUserName.Text + ''', ''' + txtUserName.Text + ''',''' + txtUserPassword.Text + ''',''' + txtSecureQuest.Text + ''')';

          ADOCommand.Execute;
              
          ADOConnection.Close;

          Result := True;
          userAdded := True; 
       
        except
            MsgBox(GetExceptionMessage, mbError, MB_OK);
            ADOConnection.Close;
            Result := false; 
        end;

      end else
	  begin 
		Result := False;
        userAdded := False; 
        MsgBox('Your e-mail is incorrect. Please enter valid e-mail.', mbError, MB_OK);
	  end
      end
      else 
     
      begin
        Result := False;
        userAdded := False; 
        MsgBox('Your password and confirm password are not the same please corect them.', mbError, MB_OK);
      end   
    
    else 
    
    begin 
        Result := False;
        userAdded := False; 
        MsgBox('Your user name could not be empty.', mbError, MB_OK);
    end

end;

procedure frmDomainReg_CancelButtonClick(Page: TWizardPage; var Cancel, Confirm: Boolean);
begin
end;

function frmDomainReg_CreatePage(PreviousPageId: Integer): Integer;
var
Page: TWizardPage;
begin
Page := CreateCustomPage(
PreviousPageId,
'Create User',
'Enter Application User Registration'
);

    { lblUserName }
    lblUserName := TLabel.Create(Page);
    with lblUserName do
    begin
    Parent := Page.Surface;
    Left := ScaleX(24);
    Top := ScaleY(24);
    Width := ScaleX(35);
    Height := ScaleY(13);
    Caption := 'User Name';
    end;

    { Password }
    lblPassword := TLabel.Create(Page);
    with lblPassword do
    begin
    Parent := Page.Surface;
    Left := ScaleX(24);
    Top := ScaleY(56);
    Width := ScaleX(52);
    Height := ScaleY(13);
    Caption := 'Password';
    end;

    { lblPassword }
    lblPasswordConfirm := TLabel.Create(Page);
    with lblPasswordConfirm do
    begin
    Parent := Page.Surface;
    Left := ScaleX(24);
    Top := ScaleY(88);
    Width := ScaleX(46);
    Height := ScaleY(13);
    Caption := 'Password Confirm';
    end;

    lblSecureQuest := TLabel.Create(Page);
    with lblSecureQuest do
    begin
    Parent := Page.Surface;
    Left := ScaleX(24);
    Top := ScaleY(120);
    Width := ScaleX(46);
    Height := ScaleY(13);
    Caption := 'E-mail address';
    end;

    { txtUserName }
    txtUserName := TEdit.Create(Page);
    with txtUserName do
    begin
    Parent := Page.Surface;
    Left := ScaleX(120);
    Top := ScaleY(16);
    Width := ScaleX(185);
    Height := ScaleY(21);
    TabOrder := 0;
    end;

    { txtUserPassword }
    txtUserPassword := TPasswordEdit.Create(Page);
    with txtUserPassword do
    begin
    Parent := Page.Surface;
    Left := ScaleX(120);
    Top := ScaleY(48);
    Width := ScaleX(185);
    Height := ScaleY(21);
    TabOrder := 1;
    end;

    { txtUserPassword }
    txtUserPasswordConfirm := TPasswordEdit.Create(Page);
    with txtUserPasswordConfirm do
    begin
    Parent := Page.Surface;
    Left := ScaleX(120);
    Top := ScaleY(80);
    Width := ScaleX(185);
    Height := ScaleY(21);
    TabOrder := 2;
    end;

    txtSecureQuest:= TEdit.Create(Page);
    with txtSecureQuest do
    begin
    Parent := Page.Surface;
    Left := ScaleX(120);
    Top := ScaleY(115);
    Width := ScaleX(185);
    Height := ScaleY(21);
    TabOrder := 3;
    Text := Email;
    end;

    

    with Page do
    begin
    OnActivate := @frmDomainReg_Activate;
    OnShouldSkipPage := @frmDomainReg_ShouldSkipPage;
    OnBackButtonClick := @frmDomainReg_BackButtonClick;
    OnNextButtonClick := @frmDomainReg_NextButtonClick;
    OnCancelButtonClick := @frmDomainReg_CancelButtonClick;
    end;

    Result := Page.ID;
end;

procedure CurStepChanged(CurStep: TSetupStep);
var
	CustomPage: TOutputProgressWizardPage;
  TmpFileName, ExecStdout, mysqldump, params, command, logfilepathname, logfilename, newfilepathname, userInfoValue: string;
	ResultCode: Integer;
begin

	if (CurStep = ssPostInstall) then
	begin
		
    CopyAppSettingsFile();

		CustomPage := CreateOutputProgressPage( CustomMessage( 'PostInstallTasks' ), CustomMessage( 'PleaseWait' ) );
		CustomPage.Show();


		if {#InstallUpdateCenter} then
		begin
		
      if not IsEZUpdateCenterInstalled() then
		
      begin
				CustomPage.SetText( CustomMessage( 'InstallingEZUpdateCenter' ), '' );
				Exec(ExpandConstant('{tmp}\{#EZUpdateCenterSetup}'), '/VERYSILENT', ExpandConstant('{tmp}'), SW_HIDE, ewWaitUntilTerminated, ResultCode);
			end;
		
    end;                             

    if not IsQBSDKInstalled() then
      
      begin
        LOG('Install QB SDK');
        CustomPage.SetText( CustomMessage( 'InstallingQBSDK' ), '' );
        Exec(ExpandConstant('{tmp}\{#QBSDK}'), '/s /v"/qn', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);
        LOG('End of install QB SDK'); 
      end;
	  
	if IsWindowsXP() then
		
		begin 
		    LOG('System was Windows XP - install additional register script');

			  Exec(ExpandConstant('{tmp}\{#WinInst}'), '/quiet', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);
        
        LOG('Installed WinInstaller'); 

			  if not RegValueExists(HKLM, 'SOFTWARE\Microsoft\Cryptography\Defaults\Provider\Microsoft Enhanced RSA and AES Cryptographic Provider', 'Image Path') then
			  begin
				RegWriteStringValue( HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Cryptography\Defaults\Provider\Microsoft Enhanced RSA and AES Cryptographic Provider', 'Image Path', 'rsaenh.dll' );
			  end;

			  if not RegValueExists(HKLM, 'SOFTWARE\Microsoft\Cryptography\Defaults\Provider\Microsoft Enhanced RSA and AES Cryptographic Provider', 'Type') then
			  begin
				RegWriteDWordValue( HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Cryptography\Defaults\Provider\Microsoft Enhanced RSA and AES Cryptographic Provider', 'Type', 24 );
			  end;

			  if not RegValueExists(HKLM, 'SOFTWARE\Microsoft\Cryptography\Defaults\Provider\Microsoft Enhanced RSA and AES Cryptographic Provider', 'SigInFile') then
			  begin
				RegWriteDWordValue( HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Cryptography\Defaults\Provider\Microsoft Enhanced RSA and AES Cryptographic Provider', 'SigInFile', 0 );
			  end;

		end

    if not IsMSSQLInstalled() then
      begin
        LOG('Start MSSQL  install');
        
        CustomPage.SetText( CustomMessage( 'InstallingMSSQL' ), '' );
        Exec(ExpandConstant('{tmp}\{#MSSQL}'), '/QS /ACTION=Install /FEATURES= SQL  /ADDCURRENTUSERASSQLADMIN=false /SQLSYSADMINACCOUNTS="NT AUTHORITY\SYSTEM" /SQLCOLLATION="SQL_Latin1_General_CP1_CI_AS" /INSTANCENAME=QBUniverse /SECURITYMODE=SQL /SAPWD=asSA1#9$ /SQLSVCACCOUNT="NT AUTHORITY\SYSTEM"  /SQLSVCSTARTUPTYPE=Automatic /NPENABLED=1 /TCPENABLED=1 /IAcceptSQLServerLicenseTerms=1', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);
        
        LOG('End MSSQL  install');
      end;

    LOG('Prepare SSDT database install');

    Exec('cmd.exe', ExpandConstant('/C {tmp}\SQLPackage.exe /Action:Publish  /Sf:{tmp}\QB.dacpac  /TargetDatabaseName:QBExporterDB  /TargetServerName:localhost\QBUniverse /TargetUser:sa /TargetPassword:asSA1#9$  > {tmp}\dat.txt'), '', SW_SHOW, ewWaitUntilTerminated, ResultCode);

    if LoadStringFromFile(ExpandConstant('{tmp}\dat.txt'), ExecStdout) then begin
  
         LOG(ExecStdout);

    end

     LOG('End of SSDT database install');

	
    CustomPage.SetText( CustomMessage( 'CompilingApplication' ), '' );
    
    LOG('Start compiling application');

    Exec(ExpandConstant('{win}\Microsoft.NET\Framework\v4.0.30319\ngen.exe'), ExpandConstant('install "{app}\Bin\XmlRuntime.exe"'), '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
      
    LOG('End compiling application');
             
    CustomPage.Hide();


    if (not IsUserInDatabase()) AND (IsMSSQLInstalled()) then
     begin
        userAdded := False; 
        frmDomainReg_CreatePage(wpInfoAfter);

          logfilepathname := expandconstant('{log}');
          logfilename := ExtractFileName(logfilepathname);
          // Set the new target path as the directory where the installer is being run from
          newfilepathname := expandconstant('{tmp}\') +'log.txt';

          filecopy(logfilepathname, newfilepathname, false);

          RegQueryStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'UserInfo', userInfoValue );

          Exec(ExpandConstant('{tmp}\MailSender.exe'),   userInfoValue +' "First Installation" ' + ExpandConstant('"{#Versiu}{#Rev}"') + ExpandConstant('  "{tmp}\log.txt"'),'', SW_SHOW, ewWaitUntilTerminated, ResultCode);

     end
     else 
     begin
          
          logfilepathname := expandconstant('{log}');
          logfilename := ExtractFileName(logfilepathname);
          // Set the new target path as the directory where the installer is being run from
          newfilepathname := expandconstant('{tmp}\') +'log.txt';

          filecopy(logfilepathname, newfilepathname, false);

          RegQueryStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Versiu}' ), 'UserInfo', userInfoValue );

          Exec(ExpandConstant('{tmp}\MailSender.exe'),   userInfoValue +' "Update Installation" ' + ExpandConstant('"{#Versiu}{#Rev}"') + ExpandConstant('  "{tmp}\log.txt"'),'', SW_SHOW, ewWaitUntilTerminated, ResultCode);

     end;
        
    end;
end;
