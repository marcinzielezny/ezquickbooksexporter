#include "scripts\products.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#include "scripts\products\dotnetfx40.iss"

#define Company = 'EZUniverse Inc.'
#define ShortCompany = 'EZUniverse'
// not change RegCompany - always have to be EZuniverse Inc.
#define RegCompany = 'EZUniverse Inc.'
#define AppID='{?}'
#define AppName = '?'
#define Ver = '2.8'
#define Rev = '.0.37'

#define InstallVideo = 'true'
#define InstallUpdateCenter = 'true'
#define RunSurvey = 'false'
// setups
#define EZVideoPlayerReg = 'EZVideoPlayer.1.0'
#define EZVideoPlayerSetup = 'EZVideoPlayer.Setup.v.1.0.0.28.exe'
#define EZVideoPlayerVersion = '1.0.0.28'
#define EZUpdateCenterReg = 'EZUpdateCenter' 
#define EZUpdateCenterSetup = 'EZUpdateCenter.Setup.v.1.0.0.14.exe'
#define EZUpdateCenterVersion = '1.0.0.14'
// other
#define CommonFolder = '..\..\..\_CommonSetup'

[CustomMessages]
win2000sp3_title=Windows 2000 Service Pack 3
winxpsp2_title=Windows XP Service Pack 2
PostInstallTasks=Post install tasks.
PleaseWait=Please wait...
InstallingVisualCRuntimes=Installing Visual C++ 2005 SP1 Runtimes... This might take a few minutes.
InstallingCodecs=Installing codecs... This might take a few minutes.
InstallingEZVideoPlayer=Installing EZVideoPlayer... This might take a few minutes.
InstallingEZUpdateCenter=Installing EZUpdateCenter... This might take a few minutes.
CompilingApplication=Compiling application... This might take a few minutes.
OpenReleaseNotes=Open Release Notes
OpenHelpFile=Open Help File
AddExceptionRule=Add exception rule
;InstallingDVRDrivers=Installing HEM DVR Drivers... This might take a few minutes.
; cz
CZ.PostInstallTasks=Po instalaci �koly.
CZ.PleaseWait=Pros�m, vy�kejte...
CZ.InstallingVisualCRuntimes=Instalace Visual C + + 2005 SP1 Runtime ... To m��e trvat n�kolik minut.
CZ.InstallingCodecs=Instalace kodek� ... To m��e trvat n�kolik minut.
CZ.InstallingEZVideoPlayer=Instalace EZVideoPlayer... To m��e trvat n�kolik minut.
CZ.InstallingEZUpdateCenter=Instalace EZUpdateCenter... To m��e trvat n�kolik minut.
CZ.CompilingApplication=Kompilace aplikace ... To m��e trvat n�kolik minut.
CZ.OpenReleaseNotes=Open Pozn�mky k vyd�n�
CZ.OpenHelpFile=Otev��t soubor n�pov�dy
CZ.AddExceptionRule=Add exception rule
;CZ.InstallingDVRDrivers=Instalace ovlada�� HEM DVR ... To m��e trvat n�kolik minut.
; PL
PL.PostInstallTasks=Zadania po instalacji.
PL.PleaseWait=Prosz� czeka�...
PL.InstallingVisualCRuntimes=Instalacja Visual C++ 2005 SP1 Runtimes... To mo�e zaj�� kilka minut.
PL.InstallingCodecs=Instalacja kodek�w... To mo�e zaj�� kilka minut.
PL.InstallingEZVideoPlayer=Instalacja EZVideoPlayer... To mo�e zaj�� kilka minut.
PL.InstallingEZUpdateCenter=Instalacja EZUpdateCenter... To mo�e zaj�� kilka minut.
PL.CompilingApplication=Kompilowanie aplikacji... To mo�e zaj�� kilka minut.
PL.OpenReleaseNotes=Otwarcie Release manifestu
PL.OpenHelpFile=Otwarcie pliku pomocy
PL.AddExceptionRule=Dodaj wyj�tek
;PL.InstallingDVRDrivers=Instalacja HEM DVR Drivers... To mo�e zaj�� kilka minut.

[Languages]
Name: en; MessagesFile: compiler:Default.isl
Name: PL; MessagesFile: compiler:Languages\Polish.isl
Name: CZ; MessagesFile: compiler:Languages\Czech.isl

[Setup]
CloseApplications=false
OutputDir={#AppName}.v.{#Ver}
OutputBaseFilename={#AppName}.Setup.v.{#Ver}{#Rev}
AppCopyright=Copyright � 2004-2011 by {#Company}
AppName={#AppName}
AppVerName={#AppName}.v.{#Ver}{#Rev}
VersionInfoVersion={#Ver}{#Rev}
LanguageDetectionMethod=locale
ShowLanguageDialog=no
DefaultDirName={pf}\{#ShortCompany}\{#AppName}.v.{#Ver}
DefaultGroupName={#ShortCompany}\{#AppName}.v.{#Ver}
AlwaysUsePersonalGroup=false
DisableProgramGroupPage=false
DisableDirPage=false
AppID={{#AppID}
SetupIconFile={#CommonFolder}\SetupIcons\inst.ico
UninstallLogMode=overwrite
AppPublisher={#Company}
AppPublisherURL=www.{#ShortCompany}.com
AppVersion={#Ver}{#Rev}
AppSupportURL=www.{#ShortCompany}.com
AppUpdatesURL=www.{#ShortCompany}.com
UninstallDisplayIcon=
UninstallDisplayName={#AppName}.v.{#Ver}
WizardImageFile={#CommonFolder}\SetupImages\{#AppName}_Wizard.bmp
WizardSmallImageFile={#CommonFolder}\SetupImages\{#AppName}_WizardSmall.bmp
PrivilegesRequired=admin
LicenseFile=..\Application\Docs\{#AppName}(TM) License Agreement.rtf
SignedUninstaller =yes
VersionInfoCompany={#Company}

[Files]
Source: ..\Application\Bin\*.*; DestDir: {app}\Bin; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly onlyifdoesntexist; Permissions: users-full; Tasks: 
Source: ..\Application\App\*.*; DestDir: {app}\App; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly; Permissions: users-full
Source: ..\Application\Debug\*.*; DestDir: {app}\Debug; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly; Permissions: users-full
Source: ..\Application\Docs\*.*; DestDir: {app}\Docs; Flags: replacesameversion createallsubdirs recursesubdirs overwritereadonly; Permissions: users-full
Source: ..\Application\Bin\XmlRuntime.exe; DestDir: {app}\Bin; Flags: overwritereadonly replacesameversion; Permissions: users-full
Source: {#CommonFolder}\SetupIcons\*.*; DestDir: {app}\App\Icons; Flags: onlyifdoesntexist; Permissions: users-full
Source: {#CommonFolder}\WebSite\WebSite {#ShortCompany}.url; DestDir: {app}; Flags: overwritereadonly replacesameversion; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\ffdshow-rev2547_20081228.exe; DestDir: {tmp}; DestName: ffdshow-rev2547_20081228.exe; Flags: onlyifdoesntexist; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\ffdshow.reg; DestDir: {app}\Bin; DestName: ffdshow.reg; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\{#EZVideoPlayerSetup}; DestDir: {tmp}; DestName: {#EZVideoPlayerSetup}; Flags: onlyifdoesntexist; Permissions: users-full;
Source: {#CommonFolder}\{#EZUpdateCenterSetup}; DestDir: {tmp}; DestName: {#EZUpdateCenterSetup}; Flags: onlyifdoesntexist; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\l3codec\*; DestDir: {app}\Bin; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\AxisH264DecoderSetup.msi; DestDir: {tmp}; DestName: AxisH264DecoderSetup.msi; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\vcredis1.cab; DestDir: {tmp}; DestName: vcredis1.cab; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\vcredist.msi; DestDir: {tmp}; DestName: vcredist.msi; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupVideo\vcredist_x86_90\*.*; DestDir: {tmp}\vcredist_x86_90; Permissions: users-full; Flags: deleteafterinstall overwritereadonly ignoreversion uninsremovereadonly replacesameversion
Source: {#CommonFolder}\Dll\Skype4COM.dll; DestDir: {cf}\Skype; Flags: overwritereadonly; Permissions: users-full
Source: {#CommonFolder}\SetupLanguages\AppSettings\{#AppName}*.*; DestDir: {tmp}\AppSettings; Permissions: users-full; Flags: deleteafterinstall overwritereadonly ignoreversion uninsremovereadonly replacesameversion

[Dirs]
Name: {app}\Bin; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {app}\App; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {app}\App\Icons; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {app}\Update; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {app}\Debug; Flags: uninsalwaysuninstall; Permissions: users-full
Name: {pf}\{#ShortCompany}\Licenses; Attribs: hidden system
Name: {app}\Docs; Flags: uninsalwaysuninstall; Permissions: users-full

; user documents
Name: {userdocs}\{#AppName}\SnapShots; Permissions: users-full; Flags: uninsneveruninstall
Name: {userdocs}\{#AppName}\VideoFiles; Permissions: users-full; Flags: uninsneveruninstall

; temp direcotry for logs
Name: {localappdata}\{#ShortCompany}\{#AppName}\Download; Permissions: users-full
Name: {localappdata}\{#ShortCompany}\{#AppName}\Export; Permissions: users-full
Name: {localappdata}\{#ShortCompany}\{#AppName}\Logs; Permissions: users-full; Flags: uninsalwaysuninstall
Name: {localappdata}\{#ShortCompany}\{#AppName}\Logs\CSV; Permissions: users-full; Flags: uninsalwaysuninstall
Name: {localappdata}\{#ShortCompany}\{#AppName}\Settings; Permissions: users-full
Name: {app}\Bin\logs; Flags: uninsalwaysuninstall; Tasks: ; Languages: ; Permissions: users-full

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Check: MyFunction()
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Check: ShowQuickLunch()
Name: addruletofirewall; Description: {cm:AddExceptionRule}; GroupDescription: Firewall:; Check: MyFunction()

[Icons]
Name: {commondesktop}\{#AppName} v.{#Ver}{#Rev}; Filename: {app}\Bin\XmlRuntime.exe; Tasks: desktopicon; Parameters: """{app}\App\{#AppName}.v.{#Ver}.apppc"""; WorkingDir: {app}\Bin; IconFilename: {app}\App\Icons\{#AppName}.ico; Languages: 
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppName}.v.{#Ver}{#Rev}; Filename: {app}\Bin\XmlRuntime.exe; Tasks: quicklaunchicon; Parameters: """{app}\App\{#AppName}.v.{#Ver}.apppc"""; WorkingDir: {app}\Bin; IconFilename: {app}\App\Icons\{#AppName}.ico; Check: ShowQuickLunch()
; folders
Name: {group}\{#AppName}.v.{#Ver} Documents; Filename: {userdocs}\{#AppName}; IconFilename: {app}\App\Icons\foldergreen.ico
;Name: {group}\{#AppName}.v.{#Ver} Logs; Filename: {userappdata}\{#ShortCompany}\{#AppName}.v.{#Ver}\Debug; IconFilename: {app}\App\Icons\Folder_stuffed.ico
; shortcuts
Name: {group}\{#AppName}.v.{#Ver}{#Rev}; Filename: {app}\Bin\XmlRuntime.exe; WorkingDir: {app}\Bin; IconFilename: {app}\App\Icons\{#AppName}.ico; IconIndex: 0; Parameters: """{app}\App\{#AppName}.v.{#Ver}.apppc"""
Name: {group}\{#ShortCompany} Website; Filename: {app}\WebSite {#ShortCompany}.url; IconFilename: {app}\App\Icons\WWW.ico
Name: {group}\{cm:UninstallProgram,{#AppName}.v.{#Ver}{#Rev}}; Filename: {uninstallexe}

[Run]
Filename: msiexec.exe; Parameters: /i {tmp}\vcredist.msi /quiet; WorkingDir: {tmp}; Description: Register C++ Library
Filename: msiexec.exe; Parameters: /i {tmp}\AxisH264DecoderSetup.msi /passive; WorkingDir: {tmp}; Description: Install Axis H.264 Decoder
Filename: regedit.exe; Parameters: /S ffdshow.reg; WorkingDir: {app}\Bin; Flags: shellexec; Description: Install codec config file
Filename: regsvr32.exe; Parameters: /S LMVYUVxf.dll; WorkingDir: {app}\Bin; Description: Register video library
Filename: regsvr32.exe; Parameters: /S OvTool.dll; WorkingDir: {app}\Bin; Description: Register text overlay library
Filename: regsvr32.exe; Parameters: /S GSSF.ax; WorkingDir: {app}\Bin; Description: Register video library
Filename: regsvr32.exe; Parameters: /S AxH264Dec.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S AxisMediaViewer.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S AxMP4Dec.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S AxMJPGDec.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S Audio.dll; WorkingDir: {app}\Bin; Description: Register AXIS camera support library
Filename: regsvr32.exe; Parameters: /S l3codecx.ax; WorkingDir: {app}\Bin; Description: Register mpeg codecs
Filename: regsvr32.exe; Parameters: /S Skype4COM.dll; WorkingDir: {cf}\Skype; Description: Register skype4COMLib
Filename: notepad.exe; Description: {cm:OpenReleaseNotes}; Parameters: {app}\Docs\ReleaseManifest.txt; Flags: nowait postinstall skipifsilent; WorkingDir: {app}\Docs; Languages: 
Filename: {app}\App\Help\{#AppName}.chm; Description: {cm:OpenHelpFile}; Flags: nowait postinstall skipifsilent shellexec unchecked
Filename: {app}\Bin\XmlRuntime.exe; Description: {cm:LaunchProgram,{#AppName}.v.{#Ver}{#Rev}}; Flags: nowait postinstall skipifsilent; Parameters: """{app}\App\{#AppName}.v.{#Ver}.apppc"""; WorkingDir: {app}\Bin; Languages: 
Filename: {sys}\netsh.exe; Tasks: addruletofirewall; Parameters: "firewall add allowedprogram program=""{app}\Bin\XmlRuntime.exe"" name=""{#AppName}.v.{#Ver}{#Rev}"" ENABLE ALL"; StatusMsg: {#AppName}.v.{#Ver}{#Rev} add exception rule to firewall; Flags: runhidden; MinVersion: 0,5.01.2600sp2

[UninstallDelete]
Name: {app}\{#AppName}; Type: filesandordirs; Tasks: ; Languages: 
Name: {app}\Bin; Type: filesandordirs; Tasks: ; Languages: 
Name: {app}\Debug; Type: filesandordirs; Tasks: ; Languages: 

[UninstallRun]
Filename: {sys}\netsh.exe; Parameters: "firewall delete allowedprogram program=""{app}\Bin\XmlRuntime.exe"" profile=ALL"; StatusMsg: {#AppName}.v.{#Ver}{#Rev} removing exception rule to firewall; Flags: runhidden

[Registry]
Root: HKLM; Subkey: Software\{#RegCompany}
Root: HKLM; Subkey: Software\{#RegCompany}\{#AppName}.{#Ver}
Root: HKLM; Subkey: Software\{#RegCompany}\{#AppName}.{#Ver}; ValueType: string; ValueName: AppID; ValueData: {{#AppID}
Root: HKLM; Subkey: Software\{#RegCompany}\{#AppName}.{#Ver}; ValueType: string; ValueName: Version; ValueData: {#Ver}{#Rev}


[Code]
const
	Netfx35RegKeyName = 'Software\Microsoft\NET Framework Setup\NDP\v3.5';
	Netfx40RegKeyName = 'Software\Microsoft\NET Framework Setup\NDP\v4\Full';

	NetfxStandardRegValueName = 'Install';
	NetfxStandardSPxRegValueName = 'SP';
	NetfxStandardVersionRegValueName = 'Version';

	// Version information for final release of .NET Framework 3.0
	Netfx30VersionMajor = 3;
	Netfx30VersionMinor = 0;
	Netfx30VersionBuild = 4506;
	Netfx30VersionRevision = 26;

	// Version information for final release of .NET Framework 3.5
	Netfx35VersionMajor = 3;
	Netfx35VersionMinor = 5;
	Netfx35VersionBuild = 21022;
	Netfx35VersionRevision = 8;

	// Version information for final release of .NET Framework 4.0
	Netfx40VersionMajor = 4;
	Netfx40VersionMinor = 0;
	Netfx40VersionBuild = 30319;

	// network path for .Net 3.5
  Netfx35InstallPath = 'http://download.microsoft.com/download/6/0/f/60fc5854-3cb8-4892-b6db-bd4f42510f28/dotnetfx35.exe';
	Netfx40InstallPath = 'http://www.microsoft.com/downloads/info.aspx?na=41&srcfamilyid=0a391abd-25c1-4fc0-919f-b21f31ab88b7&srcdisplaylang=en&u=http%3a%2f%2fdownload.microsoft.com%2fdownload%2f9%2f5%2fA%2f95A9616B-7A37-4AF6-BC36-D6EA96C8DAAE%2fdotNetFx40_Full_x86_x64.exe';

	// ffdshow
	ffdshowRegKeyName = 'SOFTWARE\GNU\ffdshow';
 	ffdShowStandardVersionRegValueName = 'revision';
	ffdshowRevision = 2547;
  ffdshowDirectory = '{pf32}\{#ShortCompany}\ffdshow';

	// vcredist
	vcredistRegKeyName = 'SOFTWARE\Classes\Installer\Products\6F9E66FF7E38E3A3FA41D89E8A906A4A';
 	vcredistStandardVersionRegValueName = 'Version';
	vcredistRevision = 151015966;

  // EZVideoPlayer
	EZVideoPlayerRegKeyName = 'Software\{#RegCompany}\{#EZVideoPlayerReg}';
 	EZVideoPlayerVersionRegValueName = 'Version';

  // EZUpdateCenter
	EZUpdateCenterRegKeyName = 'Software\{#RegCompany}\{#EZUpdateCenterReg}';
 	EZUpdateCenterVersionRegValueName = 'Version';

	//HEM drivers path
	//UnInstRegPathHEM = 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{8629A6E3-E2B6-4EDC-8BBB-826EF9369E67}';

	// path in registry to unintall data
	UnInstRegPath = 'Software\Microsoft\Windows\CurrentVersion\Uninstall\{#AppID}_is1';

	// path in registry to unintall data
	UnInstRegValueName = 'UninstallString';

	// path to install location
	InstLocRegValueName = 'InstallLocation';

	// Address of survey webpage
  SurveyWebPage = 'http://survey.ezconnectsystem.com';

  // The constants for CreateFile ().
  GENERIC_READ        = $80000000;
  GENERIC_WRITE       = $40000000;
  GENERIC_EXECUTE     = $20000000;
  GENERIC_ALL         = $10000000;
  FILE_SHARE_READ     = 1;
  FILE_SHARE_WRITE    = 2;
  FILE_SHARE_DELETE   = 4;
  CREATE_NEW          = 1;
  CREATE_ALWAYS       = 2;
  OPEN_EXISTING       = 3;
  OPEN_ALWAYS         = 4;
  TRUNCATE_EXISTING   = 5;

  // General Win32.
  INVALID_HANDLE_VALUE = -1;

var
  autostartParameter: Boolean;
  helpParameter: Boolean;
  supressMessageBoxesParameter: Boolean;
  verySilentParameter: Boolean;
  logParameter: Boolean;
  rebootRequired: Boolean;
  language : String;
  UnInstallMessage : String;
  ffdshowParameter: Boolean;

//inno defined event -
//returns 'true' if dotnet was installed for first time
function NeedRestart: Boolean;
begin
	Result := rebootRequired
end;


// The required Win32 functions.
function CreateFile (
	 lpFileName: String; // Could be PChar, but String is good enough for us here.
	 dwDesiredAccess: Cardinal;
	 dwShareMode: Cardinal;
	 lpSecurityAttributes: Cardinal;
	 dwCreationDisposition: Cardinal;
	 dwFlagsAndAttributes: Cardinal;
	 hTemplateFile:Integer
): Integer;
external 'CreateFileA@kernel32.dll stdcall';

function WriteFile (
	 hFile : THandle;
	 lpBuffer : String;
	 nNumberOfBytesToWrite : LongInt;
	 var lpNumberOfBytesWritten: LongInt;
	 lpOverlapped : LongInt
) : Boolean;
external 'WriteFile@kernel32.dll stdcall';

function CloseHandle (hHandle: Integer): Integer;
external 'CloseHandle@kernel32.dll stdcall';

function CompareVersionNumber( versionNumber: string; verMajor, verMinor, verBuild, verRevision: Integer ): Boolean;
var
	Major, Minor, Build, Revision: Integer;
	num, i, len, p: Integer;
	ToConvert, Rest: string;
begin
	Major := 0;
	Minor := 0;
	Build := 0;
	Revision := 0;
	i := 0;
	Rest := versionNumber;
	while i < 4 do
	begin
		p := Pos( '.', Rest );
		len := Length( Rest );

		if p = 0 then
		begin
			// try converting the last number available
			ToConvert := Copy( Rest, 1, len );
			Rest := '';
		end
		else
		begin
			ToConvert := Copy( Rest, 1, p - 1 );
			Rest := Copy( Rest, p + 1, len - p );
		end

		num := StrToIntDef( ToConvert, 0 );

		case i of
			0 : Major := num;
			1 : Minor := num;
			2 : Build := num;
			3 : Revision := num;
		end;

		if p = 0 then
		begin
			break;
		end;

		i := i + 1;
	end;

	Result := False;
	// compare numbers
	if Major > verMajor then
	begin
		Result := True;
	end
	else if Major = verMajor then
	begin
		if Minor > verMinor then
		begin
			Result := True;
		end
		else if Minor = verMinor then
		begin
			if Build > verBuild then
			begin
				Result := True;
			end
			else if Build = verBuild then
			begin
				if Revision >= verRevision then
				begin
					Result := True;
				end;
			end;
		end;
	end;
end;

// compare version by string
function GetNumber(var temp: String): Integer;
var
  part: String;
  pos1: Integer;
begin
  if Length(temp) = 0 then
  begin
    Result := -1;
    Exit;
  end;
    pos1 := Pos('.', temp);
    if (pos1 = 0) then
    begin
      Result := StrToInt(temp);
    temp := '';
    end
    else
    begin
    part := Copy(temp, 1, pos1 - 1);
      temp := Copy(temp, pos1 + 1, Length(temp));
      Result := StrToInt(part);
    end;
end;
 
function CompareInner(var temp1, temp2: String): Integer;
var
  num1, num2: Integer;
begin
    num1 := GetNumber(temp1);
  num2 := GetNumber(temp2);
  if (num1 = -1) or (num2 = -1) then
  begin
    Result := 0;
    Exit;
  end;
      if (num1 > num2) then
      begin
        Result := 1;
      end
      else if (num1 < num2) then
      begin
        Result := -1;
      end
      else
      begin
        Result := CompareInner(temp1, temp2);
      end;
end;
 
function CompareVersion(str1, str2: String): Integer;
var
  temp1, temp2: String;
begin
    temp1 := str1;
    temp2 := str2;
    Result := CompareInner(temp1, temp2);
end;

function IsNetfx35Installed( var version: string ): Boolean;
var
	ValDword: Cardinal;
	ValString: string;
begin
	Result := False;

	// Check that the Install registry value exists and equals 1
	if RegQueryDWordValue( HKEY_LOCAL_MACHINE, Netfx35RegKeyName, NetfxStandardRegValueName, ValDword ) then
	begin
		if ValDword = 1 then
		begin
			// A system with a pre-release version of the .NET Framework 3.5 can
			// have the Install value.  As an added verification, check the
			// version number listed in the registry
			// Get version number
			if RegQueryStringValue( HKEY_LOCAL_MACHINE, Netfx35RegKeyName, NetfxStandardVersionRegValueName, ValString ) then
			begin
				version := ValString;
				if CompareVersionNumber( ValString
					, Netfx35VersionMajor
					, Netfx35VersionMinor
					, Netfx35VersionBuild
					, Netfx35VersionRevision ) then
				begin
					Result := True;
				end
			end;
		end
	end;
end;

function IsNetfx40Installed(): Boolean;
var
	ValDword: Cardinal;
	ValString: string;
begin
	Result := False;

	// Check that the Install registry value exists and equals 1
	if RegQueryDWordValue( HKEY_LOCAL_MACHINE, Netfx40RegKeyName, NetfxStandardRegValueName, ValDword ) then
	begin
		if ValDword = 1 then
		begin
			Result := True;
		end
	end;
end;

function IsFfdshowInstalled(): Boolean;
var
	ValDword: Cardinal;
	ValRevision: DWord;
begin
	Result := False;
	ValRevision := 0;

  begin 
    // Check that the ffdshow was installed or not
    if RegKeyExists( HKLM32, ffdshowRegKeyName ) then
    begin
      // Get revision number
      if RegQueryDWordValue( HKLM32, ffdshowRegKeyName, ffdShowStandardVersionRegValueName, ValRevision ) then
      begin
        if ValRevision = ffdshowRevision then
        begin
            if FileOrDirExists(ExpandConstant(ffdshowDirectory)) then
              begin
                Result := True;
              end;
         end; 
      end;
    end;
  end;
end;

function IsVcredistInstalled(): Boolean;
var
	ValDword: Cardinal;
	ValVersion: DWord;
begin
	Result := False;
	ValVersion := 0;

	// Check that the vcredist was installed or not
	if RegKeyExists( HKEY_LOCAL_MACHINE, vcredistRegKeyName ) then
	begin
		// Get version number
		if RegQueryDWordValue( HKEY_LOCAL_MACHINE, vcredistRegKeyName, vcredistStandardVersionRegValueName, ValVersion ) then
		begin
      if ValVersion >= vcredistRevision then
			begin
				Result := True;
			end
		end;
	end;
end;

function IsEZVideoPlayerInstalled(): Boolean;
var
	ValString: string;
  RootKey: Integer;
  CompreResult: Integer;
begin
	Result := False;

  if IsWin64 then
  begin
    RootKey:= HKLM64;
  end
  else
  begin
    RootKey:= HKEY_LOCAL_MACHINE;
  end;

  // Check that the EZVideoPlayer was installed or not
	if RegKeyExists( RootKey, EZVideoPlayerRegKeyName ) then
  begin
    if RegQueryStringValue( RootKey, EZVideoPlayerRegKeyName, EZVideoPlayerVersionRegValueName, ValString ) then
			begin
        if ValString <> '' then
        begin
          CompreResult := CompareVersion(ExpandConstant('{#EZVideoPlayerVersion}'), ValString);

          if CompreResult < 1 then
          begin
            Result := True;
          end
        end;
			end;
  end;
end;

function IsEZUpdateCenterInstalled(): Boolean;
var
	ValString: string;
  CompreResult: Integer;
begin
	Result := False;

  // Check that the EZUpdateCenter was installed or not
	if RegKeyExists( HKEY_LOCAL_MACHINE, EZUpdateCenterRegKeyName ) then
  begin
    if RegQueryStringValue( HKEY_LOCAL_MACHINE, EZUpdateCenterRegKeyName, EZUpdateCenterVersionRegValueName, ValString ) then
			begin
        if ValString <> '' then
        begin
          CompreResult := CompareVersion(ExpandConstant('{#EZUpdateCenterVersion}'), ValString);

          if CompreResult < 1 then
          begin
            Result := True;
          end
        end;
			end;
  end;
end;

function IsUnInstallRequired( var unInstCmd: string ): Boolean;
var
	ValString: string;
begin
	Result := False;
	if (RegQueryStringValue( HKEY_LOCAL_MACHINE, UnInstRegPath, UnInstRegValueName, ValString) ) then
	begin
		if ValString <> '' then
		begin
			unInstCmd := RemoveQuotes( ValString );
			Result := True;
		end;
	end
end;

function GetInstallLocation( var instLocCmd: string ): Boolean;
var
	ValString: string;
begin
	Result := False;
	if RegQueryStringValue( HKEY_LOCAL_MACHINE, UnInstRegPath, InstLocRegValueName, ValString ) then
	begin
		if ValString <> '' then
		begin
			instLocCmd := RemoveQuotes( ValString );
			Result := True;
		end;
	end
end;

function PerformUnInstall( unInstCmd: string ): Boolean;
var
	ErrorCode: Integer;
	ResultCode: Integer;
 	InstLocCmd: String;
 	UninstallResult: Boolean;
begin
	if GetInstallLocation( InstLocCmd ) then
	begin
		Exec(ExpandConstant('{win}\Microsoft.NET\Framework\v4.0.30319\ngen.exe'), 'uninstall "' + InstLocCmd + 'Bin\XmlRuntime.exe"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
	end

	if verySilentParameter then
	begin
		UninstallResult := Exec( unInstCmd, '/VERYSILENT /NORESTART', '', SW_HIDE, ewWaitUntilTerminated, ErrorCode );
	end
	else
	begin
		UninstallResult := Exec( unInstCmd, '/SILENT /NORESTART', '', SW_HIDE, ewWaitUntilTerminated, ErrorCode );
	end

	if UninstallResult then
	begin
		Result := True;
	end
	else
	begin
		Result := False;
	end;
end;

function UnInstallOldVersion(): Integer;
var
	sUnInstPath: string;
	sUnInstallString: string;
	iResultCode: integer;
	ResultCode: integer;
 	instLocCmd: string;
 	UninstallResult: Boolean;
begin
	// Return Values:
	// 0 - no idea
	// 1 - can't find the registry key (probably no previous version installed)
	// 2 - uninstall string is empty
	// 3 - error executing the UnInstallString
	// 4 - successfully executed the UnInstallString

	// default return value
    Result := 0;

	sUnInstPath :='Software\Microsoft\Windows\CurrentVersion\Uninstall\{#AppID}_is1';
	sUnInstallString := '';

	// get the uninstall string of the old app
	if RegQueryStringValue( HKLM, sUnInstPath, 'UninstallString', sUnInstallString ) then
	begin
		if sUnInstallString <> '' then
		begin

			if GetInstallLocation( instLocCmd ) then
			begin
				Exec(ExpandConstant('{win}\Microsoft.NET\Framework\v2.0.50727\ngen.exe'), 'uninstall "' + instLocCmd + 'Bin\XmlRuntime.exe"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
			end

			sUnInstallString := RemoveQuotes(sUnInstallString);

			if verySilentParameter then
			begin
				UninstallResult := Exec( sUnInstallString, '/VERYSILENT /NORESTART','', SW_HIDE, ewWaitUntilTerminated, iResultCode )
			end
			else
			begin
				UninstallResult := Exec( sUnInstallString, '/SILENT /NORESTART','', SW_HIDE, ewWaitUntilTerminated, iResultCode )
			end

			if UninstallResult then
			begin
				Result := 4;
			end
			else
			begin
				Result := 3;
			end
		end
		else
		begin
			Result := 2;
		end
	end
	else
	begin
		Result := 1;
	end
end;

function MyFunction(): Boolean;
begin
	Result:=true;
end;

// check Windows version
function ShowQuickLunch(): Boolean;
var
  Version: TWindowsVersion;
begin
  GetWindowsVersionEx(Version);

  if Version.NTPlatform and
     (Version.Major = 6) and
     (Version.Minor = 1) then
  begin
    Result:=false;
  end
  else
  begin
    Result:=true;
  end;
end;

procedure WriteAgreementLog();
var
	hFile: THandle;
	bytesWritten: LongInt;
	fileName, agreement : String;
begin

	fileName := ExpandConstant('{pf}\{#ShortCompany}\Licenses\{#AppName}.v.{#Ver}{#Rev}') + '.' + GetDateTimeString('(yyyy.mm.dd)(hh.nn.ss)', #0, #0 ) + '.log'
	hFile := CreateFile (fileName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY, 0);

	if ( hFile <> INVALID_HANDLE_VALUE ) then
	begin
		agreement := 'User accepted license agreement for ' + ExpandConstant('{#AppName}.v.{#Ver}{#Rev}') + ' at ' + GetDateTimeString('yyyy.mm.dd hh:nn:ss', #0, #0 ) + '.'
		WriteFile(hFile, agreement, length(agreement), bytesWritten, 0);
		CloseHandle(hFile);
	end;
end;

procedure CommandLineInit();
var
	i: Integer;
	arg: String;
begin
	autostartParameter := true;
	helpParameter := false;
	supressMessageBoxesParameter := false;
	verySilentParameter := false;
	logParameter := false;
	ffdshowParameter := true

	i:=0;
	while (i <= ParamCount) do
	begin
		arg := lowercase(ParamStr(i));

		if arg = '/?' then helpParameter := true
		else if arg = '/help' then helpParameter := true
		else if arg = '/noautostart' then autostartParameter := false
		else if arg = '/verysilent' then verySilentParameter := true
		else if arg = '/suppressmsgboxes' then supressMessageBoxesParameter := true
		else if arg = '/log' then logParameter := true
		else if arg = '/noffdshow' then ffdshowParameter := false
		i := i +1;
	end
end;

procedure SetLanguageMessages();
var
	i: Integer;
	arg: String;
begin
	language := ExpandConstant('{language}');

	if language = 'PL' then
	begin
		UnInstallMessage := 'Odinstalowanie aktualnie zainstalowanej wersji jest wymagane' + Chr( 13 ) + Chr( 10 )
			+ 'Czy chcesz odinstalowac program?';
	end
	else if language = 'CZ' then
	begin
		UnInstallMessage := 'Odinstalov�n� aktu�ln� nainstalovanou verzi je nutn�' + Chr( 13 ) + Chr( 10 )
			+ 'Cht�li byste prov�st odinstalaci?';
	end
	else
	begin
		UnInstallMessage :=  'Uninstalling of currently installed version is required' + Chr( 13 ) + Chr( 10 )
			+ 'Would you like to proceed with uninstall?';
	end;
end;

function InitializeSetup(): Boolean;
var
    ErrorCode: Integer;
    Net35, Inst35, UnInst, Install: Boolean;
    Net40, Inst40: Boolean;
   	UnInstCmd, Message, Ver: string;
   	ResultCode: integer;
   	mutex: string;
   	version: cardinal;
   	rebootR: Boolean;
    ezAppRun: Boolean;
begin
	Result := False;
  ezAppRun := false;

	CommandLineInit();
	SetLanguageMessages();

	if IsUnInstallRequired( UnInstCmd ) then
	begin
		Message := UnInstallMessage;

		if not verySilentParameter then
		begin
			UnInst := MsgBox( Message, mbConfirmation, MB_YESNO ) = idYes;
		end
		else
		begin
			UnInst := true;
		end

		if UnInst then
		begin
		   //check if application is running
			mutex := ExpandConstant('{#AppName} {#Ver}');

			if CheckForMutexes( mutex ) then
			begin
        if not verySilentParameter then
        begin
          MsgBox( mutex + ' is running, please close it and run again setup.', mbError, MB_OK );
          Install := False;
          ezAppRun := true;
        end
        else
        begin
          // kill app
          Exec(ExpandConstant('taskkill.exe'), ExpandConstant('/FI "WINDOWTITLE eq {#AppName}*" /FI "WINDOWTITLE ne {#AppName}Client*" /IM XMLRuntime.exe'), '', SW_HIDE , ewWaitUntilTerminated, ResultCode);
          Sleep(2000);
        end;
			end
			
      if not ezAppRun then
			begin
				RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'UpdateDate', GetDateTimeString('yyyy.mm.dd hh:nn:ss', '-', ':') );
				RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey', 'false' );
				PerformUnInstall( UnInstCmd );
				Install := True;
			end
		end
		else
		begin
			Install := False;
		end;
	end
	else
	begin
		RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'InstallDate', GetDateTimeString('yyyy.mm.dd hh:nn:ss', '-', ':') );
		Install := True;
	end;

	if Install then
	begin
		//uninstall old version
		if RegValueExists(HKLM, ExpandConstant('Software\{#RegCompany}\{#AppName}'), 'Version') then
		begin
			RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}' ), 'ShowSurvey', 'false' );
		end;

		UnInstallOldVersion;

		//init windows version
		//initwinversion();

		RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v4\Full', 'Install', version);
		if version <> 1 then
		begin
			rebootR := True;
		end
		else
		begin
			rebootR := False;
		end;

		dotnetfx40();
		rebootRequired := rebootR;
		Result := True;
	end;
end;

procedure DeinitializeSetup();
var
	sPath: String;
    remove: Boolean;
begin

	WriteAgreementLog();

	if RegValueExists(HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey') then
	begin
		RegDeleteValue(HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey');
	end

	sPath := 'C:\'
	remove := True;
	// removing files
	while (remove) do
	begin
		if FileExists(sPath + 'eula.1028.txt') then
		begin
			DeleteFile(sPath + 'eula.1028.txt')
			DeleteFile(sPath + 'eula.1031.txt')
			DeleteFile(sPath + 'eula.1033.txt')
			DeleteFile(sPath + 'eula.1036.txt')
			DeleteFile(sPath + 'eula.1040.txt')
			DeleteFile(sPath + 'eula.1041.txt')
			DeleteFile(sPath + 'eula.1042.txt')
			DeleteFile(sPath + 'eula.2052.txt')
			DeleteFile(sPath + 'eula.3082.txt')
			DeleteFile(sPath + 'globdata.ini')
			DeleteFile(sPath + 'install.exe')
			DeleteFile(sPath + 'install.ini')
			DeleteFile(sPath + 'install.res.1028.dll')
			DeleteFile(sPath + 'install.res.1031.dll')
			DeleteFile(sPath + 'install.res.1033.dll')
			DeleteFile(sPath + 'install.res.1036.dll')
			DeleteFile(sPath + 'install.res.1040.dll')
			DeleteFile(sPath + 'install.res.1041.dll')
			DeleteFile(sPath + 'install.res.1042.dll')
			DeleteFile(sPath + 'install.res.2052.dll')
			DeleteFile(sPath + 'install.res.3082.dll')
			DeleteFile(sPath + 'VC_RED.cab')
			DeleteFile(sPath + 'VC_RED.MSI')
			DeleteFile(sPath + 'vcredist.bmp')
		end

		if (sPath = 'C:\') then
			sPath := 'D:\'
		else if (sPath = 'D:\') then
			sPath := 'E:\'
		else
			remove := False
	end
end;

function InitializeUninstall(): Boolean;
var
	mutex : string;
	showSurvay : string;
  ResultCode: integer;
begin
	mutex := ExpandConstant('{#AppName} {#Ver}');

	if CheckForMutexes( mutex ) then
	begin
    if not verySilentParameter then
    begin
      MsgBox( mutex + ' is running, please close it and run again uninstall setup.', mbError, MB_OK );
      RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey', 'false' );
      Result := false;
    end
    else
    begin
      // kill app
      Exec(ExpandConstant('taskkill.exe'), ExpandConstant('/FI "WINDOWTITLE eq {#AppName}*" /FI "WINDOWTITLE ne {#AppName}Client*" /IM XMLRuntime.exe'), '', SW_HIDE , ewWaitUntilTerminated, ResultCode);
      Sleep(2000);
      Result := true;
    end;
	end
	else
	begin
		RegQueryStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey', showSurvay );

		if showSurvay = '' then
		begin
			RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey', 'true' );
		end

		Result := true;
	end
end;

procedure DeinitializeUninstall();
var
	ErrorCode : Integer;
	showSurvay : string;
begin

	RegQueryStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey', showSurvay );

	if (showSurvay <> 'false') and ({#RunSurvey}) then
	begin
		RegWriteStringValue( HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'RemoveDate', GetDateTimeString('yyyy.mm.dd hh:nn:ss', '-', ':') );
		ShellExec('open', SurveyWebPage, '', '', SW_SHOWNORMAL, ewNoWait, ErrorCode);
	end

	if RegValueExists(HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey') then
	begin
		RegDeleteValue(HKEY_LOCAL_MACHINE, ExpandConstant( 'Software\{#RegCompany}\{#AppName}.{#Ver}' ), 'ShowSurvey');
	end
end;

procedure CopyAppSettingsFile();
var
	srcPath: String;
	destPath: String;
begin
	// copy settings file if not exists in destination
	destPath := ExpandConstant('{app}\App\{#AppName}.v.{#Ver}.settings')
	srcPath := ExpandConstant('{tmp}\AppSettings\{#AppName}.v.{#Ver}{language}.settings');
	FileCopy(srcPath, destPath, True);
end;

procedure CurStepChanged(CurStep: TSetupStep);
var
	CustomPage: TOutputProgressWizardPage;
	ResultCode: Integer;
begin

	if (CurStep = ssPostInstall) then
	begin
		CopyAppSettingsFile();

		CustomPage := CreateOutputProgressPage( CustomMessage( 'PostInstallTasks' ), CustomMessage( 'PleaseWait' ) );
		CustomPage.Show();

    if {#InstallVideo} then
    begin
      if not IsVcredistInstalled() then
      begin
        CustomPage.SetText( CustomMessage( 'InstallingVisualCRuntimes' ), '' );
        Exec(ExpandConstant('{tmp}\vcredist_x86_90\install.exe'), '/q', ExpandConstant('{tmp}'), SW_HIDE, ewWaitUntilTerminated, ResultCode);
      end;

      if ffdshowParameter then
      begin
        if not IsFfdshowInstalled() then
        begin
          CustomPage.SetText( CustomMessage( 'InstallingCodecs' ), '' );

          if verySilentParameter then
            begin
              Exec(ExpandConstant('{tmp}\ffdshow-rev2547_20081228.exe'), ExpandConstant('/NORESTART /VERYSILENT /DIR="{pf32}\{#ShortCompany}\ffdshow"'), ExpandConstant('{tmp}'), SW_HIDE, ewWaitUntilTerminated, ResultCode);
            end
            else
            begin
              Exec(ExpandConstant('{tmp}\ffdshow-rev2547_20081228.exe'), ExpandConstant('/SILENT /DIR="{pf32}\{#ShortCompany}\ffdshow"'), ExpandConstant('{tmp}'), SW_HIDE, ewWaitUntilTerminated, ResultCode);
            end
        end;
      end;

      if not IsEZVideoPlayerInstalled() then
      begin
        CustomPage.SetText( CustomMessage( 'InstallingEZVideoPlayer' ), '' );
        Exec(ExpandConstant('{tmp}\{#EZVideoPlayerSetup}'), '/verysilent', ExpandConstant('{tmp}'), SW_HIDE, ewWaitUntilTerminated, ResultCode);
      end;
    end;

    if {#InstallUpdateCenter} then
    begin
      if not IsEZUpdateCenterInstalled() then
      begin
        CustomPage.SetText( CustomMessage( 'InstallingEZUpdateCenter' ), '' );
        Exec(ExpandConstant('{tmp}\{#EZUpdateCenterSetup}'), '/verysilent', ExpandConstant('{tmp}'), SW_HIDE, ewWaitUntilTerminated, ResultCode);
      end;
    end;

		CustomPage.SetText( CustomMessage( 'CompilingApplication' ), '' );
		Exec(ExpandConstant('{win}\Microsoft.NET\Framework\v4.0.30319\ngen.exe'), ExpandConstant('install "{app}\Bin\XmlRuntime.exe"'), '', SW_HIDE, ewWaitUntilTerminated, ResultCode);

		CustomPage.Hide();

	end;
end;
