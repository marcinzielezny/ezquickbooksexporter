; *** Inno Setup version 4.2.2+ Polish messages ***
; Slawomir Adamski <sadam@wa.onet.pl>
;
; To download user-contributed translations of this file, go to:
;   http://www.jrsoftware.org/is3rdparty.php
;
; Note: When translating this text, do not add periods (.) to the end of
; messages that didn't have them already, because on those messages Inno
; Setup adds the periods automatically (appending a period would result in
; two periods being displayed).
;
; $jrsoftware: issrc/Files/Languages/Polish.isl,v 1.58 2004/05/04 19:38:11 jr Exp $

[LangOptions]
LanguageName=Polski
LanguageID=$0415
LanguageCodePage=1250
; If the language you are translating to requires special font faces or
; sizes, uncomment any of the following entries and change them accordingly.
;DialogFontName=MS Shell Dlg
;DialogFontSize=8
;WelcomeFontName=Verdana
;WelcomeFontSize=12
;TitleFontName=Arial
;TitleFontSize=29
;CopyrightFontName=Arial
;CopyrightFontSize=8

[Messages]

; *** Application titles
SetupAppTitle=Instalator
SetupWindowTitle=Instalacja - %1
UninstallAppTitle=Odinstalowanie
UninstallAppFullTitle=Odinstaluj %1

; *** Misc. common
InformationTitle=Informacja
ConfirmTitle=Potwierdzenie
ErrorTitle=Blad

; *** SetupLdr messages
SetupLdrStartupMessage=Ten program zainstaluje aplikacje %1. Czy chcesz kontynuowac?
LdrCannotCreateTemp=Nie mozna utworzyc pliku tymczasowego. Instalacja przerwana
LdrCannotExecTemp=Nie mozna uruchomic pliku w tymczasowym folderze. Instalacja przerwana

; *** Startup error messages
LastErrorMessage=%1.%n%nBlad %2: %3
SetupFileMissing=W folderze Instalatora brakuje pliku %1. Prosze rozwiazac ten problem lub uzyskac nowa kopie Instalatora tego programu od producenta.
SetupFileCorrupt=Pliki skladowe Instalatora sa uszkodzone. Prosze uzyskac nowa kopie Instalatora od producenta.
SetupFileCorruptOrWrongVer=Pliki skladowe instalatora sa uszkodzone lub niezgodne z ta wersja Instalatora. Prosze rozwiazac ten problem lub uzyskac nowa kopie Instalatora od producenta.
NotOnThisPlatform=Ten program nie dziala pod %1.
OnlyOnThisPlatform=Ten program musi byc uruchomiony z %1.
WinVersionTooLowError=Ten program wymaga %1 w wersji %2 lub p�zniejszej.
WinVersionTooHighError=Ten program nie moze byc zainstalowany w wersji %2 lub p�zniejszej systemu %1.
AdminPrivilegesRequired=Musisz miec uprawnienia administratora aby instalowac ten program.
PowerUserPrivilegesRequired=Musisz miec uprawnienia administratora lub uzytkownika zaawansowanego aby instalowac ten program.
SetupAppRunningError=Instalator wykryl, ze jest uruchomiony %1.%n%nZamknij teraz wszystkie okienka tej aplikacji, a potem wybierz przycisk OK, aby kontynuowac, lub Anuluj, aby przerwac instalacje.
UninstallAppRunningError=Program odinstalowujacy wykryl, ze jest uruchomiony %1.%n%nZamknij teraz wszystkie okna tej aplikacji, a potem wybierz przycisk OK, aby kontynuowac, lub Anuluj, aby przerwac odinstalowywanie.

; *** Misc. errors
ErrorCreatingDir=Instalator nie m�gl utworzyc folderu "%1"
ErrorTooManyFilesInDir=Nie mozna utworzyc pliku w folderze %1, poniewaz zawiera on za duzo plik�w

; *** Setup common messages
ExitSetupTitle=Zakoncz instalacje
ExitSetupMessage=Instalacja nie zostala zakonczona. Jesli zakonczysz ja teraz, aplikacja nie bedzie zainstalowana.%n%nJesli chcesz dokonczyc instalacje innym razem, uruchom ponownie Instalatora.%n%nZakonczyc instalacje?
AboutSetupMenuItem=&O Instalatorze...
AboutSetupTitle=O Instalatorze
AboutSetupMessage=%1 wersja %2%n%3%n%n Strona domowa %1:%n%4
AboutSetupNote=Polska wersja: Rafal Platek, Slawomir Adamski

; *** Buttons
ButtonBack=< &Wstecz
ButtonNext=&Dalej >
ButtonInstall=&Instaluj
ButtonOK=OK
ButtonCancel=Anuluj
ButtonYes=&Tak
ButtonYesToAll=Tak na &wszystkie
ButtonNo=&Nie
ButtonNoToAll=N&ie na wszystkie
ButtonFinish=&Zakoncz
ButtonBrowse=&Przegladaj...
ButtonWizardBrowse=P&rzegladaj...
ButtonNewFolder=&Utw�rz nowy folder

; *** "Select Language" dialog messages
SelectLanguageTitle=Wybierz jezyk instalacji
SelectLanguageLabel=Wybierz jezyk uzywany w czasie instalacji:

; *** Common wizard text
ClickNext=Wybierz przycisk Dalej, aby kontynuowac, lub Anuluj, aby zakonczyc instalacje.
BeveledLabel=
BrowseDialogTitle=Wskaz folder
BrowseDialogLabel=Wybierz folder z ponizszej listy, a potem wybierz przycisk OK.
NewFolderName=Nowy folder

; *** "Welcome" wizard page
WelcomeLabel1=Witamy w Kreatorze instalacji programu [name].
WelcomeLabel2=Instalator zainstaluje teraz program [name/ver] na Twoim komputerze.%n%nZalecamy zakonczenie pracy z wszystkimi innymi przez Ciebie uruchomionymi aplikacjami przed rozpoczeciem instalacji.

; *** "Password" wizard page
WizardPassword=Haslo
PasswordLabel1=Ta instalacja jest zabezpieczona haslem.
PasswordLabel3=Podaj haslo, potem wybierz przycisk Dalej, aby kontynuowac. W haslach sa rozr�zniane duze i male litery.
PasswordEditLabel=&Haslo:
IncorrectPassword=Wprowadzone haslo nie jest poprawne. Spr�buj ponownie.

; *** "License Agreement" wizard page
WizardLicense=Umowa Licencyjna
LicenseLabel=Przed kontynuacja prosze uwaznie przeczytac te informacje.
LicenseLabel3=Prosze przeczytac tekst Umowy Licencyjnej. Musisz zgodzic sie na warunki tej umowy przed kontynuacja instalacji.
LicenseAccepted=&Akceptuje warunki umowy
LicenseNotAccepted=&Nie akceptuje warunk�w umowy

; *** "Information" wizard pages
WizardInfoBefore=Informacja
InfoBeforeLabel=Przed przejsciem do dalszego etapu instalacji, prosze przeczytac ta wazna informacje.
InfoBeforeClickLabel=Kiedy bedziesz gotowy do instalacji, wybierz przycisk Dalej.
WizardInfoAfter=Informacja
InfoAfterLabel=Przed przejsciem do dalszego etapu instalacji, prosze przeczytac ta wazna informacje.
InfoAfterClickLabel=Gdy bedziesz gotowy do zakonczenia instalacji, wybierz przycisk Dalej.

; *** "User Information" wizard page
WizardUserInfo=Dane Uzytkownika
UserInfoDesc=Prosze podac swoje dane.
UserInfoName=&Nazwisko:
UserInfoOrg=&Organizacja:
UserInfoSerial=Numer &seryjny:
UserInfoNameRequired=Musisz podac nazwisko.

; *** "Select Destination Location" wizard page
WizardSelectDir=Wybierz docelowa lokalizacje
SelectDirDesc=Gdzie ma byc zainstalowany program [name]?
SelectDirLabel3=Instalator zainstaluje  [name] do ponizszego folderu.
SelectDirBrowseLabel=Wybierz przycisk Nastepny, aby kontynuowac. Jesli chcesz okreslic iny folder, wybierz przycisk Przegladaj.
DiskSpaceMBLabel=Potrzeba przynajmniej [mb] MB wolnego miejsca na dysku.
ToUNCPathname=Instalator nie moze instalowac do sciezki UNC. Jesli pr�bujesz instalowac program na dysku sieciowym, najpierw zmapuj ten dysk.
InvalidPath=Musisz wprowadzic pelna sciezke wraz z litera dysku, np.:%nC:\PROGRAM
InvalidDrive=Wybrany dysk nie istnieje. Prosze wybrac inny.
DiskSpaceWarningTitle=Niewystarczajaca ilosc wolnego miejsca na dysku
DiskSpaceWarning=Instalator wymaga co najmniej %1 KB wolnego miejsca na dysku. Wybrany dysk posiada tylko %2 KB dostepnego miejsca.%n%nCzy mimo to chcesz kontynuowac?
DirNameTooLong=Nazwa folderu lub sciezki jest za dluga.
InvalidDirName=Niepoprawna nazwa folderu.
BadDirName32=Nazwa folderu nie moze zawierac zadnego z nastepujacych znak�w po dwukropku:%n%n%1
DirExistsTitle=Ten folder juz istnieje
DirExists=Folder%n%n%1%n%njuz istnieje. Czy chcesz zainstalowac program w tym folderze?
DirDoesntExistTitle=Nie ma takiego folderu
DirDoesntExist=Folder:%n%n%1%n%nnie istnieje. Czy chcesz, aby zostal utworzony?

; *** "Select Components" wizard page
WizardSelectComponents=Zaznacz skladniki
SelectComponentsDesc=Kt�re komponenty maja byc zainstalowane?
SelectComponentsLabel2=Zaznacz skladniki, kt�re chcesz zainstalowac, odznacz te, kt�rych nie chcesz zainstalowac. Wybierz przycisk Dalej, aby kontynuowac.
FullInstallation=Instalacja pelna
; if possible don't translate 'Compact' as 'Minimal' (I mean 'Minimal' in your language)
CompactInstallation=Instalacja podstawowa
CustomInstallation=Dopasowanie instalacji
NoUninstallWarningTitle=Zainstalowane skladniki
NoUninstallWarning=Instalator wykryl, ze w twoim komputerze sa juz zainstalowane nastepujace skladniki:%n%n%1%n%nOdznaczenie ich nie spowoduje odinstalowania.%n%nCzy mimo tego chcesz kontynuowac?
ComponentSize1=%1 KB
ComponentSize2=%1 MB
ComponentsDiskSpaceMBLabel=Wybrane skladniki wymagaja co najmniej [mb] MB na dysku.

; *** "Select Additional Tasks" wizard page
WizardSelectTasks=Zaznacz dodatkowe zadania
SelectTasksDesc=Kt�re dodatkowe zadania maja byc wykonane?
SelectTasksLabel2=Zaznacz dodatkowe zadania, kt�re Instalator ma wykonac podczas instalacji programu [name], potem wybierz przycisk Dalej, aby kontynuowac.

; *** "Select Start Menu Folder" wizard page
WizardSelectProgramGroup=Wybierz folder Menu Start
SelectStartMenuFolderDesc=Gdzie maja byc umieszczone skr�ty do programu?
SelectStartMenuFolderLabel3=Instalator utworzy skr�ty do programu w ponizszym folderze Menu Start.
SelectStartMenuFolderBrowseLabel=Wybierz przycisk Nastepny, aby kontynuowac. Jesli chcesz okreslic inny folder, wybierz przycisk Przegladaj.
NoIconsCheck=&Nie tw�rz zadnych skr�t�w
MustEnterGroupName=Musisz wprowadzic nazwe folderu.
GroupNameTooLong=Nazwa folderu lub sciezki jest za dluga.
InvalidGroupName=Niepoprawna nazwa folderu.
BadGroupName=Nazwa folderu nie moze zawierac zadnego z nastepujacych znak�w:%n%n%1
NoProgramGroupCheck2=&Nie tw�rz folderu w Menu Start

; *** "Ready to Install" wizard page
WizardReady=Gotowy do rozpoczecia instalacji
ReadyLabel1=Instalator jest juz gotowy do rozpoczecia instalacji programu [name] na twoim komputerze.
ReadyLabel2a=Wybierz przycisk Instaluj, aby rozpoczac instalacje lub Wstecz, jesli chcesz przejrzec lub zmienic ustawienia.
ReadyLabel2b=Wybierz przycisk Instaluj, aby kontynuowac instalacje.
ReadyMemoUserInfo=Informacje uzytkownika:
ReadyMemoDir=Lokalizacja docelowa:
ReadyMemoType=Rodzaj instalacji:
ReadyMemoComponents=Wybrane skladniki:
ReadyMemoGroup=Folder w Menu Start:
ReadyMemoTasks=Dodatkowe zadania:

; *** "Preparing to Install" wizard page
WizardPreparing=Przygotowanie do instalacji
PreparingDesc=Instalator przygotowuje sie do instalacji [name] na Twoim komputerze.
PreviousInstallNotCompleted=Instalacja (usuniecie) poprzedniej wersji programu nie zostala zakonczona. Bedziesz musial ponownie uruchomic komputer, aby zakonczyc instalacje. %n%nPo ponownym uruchomieniu komputera uruchom ponownie instalatora, aby zakonczyc instalacje [name].
CannotContinue=Instalator nie moze kontynuowac. Wybierz przycisk Anuluj, aby przerwac instalacje.


; *** "Installing" wizard page
WizardInstalling=Instalowanie
InstallingLabel=Poczekaj, az instalator zainstaluje [name] na Twoim komputerze.

; *** "Setup Completed" wizard page
FinishedHeadingLabel=Zakonczono instalacje [name]
FinishedLabelNoIcons=Instalator zakonczyl instalacje programu [name] na Twoim komputerze.
FinishedLabel=Instalator zakonczyl instalacje programu [name] na Twoim komputerze. Aplikacja moze byc wywolana poprzez uzycie zainstalowanych skr�t�w.
ClickFinish=Wybierz przycisk Zakoncz, aby zakonczyc instalacje.
FinishedRestartLabel=Aby zakonczyc instalacje programu [name], Instalator musi ponownie uruchomic Tw�j komputer. Czy chcesz teraz ponownie uruchomic sw�j komputer?
FinishedRestartMessage=Aby zakonczyc instalacje programu [name], Instalator musi ponownie uruchomic Tw�j komputer.%n%nCzy chcesz teraz ponownie uruchomic sw�j komputer?
ShowReadmeCheck=Tak, chce przeczytac dodatkowe informacje
YesRadio=&Tak, teraz uruchom ponownie
NoRadio=&Nie, sam zrestartuje p�zniej
; used for example as 'Run MyProg.exe'
RunEntryExec=Uruchom %1
; used for example as 'View Readme.txt'
RunEntryShellExec=Pokaz %1

; *** "Setup Needs the Next Disk" stuff
ChangeDiskTitle=Instalator potrzebuje nastepnej dyskietki
SelectDiskLabel2=Prosze wlozyc dyskietke %1 i wybrac przycisk OK.%n%nJesli pokazany ponizej folder nie okresla polozenia plik�w z tej dyskietki, wprowadz poprawna sciezke lub wybierz przycisk Przegladaj.
PathLabel=?&ciezka:
FileNotInDir2=Plik "%1" nie zostal znaleziony na dyskietce "%2". Prosze wlozyc wlasciwa dyskietke lub wybrac inny folder.
SelectDirectoryLabel=Prosze okreslic lokalizacje nastepnej dyskietki.

; *** Installation phase messages
SetupAborted=Instalacja nie zostala zakonczona.%n%nProsze rozwiazac problem i ponownie rozpoczac instalacje.
EntryAbortRetryIgnore=Mozesz ponowic nieudana czynnosc, zignorowac ja (nie zalecane) lub przerwac instalacje.

; *** Installation status messages
StatusCreateDirs=Tworzenie folder�w...
StatusExtractFiles=Dekompresja plik�w...
StatusCreateIcons=Tworzenie ikon aplikacji...
StatusCreateIniEntries=Tworzenie zapis�w w plikach INI...
StatusCreateRegistryEntries=Tworzenie zapis�w w rejestrze...
StatusRegisterFiles=Rejestrowanie plik�w...
StatusSavingUninstall=Zachowanie informacji o odinstalowywaniu...
StatusRunProgram=Konczenie instalacji...
StatusRollback=Wycofywanie zmian...

; *** Misc. errors
ErrorInternal2=Wewnetrzny blad: %1
ErrorFunctionFailedNoCode=Blad podczas wykonywania %1
ErrorFunctionFailed=Blad podczas wykonywania %1; kod %2
ErrorFunctionFailedWithMessage=Blad podczas wykonywania %1; code %2.%n%3
ErrorExecutingProgram=Nie moge uruchomic:%n%1

; *** Registry errors
ErrorRegOpenKey=Blad podczas otwierania klucza rejestru:%n%1\%2
ErrorRegCreateKey=Blad podczas tworzenia klucza rejestru:%n%1\%2
ErrorRegWriteKey=Blad podczas zapisu do klucza rejestru:%n%1\%2

; *** INI errors
ErrorIniEntry=Blad podczas tworzenia pozycji w pliku INI: "%1".

; *** File copying errors
FileAbortRetryIgnore=Mozesz ponowic nieudana czynnosc, zignorowac ja, aby ominac ten plik (nie zalecane), lub przerwac instalacje.
FileAbortRetryIgnore2=Mozesz ponowic nieudana czynnosc, zignorowac ja (nie zalecane) lub przerwac instalacje.
SourceIsCorrupted=Plik zr�dlowy jest uszkodzony
SourceDoesntExist=Plik zr�dlowy "%1" nie istnieje
ExistingFileReadOnly=Istniejacy plik jest oznaczony jako tylko-do-odczytu.%n%nMozesz ponowic (aby usunac oznaczenie) zignorowac (aby ominac ten plik) lub przerwac instalacje.
ErrorReadingExistingDest=Wystapil blad podczas pr�by odczytu istniejacego pliku:
FileExists=Plik juz istnieje.%n%nCzy chcesz, aby Instalator zamienil go na nowy?
ExistingFileNewer=Istniejacy plik jest nowszy niz ten, kt�ry Instalator pr�buje skopiowac. Zalecanym jest zachowanie istniejacego pliku.%n%nCzy chcesz zachowac istniejacy plik?
ErrorChangingAttr=Podczas pr�by zmiany atrybut�w istniejacego pliku wystapil blad:
ErrorCreatingTemp=Podczas pr�by utworzenia pliku w folderze docelowym wystapil blad:
ErrorReadingSource=Podczas pr�by odczytu pliku zr�dlowego wystapil blad:
ErrorCopying=Podczas pr�by kopiowania pliku wystapil blad:
ErrorReplacingExistingFile=Podczas pr�by zamiany istniejacego pliku wystapil blad:
ErrorRestartReplace=Blad RestartReplace:
ErrorRenamingTemp=Podczas pr�by zmiany nazwy pliku w folderze docelowym wystapil blad:
ErrorRegisterServer=Nie mozna zarejestrowac DLL/OCX: %1
ErrorRegisterServerMissingExport=Eksportowana funkcja DllRegisterServer nie zostala znaleziony
ErrorRegisterTypeLib=Nie moge zarejestrowac biblioteki typ�w: %1

; *** Post-installation errors
ErrorOpeningReadme=Wystapil blad podczas pr�by otwarcia pliku README.
ErrorRestartingComputer=Instalator nie m�gl zrestartowac tego komputera. Prosze zrobic to samodzielnie.

; *** Uninstaller messages
UninstallNotFound=Plik "%1" nie istnieje. Nie mozna go odinstalowac.
UninstallOpenError=Plik "%1" nie m�gl byc otwarty. Nie mozna odinstalowac
UninstallUnsupportedVer=Ta wersja programu odinstalowujacego nie rozpoznaje formatu logu deinstalacji. Nie mozna odinstalowac
UninstallUnknownEntry=W logu deinstalacji wystapila nie znana pozycja (%1)
ConfirmUninstall=Czy na pewno chcesz usunac program %1 i wszystkie jego skladniki?
OnlyAdminCanUninstall=Ta instalacja moze byc odinstalowana tylko przez uzytkownika z prawami administratora.
UninstallStatusLabel=Poczekaj az %1 zostanie usuniety z Twojego komputera.
UninstalledAll=%1 zostal usuniety z Twojego komputera.
UninstalledMost=Odinstalowywanie programu %1 zakonczone.%n%nNiekt�re elementy nie mogly byc usuniete. Mozesz je usunac recznie.
UninstalledAndNeedsRestart=Tw�j komputer musi byc ponownie uruchomiony, aby zakonczyc odinstalowywanie %1.%n%nCzy chcesz teraz ponownie uruchomic komputer?
UninstallDataCorrupted=Plik "%1" jest uszkodzony. Nie mozna odinstalowac

; *** Uninstallation phase messages
ConfirmDeleteSharedFileTitle=Usunac plik wsp�ldzielony?
ConfirmDeleteSharedFile2=System wykryl, ze nastepujacy plik nie jest juz uzywany przez zaden program. Czy chcesz odinstalowac ten wsp�ldzielony plik?%n%nJesli inne programy nadal uzywaja tego pliku, a zostanie on usuniety, moga one przestac dzialac prawidlowo. Jesli nie jestes pewny, wybierz przycisk Nie. Pozostawienie tego pliku w Twoim systemie nie spowoduje zadnych szk�d.
SharedFileNameLabel=Nazwa pliku:
SharedFileLocationLabel=Polozenie:
WizardUninstalling=Stan odinstalowywania
StatusUninstalling=Odinstalowywanie %1...

; The custom messages below aren't used by Setup itself, but if you make
; use of them in your scripts, you'll want to translate them.

[CustomMessages]

NameAndVersion=%1 wersja %2
AdditionalIcons=Dodatkowe skr�ty:
CreateDesktopIcon=Utw�rz skr�t na &pulpicie
CreateQuickLaunchIcon=Utw�rz skr�t &Szybkiego uruchamiania
ProgramOnTheWeb=%1 w Sieci
UninstallProgram=Odinstaluj %1
LaunchProgram=Uruchom %1
AssocFileExtension=&Skojarz %1 z rozszerzeniem nazwy pliku %2
AssocingFileExtension=Kojarzenie %1 z rozszerzeniem nazwy pliku %2 ...

PostInstallTasks=Zadania po instalacji.
PleaseWait=Prosze czekac...
InstallingVisualCRuntimes=Instalowanie Visual C++ SP1 Runtimes... To moze potrwac kilka minut.
InstallingCodecs=Instalowanie kodek�w... This To moze potrwac kilka minut.
CompilingApplication=Kompilowanie aplikacji... To moze potrwac kilka minut.
OpenReleaseNotes=Otw�rz manifest
OpenHelpFile=Otw�rz plik pomocy