﻿
/* 2013  ezUniverseLLC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 06/05/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE FUNCTION [V100].[Filter_GetKeyValue_ByKeyNameKeyID]
(
	@SessionID		uniqueidentifier
	, @KeyName		varchar(50)
	, @KeyID		varchar(500)
)
RETURNS sql_variant
AS
BEGIN
	DECLARE @KeyValue	sql_variant

	SET @KeyValue = NULL
	
	--VALIDATE INPUT
	IF ( ( @KeyName IS NULL ) OR ( @SessionID IS NULL ) OR ( @KeyID IS NULL ) )
	BEGIN
		RETURN NULL
	END
	
	--GET KEY VALUE
	SELECT TOP 1 
		@KeyValue  = FieldValue
	FROM FiltersMaster WITH(nolock)
	WHERE FieldName = @KeyName 
		AND KeyID = @KeyID
		AND RangeValue = @SessionID
	ORDER BY CreatedOn DESC

	RETURN @KeyValue
END