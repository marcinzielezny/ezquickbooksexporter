﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 06/06/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE FUNCTION [V100].[FILTER_DateTo]
(
	@KeyID			uniqueidentifier
	, @SessionID    uniqueidentifier
)
RETURNS DateTime
AS
BEGIN
	DECLARE 
		@UserID				bigint
		, @UTC				int
		, @TodayDate		datetime

    -- =========  SET DATES  ============================================================
	SELECT @UserID = UserID
	FROM Sessions WITH(nolock)
	WHERE RecordID = @SessionID

	SET @UTC = ISNULL( (
			SELECT MIN(UTC)
			FROM Data.Stores AS DS WITH(nolock) 
			INNER JOIN Data.UsersPermissions AS DU WITH(nolock) 
					ON DS.StoreID=DU.StoreID
			WHERE DU.UserID=@UserID AND Allow=1
		
		), DATEDIFF( mi, GETUTCDATE(), GETDATE() ) )
		
	SET @TodayDate = DATEADD(mi, @UTC, GETUTCDATE())
	-- Return a datetime in 2009-09-10 23:59:59.997 format
	SET @TodayDate = DATEADD( ms, -3, DATEADD( day, 0, DATEDIFF( day, 0, @TodayDate ) + 1 ) )
	-- =================================================================================

    RETURN
        CASE CAST( V100.Filter_GetKeyValue_ByKeyNameKeyID( @SessionID, 'Period', @KeyID ) AS varchar(50) )
            WHEN 'All' THEN @TodayDate
            WHEN 'Today' THEN @TodayDate
            WHEN 'Yesterday' THEN  DATEADD( day, -1, @TodayDate )
            WHEN 'Week' THEN @TodayDate
            WHEN 'Month' THEN @TodayDate
            WHEN 'Date' THEN  CASE 
                    WHEN V100.Filter_GetKeyDescription_ByKeyNameKeyID( @SessionID, 'Period', @KeyID ) IS NULL THEN DATEADD( ms, -3, DATEADD( day, 0, DATEDIFF( day, 0, V100.FILTER_DateFrom( @KeyID, @SessionID ) ) + 1 ) )
                    ELSE DATEADD( ms, -3, DATEADD( day, 0, DATEDIFF( day, 0, CAST( V100.Filter_GetKeyDescription_ByKeyNameKeyID( @SessionID, 'Period', @KeyID ) AS datetime ) ) + 1 ) )
                END
            WHEN 'Range' THEN
                CASE 
                    WHEN V100.Filter_GetKeyValue_ByKeyNameKeyID( @SessionID, 'DateTo', @KeyID ) IS NULL THEN DATEADD( ms, -3, DATEADD( day, 0, DATEDIFF( day, 0, V100.FILTER_DateFrom( @KeyID, @SessionID ) ) + 1 ) )
                    ELSE DATEADD( ms, -3, DATEADD( day, 0, DATEDIFF( day, 0, CAST( V100.Filter_GetKeyValue_ByKeyNameKeyID( @SessionID, 'DateTo', @KeyID ) AS datetime ) ) + 1 ) )
                END
            ELSE @TodayDate
        END
        
END