﻿
-- =============================================
-- Author:		MZ
-- Create date: 06/05/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE FUNCTION [V100].[FILTER_DateFrom]
(
	@KeyID			uniqueidentifier
	, @SessionID	uniqueidentifier
)
RETURNS DateTime
AS
BEGIN
	DECLARE 
		@UserID				bigint
		, @UTC				int
		, @TodayDate		datetime

    -- =========  SET DATES  ============================================================
	SELECT @UserID = UserID
	FROM Sessions WITH(nolock)
	WHERE RecordID = @SessionID

	SET @UTC = ISNULL( (
			SELECT MIN(UTC)
			FROM Data.Stores AS DS WITH(nolock) 
			INNER JOIN Data.UsersPermissions AS DU WITH(nolock) 
					ON DS.StoreID=DU.StoreID
			WHERE DU.UserID=@UserID AND Allow=1
		), DATEDIFF( mi, GETUTCDATE(), GETDATE() ) )
		
		
		
	SET @TodayDate = DATEADD(mi, @UTC, GETUTCDATE())
	SET @TodayDate = DATEADD( day, 0, DATEDIFF( day, 0, @TodayDate ) )
	-- =================================================================================

	RETURN	CASE CAST( V100.Filter_GetKeyValue_ByKeyNameKeyID( @SessionID, 'Period', @KeyID ) AS varchar(50) )
				WHEN 'All' THEN '01/01/2000'
				WHEN 'Date' THEN DATEADD( day, 0, DATEDIFF( day, 0, CAST( V100.Filter_GetKeyDescription_ByKeyNameKeyID( @SessionID, 'Period', @KeyID ) AS datetime ) ) )
				WHEN 'Today' THEN @TodayDate
				WHEN 'Yesterday' THEN DATEADD( day, -1, @TodayDate )
				WHEN 'Week' THEN DATEADD( day, -6, @TodayDate )
				WHEN 'Month' THEN DATEADD( day, -30, @TodayDate )
				WHEN 'Range' THEN DATEADD( day, 0, DATEDIFF( day, 0, CAST( V100.Filter_GetKeyValue_ByKeyNameKeyID( @SessionID, 'DateFrom', @KeyID ) AS datetime ) ) )
				ELSE NULL
			END
END