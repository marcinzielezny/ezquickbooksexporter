﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description:
-- =============================================
CREATE PROCEDURE [V100].[UI_User_PasswordEmail]
(
	@UserID			uniqueidentifier
	, @OrganizationID uniqueidentifier
	, @LoginID		uniqueidentifier
	, @UserName		varchar(50)
	, @UserLogin	varchar(50)
	, @UserPassword	varchar(50)
	, @UserEmail	varchar(250)
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	-- =========  DECLARATIONS  =========================================================
	DECLARE 
		@HtmlBody			nvarchar(2500)
		, @Prefix			varchar(20)
		, @MailID			int
		, @ProfileName		varchar(255)
		, @RecipientsBCC	varchar(8000)
		, @Subject			varchar(255)
	-- ==================================================================================

	-- =========  PREPARE EMAIL BODY  ===================================================
	SET @HtmlBody = [Mail].[GetMailBody]( 
			N'Dear QBExporter valued customer,<br/><br/>'
			+ N'Your password has been changed.<br/>'
			+ N'Your new Login info are:<br/><br/>'
			+ N'Login: <b>'+ @UserLogin + N'</b><br/>'
			+ N'Password: <b>' + @UserPassword + N'</b><br/><br/>'
		)
	-- ==================================================================================
	
	
	-- =========  SEND EMAIL  ===========================================================
	SET @ProfileName = 'System'
	SET @RecipientsBCC = ''
	SET @Subject ='QBExporter: Password changed'
	
	EXEC msdb.dbo.sp_send_dbmail
		@profile_name = @ProfileName
		, @recipients = @UserEmail
		, @blind_copy_recipients = @RecipientsBCC
		, @body_format = 'HTML'
		, @body = @HtmlBody
		, @subject = @Subject
		, @mailitem_id = @MailID OUTPUT
		
	INSERT INTO MailLog (
		MailID
		, RuleID
		, CreatedOn
		, SendTo
		, NotifyID
		, SendBy
	) 
	VALUES (
		@MailID
		, @LoginID
		, GETDATE()
		, @UserEmail
		, '00000000-0000-0000-0000-000000000001'
		, @UserID
	)
	-- ==================================================================================
			
END