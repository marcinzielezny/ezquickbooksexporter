﻿

/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/09/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Configuration_Wizard_InitFieldsMatch_v2]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @PosFields		nvarchar(max)
	, @VendorFields		nvarchar(max)
	, @Mode				nvarchar(35)

)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE 
		 @UserID			bigint
		, @PosMatch		XML
	    , @QBL			XML
	    , @Ven			XML

	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @PosMatch = CAST( @PosFields AS XML)
	SET @Ven = CAST(@VendorFields AS XML)
	
	SELECT ShortNumber
			, ValueID
			, Value 
			, Type
	INTO #TempData
	FROM (
	SELECT
			  T.c.value('@ShortNumber','nvarchar(100)')	AS ShortNumber
			, T.c.value('@ValueID', 'nvarchar(100)')		AS ValueID
			, T.c.value('@Value', 'nvarchar(100)')		AS Value
			, T.c.value('@Type', 'nvarchar(100)')		AS Type
	FROM @PosMatch.nodes('/*') as T(c) ) AS TMP
	
	SELECT    '' AS Value
			, '' AS ValueID
			, '' AS ShortNumber
	UNION
	SELECT 
			 Value
			,ValueID
			,ShortNumber
	FROM #TempData
	WHERE 	ShortNumber like '11__'
		OR ShortNumber like '1599' 
		OR ShortNumber like '22__'
		OR ShortNumber like '41__'
		OR ShortNumber like  '42__'
		OR ShortNumber like  '43__'
		OR ShortNumber like  '44__'
		OR ShortNumber like  '45__'
		OR ShortNumber like  '73__'
		OR ShortNumber like  '74__'
		OR ShortNumber like  '7800'
		OR ShortNumber like  '7805' 
	FOR XML RAW('Accounts')
	
	IF @Mode='SUMMARY'
	BEGIN
	
		SELECT	DPF.PosFieldsID AS _POSFieldID
			 
			  ,	[Name]		  AS [AC_POSField] 
			  , ISNULL(ValueID,'') AS AC_QBAccount
		FROM [Dict].[POSFields] AS DPF WITH (NOLOCK) LEFT OUTER JOIN #TempData AS Data ON DPF.DefaultAccountNumber = Data.ShortNumber
		WHERE SummarySet=0
		ORDER BY OrderNumber
		FOR XML RAW('POSMatches')
	
	END
	ELSE 
	BEGIN
		SELECT	DPF.PosFieldsID AS _POSFieldID
		      ,	[Name]		  AS [AC_POSField] 
			  , ISNULL(ValueID,'') AS AC_QBAccount
		FROM [Dict].[POSFields] AS DPF WITH (NOLOCK) LEFT OUTER JOIN #TempData AS Data ON DPF.DefaultAccountNumber = Data.ShortNumber
		WHERE DetailedSet=1
		ORDER BY OrderNumber
		FOR XML RAW('POSMatches')
	
	END
	
	
	DROP TABLE  #TempData
	
	;WITH XMLNAMESPACES('http://www.ezuniverse.com/schema' AS e)
	SELECT
		'UI.ComboEditorItem'AS [class]
		, 10				AS [e:active]
		, ISNULL(ValueID,'')	AS [datavalue]
		, ISNULL(Value,'')	AS [displaytext]
	FROM (
		SELECT
			  ''		AS ValueID
			, ''		AS Value
		UNION
		SELECT
			  T.c.value('@ValueID', 'nvarchar(100)')		AS ValueID
			, T.c.value('@Value', 'nvarchar(100)')		AS Value
		FROM @Ven.nodes('/*') as T(c) 
	) AS TMP 
	ORDER BY Value ASC
	FOR XML RAW( 'Item' )

END