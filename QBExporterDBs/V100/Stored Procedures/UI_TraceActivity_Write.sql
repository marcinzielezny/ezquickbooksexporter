﻿
/* 2013  ezUniverseLLC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: Copy from EZConnect
-- =============================================
Create PROCEDURE [V100].[UI_TraceActivity_Write]
(
	@SessionID				uniqueidentifier
	, @OrganizationID		uniqueidentifier
	, @ActivityTraceData	xml
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID		bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	-- ==================================================================================

	-- =========  INSERT USER ACTIVITY  =================================================
	INSERT INTO TraceActivity (
		SessionID
		, Description
		, CreatedOn
		, UserID
	)
	SELECT
		@SessionID
		, C.value( '@Description', 'varchar(200)' )
		, C.value( '@time', 'datetime' )
		, @UserID
	FROM @ActivityTraceData.nodes('/*') AS T(C)
	-- ==================================================================================
END