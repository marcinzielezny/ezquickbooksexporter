﻿
-- =============================================
-- Author:		MZ
-- Create date: 27/05/2013
-- Description: [dbo].[UI_Session_Update]
-- =============================================
CREATE PROCEDURE [V100].[UI_Session_Update]
(
 	  @SessionID	uniqueidentifier
	, @IPAddress	varchar(50)
	, @Computer		varchar(50)
	, @GMTZone		varchar(50)
	, @Version		varchar(30)
	, @RTVersion	varchar(100)
)
AS
BEGIN
	UPDATE Sessions 
	SET IPAddress	= @IPAddress
		, Computer	= @Computer
		, GMTZone	= @GMTZone
		, Version	= @Version
		, RTVersion	= @RTVersion
		, LastAction = GETDATE()
	WHERE RecordID	= @SessionID
END