﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[ForgotPassword]
(
	  @Login	varchar(100)
	, @Answer   varchar(100)
	, @Message	varchar(500) OUTPUT
	, @Status	varchar(5) OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON
	
	-- =========  DECLARATIONS  =========================================================
	DECLARE
		   @LoginID		uniqueidentifier
		, @Email		varchar(200)
		, @UserName		varchar(50)
		, @Prefix		varchar(50)
		, @Password		varchar(50)
		, @Position		int
		, @Trial		bit
	-- ==================================================================================
	
	SET @Status = 0
	
	IF LEFT( @Login, 1 ) IN ( '@', '!', '#' )
	BEGIN
		SET @Login = SUBSTRING( @Login, 2, LEN( @Login ) - 1 )
	END
	
	
	
	SET @Message = 'Wrong user !!!'
	SET @Trial = 0
	
	IF( ISNULL( (
		SELECT COUNT(1)
		FROM Logins WITH(NOLOCK)
		WHERE Active = 1
			AND IsDeleted = 0
			AND Username = @Login
	), 0 ) = 1 )
	BEGIN
	
		SELECT
			@Password = Password
			, @Email = UserEmail
			, @LoginID = RecordID
			, @UserName = ShortName
		
		FROM Logins WITH(NOLOCK)
		WHERE Active = 1
			AND IsDeleted = 0
			AND Username = @Login
			AND  SecureAnsw = @Answer

		IF ( ISNULL( @Password, '') <> '' ) 
		BEGIN
			SET @Message = 'Your password is ' + @Password + ' . Please change it after login.'
		END 
		ELSE 
		BEGIN 
			SET @Message = 'Your answer was incorrect. Please try again.'
		END 

			
	END
	ELSE 
	BEGIN
	 SET @Message = 'Wrong user !!!'
	END
	
END