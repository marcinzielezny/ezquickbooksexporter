﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Accounting_InitData_ExportConfiguration]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	-- =========  RESULTSET  ============================================================
	SELECT 'EXPORT CONFIGURATION'

	SELECT ConfigID AS _ConfigID
		, [ConfigName] AS Name
		, [ConfigPath] AS FilePath
		, Convert(varchar(20),[CreatedOn],120)  AS [CO_CreatedOn]
		, CAST((
					SELECT DS.Name		AS Name
						  ,DS.StoreID	AS _StoreID
						  ,ES.ConfigID  AS _ConfigID
					FROM Export.StoreMatch ES WITH (NOLOCK) 
						INNER JOIN Data.Stores AS DS WITH (NOLOCK)  
						ON ES.StoreID=DS.StoreID
					WHERE ES.ConfigID = EC.ConfigID
					FOR XML RAW( 'S' )) 
				AS XML)
		, 'CM_Accounting_ExportConfiguration' AS  _class
		, '0' AS _selected
	FROM Export.Configuration EC with (NOLOCK)
	WHERE OwnerID=@UserID
	FOR XML RAW( 'C' )
	-- ==================================================================================
END