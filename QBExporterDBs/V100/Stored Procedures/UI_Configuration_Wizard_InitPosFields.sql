﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 10/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Configuration_Wizard_InitPosFields]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @Mode				nvarchar(100)
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE 
	    @UserID	bigint
	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	-- =========  Pos Fields  ============================================================
	
	IF @Mode='SUMMARY'
	BEGIN
		SELECT
			[Name]		  AS [AC_POSField]
		  ,	[PosFieldsID] AS _POSFieldID
		  , ''			  AS AC_QBAccount
		FROM [Dict].[POSFields] WITH (NOLOCK) 
		WHERE SummarySet=0
		FOR XML RAW('POSMatches')
	END
	ELSE 
	BEGIN
	  SELECT
			[Name]		  AS [AC_POSField]
		  ,	[PosFieldsID] AS _POSFieldID
		  , ''			  AS AC_QBAccount
	  FROM [Dict].[POSFields] WITH (NOLOCK) 
	  WHERE DetailedSet = 1
	  FOR XML RAW('POSMatches')
	END
	-- ==================================================================================
	
	
END