﻿
/* 2013  ezUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 06/05/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[Filter_ByXpath]
(
	 @SessionID 	uniqueidentifier
	, @XmlData  	xml 
	, @XPath		varchar(1000)
	, @Procedure	varchar(100)
	, @KeyID 		uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON
	

	INSERT INTO FiltersMaster(
		[FieldName]
		, [FieldValue]
		, [FieldType]
		, [Range]
		, [RangeValue]
		, [XPath]
		, [FieldMode]
		, [Description]
		, [Secured]
		, [KeyID]
		, [Procedure]
		, [CreatedBy]
	)
	SELECT
		KeyName
		, CASE WHEN LEN( KeyValue ) = 0 THEN Value ELSE KeyValue END
		, KeyType
		, 3
		, @SessionID
		, @XPath
		, KeyMode
		, CASE WHEN KeyValue='Date' AND KeyName='Period' THEN Value ELSE Name END
		, 0
		, @KeyID
		, @Procedure
		, @SessionID
	FROM (
	SELECT
		C.value( '@keyName', 'varchar(50)' ) AS KeyName
		, C.value( '@keyValue', 'varchar(1000)' ) AS KeyValue
		, C.value( '.', 'varchar(1000)' ) AS Value
		, C.value( '@keyType', 'char(10)' ) AS KeyType
		, C.value( '@keyMode', 'varchar(5)' ) AS KeyMode
		, C.value( '@name', 'varchar(50)' ) AS Name
	FROM @XmlData.nodes('//*[@checked="true"]') AS T(C)
	) AS T

END