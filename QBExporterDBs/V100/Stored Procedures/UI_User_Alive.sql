﻿
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[UI_User_Alive]
(
    @SessionID	uniqueidentifier
	, @Kill		bit OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT TOP 1
		@Kill = SendKill
	FROM Sessions WITH(nolock)
	WHERE RecordID = @SessionID

	UPDATE Sessions
	SET	LastAction = GETDATE()
	WHERE RecordID = @SessionID

END