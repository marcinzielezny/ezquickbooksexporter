﻿/* 2011  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[ChangePassword]
(
	@SessionID			uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @GroupID			uniqueidentifier
	, @OldPassword		varchar(50)
	, @NewPassword		varchar(50)
	, @RepeatPassword	varchar(50)
	, @Message			varchar(100) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE 
		@UserID			bigint
		, @LoginID		uniqueidentifier
		, @UserEmail	nvarchar(100)
		, @UserName		nvarchar(50)
		, @UserLogin	nvarchar(50)
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		SET @Message = 'You do not have right to change password.'
		RETURN
	END
	-- ==================================================================================
		
	-- =========  VERIFY PASSWORD  ======================================================
	IF NOT EXISTS ( 
		SELECT 1 
		FROM Logins WITH(nolock)
		WHERE LoginID = @UserID
			AND Password = @OldPassword
	)
	BEGIN
		SET @Message = 'Wrong old password.'
		RETURN
	END

	IF ( @NewPassword <> @RepeatPassword )
	BEGIN
		SET @Message = 'Passwords does not match up.'
		RETURN
	END
	-- ==================================================================================
	
	-- =========  UPDATE PASSWORD  ======================================================
	UPDATE Logins
	SET Password = @NewPassword
		, LastModifiedOn = GETDATE()
	WHERE LoginID = @UserID

	SET @Message = 'Password was changed.'
	-- ==================================================================================
	
	-- =========  PREPARE DATA  =========================================================
	SELECT
		@UserName = ShortName
		, @UserLogin = UserName
		, @UserEmail = UserEmail
		, @LoginID = RecordID
	FROM Logins WITH(nolock)
	WHERE LoginID = @UserID
	-- ==================================================================================
	
END