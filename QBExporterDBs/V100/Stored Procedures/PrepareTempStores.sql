﻿
/* 2009  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 06/05/2013
-- Description: Created on
-- =============================================
CREATE PROCEDURE [V100].[PrepareTempStores]
(
      @SessionID  uniqueidentifier
    , @UserID	bigint  
    , @KeyID    uniqueidentifier
)
AS
BEGIN

    SET NOCOUNT ON
    
    -- =========  DECLARATIONS  =========================================================
	DECLARE @StoreViewType	    varchar(50)
	-- ==================================================================================
	
	
	SET @StoreViewType = CAST( (
	        SELECT TOP 1 
		        FieldValue
	        FROM FiltersMaster WITH(nolock)
	        WHERE FieldName = 'StoreType' 
		        AND KeyID = @KeyID
		        AND RangeValue = @SessionID
	        ORDER BY CreatedOn DESC
	    ) AS varchar(50) )
	    
	
    IF ( @StoreViewType = 'ALL' )
    BEGIN

	    INSERT INTO #TempStores(
		      StoreID
		    , Location
		    , IsDetails
	    )
	    SELECT 
			  DS.StoreID
            , Name
            , Details
	    FROM Data.Stores AS DS WITH(nolock)  INNER JOIN Data.UsersPermissions   AS DU WITH(nolock)
			ON DS.StoreID = DU.StoreID
	    WHERE DU.UserID=@UserID AND Allow=1
			
    END
    ELSE IF ( @StoreViewType = 'SELECTED' )
    BEGIN
	      INSERT INTO #TempStores(
		      StoreID
		    , Location
		    , IsDetails 
	    )
	    SELECT 
			  DS.StoreID
            , Name
            , Details
	    FROM Data.Stores AS DS WITH(nolock)
			INNER JOIN Data.UsersPermissions   AS DU WITH(nolock)
				ON DS.StoreID = DU.StoreID
	    WHERE DU.UserID=@UserID AND Allow=1 AND DS.StoreID in (
			SELECT CAST(FieldValue AS bigint)
	        FROM FiltersMaster WITH(nolock)
	        WHERE FieldName = 'StoreID' 
		        AND KeyID = @KeyID
		        AND RangeValue = @SessionID
	    )
			
    END
    ELSE
    BEGIN
	    PRINT 'UNKNOWN @StoreViewType: ' + ISNULL( @StoreViewType, 'NULL' )
	END
END