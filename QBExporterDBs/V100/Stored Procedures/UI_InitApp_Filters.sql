﻿
/* 2013  ezUniverseLLC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 13/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_InitApp_Filters]
(
	@SessionID			uniqueidentifier
	, @OrganizationID	uniqueidentifier
)
AS
BEGIN
	
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE 
		@UserID		bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	-- ==================================================================================

	-- =========  FILTERS ===============================================================
	;WITH XMLNAMESPACES('http://www.ezuniverse.com/schema' AS e)
	SELECT
		'UI.TreeViewNode'							AS [class]
		, 'checkbox'								AS [checkORradio]
		, 'true'									AS [checked]
		, 'Select All'								AS [name]
		, '11'										AS [e:active]
		, '[Images/Filter.SelectAll16x16]'			AS [icon]
		, '0'	AS [_StoreID]
		, '1'										AS [_S]
		, 'Filters/AC_SelectAll'					AS [_action]
		, 'Store'									AS [keyName]
		, '0'	AS [keyValue]
		, 'bigint'									AS [keyType]
	UNION ALL
	SELECT
		'UI.TreeViewNode'
		, 'checkbox'
		, 'false'
		, 'Deselect All'
		, '11'
		, '[Images/Filter.DeselectAll16x16]'
		, '0'
		, '2'
		, 'Filters/AC_DeselectAll'
		, 'Store'
		, '0'
		, 'bigint'
	UNION ALL
	SELECT
		'UI.TreeViewNode'
		, 'checkbox'
		, 'true'
		, Name
		, '11'
		, '[Images/Store.15x16]'
		, DS.StoreID
		, '3'
		, ''
		, 'StoreID'
		, DS.StoreID
		, 'bigint'	
	FROM Data.Stores AS DS WITH(nolock)  
		 INNER JOIN Data.UsersPermissions   AS DU WITH(nolock)
			ON DS.StoreID = DU.StoreID
	WHERE DU.UserID=@UserID AND Allow=1
	FOR XML RAW( 'Stores' )

	-- ==================================================================================

	
END