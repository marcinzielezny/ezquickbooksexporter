﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 05/10/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Configuration_Wizard_SaveData_v2]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @FilePath			nvarchar(max)
	, @ConfigName		nvarchar(max)
	, @ConfigID			bigint
	, @Mode				nvarchar(max)
	, @PosFieldsMatch	nvarchar(max)
	, @StoresMatch		nvarchar(max)
	, @EmployeesMatch	nvarchar(max)
	, @QBVendorMatch	nvarchar(max)
	, @QBList			nvarchar(max)
	, @PosDict			nvarchar(max)
	, @StoreDict		nvarchar(max)
	, @EmployeeDict		nvarchar(max)
	, @PayDict			nvarchar(max)
	, @VendorDict		nvarchar(max)
	, @Message			nvarchar(250) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE 
	    @UserID			bigint
	    , @StoresXML	XML
	    , @EmployXML	XML
	    , @PosMatch		XML
	    , @QBL			XML
	    , @PD			XML
	    , @SD			XML
	    , @ED			XML
	    , @YD			XML
	    , @VD			XML

	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @Message = 'OK' 
	SET @PosMatch = CAST( @PosFieldsMatch AS XML)
	SET @EmployXML = CAST( @EmployeesMatch AS XML)
	SET @StoresXML = CAST( @StoresMatch AS XML)
	SET @QBL = CAST( @QBList AS XML)
	SET @PD = CAST( @PosDict AS XML)
	SET @SD = CAST( @StoreDict AS XML)
	SET @ED = CAST( @EmployeeDict AS XML)
	SET @YD = CAST( @PayDict AS XML)
	SET @VD	= CAST( @VendorDict AS XML)

	CREATE TABLE #Diction
		(
			  Value nvarchar(50)
			, ValueID nvarchar(50)
			, ShortNumber nvarchar(50)
			, Type nvarchar(50)
			, AccountType nvarchar(50)
		)
		
	INSERT INTO #Diction
		(
			Value
			, ValueID
			, Type
			, ShortNumber
			, AccountType
		)
	SELECT Value
		, ValueID
		, Type
		, ShortNumber
		, AccountType
	FROM (
		SELECT
				   T.c.value('@Value','nvarchar(50)')		AS Value
				,  T.c.value('@ValueID','nvarchar(50)')		AS ValueID
				,  T.c.value('@ShortNumber','nvarchar(50)')	AS ShortNumber
				,  T.c.value('@Type','nvarchar(50)')		AS AccountType
				, 'Account'	AS Type
		FROM @PD.nodes('/*') as T(c)
		UNION ALL
		SELECT
				   T.c.value('@Value','nvarchar(50)')		AS Value
				,  T.c.value('@ValueID','nvarchar(50)')		AS ValueID
				,  T.c.value('@ShortNumber','nvarchar(50)')		AS ShortNumber
				,  ''	AS AccountType
				, 'Store'	AS Type
		FROM @SD.nodes('/*') as T(c) 
		UNION ALL
		SELECT
				   T.c.value('@Value','nvarchar(50)')		AS Value
				,  T.c.value('@ValueID','nvarchar(50)')		AS ValueID
				,  T.c.value('@ShortNumber','nvarchar(50)')		AS ShortNumber
				,  ''	AS AccountType
				, 'Employee'	AS Type
		FROM @ED.nodes('/*') as T(c)  
		UNION ALL
		SELECT
				   T.c.value('@Value','nvarchar(50)')		AS Value
				,  T.c.value('@ValueID','nvarchar(50)')		AS ValueID
				,  T.c.value('@ShortNumber','nvarchar(50)')		AS ShortNumber
				,  ''	AS AccountType
				, 'PayType'		AS Type
		FROM @YD.nodes('/*') as T(c)  
		UNION ALL
		SELECT
				   T.c.value('@displaytext','nvarchar(50)')		AS Value
				,  T.c.value('@datavalue','nvarchar(50)')		AS ValueID
				,  ''											AS ShortNumber
				,  ''											AS AccountType
				, 'Vendor'		AS Type
		FROM @VD.nodes('/*') as T(c)  
	) AS M

	
	SELECT
			 T.c.value('@_StoreID','bigint')		AS StoreID
		   , T.c.value('@AC_StoreSelected','bit')	AS Selected
		   , T.c.value('@AC_QBClass', 'nvarchar(50)')	AS QBClass
	INTO #StoreMatch 
	FROM @StoresXML.nodes('/*') as T(c) 
	
	SELECT
			 T.c.value('@_POSFieldID','bigint')		AS PosFieldID
		   , T.c.value('@AC_QBAccount','nvarchar(50)')	AS QBAccount
		   , T.c.value('@_modified', 'bit')			AS Modified
		   , T.c.value('@AC_Vendor', 'nvarchar(100)') AS QBVendor
	INTO #PosMatch
	FROM @PosMatch.nodes('/*') as T(c) 
	
	
	IF (EXISTS (SELECT TOP 1 1 FROM #PosMatch PM INNER JOIN #Diction DI ON PM.QBAccount = DI.ValueID  WHERE QBVendor!='' AND ShortNumber!='2205' ))
	BEGIN
		SET @Message = 'In current version of application vendor could be bound only with account 2205'
		RETURN 
	END

	-- ========= Validation ===========================================================	
	
	IF ( NOT EXISTS(SELECT TOP 1 1  FROM #StoreMatch WHERE Selected=1))
	BEGIN
		SET @Message = 'Please choose stores for configuration'
		RETURN
	END 
	
	IF (EXISTS (SELECT ConfigID FROM Export.StoreMatch ESM INNER JOIN #StoreMatch SM ON ESM.StoreID = SM.StoreID AND ConfigID!=@ConfigID AND SM.Selected=1 ))
	BEGIN
		SET @Message = 'Each store can only be configured to one QuickBooks file.  You are receiving this message because you are attempting to configure the same store to multiple QuickBooks files.  Please review your configuration.'
		RETURN 
	END
	
	IF (EXISTS (SELECT TOP 1 1 FROM #PosMatch WHERE QBAccount=''))
	BEGIN
		SET @Message = 'Please match all fields in Pos Match table'
		RETURN 
	END

	IF (@ConfigID=0)
	BEGIN
		-- ========= ADD NEW CONFIGURATION ==================================================	
		INSERT INTO [Export].[Configuration]
			   (
				[ConfigName]
			   ,[ConfigPath]
			   ,[OwnerID]
			   ,[IsActive]
			   ,[CreatedOn]
			   ,[CreatedBy]
			   ,[Mode]
			   )
		 VALUES
			( 
				  @ConfigName
				, @FilePath
				, @UserID
				, 1
				, GETDATE()
				, @UserID
				, CASE WHEN @Mode='SUMMARY' THEN 0 ELSE 1 END
			)

		-- ==================================================================================	

		SET @ConfigID =  @@IDENTITY
		-- =========  STORES  ===============================================================
		INSERT INTO Export.StoreMatch
			(
				[ConfigID]
				,[StoreID]
				,[QBAStore]
				,[ModifiedOn]
				,[ModifiedBy]
			)
		SELECT	@ConfigID
			  , StoreID
			  , QBClass
			  , GETDATE()
			  , @UserID
		FROM  #StoreMatch AS Data
		WHERE Selected = 1
		-- ==================================================================================
		
		-- =========  PosFieldsMatch  =======================================================
		
		INSERT INTO [Export].[PosFieldsMatch]
			(
				[ConfigID]
				,[PosFieldID]
				,[QBAccount]
				,[QBAccountName]
				,[QBVendor] 
				,[ModifiedOn]
				,[ModifiedBy]
			)
		SELECT @ConfigID
			   ,PosFieldID
			   ,QBAccount
			   ,Value
			   ,QBVendor
			   ,GETDATE()
			   ,@UserID
		FROM #PosMatch AS DP  INNER JOIN (
			SELECT
					 T.c.value('@Value','nvarchar(50)')		AS Value
				   , T.c.value('@ValueID','nvarchar(50)')	AS ValueID
				FROM @QBL.nodes('/*')	as T(c) 
		) AS DM ON DP.QBAccount=DM.ValueID

		-- ==================================================================================
		-- =========  EmployeesMatch  =======================================================
		
		INSERT INTO [Export].[EmployeeMatch]
				([ConfigID]
				,[EmployeeID]
				,[QBEmployee]
				,[QBPayType]
				,[ModifiedOn]
				,[ModifiedBy]
			   )
		SELECT @ConfigID
			   ,EmployeeID
			   ,QBEmployee
			   ,PayType
			   ,GETDATE()
			   ,@UserID
		FROM (
				SELECT
					 T.c.value('@_EmployeeID','bigint')		AS EmployeeID
				   , T.c.value('@AC_QBEmployee','nvarchar(50)')	AS QBEmployee
				   , T.c.value('@AC_QBPayType','nvarchar(50)')	AS PayType
				   , T.c.value('@_modified', 'bit')			AS Modified
				FROM @EmployXML.nodes('/*') as T(c) 
		) AS DP
		WHERE Modified=1
		 
		-- ==================================================================================
	END 
	ELSE
	BEGIN
	
		-- ========= UPDATE CONFIGURATION ==================================================	
		UPDATE Export.Configuration
		SET [ConfigName]=@ConfigName
			,[ConfigPath]=@FilePath
			, Mode =  CASE WHEN @Mode='SUMMARY' THEN 0 ELSE 1 END
			,[ModifiedOn]=GETDATE()
			,[ModifiedBy]=@UserID
		WHERE ConfigID=@ConfigID
		
		
		
	
		-- ========= UPDATE Stores ==================================================	
		MERGE Export.StoreMatch T
		USING (	
			SELECT StoreID, Selected, QBClass 
			FROM #StoreMatch 
			WHERE Selected=1
		) AS S
		ON T.StoreID=S.StoreID AND T.ConfigID=@ConfigID
		WHEN MATCHED  THEN
		UPDATE
		SET [QBAStore]=S.QBClass
			,[ModifiedOn]= GETDATE()
			,[ModifiedBy]= @UserID
		WHEN NOT MATCHED BY TARGET THEN
		INSERT([ConfigID] ,[StoreID],[QBAStore],[ModifiedOn],[ModifiedBy])
		VALUES(	@ConfigID, S.StoreID, S.QBClass,GETDATE(),@UserID)
		WHEN NOT MATCHED BY SOURCE AND T.ConfigID=@ConfigID THEN 
		DELETE;
		
		-- =============================================================================
		
		-- ========= UPDATE Pos Matches ==================================================	
		MERGE Export.PosFieldsMatch T
		USING (
			SELECT PosFieldID
			   , QBAccount
			   , Value
			   , QBVendor	
			FROM #PosMatch AS DP  INNER JOIN (
			SELECT
					 T.c.value('@Value','nvarchar(50)')		AS Value
				   , T.c.value('@ValueID','nvarchar(50)')	AS ValueID
				FROM @QBL.nodes('/*')	as T(c) ) AS DM ON DP.QBAccount=DM.ValueID

		) AS S
		ON T.PosFieldID = S.PosFieldID AND T.ConfigID=@ConfigID
		WHEN MATCHED  THEN
		UPDATE
		SET [QBAccount]=S.QBAccount
			,QBAccountName = S.Value
			,QBVendor = S.QBVendor 
			,[ModifiedOn]= GETDATE()
			,[ModifiedBy]= @UserID
		WHEN NOT MATCHED BY TARGET THEN
		INSERT([ConfigID] ,PosFieldID,[QBAccount],[QBAccountName], QBVendor,[ModifiedOn],[ModifiedBy])
		VALUES(	@ConfigID, S.PosFieldID, S.QBAccount, S.Value, S.QBVendor , GETDATE(), @UserID)
		WHEN NOT MATCHED BY SOURCE AND T.ConfigID=@ConfigID THEN 
		DELETE;
		
	
		-- =============================================================================
		
		-- ========= UPDATE EMPLOYEES ==================================================	
		
		MERGE [Export].[EmployeeMatch] T
		USING (	
			SELECT EmployeeID, QBEmployee, PayType
			FROM
			(
				SELECT
					 T.c.value('@_EmployeeID','bigint')		AS EmployeeID
				   , T.c.value('@AC_QBEmployee','nvarchar(50)')	AS QBEmployee
				   , T.c.value('@AC_QBPayType','nvarchar(50)')	AS PayType
				   , T.c.value('@_modified', 'bit')			AS Modified
				FROM @EmployXML.nodes('/*') as T(c) 
			)AS MS
			WHERE QBEmployee IS NOT NULL
		) AS S
		ON T.EmployeeID=S.EmployeeID AND T.ConfigID=@ConfigID
		WHEN MATCHED  THEN
		UPDATE
		SET  [QBEmployee]=S.QBEmployee
			,[QBPayType]=PayType
			,[ModifiedOn]= GETDATE()
			,[ModifiedBy]= @UserID
		WHEN NOT MATCHED BY TARGET THEN
		INSERT([ConfigID] ,[EmployeeID],[QBEmployee], [QBPayType],[ModifiedOn],[ModifiedBy])
		VALUES(	@ConfigID, S.[EmployeeID], S.QBEmployee, PayType, GETDATE(), @UserID )
		WHEN NOT MATCHED BY SOURCE AND T.ConfigID=@ConfigID THEN 
		DELETE;
		
	END	
	
	IF (@QBVendorMatch IS NOT NULL )
	BEGIN
		
		UPDATE Export.PosFieldsMatch
		SET QBVendor = @QBVendorMatch
		FROM Export.PosFieldsMatch INNER JOIN Dict.POSFields ON POSFieldID=PosFieldsID
		WHERE ConfigID=@ConfigID and DefaultAccountNumber=2205
		
	END
	
	MERGE [Dict].[ConfigFields] AS T
	USING (	
		SELECT Value
			 , ValueID
			 , Type
			 , ShortNumber
		FROM #Diction
	) AS S
	ON T.LongAccountNumber=S.ValueID AND T.ConfigID=@ConfigID AND T.[ShortAccountNumber] = S.ShortNumber
	WHEN MATCHED THEN
	UPDATE
	SET  [Name] = Value
	WHEN NOT MATCHED BY TARGET THEN
	INSERT([Name]
		,[ShortAccountNumber]
		,[LongAccountNumber]
		,[ConfigID]
		,[ListType])
	VALUES(	Value, ShortNumber, ValueID, @ConfigID, Type )
	WHEN NOT MATCHED BY SOURCE AND T.ConfigID=@ConfigID THEN 
	DELETE;
	
	
	IF OBJECT_ID( N'tempdb.dbo.#DICTION', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #DICTION
	END
END