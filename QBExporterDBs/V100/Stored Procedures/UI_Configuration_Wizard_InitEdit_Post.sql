﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Configuration_Wizard_InitEdit_Post]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
	, @ConfigID				bigint
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	-- =========  SELECT Values  ===========================================================
	SELECT 
		  DS.StoreID AS [_StoreID]
        , CASE WHEN DM.StoreID IS NULL THEN 'false' ELSE 'true' END	 AS [AC_StoreSelected]
        , Name		 AS [AC_StoreName]
        , DE.[QBAStore]	 AS [AC_QBClass]
        ,''			 AS [_onMouseDownRow]
        ,'0'         AS _ReloadRow
        , CASE WHEN ISNULL(CM.ConfigName,'')='' OR CM.ConfigID=@ConfigID THEN '' ELSE 'Store is in configuration: '+CM.ConfigName END AS [AC_Description] 
        , CASE WHEN ISNULL(CM.ConfigName,'')='' OR CM.ConfigID=@ConfigID THEN '' ELSE 'True' END AS [_disabled]
	FROM   Data.Stores DS 
		INNER JOIN Data.UsersPermissions   AS DU WITH(nolock) ON DS.StoreID = DU.StoreID
		LEFT OUTER JOIN  Export.StoreMatch AS DM WITH(nolock) ON DM.StoreID = DS.StoreID  AND DM.ConfigID=@ConfigID
		LEFT OUTER JOIN  Export.StoreMatch AS DE WITH (NOLOCK) ON DE.StoreID = DS.StoreID 
		LEFT OUTER JOIN  Export.Configuration AS CM WITH(nolock) ON DE.ConfigID=CM.ConfigID
	WHERE DU.UserID=@UserID AND Allow=1
	FOR XML RAW( 'Store' )
	
	SELECT
			[Name]		  AS [AC_POSField]
		  ,	[PosFieldsID] AS _POSFieldID
		  ,	 QBAccount	  AS AC_QBAccount
		  , ''			  AS _onMouseDownRow
		  , ''			  AS _onMouseDownColumn
	FROM [Dict].[POSFields] AS PF
		LEFT OUTER JOIN [Export].[PosFieldsMatch] AS PSM  WITH (NOLOCK)  ON PSM.[PosFieldID] = PF.PosFieldsID
	WHERE PSM.ConfigID=@ConfigID
	ORDER BY OrderNumber
	FOR XML RAW('POSMatches')
	
	SELECT   DE.[EmployeeID]	AS _EmployeeID
		   , DS.[StoreID]		AS _StoreID
		   , DS.Name			AS AC_StoreName
		   , DE.[Name]			AS AC_Employee
		   , QBEmployee			AS AC_QBEmployee
		   , QBPayType			AS AC_QBPayType
	FROM  Data.Employee DE
		LEFT JOIN [Export].[EmployeeMatch] EM WITH (NOLOCK) ON EM.EmployeeID = DE.EmployeeID AND EM.ConfigID=@ConfigID
		LEFT JOIN Data.Stores DS WITH (NOLOCK) ON DE.StoreID = DS.StoreID
		INNER JOIN  Export.StoreMatch ESM WITH (NOLOCK) ON ESM.StoreID = DS.StoreID AND EM.ConfigID=@ConfigID
		INNER JOIN Data.UsersPermissions   AS DU WITH(nolock) ON DS.StoreID = DU.StoreID
	WHERE  DU.UserID=@UserID AND Allow=1
	FOR XML RAW( 'Employees' )
	
	-- ==================================================================================
	
	-- =========  SELECT Dictionaries  ==================================================
	SELECT	  Name AS [Value]
			, LongAccountNumber	AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK) 
	WHERE ConfigID=@ConfigID AND ListType='PayType'
	FOR XML RAW ('QBPayType')
	
	SELECT	  Name AS [Value]
			, LongAccountNumber	AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK) 
	WHERE ConfigID=@ConfigID AND ListType='Employee'
	FOR XML RAW ('QBEmployee')
	
	SELECT	  Name AS [Value]
			, LongAccountNumber	AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK) 
	WHERE ConfigID=@ConfigID AND ListType='Store'
	FOR XML RAW ('QBClass')
	
	SELECT	  Name AS [Value]
			, LongAccountNumber	AS [ValueID]
			, ShortAccountNumber AS ShortNumber
	FROM Dict.ConfigFields WITH (NOLOCK) 
	WHERE ConfigID=@ConfigID AND ListType='Account' 
	AND (ShortAccountNumber like '11__'
		OR ShortAccountNumber like '1599'
		OR ShortAccountNumber like '22__'
		OR ShortAccountNumber like '41__'
		OR ShortAccountNumber like  '42__'
		OR ShortAccountNumber like  '43__'
		OR ShortAccountNumber like  '44__'
		OR ShortAccountNumber like  '45__'
		OR ShortAccountNumber like  '73__'
		OR ShortAccountNumber like  '74__'
		OR ShortAccountNumber like  '7800'
		OR ShortAccountNumber like  '7805' )
	FOR XML RAW ('QBAccount')
	
	--;WITH XMLNAMESPACES('http://www.ezuniverse.com/schema' AS e)
	SELECT
		'UI.ComboEditorItem'AS [class]
		, 10				AS [e:active]
		, ISNULL(ValueID,'')	AS [displaytext]
		, ISNULL(Value,'')	AS [datavalue]
	FROM (
		SELECT
			  ''		AS ValueID
			, ''		AS Value
		UNION
		SELECT	  Name	AS [Value]
				, LongAccountNumber	AS [ValueID]
		FROM Dict.ConfigFields WITH (NOLOCK) 
		WHERE ConfigID=@ConfigID AND ListType='Vendor'
	) AS TMP 
	ORDER BY Value ASC
	FOR XML RAW( 'Item' )
	
	
	SELECT	ISNULL([QBVendor],'') AS [Value]
	FROM [Dict].[POSFields] AS PF
		LEFT OUTER JOIN [Export].[PosFieldsMatch] AS PSM  WITH (NOLOCK)  
		ON PSM.[PosFieldID] = PF.PosFieldsID
	WHERE PSM.ConfigID=@ConfigID AND DefaultAccountNumber=2205

	-- ==================================================================================
	
END