﻿

/* 2013  ezUniverseLLC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[UI_TraceActivity_Add]
(
	  @SessionID		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @Description		varchar(200)
	, @Type				varchar(50) = ''
)
AS
BEGIN
	INSERT INTO TraceActivity (
		  [SessionID]
		, [Description]
		, [Type]
	)
	VALUES (
		 @SessionID
		, @Description
		, @Type
	)
END