﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[GetSecureQuest]
(
	  @Login	varchar(100)
	, @Message	varchar(500) OUTPUT
	, @SecureQuest nvarchar(MAX) OUTPUT

)
AS
BEGIN

	SET NOCOUNT ON
	
	-- =========  DECLARATIONS  =========================================================
	DECLARE
		   @LoginID		uniqueidentifier
		, @Email		varchar(200)
		, @UserName		varchar(50)
		, @Prefix		varchar(50)
		, @Password		varchar(50)
		, @Position		int
		, @Trial		bit
	-- ==================================================================================

	
	IF LEFT( @Login, 1 ) IN ( '@', '!', '#' )
	BEGIN
		SET @Login = SUBSTRING( @Login, 2, LEN( @Login ) - 1 )
	END
	
	
	
	SET @Message = 'Wrong user !!!'
	SET @Trial = 0
	
	IF( ISNULL( (
		SELECT COUNT(1)
		FROM Logins WITH(NOLOCK)
		WHERE Active = 1
			AND IsDeleted = 0
			AND Username = @Login
	), 0 ) = 1 )
	BEGIN
	
		SELECT
			@Password = Password
			, @Email = UserEmail
			, @LoginID = RecordID
			, @UserName = ShortName
			, @SecureQuest = SecureQuest
		FROM Logins WITH(NOLOCK)
		WHERE Active = 1
			AND IsDeleted = 0
			AND Username = @Login
			
	END
	ELSE 
	BEGIN
	 SET @Message = 'Wrong user !!!'
	END
	
END