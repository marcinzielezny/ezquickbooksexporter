﻿-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[AddTraceActivity]
(
	@SessionID uniqueidentifier,
	@Description varchar(200),
	@Type varchar(50) = ''
)
 AS
BEGIN
	INSERT INTO TraceActivity (
		  [SessionID]
		, [Description]
		, [Type]
	)
	VALUES (
		 @SessionID
		, @Description
		, @Type
	)
END