﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: Copy from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[Logout]
(
	@SessionID uniqueidentifier
)
AS
BEGIN

	SET NOCOUNT ON
	
	UPDATE Sessions
	SET	Status = 'Closed'
		, LastModifiedOn = GETDATE()
		, LastAction = GETDATE()
	WHERE RecordID = @SessionID

	EXEC V100.AddTraceActivity @SessionID, 'Leave System'

	DELETE FROM dbo.FiltersMaster
	WHERE CreatedBy = @SessionID
	

END