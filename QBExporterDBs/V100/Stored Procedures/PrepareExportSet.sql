﻿

/* 2013  ezUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 13/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[PrepareExportSet]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @StoreDay			nvarchar(max)
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID bigint
			,@StoreD	XML
	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @StoreD = CAST( @StoreDay AS XML)
	-- ===================================================================================
	
	SELECT	StoreID
		  , BussinessDate
	INTO #TempStoreDay
	FROM ( 	
		SELECT
			 T.c.value('@_StoreID','bigint')		AS StoreID
		   , T.c.value('@EX_BussinessDate','date')	AS BussinessDate
		   , T.c.value('@EX_CheckStore', 'bit')		AS Selected
		FROM @StoreD.nodes('/*') as T(c) 
	) AS Data
	WHERE Data.Selected = 1

	-- ========= Return Store Data =======================================================

	SELECT  
			ABS(DDD.Value) AS Amount
		  , 'Subway Export'		  AS Description
		  , PFM.QBAccount AS AccountListID
		  , PFM.QBAccountName AS AccountFullName
		  , CASE WHEN DDD.Value < 0 THEN 'Credit'
				ELSE 'Debit' END AS AccountType
		  , CAST(BusinessDate AS Datetime)  AS BusinessDate
		  , TSD.StoreID
	FROM #TempStoreDay TSD
	 INNER JOIN [Data].[DetailsData] DDD WITH (NOLOCK) ON TSD.StoreID=DDD.StoreID AND TSD.BussinessDate=DDD.BusinessDate AND EmployeeID IS NULL
	 INNER JOIN [Export].[StoreMatch] SM WITH (NOLOCK) ON DDD.StoreID=SM.StoreID
	 INNER JOIN  [Export].[PosFieldsMatch] PFM WITH (NOLOCK) ON DDD.PosField=PFM.PosFieldID AND PFM.ConfigID=SM.ConfigID
	FOR XML RAW('T')

	print('one')
	
	
	
	SELECT 
	     ''		 AS blank 
	   , [QBEmployee] AS QBEmployee
	   , ''			 AS blank1
       , [QBPayType]  AS PayRoll
       , DATEADD(hh, DDD.[HoursWorked], CAST(TSD.BussinessDate AS Datetime)) AS HrsWrkd
       , TSD.BussinessDate	
	FROM #TempStoreDay TSD
	 INNER JOIN [Data].[DetailsData] DDD WITH (NOLOCK)  ON TSD.StoreID=DDD.StoreID AND TSD.BussinessDate=DDD.BusinessDate
	 INNER JOIN [Export].[StoreMatch] SM WITH (NOLOCK)  ON DDD.StoreID=SM.StoreID
	 INNER JOIN  [Export].[EmployeeMatch] EM WITH (NOLOCK)  ON DDD.EmployeeID = EM.EmployeeID
	FOR XML RAW('Employee')
	
	print('one')
	
	
	
	-- ===================================================================================
	
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDay', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDay
	END
	
END