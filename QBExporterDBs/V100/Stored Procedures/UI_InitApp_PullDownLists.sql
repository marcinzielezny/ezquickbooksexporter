﻿/* 2013  ezUniverseLLC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 06/05/2013
-- Description: Moved from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[UI_InitApp_PullDownLists]
(
	@SessionID			uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @Section			varchar(50)
	, @AddParameter1	varchar(50) = ''
)
AS
BEGIN
	
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE 
		@UserID		bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	-- ==================================================================================

	-- =========  TITLES  ===============================================================
	IF ( (@Section = 'ALL' ) OR ( @Section = 'TITLES' ) )
	BEGIN
		SELECT 
			'Employee'		AS [ValueID]
			, 'Employee'	AS [Value]
		UNION ALL
		SELECT
			'Manager'
			, 'Manager'
		UNION ALL
		SELECT
			'Owner'
			, 'Owner'
		UNION ALL
		SELECT
			'Main Contact'
			, 'Main Contact'
		FOR XML RAW('Title')
	END
	-- ==================================================================================

	
END