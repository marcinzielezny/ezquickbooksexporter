﻿
-- =============================================
-- Author:		MZ
-- Create date: 06/05/2013
-- Description:	Moved from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[UI_InitApp_ColumnsConfig]
(
	@SessionID		uniqueidentifier	
	, @OrganizationID uniqueidentifier
	, @Section		varchar(500) = 'ALL'
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID		bigint
	-- ==================================================================================

	IF ( @Section = 'ALL.COLUMNS' )
	BEGIN
		WITH XMLNAMESPACES('http://www.ezuniverse.com/schema' AS e)
		SELECT
			[name]								AS '@name'
			, headertext						AS '@headertext'
			, alignment							AS '@alignment'
			, [type]							AS '@type'
			, format							AS '@format'
			, formatENG_BR						AS '@formatENG_BR'
			, formatPL							AS '@formatPL'
			, formatGER							AS '@formatGER'
			, maskinput							AS '@maskinput'
			, maskinputENG_BR					AS '@maskinputENG_BR'
			, maskinputPL						AS '@maskinputPL'
			, maskinputGER						AS '@maskinputGER'
			, width								AS '@width'
			, cellactivation					AS '@cellActivation'
			, cellclickaction					AS '@cellClickAction'
			, formula							AS '@formula'
			, visible							AS '@visible'
			, _valueeditclass					AS '@_ValueEditClass'
			, _valuelist						AS '@_ValueList'
			, '10'								AS '@e:active'
			, 0									AS '@delete'
			, 'UI_InitApp_ColumnsConfig'		AS '@Source'
			, CAST((SummarySettings) AS xml)
			, CAST((ExportedSettings) AS xml)
		FROM dbo.ColumnsConfigs WITH(NOLOCK)
		FOR XML PATH ('Column'), ELEMENTS XSINIL
	END
END