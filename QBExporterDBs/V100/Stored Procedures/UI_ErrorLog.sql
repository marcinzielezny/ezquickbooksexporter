﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 09/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_ErrorLog]
(
	  @SessionID 			uniqueidentifier
	, @ErrorMessage			nvarchar(max)
	, @ErrorFile			nvarchar(500)
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
		, @Source				varchar(50)
		, @DateFrom 			datetime
		, @DateTo				datetime
		, @FirstRow				int
		, @LastRow				int
		, @XmlData				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'UI_ErrorLog'

	INSERT INTO [dbo].[ErrorLog]
	 (
	   [ErrorProcedure]
      ,[ErrorMessage]
      ,[QBFilePath]
      ,[CreatedOn]
      ,[CreatedBy]
	  ,[SessionID]
     )
     VALUES 
     (  @Source
	  , @ErrorMessage
	  , @ErrorFile
	  , GETDATE()
	  , @UserID
	  , @SessionID
	 )
	-- ==================================================================================
END