﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Configuration_Wizard_InitEdit]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
	, @ConfigID				bigint
	, @ConfigName			nvarchar(50)  OUTPUT
	, @ConfigFile			nvarchar(255) OUTPUT
	, @StoresType			nvarchar(50)  OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @StoresType='NONE'
	
	-- =========  SET Config  ===========================================================

	SELECT @ConfigName = [ConfigName]
		,  @ConfigFile = [ConfigPath]
	FROM [Export].[Configuration] WITH (NOLOCK) 
	
	-- ==================================================================================
	
	-- =========  SELECT Data ===========================================================
	SELECT 
		  DS.StoreID AS [_StoreID]
        , 'true'	 AS [AC_StoreSelected]
        , Name		 AS [AC_StoreName]
        , DE.[QBAStore]	 AS [AC_QBClass]
        ,''			 AS [_onMouseDownRow]
        ,'0'         AS _ReloadRow
	FROM Export.StoreMatch DE WITH (NOLOCK) 
		INNER JOIN Data.Stores DS WITH (NOLOCK) ON DE.StoreID = DS.StoreID
	FOR XML RAW( 'Store' )
	
	SELECT
			[Name]		  AS [AC_POSField]
		  ,	[PosFieldsID] AS _POSFieldID
		  ,	 QBAccount	  AS AC_QBAccount
		  , ''			  AS _onMouseDownRow
		  , ''			  AS _onMouseDownColumn
	FROM [Export].[PosFieldsMatch] AS PSM WITH (NOLOCK) 
		  INNER JOIN  [Dict].[POSFields] PF WITH (NOLOCK) ON PSM.[PosFieldID] = PF.PosFieldsID
	WHERE SummarySet=0
	FOR XML RAW('POSMatches')
	
	SELECT   EM.[EmployeeID]	AS _EmployeeID
		   , DS.[StoreID]	AS _StoreID
		   , DS.Name		AS AC_StoreName
		   , DE.[Name]		AS AC_Employee
		   , QBEmployee		AS AC_QBEmployee
		   , QBPayType			AS AC_QBPayType
	FROM [Export].[EmployeeMatch] EM WITH (NOLOCK) 
		INNER JOIN Data.Employee DE WITH (NOLOCK) ON EM.EmployeeID = DE.EmployeeID
		INNER JOIN Data.Stores DS WITH (NOLOCK) ON DE.StoreID = DS.StoreID
	FOR XML RAW( 'Employees' )
		
	-- ==================================================================================
	
END