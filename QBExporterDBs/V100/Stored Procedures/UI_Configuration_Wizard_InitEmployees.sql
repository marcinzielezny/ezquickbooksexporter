﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 10/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Configuration_Wizard_InitEmployees]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @Stores			nvarchar(MAX)
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE 
	    @UserID	bigint
	    , @StoresXML XML
	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @StoresXML=CAST (@Stores AS XML)

	
	-- =========  Employees  ===============================================================
	
	SELECT   [EmployeeID]	AS _EmployeeID
		   , ST.[StoreID]	AS _StoreID
		   , StoreName		AS AC_StoreName
		   , [Name]			AS AC_Employee
		   , ''				AS AC_QBEmployee
		   , ''				AS AC_QBPayType
	FROM Data.Employee DE WITH (NOLOCK) 
	INNER JOIN (
		SELECT StoreID
			  , Selected
			  , StoreName
		FROM (
				SELECT
					 T.c.value('@_StoreID','bigint')			AS StoreID
					,T.c.value('@AC_StoreName','nvarchar(50)')	AS StoreName
					,T.c.value('@AC_StoreSelected','bit')	AS Selected
				FROM @StoresXML.nodes('/Data/*') as T(c) )	AS DA
		WHERE Selected = 1
	) AS ST ON ST.StoreID=DE.StoreID
	FOR XML RAW( 'Store' )
	
	-- ==================================================================================
	
	
END