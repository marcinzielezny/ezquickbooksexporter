﻿
/* 2013  ezUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 13/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[PrepareExportSet_v2]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @StoreDay			nvarchar(max)
	, @Message			nvarchar(255) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID bigint
			,@StoreD	XML
	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @StoreD = CAST( @StoreDay AS XML)
	-- ===================================================================================
	
	SELECT	ESM.StoreID
		  , BussinessDate
		  , EC.ConfigID
		  , IsZero
	INTO #TempStoreDay
	FROM ( 	
		SELECT
			 T.c.value('@_StoreID','bigint')		AS StoreID
		   , T.c.value('@EX_BussinessDate','date')	AS BussinessDate
		   , T.c.value('@EX_CheckStore', 'bit')		AS Selected
		   , T.c.value('@_IsZero', 'money')			AS IsZero
		FROM @StoreD.nodes('/*') as T(c) 
	) AS Data 
		  INNER JOIN Export.StoreMatch ESM WITH(NOLOCK) ON Data.StoreID = ESM.StoreID
		  INNER JOIN Export.Configuration EC WITH(NOLOCK) ON ESM.ConfigID = EC.ConfigID 
	WHERE Data.Selected = 1
	
	
	IF (EXISTS (SELECT TOP 1 1 FROM #TempStoreDay WHERE IsZero<>0))
	BEGIN
		SET @Message = 'Data can''t be exported one of selected stores has incorrect values of Debit/Credit'
		RETURN 
	END
	
	;MERGE [Export].[Log] AS DDD
	USING (
		SELECT BussinessDate, StoreID
		FROM (
  		SELECT	DISTINCT StoreID
			  , BussinessDate
		FROM #TempStoreDay
		) AS Data
	) AS T 
	ON DDD.StoreID = T.StoreID
		AND DDD.[BusinessDate] = BussinessDate
	WHEN MATCHED THEN
		UPDATE
		SET  [ExportedOn] = GETDATE()
			,[ExportedBy] = @UserID
			,[Status]	  = 'In Progress'
			,[ExportNumber] = [ExportNumber]+1
	WHEN NOT MATCHED THEN 
	INSERT ( [StoreID], [BusinessDate], [ExportedOn], [ExportedBy], [Status])
	VALUES ( T.StoreID, BussinessDate, GETDATE(), @UserID, 'In Progress');

	-- ========= Return Store Data =======================================================

	SELECT   EC.ConfigName 
		   , EC.ConfigPath
		   , EC.ConfigID
		   ,CAST((
				SELECT  
						ABS(DDD.Value) AS Amount
					  , Description	  AS Description
					  , PFM.QBAccount AS AccountListID
					  , PFM.QBAccountName AS AccountFullName
					  , CASE WHEN DDD.Value < 0 THEN 'Credit'
							ELSE 'Debit' END AS AccountType
					  , CAST(DDD.BusinessDate AS Datetime)  AS BusinessDate
					  , SM.[QBAStore]   AS QBStoreListID
					  , RIGHT('000000000000'+CAST(EL.[ExportLogID] AS varchar(10))+'/'+CAST(EL.ExportNumber as varchar(10)),11) AS ExportID
					  , ISNULL(PFM.QBVendor,'') AS QBVendor 
					  , TSD.StoreID
					  , DDD.DataID
					  , SM.ConfigID
				FROM #TempStoreDay TSD
				 INNER JOIN [Data].[DetailsData] DDD WITH (NOLOCK) ON TSD.StoreID=DDD.StoreID AND TSD.BussinessDate=DDD.BusinessDate AND EmployeeID IS NULL
				 INNER JOIN [Export].[StoreMatch] SM WITH (NOLOCK) ON DDD.StoreID=SM.StoreID
				 INNER JOIN  [Export].[PosFieldsMatch] PFM WITH (NOLOCK) ON DDD.PosField=PFM.PosFieldID AND PFM.ConfigID=SM.ConfigID
				 INNER JOIN [Export].[Log] EL WITH (NOLOCK) ON TSD.StoreID=EL.StoreID AND TSD.BussinessDate=EL.BusinessDate
				 WHERE  SM.ConfigID=C.ConfigID
				FOR XML RAW('Stores')
			) AS XML )
		   , CAST((	SELECT 
					''		 AS blank 
				   , [QBEmployee] AS QBEmployee
				   , ''			 AS blank1
				   , [QBPayType]  AS PayRoll
				   , CAST(TSD.BussinessDate AS varchar)+' '+[HoursWorked] AS HrsWrkd
				   , TSD.BussinessDate
				   , RIGHT('000000000000'+CAST(EL.[ExportLogID] AS varchar(10))+'/'+CAST(EL.ExportNumber as varchar(10)),11) AS ExportID
				   , DDD.DataID	
				   , SM.ConfigID
				   , DDD.EmployeeID
				   , DE.Name as [EmployeeName]
				FROM #TempStoreDay TSD
				 INNER JOIN [Data].[DetailsData] DDD WITH (NOLOCK) ON TSD.StoreID=DDD.StoreID AND TSD.BussinessDate=DDD.BusinessDate
				 INNER JOIN [Export].[StoreMatch] SM WITH (NOLOCK) ON DDD.StoreID=SM.StoreID
				 INNER JOIN [Export].[EmployeeMatch] EM WITH (NOLOCK) ON DDD.EmployeeID = EM.EmployeeID
				 INNER JOIN Data.Employee DE WITH (NOLOCK) ON DDD.EmployeeID = DE.EmployeeID
				 INNER JOIN [Export].[Log] EL WITH (NOLOCK) ON TSD.StoreID=EL.StoreID AND TSD.BussinessDate=EL.BusinessDate
				 WHERE  SM.ConfigID=C.ConfigID
				FOR XML RAW('Employee')) AS XML)
	FROM  Export.Configuration AS EC 
	INNER JOIN (SELECT DISTINCT ConfigID 
				FROM #TempStoreDay) AS C ON EC.ConfigID = C.ConfigID
	FOR XML RAW('Data')
	
	-- ===================================================================================

	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDay', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDay
	END
	
END