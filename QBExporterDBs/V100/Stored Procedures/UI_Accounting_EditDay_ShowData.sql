﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Accounting_EditDay_ShowData]
(
	  @SessionID 		uniqueidentifier
	, @Rows				int = 50
	, @BussinessDay		nvarchar(50)
	, @StoreID			bigint
	, @PageNumber		int = 1 OUTPUT
	, @KeyID			uniqueidentifier = NULL OUTPUT
	, @TotalPages		int = 1 OUTPUT
	, @TotalRows		int = 1 OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
		, @Source				varchar(50)
		, @DateFrom 			datetime
		, @DateTo				datetime
		, @FirstRow				int
		, @LastRow				int
		, @XmlData				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'UI_Accounting_InitData_ExportSummary'

	IF ( ISNULL( @Rows, 0 ) = 0 )
	BEGIN
		SET @Rows = 50
	END
	
	SET @PageNumber = ISNULL( @PageNumber, 1 )
	-- ==================================================================================

	-- =========  SET FILTERS  ===========================================================
	
	SET @DateFrom = CAST(@BussinessDay AS Datetime)
	SET @DateTo = DATEADD(day,1, CAST(@BussinessDay AS Datetime))
	-- ==================================================================================

	-- =========  CREATE TEMP TABLES  ===================================================
	SELECT BussinessDate, _StoreID, StoreName, IsDetails
	INTO #TempStoreDate
	FROM (SELECT @BussinessDay AS BussinessDate, StoreID AS _StoreID, Name AS StoreName, Details AS IsDetails 
		  FROM Data.Stores S
		  WHERE StoreID=@StoreID ) AS Data
	-- ==================================================================================
	-- =========  COUNT FIRST AND LAST ROW  =============================================
	IF ( @PageNumber > @TotalPages )
	BEGIN
		SET @PageNumber = @TotalPages
	END

	SET	@FirstRow = ( @PageNumber - 1 ) * @Rows + 1
	SET @LastRow = @PageNumber * @Rows
	-- ==================================================================================

	-- =========  RESULTSET  ============================================================
	SELECT 'EDIT STORE PER DAY '
	-- ==================================================================================


	SELECT  _StoreID
			, [StoreName]			AS ED_StoreName
			, [BussinessDate]		AS ED_BussinessDate
			, [POS Field Name]		AS ED_POSFieldName
			,  [_PosFieldID]		
			, [Finnancial Account]	AS ED_FinnancialAccount
			, Debit					AS ED_Debit
			, Credit				AS ED_Credit
			, ISNULL(Description,[Pos Field Name])  AS Description
	FROM(
		SELECT 
			 _StoreID
			, [StoreName]				
			, [BussinessDate]			
			, My.Name AS [POS Field Name]			
			, [QBAccountName] AS [Finnancial Account]
			, My.PosFieldsID AS [_PosFieldID]
			, CASE WHEN DD.Value>0 THEN  DD.Value ELSE 0.0 END			AS Debit
			, CASE WHEN DD.Value<0 THEN  (-1)*DD.Value ELSE 0.0 END		AS Credit
			, 0		AS [Hours Worked]					
			, ROW_NUMBER() OVER( ORDER BY [BussinessDate] DESC, [StoreName], OrderNumber ) AS [RowNumber1]
			, Description
			, OrderNumber 
		FROM ( SELECT  _StoreID
					, [StoreName]
					, [BussinessDate]
				
			FROM   #TempStoreDate TSD 
			  ) AS DP
			INNER JOIN [Export].StoreMatch ESM WITH (NOLOCK) ON DP._StoreID=ESM.StoreID
			INNER JOIN [Export].[PosFieldsMatch] PFM WITH (NOLOCK) ON PFM.ConfigID = ESM.ConfigID  
			INNER JOIN [Dict].[POSFields] AS My WITH (NOLOCK) ON PFM.[PosFieldID]=My.POSFieldsID
			LEFT OUTER JOIN Data.DetailsData DD WITH (NOLOCK) ON DP._StoreID = DD.StoreID  AND DP.BussinessDate = DD.BusinessDate AND PFM.[PosFieldID]=DD.PosField
	) AS My
	-- WHERE RowNumber1 BETWEEN @FirstRow AND @LastRow
	ORDER BY  OrderNumber 
	FOR XML RAW('T')
	-- ==================================================================================
	
	SELECT	DISTINCT  _StoreID				
		    , [StoreName]				AS ED_StoreName
			, [BussinessDate]			AS ED_BussinessDate
			, DE.Name					AS ED_EmployeeName
			, DE.EmployeeID				AS _EmployeeID
			, ISNULL([HoursWorked],'00:00:00')	AS ED_HrsWorked
	FROM   #TempStoreDate TSD 
		LEFT OUTER JOIN Data.Employee DE WITH (NOLOCK) ON DE.StoreID = TSD._StoreID
		INNER JOIN Export.EmployeeMatch EE WITH (NOLOCK) ON DE.EmployeeID = EE.EmployeeID 
		LEFT OUTER JOIN Data.DetailsData DD WITH (NOLOCK) ON  TSD._StoreID = DD.StoreID  AND TSD.BussinessDate = DD.BusinessDate  AND DE.EmployeeID= DD.EmployeeID
	ORDER BY  [BussinessDate] DESC	
	FOR XML RAW('Employee')
	
	-- ==================================================================================

	-- =========  REMOVE TEMP TABLE  ====================================================
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDate', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDate
	END

	-- ==================================================================================
END