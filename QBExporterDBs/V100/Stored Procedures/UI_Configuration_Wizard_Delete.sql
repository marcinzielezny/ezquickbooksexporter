﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 22/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Configuration_Wizard_Delete]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
	, @ConfigID				bigint
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	-- ========= Delete Data  ===========================================================
	-- Archiving set on Trigger 
	-- ==================================================================================
	
	DELETE FROM [Export].[Configuration] WHERE ConfigID = @ConfigID
	
	DELETE FROM [Export].[StoreMatch]	WHERE ConfigID = @ConfigID
	
	DELETE FROM [Export].[EmployeeMatch] WHERE ConfigID = @ConfigID
	
	DELETE FROM [Export].[PosFieldsMatch] WHERE ConfigID = @ConfigID 
	
	DELETE FROM [Dict].[ConfigFields]	WHERE ConfigID=@ConfigID
	
	-- ===================================================================================

END