﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 17/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_HistoricExport_Details]
(
	  @SessionID 		uniqueidentifier
	, @Rows				int = 50
	, @BussinessDay		nvarchar(50)
	, @StoreID			bigint
	, @PageNumber		int = 1 OUTPUT
	, @KeyID			uniqueidentifier = NULL OUTPUT
	, @TotalPages		int = 1 OUTPUT
	, @TotalRows		int = 1 OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
		, @Source				varchar(50)
		, @DateFrom 			datetime
		, @DateTo				datetime
		, @FirstRow				int
		, @LastRow				int
		, @XmlData				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'UI_Accounting_InitData_ExportDetails'

	IF ( ISNULL( @Rows, 0 ) = 0 )
	BEGIN
		SET @Rows = 50
	END
	
	SET @PageNumber = ISNULL( @PageNumber, 1 )
	-- ==================================================================================
 
	-- =========  SET FILTERS  ===========================================================

	SET @DateFrom =CAST(@BussinessDay AS Datetime)
	SET @DateTo = DATEADD(day,1, @DateFrom)
	-- ==================================================================================
		
	-- =========  CREATE TEMP TABLES  ===================================================
			
	CREATE TABLE #TempStoreDetails
	 (	  ArchiveID			bigint
		, _StoreID			bigint
		, [StoreName]		nvarchar(50)
		, [BussinessDate]	nvarchar(50)
		, [POS Field Name]	nvarchar(100)
		, [Finnancial Account] nvarchar(100)
		, [_PosFieldID]     bigint
		, Debit				money
		, Credit			money
		, [Hours Worked]	nvarchar(50)
		, [Destination]		nvarchar(100)
		, [Operation]		nvarchar(100)
		, Colors			nvarchar(50)
		, [LastMdfOn]		datetime
		, [LastMdfBy]		bigint
		, [ExportedOn]		datetime
		, [ExportedBy]		bigint
		, [ExportStatus]	nvarchar(50)
		, RN				int
	 ) 
	-- ==================================================================================

	;WITH DateRange AS
	(
		SELECT CAST( @DateFrom AS date ) AS Val
		UNION ALL
		SELECT DATEADD( DAY, 1, Val )
		FROM DateRange
		WHERE Val < CAST( @DateTo AS date )
	)
	SELECT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , [Destination]
		    , [CreatedOn] 
		    , [ExportedOn]
		    , [RowNumber]
			,  CASE WHEN (RowNumber % 2)=1 THEN
			   'AliceBlue'
			  ELSE 
			   'Honeydew'
			  END AS Colors
	INTO #TempStoreDate
	FROM (
	SELECT 
		 Data.BussinessDate
		, _StoreID
		, StoreName
		, IsDetails
		, Destination
		, CreatedOn
		, ExportedOn
		, ROW_NUMBER() OVER( ORDER BY Data.[BussinessDate] DESC, StoreName, AD.CreatedOn, AD.ExportedOn  ) AS [RowNumber]
	FROM (
	SELECT  @BussinessDay AS [BussinessDate]
		, StoreID AS _StoreID
		, Name AS [StoreName]
		, '' AS [IsDetails]
		, '' AS [Destination]
	FROM Data.Stores WHERE StoreID=@StoreID
		) AS Data INNER JOIN (
					SELECT DISTINCT  
						   StoreID 
						 , [BusinessDate]
						 , dateadd(mi, datediff(mi, 0, CreatedOn), 0) AS CreatedOn
						 , ExportedOn 
						 , Operation 
						 FROM [Archive].[DetailsData]) AS AD ON Data._StoreID=AD.StoreId AND Data.[BussinessDate]=AD.[BusinessDate] AND Operation='Export'
	) AS Data
	
	-- =========  RESULTSET  ============================================================
	SELECT 'EXPORT ARCHIVE FROM ' + CONVERT( varchar(100 ), @DateFrom, 101 ) + ' TO ' + CONVERT( varchar(100 ), @DateTo, 101 )

	
	INSERT INTO #TempStoreDetails
	 (	  ArchiveID
		,_StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [Finnancial Account] 
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]	
		, [Operation]	
		, Colors
		, [LastMdfOn]
		, [LastMdfBy]
		, [ExportedOn]
		, [ExportedBy]
		, [ExportStatus]
		, [RN]		
	 ) 
	SELECT 
			DISTINCT ArchiveDataID
			, _StoreID
			, [StoreName]
			, [BussinessDate]
			, PF.Name [POS Field Name]
			, [ExportAccountName] AS [Finnancial Account]
			, PF.PosFieldsID AS [_PosFieldID]
			, CASE WHEN DD.Value>0 THEN  DD.Value ELSE 0.0 END AS Debit
			, CASE WHEN DD.Value<0 THEN  (-1)*DD.Value ELSE 0.0 END AS Credit
			, [HoursWorked] AS [Hours Worked]
			, ExportedTo AS [Destination]
			, [Operation]
			, Colors
			, DD.[CreatedOn] AS [LastMdf]
			, DD.[CreatedBy] AS [LastMdfBy]
			, DD.[ExportedOn]
			, [ExportedBy]
			, [ExportStatus]
			, RN
	 FROM (
			SELECT 	
					_StoreID
					, [StoreName]
					, [BussinessDate]
					, Destination
					, Colors
					, CreatedOn
					, ExportedOn
					, RowNumber AS RN
			FROM   #TempStoreDate TSD 
			  ) AS DP
	LEFT OUTER JOIN Archive.DetailsData DD ON DP._StoreID = DD.StoreID  AND DP.BussinessDate = DD.BusinessDate   AND dateadd(mi, datediff(mi, 0, DD.CreatedOn), 0)= DP.CreatedOn AND ((DP.ExportedOn IS NULL AND DD.ExportedOn IS NULL) OR (DD.ExportedOn = DP.ExportedOn))
	LEFT OUTER JOIN Dict.POSFields PF ON DD.PosField=PF.PosFieldsID
	WHERE OPERATION IS NOT NULL
	ORDER BY  [BussinessDate]
	

	-- ==================================================================================

	SET @TotalRows = ( SELECT COUNT(*) FROM  #TempStoreDetails )
	SET @TotalPages = CEILING( CAST( @TotalRows AS float ) / @Rows )
	-- ==================================================================================

	-- =========  COUNT FIRST AND LAST ROW  =============================================
	IF ( @PageNumber > @TotalPages )
	BEGIN
		SET @PageNumber = @TotalPages
	END

	SET	@FirstRow = ( @PageNumber - 1 ) * @Rows + 1
	SET @LastRow = @PageNumber * @Rows
	-- ==================================================================================
		
	SELECT  _StoreID
			, [StoreName]			AS [HD_StoreName]
			, [BussinessDate]		AS [HD_BussinessDate]
			, [Operation]			
			, [POS Field Name]		AS [HD_POSFieldName]
			, [Finnancial Account]	AS [HD_FinnancialAccount]
			, Debit					AS [HD_Debit]
			, Credit				AS [HD_Credit]
			, ISNULL([Hours Worked],'')		AS [HD_HrsWorked2]
			, [Destination]			AS [HD_Destination]
			, [Exported On]			AS [HD_ExportedOn]
			, [Exported By]			AS [HD_ExportedBy]
			, [ExportStatus]		AS [HD_ExportStatus]
			, _class
			, _KeyID
			, [Last Modification On]	AS [HD_LastModificationOn]
			, [Last Modification By]	AS [HD_LastModificationBy]
			,  CASE WHEN (RowNumber1 % 2)=1 THEN 
				 CASE WHEN (Colors='Honeydew') THEN  'LightGreen' ELSE 'LightBlue' END
			   ELSE Colors END AS _style
	FROM(
	SELECT ArchiveID
	    , _StoreID			
		, [StoreName]		
		, [BussinessDate]
		, Operation	
		, [POS Field Name]	
		, [Finnancial Account] 
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]	
		, 'CM_Accounting_ExportDetails' AS  _class
		, @KeyID AS _KeyID	
		, ISNULL(CAST(DP.ExportedOn AS VARCHAR),'') AS [Exported On]
		, ISNULL(CAST(DL.Username AS VARCHAR),'') AS [Exported By]
		, DP.[ExportStatus] AS [ExportStatus]	
		, Colors
		, ISNULL(CAST([LastMdfOn] AS VARCHAR),'') AS [Last Modification On]
		, DA.Username AS [Last Modification By]
		, ROW_NUMBER() OVER( PARTITION BY [BussinessDate], _StoreID ORDER BY [BussinessDate] DESC, _StoreID) AS [RowNumber1]
		, ROW_NUMBER() OVER( ORDER BY [BussinessDate] DESC , _StoreID ) AS RowNumber
	 FROM #TempStoreDetails AS DP
			-- LEFT OUTER JOIN  Export.Log EL ON DP._StoreID = EL.StoreID  AND DP.BussinessDate = EL.BusinessDate
			LEFT OUTER JOIN dbo.Logins DL ON DL.LoginID = DP.ExportedBy
			LEFT OUTER JOIN dbo.Logins DA ON DA.LoginID = DP.[LastMdfBy]
	) AS My
	WHERE RowNumber BETWEEN @FirstRow AND @LastRow
	FOR XML RAW ('T')
	
	-- =========  REMOVE TEMP TABLE  ====================================================
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDate', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDate
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDetails', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDetails
	END

	-- ==================================================================================
END