﻿
/* 2013  ezUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 06/05/2013
-- Description: Moved from EZConnect
-- =============================================
CREATE PROCEDURE [V100].[UI_InitApp_EmptyRows]
(
	  @SessionID		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @Section			varchar(50)
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID		bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	-- ==================================================================================

	-- =========  MANAGE SALES ACTIVITY =================================================
	IF ( ( @Section = 'ALL' ) OR ( @Section = 'MANAGE.SALES.ACTIVITY' ) )
	BEGIN 
		SELECT
			''						AS [MSA_Name]
			, '1900-01-01T00:00:00'	AS [MSA_StartHour]
			, '1900-01-01T00:00:00'	AS [MSA_EndHour]
			, ''					AS [_RecordID]
			, ''					AS [_sh]
			, @OrganizationID		AS [_OrganizationID]
			, '1'					AS [_newRecord]
		FOR XML RAW('SalesActivity')
	END
	-- ==================================================================================

	-- =========  MANAGE USERS ==========================================================
	IF ( ( @Section = 'ALL' ) OR ( @Section = 'MANAGE.USERS' ) )
	BEGIN
		SELECT
			  ''									AS [MU_FullName]
			, CONVERT(varchar(36),@OrganizationID)	AS [_Organization]
			, ''									AS [MU_UserID]
			, ''									AS [MU_Password]
			, ''									AS [_DisplayAs]
			, '0'									AS [MU_UserGroup]
			, ''								    AS [MU_Email]
			, ''									AS [MU_CellNumber]
			, ''									AS [MU_Provider]
			, ''									AS [MU_SkypeName]
			, ' 1'									AS [_ResourceType]
			, ''									AS [_ValueID]
			, 'ADD'									AS [_TimeKey]
		FOR XML RAW('User')
	END
	-- ==================================================================================

	-- =========  GAUGE PROPERTY - SALES ================================================
	IF ( ( @Section = 'ALL' ) OR ( @Section = 'GAUGE.PROPERTY.SALES' ) )
	BEGIN 
		SELECT
			  '1900-01-01T00:00:00'	AS [DGS_StartHour]
			, '1900-01-01T00:00:00'	AS [DGS_EndHour]
			, '20'					AS [DGS_Level1]
			, '5000'				AS [DGS_Level2]
			, '10000'				AS [DGS_MaxValue]
			, ''					AS [_RecordID]
			, ''					AS [_GaugeID]
			, @OrganizationID		AS [_OrganizationID]
			, '1'					AS [_newRecord]
		FOR XML RAW('Sales')
	END
	-- ==================================================================================

	-- =========  GAUGE PROPERTY - LABOR ================================================
	IF ( ( @Section = 'ALL' ) OR ( @Section = 'GAUGE.PROPERTY.LABOR' ) )
	BEGIN 
		SELECT
			 '1900-01-01T00:00:00'	AS [DGL_StartHour]
			, '1900-01-01T00:00:00'	AS [DGL_EndHour]
			, '0'					AS [DGL_Level1]
			, '100'					AS [DGL_Level2]
			, ''					AS [_RecordID]
			, ''					AS [_GaugeID]
			, @OrganizationID		AS [_OrganizationID]
			, '1'					AS [_newRecord]
		FOR XML RAW('Labor')
	END
	-- ==================================================================================

END