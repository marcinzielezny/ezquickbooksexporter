﻿/* 2010  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		PM
-- Create date: 2011.02.11
-- Description:
-- =============================================
CREATE PROCEDURE [V100].[UI_User_RemaindEmail]
(
	@UserID			uniqueidentifier
	, @LoginID		uniqueidentifier
	, @UserName		varchar(50)
	, @UserLogin	varchar(50)
	, @UserPassword	varchar(50)
	, @UserEmail	varchar(250)
	, @Trial		bit
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	-- =========  DECLARATIONS  =========================================================
	DECLARE 
		@HtmlBody			nvarchar(2500)
		, @Prefix			varchar(20)
		, @MailID			int
		, @ProfileName		varchar(255)
		, @RecipientsBCC	varchar(8000)
		, @Subject			varchar(255)
	-- ==================================================================================

	-- ==================================================================================

	-- =========  PREPARE EMAIL BODY  ===================================================
	SET @HtmlBody = [Mail].[GetMailBody]( 
			N'Dear QuickBooksExporter valued customer,<br/><br/>'
			+ N'Your current Login info are:<br/><br/>'
			+ N'Login: <b>' + @UserLogin + N'</b><br/>'
			+ N'Password: <b>' + @UserPassword + N'</b><br/><br/>'
		)
	-- ==================================================================================
	
	
	-- =========  SEND EMAIL  ===========================================================
	SET @ProfileName = 'EZReports'
	SET @RecipientsBCC = ''
	SET @Subject = 'QBExporter: Password reminder'
	
	EXEC msdb.dbo.sp_send_dbmail
		@profile_name = @ProfileName
		, @recipients = @UserEmail
		, @blind_copy_recipients = @RecipientsBCC
		, @body_format = 'HTML'
		, @body = @HtmlBody
		, @subject = @Subject
		, @mailitem_id = @MailID OUTPUT
		
	INSERT INTO MailLog (
		MailID
		, RuleID
		, CreatedOn
		, SendTo
		, NotifyID
		, SendBy
	) 
	VALUES (
		@MailID
		, @LoginID
		, GETDATE()
		, @UserEmail
		, '00000000-0000-0000-0000-000000000002'
		, @UserID
	)
	-- ==================================================================================
			
END