﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: 
-- =============================================
/*

DECLARE @Email varchar(max) 
DECLARE @Mess varchar(max)

EXEC [V100].[ForgotUserAndPassword] @Email OUTPUT, @MESS OUTPUT


*/
CREATE PROCEDURE [V100].[ForgotUserAndPassword]
(
	  @Email	varchar(255) OUTPUT
	, @Message	nvarchar(max) OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE 
	    @len int = 6
	  , @min tinyint = 48
	  , @range tinyint = 74
	  , @exclude varchar(50) = '0:;<=>?@O[]`^\/'
	  , @username nvarchar(255)
    
    DECLARE 
		@char char
	  , @newpass nvarchar(max) = ''
 
    while @len > 0 begin
       select @char = char(round(rand() * @range + @min, 0))
       if charindex(@char, @exclude) = 0 begin
           set @newpass += @char
           set @len = @len - 1
       end
    end

	
	SELECT
	      @Email = UserEmail
		, @Username = Username

	FROM Logins WITH(NOLOCK)
	WHERE Active = 1
			AND IsDeleted = 0
	
	UPDATE Logins
	SET Password = @newpass

	SET @message = '<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 14">
<meta name=Originator content="Microsoft Word 14">
<link rel=File-List href="Untitled1_files/filelist.xml">
<link rel=Edit-Time-Data href="Untitled1_files/editdata.mso">
<link rel=themeData href="Untitled1_files/themedata.thmx">
<link rel=colorSchemeMapping href="Untitled1_files/colorschememapping.xml">
</head>

<body lang=EN-US link=blue vlink=purple style=''tab-interval:30.0pt''>
<div class=WordSection1>
<p class=MsoNormal style=''mso-layout-grid-align:none;text-autospace:none''><span
style=''mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:
Calibri;color:black''><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal>Dear QuickBooksExporter user<o:p></o:p></p>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal>Your password have been changed and now your credentials are
set to: <o:p></o:p></p>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><b style=''mso-bidi-font-weight:normal''>Username: '+ @username +' <o:p></o:p></b></p>
<p class=MsoNormal><b style=''mso-bidi-font-weight:normal''>Password: '+ @newpass +'<o:p></o:p></b></p>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal>Please change your credentials immediately when you log into
application. <o:p></o:p></p>
<p class=MsoNormal>For changing password click “Help” and then “Change Password”<o:p></o:p></p>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal>EZUniverse Team<o:p></o:p></p>
</div>
</body>
</html>
'


END