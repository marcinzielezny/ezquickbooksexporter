﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 07/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [V100].[UI_Accounting_InitData_ExportDetails]
(
	  @SessionID 		uniqueidentifier
	, @Rows				int = 50
	, @Period    		varchar(max) = NULL
	, @Stores    		varchar(max) = NULL
	, @PageNumber		int = 1 OUTPUT
	, @KeyID			uniqueidentifier = NULL OUTPUT
	, @TotalPages		int = 1 OUTPUT
	, @TotalRows		int = 1 OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
		, @Source				varchar(50)
		, @DateFrom 			datetime
		, @DateTo				datetime
		, @FirstRow				int
		, @LastRow				int
		, @XmlData				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'UI_Accounting_InitData_ExportDetails'

	IF ( ISNULL( @Rows, 0 ) = 0 )
	BEGIN
		SET @Rows = 50
	END
	
	SET @PageNumber = ISNULL( @PageNumber, 1 )
	-- ==================================================================================
 
	-- =========  SET FILTERS  ===========================================================
	IF ( @KeyID IS NULL )
	BEGIN
		SET @KeyID = NEWID()

		SET @XmlData = CAST( (
				ISNULL( @Period, '' )
				+ ISNULL( @Stores, '' )
			) AS xml )

	EXEC V100.Filter_ByXpath
				@SessionID		= @SessionID
				, @XmlData  	= @XmlData 
				, @XPath		= @Source
				, @Procedure	= @Source
				, @KeyID 		= @KeyID
	END

	SET @DateFrom = V100.FILTER_DateFrom( @KeyID, @SessionID )
	SET @DateTo = V100.FILTER_DateTo( @KeyID, @SessionID )
	-- ==================================================================================
	

	
	-- =========  CREATE TEMP TABLES  ===================================================
	CREATE TABLE #TempStores(
		StoreID bigint
		, Location varchar(50)
		, IsDetails bit  
	)

	EXEC V100.PrepareTempStores
			@SessionID  = @SessionID
			, @UserID  = @UserID
			, @KeyID    = @KeyID
			
			
	 CREATE TABLE #TempStoreDetails
	 (
		_StoreID			bigint
		, [StoreName]		nvarchar(50)
		, [BussinessDate]	nvarchar(50)
		, [POS Field Name]	nvarchar(100)
		, [Finnancial Account] nvarchar(100)
		, [_PosFieldID]     bigint
		, Debit				nvarchar(50)
		, Credit			nvarchar(50)
		, [Hours Worked]	nvarchar(50)
		, [Destination]		nvarchar(100)
		, Colors			nvarchar(50)
		, [LastMdfOn]		datetime
		, [LastMdfBy]		bigint
		, [Type]			nvarchar(1)
		, [OrderNumber]		int
	 ) 
	-- ==================================================================================


	-- =========  PREPATE TEMP DATA  ====================================================
	IF DATEDIFF( DAY, @DateFrom, @DateTo ) > 30
	BEGIN
		SET @DateFrom = DATEADD( DAY, -30, @DateTo )
	END 
	ELSE IF DATEDIFF( DAY, @DateFrom, @DateTo ) < 0
	BEGIN
		SET @DateFrom = DATEADD( DAY, 0, @DateTo )
	END 

	;WITH DateRange AS
	(
		SELECT CAST( @DateFrom AS date ) AS Val
		UNION ALL
		SELECT DATEADD( DAY, 1, Val )
		FROM DateRange
		WHERE Val < CAST( @DateTo AS date )
	)
	
	SELECT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , [Destination]
		    , [ExportedOn]
		    , [ExportStatus]
		    , [RowNumber]
			,  CASE WHEN (RowNumber % 2)=1 THEN
			   'AliceBlue'
			  ELSE 
			   'Honeydew'
			  END AS Colors
	INTO #TempStoreDate
	FROM		  (
	SELECT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , EC.ConfigName AS [Destination]
		    , [ExportedOn]
		    , [ExportStatus]
			, ROW_NUMBER() OVER( ORDER BY  [BussinessDate] DESC, _StoreID ) AS [RowNumber]
	FROM (
	SELECT  CONVERT( varchar(100), DR.Val, 101 ) AS [BussinessDate]
		, EX.StoreID AS _StoreID
		, EX.Location AS [StoreName]
		, EX.IsDetails AS [IsDetails]
		, '' AS [Destination]
		, '' AS [ExportedOn]
		, '' AS [ExportStatus]
		
	FROM DateRange DR
		CROSS JOIN #TempStores EX ) AS Data
	INNER JOIN Export.StoreMatch ESM WITH (NOLOCK) ON Data._StoreID = ESM.StoreID
	LEFT OUTER JOIN Export.Configuration EC WITH (NOLOCK) ON ESM.ConfigID = EC.ConfigID
	) AS MY
		

	

	-- =========  RESULTSET  ============================================================
	SELECT 'EXPORT DETAILS FROM ' + CONVERT( varchar(100 ), @DateFrom, 101 ) + ' TO ' + CONVERT( varchar(100 ), @DateTo, 101 )

	INSERT INTO #TempStoreDetails
	 (
		_StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [Finnancial Account] 
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]		
		, Colors
		, [LastMdfOn]
		, [LastMdfBy]	
		, Type
		, OrderNumber	
	 ) 
	SELECT 
			 _StoreID
			, [StoreName]
			, [BussinessDate]
			, Name AS [POS Field Name]
			, [QBAccountName] AS [Finnancial Account]
			, [PosFieldsID]
			, CASE WHEN DD.Value>0 THEN  DD.Value ELSE 0.0 END AS Debit
			, CASE WHEN DD.Value<0 THEN  (-1)*DD.Value ELSE 0.0 END AS Credit
			, '' AS [Hours Worked]
			, [Destination]
			, Colors
			, COALESCE(DD.[LastModificationOn], DD.[CreatedOn]) AS [LastMdf]
			, COALESCE(DD.[LastModificationBy], DD.[CreatedBy]) AS [LastMdfBy]
			, 'A' AS Type
			, My.OrderNumber 
	 FROM (
			SELECT 	
					_StoreID
					, [StoreName]
					, [BussinessDate]
					, Destination
					, ExportStatus
					, Colors
			FROM   #TempStoreDate TSD 
			  ) AS DP
	INNER JOIN [Export].StoreMatch ESM WITH (NOLOCK) ON DP._StoreID=ESM.StoreID
	INNER JOIN [Export].[PosFieldsMatch] PFM WITH (NOLOCK) ON PFM.ConfigID = ESM.ConfigID  
    INNER JOIN [Dict].[POSFields] My WITH (NOLOCK) ON PFM.[PosFieldID]=My.POSFieldsID
    LEFT OUTER JOIN Data.DetailsData DD WITH (NOLOCK) ON DP._StoreID = DD.StoreID  AND DP.BussinessDate = DD.BusinessDate AND PFM.[PosFieldID]=DD.PosField
	ORDER BY  [BussinessDate] DESC
	-- ==================================================================================

	INSERT INTO #TempStoreDetails
	 (
		_StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [Finnancial Account] 
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]		
		, Colors
		, [LastMdfOn]
		, [LastMdfBy]
		, Type 			
	 )		
	 SELECT DISTINCT 
			  _StoreID
		    , [StoreName]
			, [BussinessDate]
			, DE.Name
			, ''
			, DE.EmployeeID
			, ''
			, ''
			, ISNULL(HoursWorked,'00:00:00') AS [Hours Worked]
			, [Destination]
			, Colors
			, COALESCE(DD.[LastModificationOn], DD.[CreatedOn]) AS [LastMdf]
			, COALESCE(DD.[LastModificationBy], DD.[CreatedBy],'') AS [LastMdfBy] 
			, 'E' AS Type
	FROM   #TempStoreDate TSD 
	LEFT OUTER JOIN Data.Employee DE WITH (NOLOCK) ON DE.StoreID = TSD._StoreID
	INNER JOIN Export.EmployeeMatch EM WITH (NOLOCK) ON EM.EmployeeID=DE.EmployeeID
	LEFT OUTER JOIN Data.DetailsData DD WITH (NOLOCK) ON  TSD._StoreID = DD.StoreID  AND TSD.BussinessDate = DD.BusinessDate  AND DE.EmployeeID= DD.EmployeeID
	ORDER BY  [BussinessDate] DESC	
	
	SET @TotalRows = ( SELECT COUNT(*) FROM  #TempStoreDetails )
	SET @TotalPages = CEILING( CAST( @TotalRows AS float ) / @Rows )
	-- ==================================================================================

	-- =========  COUNT FIRST AND LAST ROW  =============================================
	IF ( @PageNumber > @TotalPages )
	BEGIN
		SET @PageNumber = @TotalPages
	END

	SET	@FirstRow = ( @PageNumber - 1 ) * @Rows + 1
	SET @LastRow = @PageNumber * @Rows
	-- ==================================================================================
		
	SELECT  _StoreID
			, [StoreName]			AS [EDV_StoreName]
			, [BussinessDate]		AS [EDV_BussinessDate]
			, [POS Field Name]		AS [EDV_POSFieldName]
			, [Finnancial Account]  AS [EDV_FinnancialAccount]
			, Debit					AS [EDV_Debit]
			, Credit				AS [EDV_Credit]
			, [Hours Worked]		AS [EDV_HrsWorked]
			, [Destination]			AS [EDV_Destination]
			, [Exported On]			AS [EDV_ExportedOn]
			, [Exported By]			AS [EDV_ExportedBy]
			, [ExportStatus]		AS [EDV_ExportStatus]
			, _class
			, _KeyID
			, [Last Modification On] AS [EDV_LastModificationOn]
			, [Last Modification By] AS [EDV_LastModificationBy]
			,  CASE WHEN (RowNumber1 % 2)=1 THEN 
				 CASE WHEN (Colors='Honeydew') THEN  'LightGreen' ELSE 'LightBlue' END
			   ELSE Colors END AS _style
	FROM(
	SELECT _StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [Finnancial Account] 
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]	
		, 'CM_Accounting_ExportDetails' AS  _class
		, @KeyID AS _KeyID	
		, ISNULL(CAST(EL.ExportedON AS VARCHAR),'') AS [Exported On]
		, ISNULL(CAST(DL.UserName AS VARCHAR),'') AS [Exported By]
		, EL.[Status] AS [ExportStatus]	
		, Colors
		, ISNULL(CAST([LastMdfOn] AS VARCHAR),'') AS [Last Modification On]
		, ISNULL(DA.UserName,'') AS [Last Modification By]
		, ROW_NUMBER() OVER( PARTITION BY [BussinessDate], _StoreID ORDER BY [BussinessDate] DESC, _StoreID, Type ASC, OrderNumber) AS [RowNumber1]
		, ROW_NUMBER() OVER( ORDER BY [BussinessDate] DESC , _StoreID ) AS RowNumber
	 FROM #TempStoreDetails AS DP
			LEFT OUTER JOIN  Export.Log EL WITH (NOLOCK) ON DP._StoreID = EL.StoreID  AND DP.BussinessDate = EL.BusinessDate
			LEFT OUTER JOIN dbo.Logins DL WITH (NOLOCK) ON DL.LoginID = EL.ExportedBy
			LEFT OUTER JOIN dbo.Logins DA WITH (NOLOCK) ON DA.LoginID = DP.[LastMdfBy]
	) AS My
	WHERE RowNumber BETWEEN @FirstRow AND @LastRow
	FOR XML RAW ('T')
	
	-- =========  REMOVE TEMP TABLE  ====================================================
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDate', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDate
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDetails', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDetails
	END

	IF OBJECT_ID( N'tempdb.dbo.#TempStores', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStores
	END
	-- ==================================================================================
END