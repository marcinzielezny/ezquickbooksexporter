﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/07.2013
-- Description: 
-- =============================================
CREATE FUNCTION [Mail].[GetMailBody]
(
	@MailMessage nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN

	RETURN (
		N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
		+ N'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="en">'
		+ N'<head>'
		+ N'<meta http-equiv="Content-Type" content="text/html;" />'
		+ N'<style type="text/css">'
		+ N'p.body{font-family:Arial,Helvetica,sans-serif; font-size:11pt; margin:0px}'
		+ N'p.copyright{font-family:Arial,Helvetica,sans-serif; font-size:10px; text-align:center; margin:0px}'
		+ N'hr{color:black; height:1px; margin:0px}'
		+ N'</style>'
		+ N'<title></title>'
		+ N'</head>'
		+ N'<body>'
		+ N'<p class="body">'
		+ @MailMessage
		+ N'</p>'
		+ N'<p class="body">'
		+ N'Thank you for your business,<br/><br/>'
		+ 'QuickBooksExporter' + N'<br/>'
		+ N'</p>'
		+ N'<hr/>'
		+ N'</body>'
		+ N'</html>'
	)

END