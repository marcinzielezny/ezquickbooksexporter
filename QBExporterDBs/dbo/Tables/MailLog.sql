﻿CREATE TABLE [dbo].[MailLog] (
    [MailID]     INT              NULL,
    [EventID]    UNIQUEIDENTIFIER NULL,
    [RuleID]     UNIQUEIDENTIFIER NULL,
    [NotifyID]   UNIQUEIDENTIFIER NULL,
    [SendTo]     VARCHAR (250)    NULL,
    [SendBy]     UNIQUEIDENTIFIER NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_MailLog_CreatedOn] DEFAULT (getdate()) NULL,
    [LogID]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [InsertedOn] DATETIME         DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_MailLog] PRIMARY KEY CLUSTERED ([LogID] ASC)
);

