﻿CREATE TABLE [dbo].[ErrorLog] (
    [ErrorID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [ErrorProcedure] NVARCHAR (155)   NULL,
    [ErrorMessage]   NVARCHAR (MAX)   NULL,
    [QBFilePath]     NVARCHAR (500)   NULL,
    [CreatedOn]      DATETIME         NULL,
    [CreatedBy]      BIGINT           NULL,
    [SessionID]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ErrorLog] PRIMARY KEY CLUSTERED ([ErrorID] ASC)
);

