﻿CREATE TABLE [dbo].[TraceActivity] (
    [TraceID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [SessionID]   UNIQUEIDENTIFIER NULL,
    [Description] VARCHAR (200)    NULL,
    [Type]        VARCHAR (50)     NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_TraceActivity_CreatedOn] DEFAULT (getdate()) NULL,
    [UserID]      BIGINT           NULL,
    CONSTRAINT [PK_TraceActivity] PRIMARY KEY CLUSTERED ([TraceID] ASC)
);

