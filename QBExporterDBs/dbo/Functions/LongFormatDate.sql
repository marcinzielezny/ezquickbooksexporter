﻿
CREATE FUNCTION [dbo].[LongFormatDate]
(
	@Date datetime
)
RETURNS varchar(60)
AS
	BEGIN
		DECLARE @NewDate varchar(60)
		
		SELECT	 @NewDate =  ( Datename(dw,@Date)+', '+Datename(mm,@Date)+ ' '+convert(varchar,datepart(dd,@Date) )+ ' ' + convert(varchar,datepart(yyyy,@Date ) )) + ' ' 
  + CASE WHEN DATEPART(HH,@Date) < 13 
            THEN RIGHT(CONVERT(varchar,100+DATEPART(HH,@Date)) ,2) 
            ELSE CONVERT(varchar,DATEPART(HH,@Date)-12 )
            END + ':'

/*     + CASE WHEN DATEPART(HH,@Date) < 10 
            THEN RIGHT(CONVERT(varchar,DATEPART(hh,@Date)),1)
            ELSE CONVERT(varchar,DATEPART(HH,@Date) ) 
            END + ':'*/
+CASE WHEN DATEPART(mi,@Date) < 10
	THEN
	RIGHT(CONVERT(varchar,100+DATEPART(mi,@Date)) ,2)
	ELSE
      CONVERT(varchar,DATEPART(mi,@Date)) 
	END
     + CASE WHEN DATEPART(HH,@Date) < 13
            THEN ' AM'
            ELSE ' PM'
            END

		
		
	RETURN @NewDate
	END