﻿
CREATE FUNCTION [dbo].[floatToTime] (@input FLOAT)
RETURNS NVARCHAR(20)
AS
BEGIN
	DECLARE @hour INT = FLOOR(@input);
	DECLARE @semi INT = (
			SELECT (@input - FLOOR(@input)) * 3600
			);
	DECLARE @minutes INT = @semi / 60;
	DECLARE @seconds INT = @semi % 60;
	DECLARE @NewDate NVARCHAR(20) = ''

	SELECT @NewDate = RIGHT('00' + CONVERT(VARCHAR(2), @hour), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), @minutes), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), @seconds), 2);

	RETURN @NewDate
END