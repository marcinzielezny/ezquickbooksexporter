﻿
CREATE FUNCTION [dbo].[GetUserBySessionID]
(
	@SessionID uniqueidentifier
)
RETURNS bigint
AS
	BEGIN
		DECLARE @UserID bigint
		
		SELECT @UserID = L.LoginID
		FROM Logins L WITH(NOLOCK)
			INNER JOIN Sessions S WITH(NOLOCK) ON L.ResourceID = S.LoginID
		WHERE S.RecordID = @SessionID
			AND S.Status = 'Active'

	
	RETURN @UserID
	END