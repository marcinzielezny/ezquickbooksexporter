﻿CREATE PROCEDURE [dbo].[AutoLogout]
AS

	DECLARE @SessionID uniqueidentifier
	DECLARE @LastActive datetime

	DECLARE ALog CURSOR FOR
	SELECT RecordID, ISNULL(LastModifiedOn,CreatedOn) FROM Sessions WHERE  ( DATEDIFF(minute,LastAction,GETDATE()) >=15)  AND Status <> 'Closed' AND ISNULL(ShortName,'') <> 'WEB'
	OPEN ALog

	FETCH FROM ALog INTO @SessionID, @LastActive

	WHILE @@FETCH_STATUS = 0
	BEGIN

		UPDATE	Sessions
		SET		Status = 'Closed',
				LastModifiedOn = @LastActive,
				LastAction = @LastActive
		WHERE	RecordID = @SessionID
	
		EXEC AddTraceActivity @SessionID, 'Auto Logout From System'
	
		DELETE FROM FiltersMaster WHERE CreatedBy = @SessionID
	
		FETCH NEXT FROM ALog INTO @SessionID, @LastActive

	END

	CLOSE ALog
	DEALLOCATE ALog