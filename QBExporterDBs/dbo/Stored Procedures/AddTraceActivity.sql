﻿CREATE PROCEDURE [dbo].[AddTraceActivity]
(
	@SessionID uniqueidentifier,
	@Description varchar(200),
	@Type varchar(50) = ''
)
 AS
	INSERT INTO TraceActivity ( SessionID, Description, Type ) VALUES ( @SessionID, @Description, @Type )