﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/02/2013
-- Description: New version for dynamic wizard
-- =============================================
CREATE PROCEDURE [WIZZ].[Config_Wizard_InitData]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @Mode				nvarchar(100)
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID	bigint
	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	-- ==================================================================================
	
	-- =========  STORES  ===============================================================
	SELECT 
		  DS.StoreID AS [_StoreID]
        , 'false'	 AS [AC_StoreSelected]
        , ISNULL(Name, '')		 AS [AC_StoreName]
        , ''		 AS [AC_QBClass]
        , ''		 AS [_onMouseDownRow]
        , ''		 AS [_VendorID]
		, CASE WHEN ISNULL(CM.ConfigName,'')='' THEN '' ELSE 'Store is in configuration:'+CM.ConfigName END AS Description 
		, CASE WHEN ISNULL(CM.ConfigName,'')='' THEN '' ELSE 'True' END AS [_disabled]
	FROM Data.Stores AS DS WITH(nolock)  
		 INNER JOIN Data.UsersPermissions   AS DU WITH(nolock)	ON DS.StoreID = DU.StoreID
		 LEFT OUTER JOIN Export.StoreMatch AS SM WITH(nolock) ON DS.StoreID = SM.StoreID
		 LEFT OUTER JOIN Export.Configuration AS CM WITH(nolock) on SM.ConfigID = CM.ConfigID 
		
	WHERE DU.UserID=@UserID AND Allow=1
	FOR XML RAW( 'Store' )
	-- ==================================================================================
	
	-- =========  Configurations  =======================================================
	IF @Mode='Summary'
	BEGIN
		SELECT
			[Name]		  AS [AC_POSField]
		  ,	[PosFieldsID] AS _POSFieldID
		  , ''			  AS AC_QBAccount
		FROM [Dict].[POSFields] WITH (NOLOCK) 
		WHERE SummarySet=0 AND IsPerStore = 0
		FOR XML RAW('POSMatches')
	END
	ELSE 
	BEGIN
	  SELECT
			[Name]		  AS [AC_POSField]
		  ,	[PosFieldsID] AS _POSFieldID
		  , ''			  AS AC_QBAccount
		FROM [Dict].[POSFields] WITH (NOLOCK) 
		WHERE DetailedSet = 1 AND IsPerStore = 0
		FOR XML RAW('POSMatches')
	END
	-- ==================================================================================
	
END