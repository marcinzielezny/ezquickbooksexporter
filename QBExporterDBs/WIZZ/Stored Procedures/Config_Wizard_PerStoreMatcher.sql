﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/02/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WIZZ].[Config_Wizard_PerStoreMatcher]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @StoresPanel		nvarchar(max)
	, @Direction		nvarchar(20)
	, @ActualPosition	int
	, @NewPosition		int OUTPUT
	, @StoreName		nvarchar(50) OUTPUT
	, @StoreID			nvarchar(50) OUTPUT

)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE   @UserID		bigint
	DECLARE	  @StoresXML	XML
	DECLARE	  @Selected		int
	DECLARE	  @ActualPos	int
			
	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	SET @StoresXML = @StoresPanel
	SET @ActualPos = @ActualPosition	
	
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempData', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempData
	END
	
	SELECT StoreID
		, Selected
		, StoreName
		, ReturnValue
		, ROW_NUMBER() OVER(ORDER BY StoreID DESC) AS RN
	INTO #TempData
	FROM (
		SELECT StoreID
				  , Selected
				  , StoreName
				  , ReturnValue
		FROM (
					SELECT
						 T.c.value('@_StoreID','bigint')			AS StoreID
						,T.c.value('@AC_StoreName','nvarchar(50)')	AS StoreName
						,T.c.value('@AC_StoreSelected','bit')		AS Selected
						,T.c.query('*') AS ReturnValue
					FROM @StoresXML.nodes('/Data/*') as T(c)
			)	AS DA
		WHERE Selected=1
	) AS TMP 
	
	SET @Selected = ( SELECT COUNT(*) FROM #TempData )  
	
	IF ( @Direction = 'NEXT' ) 
	BEGIN
	
		SET @ActualPos = @ActualPos + 1	
		
		IF ( @ActualPos = 0 )
		BEGIN
			PRINT('WAS')
			SET @ActualPos = 1 
			
		END
		
		IF ( @ActualPos > @Selected )
		BEGIN
			SET @NewPosition = - 1
			IF OBJECT_ID( N'tempdb.dbo.#TempData', N'U' ) IS NOT NULL
			BEGIN
				DROP TABLE #TempData
			END
			RETURN
		END 
		ELSE
		BEGIN
			SET @NewPosition = @ActualPos
		END
		
		SELECT  @StoresXML = ReturnValue 
			  , @StoreName = StoreName
			  , @StoreID   = StoreID
		FROM #TempData
		WHERE RN = @ActualPos
		
		SELECT  @StoresXML.query('/Data/*')
		
	END
	
	IF ( @Direction = 'PREV' ) 
	BEGIN
	
		SET @ActualPos = @ActualPos - 1
		
		IF ( @ActualPos < 0 )
		BEGIN
			SET @ActualPos = @Selected
			SET @NewPosition = @ActualPos
		END
		ELSE IF ( @ActualPOS = 0 )
		BEGIN
			SET @NewPosition = 0
			IF OBJECT_ID( N'tempdb.dbo.#TempData', N'U' ) IS NOT NULL
			BEGIN
				DROP TABLE #TempData
			END
			RETURN
		END ELSE
		BEGIN
			SET @NewPosition = @ActualPos
		END
	
		SELECT  @StoresXML = ReturnValue 
			  , @StoreName = StoreName
			  , @StoreID   = StoreID
		FROM #TempData
		WHERE RN = @ActualPos
		
		SELECT  @StoresXML.query('/Data/*')
	
	END

	IF OBJECT_ID( N'tempdb.dbo.#TempData', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempData
	END

END