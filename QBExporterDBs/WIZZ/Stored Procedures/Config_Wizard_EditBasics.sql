﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/05/2013
-- Description: 
-- =============================================

CREATE PROCEDURE [WIZZ].[Config_Wizard_EditBasics]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
	, @ConfigID				bigint
	, @ConfigName			nvarchar(50)  OUTPUT
	, @ConfigFile			nvarchar(255) OUTPUT
	, @StoresType			nvarchar(50)  OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON
	
	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
	-- ==================================================================================

	-- =========  GET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @StoresType=  ( SELECT CASE WHEN mode=0 THEN 
									'SUMMARY' 
							   ELSE 'DETAIL' END AS Type
						FROM [Export].[Configuration] WITH (NOLOCK) 
						WHERE ConfigID=@ConfigID)

	SELECT @ConfigName = [ConfigName]
		,  @ConfigFile = [ConfigPath]
	FROM [Export].[Configuration] WITH (NOLOCK) 
	WHERE ConfigID=@ConfigID
	-- ==================================================================================	
	
END