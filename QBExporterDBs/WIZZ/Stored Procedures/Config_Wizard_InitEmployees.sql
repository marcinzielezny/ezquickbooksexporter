﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/07/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WIZZ].[Config_Wizard_InitEmployees]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @Stores			nvarchar(MAX)
	, @ConfigID			bigint
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE 
	    @UserID	bigint
	    , @StoresXML XML
	-- ==================================================================================
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @StoresXML=CAST (@Stores AS XML)

	
	-- =========  Employees  ===============================================================
	
	IF (@ConfigID = 0 )
	BEGIN
	SELECT   [EmployeeID]	AS _EmployeeID
		   , ST.[StoreID]	AS _StoreID
		   , StoreName		AS AC_StoreName
		   , [Name]			AS AC_Employee
		   , ''				AS AC_QBEmployee
		   , ''				AS AC_QBPayType
	FROM Data.Employee DE WITH (NOLOCK) 
	INNER JOIN (
		SELECT StoreID
			  , Selected
			  , StoreName
		FROM (
				SELECT
					 T.c.value('@_StoreID','bigint')			AS StoreID
					,T.c.value('@AC_StoreName','nvarchar(50)')	AS StoreName
					,T.c.value('@AC_StoreSelected','bit')	AS Selected
				FROM @StoresXML.nodes('/Data/*') as T(c) )	AS DA
		WHERE Selected = 1
	) AS ST ON ST.StoreID=DE.StoreID
	FOR XML RAW( 'Employees' )
	
	END
	ELSE
	BEGIN
	
		SELECT 
		   DE.[EmployeeID]	AS _EmployeeID
		   , DS.[StoreID]		AS _StoreID
		   , DS.Name			AS AC_StoreName
		   , DE.[Name]			AS AC_Employee
		   , QBEmployee			AS AC_QBEmployee
		   , QBPayType			AS AC_QBPayType
		FROM  Data.Employee DE
			LEFT JOIN [Export].[EmployeeMatch] EM WITH (NOLOCK) ON EM.EmployeeID = DE.EmployeeID AND EM.ConfigID=@ConfigID
			LEFT JOIN Data.Stores DS WITH (NOLOCK) ON DE.StoreID = DS.StoreID
			LEFT JOIN  Export.StoreMatch ESM WITH (NOLOCK) ON ESM.StoreID = DS.StoreID AND EM.ConfigID=@ConfigID
			INNER JOIN Data.UsersPermissions   AS DU WITH(nolock) ON DS.StoreID = DU.StoreID
		WHERE  DU.UserID=@UserID AND Allow=1
		FOR XML RAW( 'Employees' )
		
	END	
	-- ==================================================================================
	
	
END