﻿CREATE TABLE [Export].[Log] (
    [ExportLogID]  BIGINT        IDENTITY (1, 1) NOT NULL,
    [StoreID]      BIGINT        NULL,
    [BusinessDate] DATE          NULL,
    [ExportedOn]   DATETIME      NULL,
    [ExportedBy]   BIGINT        NULL,
    [ExportedTo]   BIGINT        NULL,
    [ExportNumber] INT           CONSTRAINT [DF_Log_ExportNumber] DEFAULT ((1)) NULL,
    [Status]       NVARCHAR (50) NULL,
    CONSTRAINT [PK_ExportLog] PRIMARY KEY CLUSTERED ([ExportLogID] ASC)
);

