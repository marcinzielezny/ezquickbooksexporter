﻿CREATE TABLE [Export].[Configuration] (
    [ConfigID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [ConfigName] NVARCHAR (100) NULL,
    [ConfigPath] NVARCHAR (500) NULL,
    [OwnerID]    BIGINT         NULL,
    [IsActive]   BIT            NULL,
    [CreatedOn]  DATETIME       NULL,
    [CreatedBy]  BIGINT         NULL,
    [ModifiedOn] DATETIME       NULL,
    [ModifiedBy] BIGINT         NULL,
    [Mode]       BIT            NULL,
    CONSTRAINT [PK_ExportConfiguration] PRIMARY KEY CLUSTERED ([ConfigID] ASC)
);


GO
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 17/05/2013
-- Description: 
-- =============================================
CREATE TRIGGER [Export].ArchiveConfiguration
   ON   [Export].[Configuration]
   AFTER DELETE 
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO [Archive].[Configuration]
           (   [ConfigID]
			  ,[ConfigName]
			  ,[ConfigPath]
			  ,[OwnerID]
			  ,[IsActive]
			  ,[CreatedOn]
			  ,[CreatedBy]
			  ,[ModifiedOn]
			  ,[ModifiedBy]
			  ,[Mode])
	SELECT [ConfigID]
      ,[ConfigName]
      ,[ConfigPath]
      ,[OwnerID]
      ,[IsActive]
      ,[CreatedOn]
      ,[CreatedBy]
      ,[ModifiedOn]
      ,[ModifiedBy]
      ,[Mode]
    FROM DELETED
    
 

END