﻿CREATE TABLE [Export].[EmployeeMatch] (
    [EmployeeID]      BIGINT        NULL,
    [ConfigID]        BIGINT        NULL,
    [QBEmployee]      NVARCHAR (50) NULL,
    [QBPayType]       NVARCHAR (50) NULL,
    [EmployeeMatchID] BIGINT        IDENTITY (1, 1) NOT NULL,
    [ModifiedOn]      DATETIME      CONSTRAINT [DF_EmployeeMatch_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]      BIGINT        NULL,
    [EXPayType]       NVARCHAR (20) NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([EmployeeMatchID] ASC)
);


GO
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 17/05/2013
-- Description: 
-- =============================================
CREATE TRIGGER [Export].ArchiveEmployeeMatch
   ON   [Export].[EmployeeMatch]
   AFTER DELETE 
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO [Archive].[EmployeeMatch]
           ([EmployeeID]
           ,[ConfigID]
           ,[QBEmployee]
           ,[QBPayType]
           ,[EmployeeMatchID]
           ,[ModifiedOn]
           ,[ModifiedBy])
	SELECT [EmployeeID]
      ,[ConfigID]
      ,[QBEmployee]
      ,[QBPayType]
      ,[EmployeeMatchID]
      ,[ModifiedOn]
      ,[ModifiedBy]
    FROM DELETED
    
 

END