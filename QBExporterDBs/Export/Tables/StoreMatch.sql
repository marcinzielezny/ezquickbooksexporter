﻿CREATE TABLE [Export].[StoreMatch] (
    [StoreMatchID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ConfigID]     BIGINT         NULL,
    [StoreID]      BIGINT         NULL,
    [QBAStore]     NVARCHAR (100) NULL,
    [ModifiedOn]   DATETIME       CONSTRAINT [DF_StoreMatch_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]   BIGINT         NULL,
    [VendorID]     NVARCHAR (50)  NULL,
    [IsPayrollOnly]    BIT            NULL,
    [StoreType]    INT            NULL,
    [IsPayroll] BIT NULL, 
    CONSTRAINT [PK_StoreMatchID] PRIMARY KEY CLUSTERED ([StoreMatchID] ASC)
);








GO
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 17/05/2013
-- Description: 
-- =============================================
CREATE TRIGGER [Export].ArchiveStoreMatch 
   ON   [Export].[StoreMatch]
   AFTER DELETE 
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO [Archive].[StoreMatch]
           ([StoreMatchID]
           ,[ConfigID]
           ,[StoreID]
           ,[QBAStore]
           ,[ModifiedOn]
           ,[ModifiedBy])
	SELECT 
	   [StoreMatchID]
      ,[ConfigID]
      ,[StoreID]
      ,[QBAStore]
      ,[ModifiedOn]
      ,[ModifiedBy]
    FROM DELETED
    
 

END