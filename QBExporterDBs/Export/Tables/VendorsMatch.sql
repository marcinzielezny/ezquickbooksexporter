﻿CREATE TABLE [Export].[VendorsMatch] (
    [VendorMatchID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ConfigID]      BIGINT         NULL,
    [PosFieldID]    BIGINT         NULL,
    [QBVendor]      NVARCHAR (100) NULL,
    [QBVendorName]  NVARCHAR (100) NULL,
    [ModifiedOn]    DATETIME       CONSTRAINT [DF_VendorsMatch_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]    BIGINT         NULL,
    CONSTRAINT [PK_VendorsMatch] PRIMARY KEY CLUSTERED ([VendorMatchID] ASC)
);

