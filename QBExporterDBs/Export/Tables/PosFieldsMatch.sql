﻿CREATE TABLE [Export].[PosFieldsMatch] (
    [PosFieldMatchID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ConfigID]        BIGINT         NULL,
    [PosFieldID]      BIGINT         NULL,
    [QBAccount]       NVARCHAR (100) NULL,
    [QBAccountName]   NVARCHAR (100) NULL,
    [ModifiedOn]      DATETIME       CONSTRAINT [DF_PosFieldsMatch_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]      BIGINT         NULL,
    [QBVendor]        NVARCHAR (100) NULL,
    [StoreSpecific]   BIGINT         NULL,
    CONSTRAINT [PK_PosFieldsMatch] PRIMARY KEY CLUSTERED ([PosFieldMatchID] ASC)
);


GO
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 17/05/2013
-- Description: 
-- =============================================
CREATE TRIGGER [Export].ArchivePosFieldsMatch
   ON   [Export].[PosFieldsMatch]
   AFTER DELETE 
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO [Archive].[PosFieldsMatch]
           ([PosFieldMatchID]
           ,[ConfigID]
           ,[PosFieldID]
           ,[QBAccount]
           ,[QBAccountName]
           ,[ModifiedOn]
           ,[ModifiedBy])
	SELECT 
	   [PosFieldMatchID]
      ,[ConfigID]
      ,[PosFieldID]
      ,[QBAccount]
      ,[QBAccountName]
      ,[ModifiedOn]
      ,[ModifiedBy]
    FROM DELETED
    
 

END