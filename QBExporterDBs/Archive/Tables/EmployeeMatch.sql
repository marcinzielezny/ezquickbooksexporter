﻿CREATE TABLE [Archive].[EmployeeMatch] (
    [EmployeeID]             BIGINT        NULL,
    [ConfigID]               BIGINT        NULL,
    [QBEmployee]             NVARCHAR (50) NULL,
    [QBPayType]              NVARCHAR (50) NULL,
    [EmployeeMatchID]        BIGINT        NULL,
    [ArchiveEmployeeMatchID] BIGINT        IDENTITY (1, 1) NOT NULL,
    [ModifiedOn]             DATETIME      NULL,
    [ModifiedBy]             BIGINT        NULL,
    CONSTRAINT [PK_EmployeeArchive] PRIMARY KEY CLUSTERED ([ArchiveEmployeeMatchID] ASC)
);

