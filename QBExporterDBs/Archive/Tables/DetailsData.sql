﻿CREATE TABLE [Archive].[DetailsData] (
    [ArchiveDataID]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [DataID]            BIGINT         NULL,
    [StoreID]           BIGINT         NULL,
    [BusinessDate]      DATE           NULL,
    [Value]             MONEY          NULL,
    [DefaultValue]      MONEY          NULL,
    [PosField]          BIGINT         NULL,
    [EmployeeID]        BIGINT         NULL,
    [HoursWorked]       NVARCHAR (50)  NULL,
    [Operation]         VARCHAR (50)   NULL,
    [CreatedOn]         DATETIME       CONSTRAINT [DF_ArchiveDetailsData_CreatedOn] DEFAULT (getdate()) NULL,
    [CreatedBy]         BIGINT         NULL,
    [ExportedOn]        DATETIME       NULL,
    [ExportedBy]        BIGINT         NULL,
    [ExportedTo]        NVARCHAR (50)  NULL,
    [ExportAccount]     NVARCHAR (50)  NULL,
    [ExportAccountName] NVARCHAR (100) NULL,
    [ExportStatus]      NVARCHAR (50)  NULL,
    [ExportNumber]      NVARCHAR (50)  NULL,
    CONSTRAINT [PK_ArchiveDetailsData] PRIMARY KEY CLUSTERED ([ArchiveDataID] ASC)
);

