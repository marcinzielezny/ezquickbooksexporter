﻿CREATE TABLE [Archive].[StoreMatch] (
    [ArchiveStoreMatchID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [StoreMatchID]        BIGINT         NULL,
    [ConfigID]            BIGINT         NULL,
    [StoreID]             BIGINT         NULL,
    [QBAStore]            NVARCHAR (100) NULL,
    [ModifiedOn]          DATETIME       CONSTRAINT [DF_StoreMatch_ModifiedON] DEFAULT (getdate()) NULL,
    [ModifiedBy]          BIGINT         NULL,
    CONSTRAINT [PK_StoreMatchIDArchive] PRIMARY KEY CLUSTERED ([ArchiveStoreMatchID] ASC)
);

