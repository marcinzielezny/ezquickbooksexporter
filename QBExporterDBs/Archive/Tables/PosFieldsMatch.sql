﻿CREATE TABLE [Archive].[PosFieldsMatch] (
    [ArchivePosFieldMatchID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [PosFieldMatchID]        BIGINT         NULL,
    [ConfigID]               BIGINT         NULL,
    [PosFieldID]             BIGINT         NULL,
    [QBAccount]              NVARCHAR (100) NULL,
    [QBAccountName]          NVARCHAR (100) NULL,
    [ModifiedOn]             DATETIME       NULL,
    [ModifiedBy]             BIGINT         NULL,
    CONSTRAINT [PK_PosFieldsMatchArchive] PRIMARY KEY CLUSTERED ([ArchivePosFieldMatchID] ASC)
);

