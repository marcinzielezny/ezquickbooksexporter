﻿CREATE TABLE [Archive].[Configuration] (
    [ArchiveConfigID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ConfigID]        BIGINT         NULL,
    [ConfigName]      NVARCHAR (100) NULL,
    [ConfigPath]      NVARCHAR (500) NULL,
    [OwnerID]         BIGINT         NULL,
    [IsActive]        BIT            NULL,
    [CreatedOn]       DATETIME       NULL,
    [CreatedBy]       BIGINT         NULL,
    [ModifiedOn]      DATETIME       NULL,
    [ModifiedBy]      BIGINT         NULL,
    [Mode]            BIT            NULL,
    CONSTRAINT [PK_ExportConfigurationArchive] PRIMARY KEY CLUSTERED ([ArchiveConfigID] ASC)
);

