﻿-- =============================================
-- Author:	MZ	
-- Alter  date: 06/05/2013
-- Description: Move from EZConnect
-- =============================================

CREATE PROCEDURE [Service].[Log_Write_Ex]
(
	@Source				nvarchar(500)	= ''
	, @Level			int				= 9
	, @Message			nvarchar(1000)
	, @Error			bit				= 0
	, @ErrorDescription nvarchar(1000)	= NULL
	, @Duration			int				= NULL
	, @CreatedBy		nvarchar(100)	= NULL		OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @LogID	bigint
	
	INSERT INTO [Service].[Logs](
		[Message]
		, [Source]
		, [Level]
		, [Error]
		, [ErrorDescription]
		, [Duration]
		, [CreatedBy]		
	)
	VALUES(	
		@Message
		, @Source
		, @Level
		, @Error
		, @ErrorDescription
		, @Duration
		, @CreatedBy
	)
	
	SET @LogID = @@IDENTITY
	
	IF( @CreatedBy IS NULL )
	BEGIN	
		SET @CreatedBy = CAST(@LogID AS nvarchar(100))
	END
	
	SET NOCOUNT OFF
END