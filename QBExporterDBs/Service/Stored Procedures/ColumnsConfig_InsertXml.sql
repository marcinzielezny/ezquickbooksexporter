﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MT
-- Create date: 2009.12.02
-- Description: 
-- =============================================
CREATE PROCEDURE [Service].[ColumnsConfig_InsertXml]
(
	@ColumnsData	xml
)
AS
BEGIN
	SET NOCOUNT ON

	-- COLUMNS CONFIG
	INSERT INTO [dbo].[ColumnsConfigs] (
		[name]
		, [headertext]
		, [nulltext]
		, [alignment]
		, [type]
		, [width]
		, [format]
		, [formatENG_BR]
		, [formatUS]
		, [formatPL]
		, [formatGER]
		, [formatCZ]
		, [maskinput]
		, [maskinputENG_BR]
		, [maskinputUS]
		, [maskinputPL]
		, [maskinputGER]
		, [maskinputCZ]
		, [cellActivation]
		, [cellclickaction]
		, [backcolor]
		, [formula]
		, [readonly]
		, [visible]
		, [_ValueEditClass]
		, [_ValueList]
		, [ExportedSettings]
		, [SummarySettings]
	)
	SELECT
		C.value( '@name', 'varchar(255)' )			AS [name]
		, C.value( '@headertext', 'varchar(255)' )	AS [headertext]
		, C.value( '@nulltext', 'varchar(100)' )	AS [nulltext]
		, C.value( '@alignment', 'varchar(255)' )	AS [alignment]
		, C.value( '@type', 'varchar(255)' )		AS [type]
		, C.value( '@width', 'int' )				AS [width]
		, C.value( '@format', 'varchar(255)' )		AS [format]
		, C.value( '@formatENG_BR', 'varchar(255)' )AS [formatENG_BR]
		, C.value( '@formatUS', 'varchar(255)' )	AS [formatUS]
		, C.value( '@formatPL', 'varchar(255)' )	AS [formatPL]
		, C.value( '@formatGER', 'varchar(255)' )	AS [formatGER]
		, C.value( '@formatCZ', 'varchar(255)' )	AS [formatCZ]
		, C.value( '@maskinput', 'varchar(255)' )	AS [maskinput]
		, C.value( '@maskinputENG_BR', 'varchar(255)' )	AS [maskinputENG_BR]
		, C.value( '@maskinputUS', 'varchar(255)' )	AS [maskinputUS]
		, C.value( '@maskinputPL', 'varchar(255)' )	AS [maskinputPL]
		, C.value( '@maskinputGER', 'varchar(255)' )	AS [maskinputGER]
		, C.value( '@maskinputCZ', 'varchar(255)' )	AS [maskinputCZ]
		, C.value( '@cellActivation', 'varchar(255)' )	AS [cellActivation]
		, C.value( '@cellClickAction', 'varchar(100)' )	AS [cellclickaction]
		, C.value( '@backcolor', 'varchar(100)' )	AS [backcolor]
		, C.value( '@formula', 'varchar(100)' )		AS [formula]
		, C.value( '@readonly', 'varchar(50)' )		AS [readonly]
		, C.value( '@visible', 'varchar(10)' )		AS [visible]
		, C.value( '@_ValueEditClass', 'varchar(255)' )	AS [_ValueEditClass]
		, C.value( '@_ValueList', 'varchar(255)' )	AS [_ValueList]
		, CAST(CAST(C.query( './ExportedHeader' ) AS varchar(MAX)) + CAST(C.query( './ExportedColumn' ) AS varchar(MAX)) AS xml)	AS [ExportedSettings]
		, C.query( './SummarySettings' )			AS [SummarySettings]
	FROM @ColumnsData.nodes('Columns/*') as T(C)
	WHERE C.value( '@name', 'varchar(255)' ) <> ''
END