﻿
-- =============================================
-- Author:        LT 
-- Created on:    04/16/2009
-- Description:   Based on sp [Service].[FindKeyword] - schema info added
-- =============================================
CREATE PROCEDURE [Service].[FindKeyword]
(
      @keyword    nvarchar(1000)    = NULL
)
AS
BEGIN
      SET @keyword = '%' + @keyword + '%'

      SELECT 
            sysobjects_child.[object_id]
            , sysobjects_parent.[name]          AS [Parent]
--          , sysobjects_child.[schema_id]
            , schema_name(sysobjects_child.[schema_id]) AS [Schema]
            , sysobjects_child.[name]           AS [Name]
            , sysobjects_child.[type]           AS [Type]
            , syscomments.[text]                AS [text]
      FROM sys.objects sysobjects_child
            LEFT JOIN sys.objects sysobjects_parent ON sysobjects_parent.[object_id] = sysobjects_child.parent_object_id
            LEFT JOIN syscomments ON sysobjects_child.[object_id] = syscomments.id
      WHERE syscomments.[text] like @keyword
            or sysobjects_child.name like @keyword
            or sysobjects_parent.name like @keyword
      ORDER BY sysobjects_child.[type],sysobjects_parent.[name],sysobjects_child.[name]
      RETURN

END