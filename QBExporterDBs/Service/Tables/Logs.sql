﻿CREATE TABLE [Service].[Logs] (
    [LogID]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [CreatedOn]        DATETIME        DEFAULT (getutcdate()) NULL,
    [CreatedBy]        NVARCHAR (1000) DEFAULT ('') NULL,
    [Message]          NVARCHAR (1000) NULL,
    [Source]           NVARCHAR (1000) NULL,
    [Error]            BIT             DEFAULT ((0)) NULL,
    [Level]            INT             DEFAULT ((9)) NULL,
    [ErrorDescription] NVARCHAR (1000) NULL,
    [IsValid]          BIT             DEFAULT ((1)) NULL,
    [Duration]         INT             NULL,
    CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED ([LogID] ASC)
);

