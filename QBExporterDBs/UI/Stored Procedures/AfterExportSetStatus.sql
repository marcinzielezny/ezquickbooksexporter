﻿
/* 2013  ezUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 13/05/2013
-- Description: 
-- Reviewed for Local: 20/03/2014
-- =============================================
CREATE PROCEDURE [UI].[AfterExportSetStatus]
(
	  @SessionID 		uniqueidentifier
	, @OrganizationID	uniqueidentifier
	, @DataSet			nvarchar(max)
	, @Status			nvarchar(max)
	, @ArchiveConfig	nvarchar(max)
)	
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID		bigint
		   ,@Data		XML
		   ,@Archive	XML
		   ,@Now		Datetime
	-- ==================================================================================
	
	SET @Now = GETDATE()
	
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF ( @UserID IS NULL )
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	SET @Archive = CAST( @ArchiveConfig AS XML)
	SET @Data = CAST( @DataSet AS XML)
	-- ===================================================================================
	
	INSERT INTO Archive.DetailsData
		(
		   [DataID]
		  ,[StoreID]
		  ,[BusinessDate]
		  ,[Value]
		  ,[PosField]
		  ,[EmployeeID]
		  ,[HoursWorked]
		  ,[Operation]
		  ,[CreatedOn]
		  ,[CreatedBy]
		  ,[ExportedOn]
		  ,[ExportedBy]
		  ,[ExportedTo]
		  ,[ExportAccount]
		  ,[ExportAccountName]
		  ,[ExportStatus]
		  ,[ExportNumber]
		)
	SELECT 
			[DDD].[DataID]
		   ,[DDD].[StoreID]
		   ,[DDD].[BusinessDate]
		   ,[Value]
		   ,[PosField]
		   ,[DDD].[EmployeeID]
		   ,[DDD].[HoursWorked]
		   ,'Export'
		   ,COALESCE( [LastModificationON], [DDD].[CreatedOn] )
		   ,COALESCE( [LastModificationBy], [DDD].[CreatedBy] )
		   ,@Now
		   ,@UserID
		   ,[EC].[ConfigName]
		   ,[QBAccountID]
		   ,[QBAccountName]
		   ,@Status
		   ,[ExportID]
	FROM (
	SELECT
		 T.c.value('@DataID', 'bigint')					AS DataID
	   , T.c.value('@AccountListID', 'nvarchar(50)')	AS QBAccountID
	   , T.c.value('@AccountFullName', 'nvarchar(50)')	AS QBAccountName
	   , T.c.value('@BusinessDate', 'Datetime')			AS BusinessDate
	   , T.c.value('@ConfigID', 'bigint')				AS ConfigID
	   , T.c.value('@ExportID', 'nvarchar(20)')			AS ExportID
	FROM @Archive.nodes('/Data/Store/Stores') as T(c) )
	AS AR INNER JOIN Data.DetailsData DDD ON AR.DataId=DDD.DataID
		  INNER JOIN Export.Configuration EC ON AR.ConfigID=EC.ConfigID


	INSERT INTO Archive.DetailsData
		(
		   [DataID]
		  ,[StoreID]
		  ,[BusinessDate]
		  ,[Value]
		  ,[PosField]
		  ,[EmployeeID]
		  ,[HoursWorked]
		  ,[Operation]
		  ,[CreatedOn]
		  ,[CreatedBy]
		  ,[ExportedOn]
		  ,[ExportedBy]
		  ,[ExportedTo]
		  ,[ExportAccount]
		  ,[ExportAccountName]
		  ,[ExportStatus]
		  ,[ExportNumber]
		)
	SELECT  
			[DDD].[DataID]
		   ,[DDD].[StoreID]
		   ,[DDD].[BusinessDate]
		   ,0.0
		   ,[PosField]
		   ,[DDD].[EmployeeID]
		   ,[DDD].[HoursWorked]
		   ,'Export'
		   ,COALESCE(LastModificationON, DDD.CreatedOn)
		   ,COALESCE(LastModificationBy, DDD.CreatedBy)
		   ,@Now
		   ,@UserID
		   ,[EC].[ConfigName]
		   ,[QBAccountID]
		   ,[QBAccountName]
		   ,@Status
		   ,[ExportID]
	FROM (
	SELECT
		 T.c.value('@DataID', 'bigint')			AS DataID
	   , T.c.value('@QBEmployee', 'nvarchar(50)')	AS QBAccountID
	   , T.c.value('@EmployeeName', 'nvarchar(50)')	AS QBAccountName
	   , T.c.value('@EmployeeID', 'bigint')		AS EmployeeID
	   , T.c.value('@BusinessDate', 'Datetime')			AS BusinessDate
	   , T.c.value('@ConfigID', 'bigint')				AS ConfigID
	   , T.c.value('@ExportID', 'nvarchar(20)')				AS ExportID
	FROM @Archive.nodes('/Data/Store/Employee') as T(c) )
	AS AR INNER JOIN Data.DetailsData DDD ON AR.DataId=DDD.DataID
		  INNER JOIN Export.Configuration EC ON AR.ConfigID=EC.ConfigID

	INSERT INTO Archive.DetailsData
		(
		   [DataID]
		  ,[StoreID]
		  ,[BusinessDate]
		  ,[Value]
		  ,[PosField]
		  ,[EmployeeID]
		  ,[HoursWorked]
		  ,[Operation]
		  ,[CreatedOn]
		  ,[CreatedBy]
		  ,[ExportedOn]
		  ,[ExportedBy]
		  ,[ExportedTo]
		  ,[ExportAccount]
		  ,[ExportAccountName]
		  ,[ExportStatus]
		  ,[ExportNumber]
		)
	SELECT  
			[DDD].[DataID]
		   ,[DDD].[StoreID]
		   ,[DDD].[BusinessDate]
		   ,0.0
		   ,[PosField]
		   ,[DDD].[EmployeeID]
		   ,[DDD].[HoursWorked]
		   ,'Export'
		   ,COALESCE(LastModificationON, DDD.CreatedOn)
		   ,COALESCE(LastModificationBy, DDD.CreatedBy)
		   ,@Now
		   ,@UserID
		   ,[EC].[ConfigName]
		   ,[QBAccountID]
		   ,[QBAccountName]
		   ,@Status
		   ,[ExportID]
	FROM (
	SELECT
		 T.c.value('@DataID', 'bigint')			AS DataID
	   , T.c.value('@QBEmployee', 'nvarchar(50)')	AS QBAccountID
	   , T.c.value('@EmployeeName', 'nvarchar(50)')	AS QBAccountName
	   , T.c.value('@EmployeeID', 'bigint')		AS EmployeeID
	   , T.c.value('@BusinessDate', 'Datetime')			AS BusinessDate
	   , T.c.value('@ConfigID', 'bigint')				AS ConfigID
	   , T.c.value('@ExportID', 'nvarchar(20)')				AS ExportID
	FROM @Archive.nodes('/Data/Employee') as T(c) )
	AS AR INNER JOIN Data.DetailsData DDD ON AR.DataId=DDD.DataID
		  INNER JOIN Export.Configuration EC ON AR.ConfigID=EC.ConfigID

	
		
   ;MERGE [Export].[Log] AS DDD
	USING (
		SELECT BussinessDate, StoreID, Selected
		FROM (
  			SELECT
				 T.c.value('@EX_BussinessDate', 'date')	AS BussinessDate
			   , T.c.value('@_StoreID', 'bigint')	AS StoreID
		       , T.c.value('@EX_CheckStore', 'bit')	AS Selected
			FROM @Data.nodes('/*') as T(c)
		) AS Data
		WHERE Selected=1
	) AS T 
	ON DDD.StoreID = T.StoreID AND DDD.[BusinessDate] = BussinessDate
	WHEN MATCHED THEN
	UPDATE
		SET  [ExportedOn] = GETDATE()
			,[ExportedBy] = @UserID
			,[Status]	  = REPLACE(@Status,'Suceed','successful')
	WHEN NOT MATCHED THEN 
	INSERT ( [StoreID], [BusinessDate], [ExportedOn], [ExportedBy], [Status])
	VALUES ( StoreID, BussinessDate, GETDATE(), @UserID, REPLACE(@Status,'Suceed','successful'));

	
END