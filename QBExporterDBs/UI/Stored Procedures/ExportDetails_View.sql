﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/06/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [UI].[ExportDetails_View]
(
	  @SessionID 		uniqueidentifier
	, @Rows				int = 50
	, @Period    		varchar(max) = NULL
	, @Stores    		varchar(max) = NULL
	, @PageNumber		int = 1 OUTPUT
	, @KeyID			uniqueidentifier = NULL OUTPUT
	, @TotalPages		int = 1 OUTPUT
	, @TotalRows		int = 1 OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
		, @Source				varchar(50)
		, @DateFrom 			datetime
		, @DateTo				datetime
		, @FirstRow				int
		, @LastRow				int
		, @XmlData				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'UI_Accounting_InitData_ExportDetails'

	IF ( ISNULL( @Rows, 0 ) = 0 )
	BEGIN
		SET @Rows = 50
	END
	
	SET @PageNumber = ISNULL( @PageNumber, 1 )
	-- ==================================================================================
 
	-- =========  SET FILTERS  ===========================================================
	IF ( @KeyID IS NULL )
	BEGIN
		SET @KeyID = NEWID()

		SET @XmlData = CAST( (
				ISNULL( @Period, '' )
				+ ISNULL( @Stores, '' )
			) AS xml )

	EXEC V100.Filter_ByXpath
				@SessionID		= @SessionID
				, @XmlData  	= @XmlData 
				, @XPath		= @Source
				, @Procedure	= @Source
				, @KeyID 		= @KeyID
	END

	SET @DateFrom = V100.FILTER_DateFrom( @KeyID, @SessionID )
	SET @DateTo = V100.FILTER_DateTo( @KeyID, @SessionID )
	-- ==================================================================================
	

	
	-- =========  CREATE TEMP TABLES  ===================================================
	CREATE TABLE #TempStores(
		StoreID bigint
		, Location varchar(50)
		, IsDetails bit  
	)

	EXEC V100.PrepareTempStores
			@SessionID  = @SessionID
			, @UserID  = @UserID
			, @KeyID    = @KeyID
			
			
	 CREATE TABLE #TempStoreDetails
	 (
		_StoreID			bigint
		, [StoreName]		nvarchar(50)
		, [BussinessDate]	nvarchar(50)
		, [POS Field Name]	nvarchar(100)
		, [_PosFieldID]     bigint
		, Debit				nvarchar(50)
		, Credit			nvarchar(50)
		, [Hours Worked]	nvarchar(50)
		, [Destination]		nvarchar(100)
		, Colors			nvarchar(50)
		, [LastMdfOn]		datetime
		, [LastMdfBy]		bigint
		, [Type]			nvarchar(1)
		, [OrderNumber]		int
		, [_DataID]			bigint
	 ) 
	-- ==================================================================================


	-- =========  PREPATE TEMP DATA  ====================================================
    IF DATEDIFF( DAY, @DateFrom, @DateTo ) < 0
	BEGIN
		SET @DateFrom = DATEADD( DAY, 0, @DateTo )
	END 

	;WITH DateRange AS
	(
		SELECT CAST( @DateFrom AS date ) AS Val
		UNION ALL
		SELECT DATEADD( DAY, 1, Val )
		FROM DateRange
		WHERE Val < CAST( @DateTo AS date )
	)
	SELECT DISTINCT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , [Destination]
		    , ConfigID
		    , [ExportedOn]
		    , [ExportStatus]
		    , [RowNumber]
			,  CASE WHEN (RowNumber % 2)=1 THEN
			   'AliceBlue'
			  ELSE 
			   'Honeydew'
			  END AS Colors
			, 0 AS PICK
			, StoreType
	INTO #TempStoreDat
	FROM		  (
	SELECT DISTINCT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , EC.ConfigName AS [Destination]
		    , EC.ConfigID
		    , [ExportedOn]
		    , [ExportStatus]
			, [ESM].[StoreType]
			, ROW_NUMBER() OVER( ORDER BY  [BussinessDate] DESC, ESM.StoreType, _StoreID ) AS [RowNumber]
	FROM (
	SELECT DISTINCT CONVERT( varchar(100), DR.Val, 101 ) AS [BussinessDate]
		, EX.StoreID AS _StoreID
		, EX.Location AS [StoreName]
		, EX.IsDetails AS [IsDetails]
		, '' AS [Destination]
		, '' AS [ExportedOn]
		, '' AS [ExportStatus]
		
	FROM DateRange DR
		CROSS JOIN #TempStores EX 
		INNER JOIN Data.DetailsData DD WITH (NOLOCK) ON EX.StoreID = DD.StoreID  AND CONVERT( varchar(100), DR.Val, 101 ) = DD.BusinessDate) AS Data
	INNER JOIN Export.StoreMatch ESM WITH (NOLOCK) ON Data._StoreID = ESM.StoreID
	LEFT OUTER JOIN Export.Configuration EC WITH (NOLOCK) ON ESM.ConfigID = EC.ConfigID
	) AS My
	ORDER BY StoreType
	
	IF (NOT EXISTS (SELECT TOP 1 1 FROM #TempStoreDat WHERE StoreType IN (0,1) ) ) 
	BEGIN 
		SET @Rows = @Rows * 5
	END
	
	SET @TotalRows = ( SELECT COUNT(*) FROM #TempStoreDat)
	SET @TotalPages = CEILING(CAST(@TotalRows as money)/CAST(@Rows as money))
	
	IF (@PageNumber>@TotalPages)
	BEGIN
		SET @PageNumber=@TotalPages
	END
	
		-- ==================================================================================
	SET	@FirstRow = ( @PageNumber - 1 ) * @Rows + 1
	SET @LastRow = @PageNumber * @Rows
	-- ==================================================================================	

	SELECT DISTINCT 
			  [DP].[BussinessDate]
		    , [DP]._StoreID
		    , [DP].[StoreName]
		    , [DP].[IsDetails]
		    , [DP].[Destination]
		    , [DP].[ConfigID]
		    , [DP].[ExportedOn]
		    , [DP].[ExportStatus]
		    , [DP].[RowNumber]
			, [DP].[Colors]
	INTO #TempStoreDate
	FROM #TempStoreDat [DP]
	WHERE RowNumber BETWEEN @FirstRow AND @LastRow	
	
	-- =========  RESULTSET  ============================================================
	SELECT 'EXPORT DETAILS FROM ' + CONVERT( varchar(100 ), @DateFrom, 101 ) + ' TO ' + CONVERT( varchar(100 ), @DateTo, 101 )

	INSERT INTO #TempStoreDetails
	 (
		_StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]		
		, Colors
		, [LastMdfOn]
		, [LastMdfBy]	
		, Type
		, OrderNumber
		, _DataID	
	 ) 
	SELECT 
			 _StoreID
			, [StoreName]
			, [BussinessDate]
			, Name AS [POS Field Name]
			, [PosFieldsID]
			, CASE WHEN ((My.SpecialGroup = 'Debit' AND  DD.SG IS NULL ) OR DD.SG = 'D') THEN  ISNULL( DD.Value, 0.0 ) ELSE 0.0 END AS Debit
			, CASE WHEN ((My.SpecialGroup = 'Credit' AND  DD.SG IS NULL) OR DD.SG = 'C') THEN  ISNULL( DD.Value, 0.0 ) ELSE 0.0 END AS Credit
			, '' AS [Hours Worked]
			, [Destination]
			, Colors
			, COALESCE(DD.[LastModificationOn], DD.[CreatedOn]) AS [LastMdf]
			, COALESCE(DD.[LastModificationBy], DD.[CreatedBy]) AS [LastMdfBy]
			, 'A' AS Type
			, My.OrderNumber 
			, DD.DataID 
	 FROM (
			SELECT 	
					_StoreID
					, [StoreName]
					, [BussinessDate]
					, Destination
					, ExportStatus
					, Colors
			FROM   #TempStoreDate TSD 
			  ) AS DP
	INNER JOIN [Export].StoreMatch ESM WITH (NOLOCK) ON DP._StoreID=ESM.StoreID
	--INNER JOIN [Export].[PosFieldsMatch] PFM WITH (NOLOCK) ON PFM.ConfigID = ESM.ConfigID  AND ( PFM.StoreSpecific=DP._StoreID OR PFM.StoreSpecific = 0 )
    INNER JOIN  [Dict].[POSFields] My WITH (NOLOCK) ON ( My.[DetailedSet] = ESM.StoreType OR My.[SummarySet] = ESM.StoreType )
    LEFT OUTER JOIN Data.DetailsData DD WITH (NOLOCK) ON DP._StoreID = DD.StoreID  AND DP.BussinessDate = DD.BusinessDate AND My.PosFieldsID = DD.PosField
	ORDER BY  [BussinessDate] DESC
	-- ==================================================================================

	INSERT INTO #TempStoreDetails
	 (
		_StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]		
		, Colors
		, [LastMdfOn]
		, [LastMdfBy]
		, [Type]
		, [_DataID] 			
	 )		
	 SELECT DISTINCT 
			  _StoreID
		    , [StoreName]
			, [BussinessDate]
			, DE.Name + ' ' + DE.EXPayType
			, DE.EmployeeID
			, ''
			, ''
			, ISNULL(HoursWorked,'00:00:00') AS [Hours Worked]
			, [Destination]
			, Colors
			, COALESCE(DD.[LastModificationOn], DD.[CreatedOn]) AS [LastMdf]
			, COALESCE(DD.[LastModificationBy], DD.[CreatedBy],'') AS [LastMdfBy] 
			, 'E' AS Type
			, DD.DataID AS [_DataID]
	FROM   #TempStoreDate TSD 
	INNER JOIN Data.Employee DE WITH (NOLOCK) ON DE.StoreID = TSD._StoreID
	INNER JOIN Export.EmployeeMatch EM WITH (NOLOCK) ON EM.EmployeeID=DE.EmployeeID
	LEFT OUTER JOIN Data.DetailsData DD WITH (NOLOCK) ON  TSD.BussinessDate = DD.BusinessDate  AND EM.EmployeeMatchID= DD.EmployeeID
	ORDER BY  [BussinessDate] DESC	
			
	SELECT  _StoreID
			, [StoreName]			AS [EDV_StoreName]
			, [BussinessDate]		AS [EDV_BussinessDate]
			, [POS Field Name]		AS [EDV_POSFieldName]
			, Debit					AS [EDV_Debit]
			, Credit				AS [EDV_Credit]
			, [Hours Worked]		AS [EDV_HrsWorked]
			, [Destination]			AS [EDV_Destination]
			, [Exported On]			AS [EDV_ExportedOn]
			, [Exported By]			AS [EDV_ExportedBy]
			, [ExportStatus]		AS [EDV_ExportStatus]
			, _class
			, _KeyID
			, [Last Modification On] AS [EDV_LastModificationOn]
			, [Last Modification By] AS [EDV_LastModificationBy]
			,  CASE WHEN (RowNumber1 % 2)=1 THEN 
				 CASE WHEN (Colors='Honeydew') THEN  'LightGreen' ELSE 'LightBlue' END
			   ELSE Colors END AS _style
	FROM(
	SELECT _StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]	
		, 'CM_Accounting_ExportDetails' AS  _class
		, @KeyID AS _KeyID	
		, ISNULL(CAST(ADB.ExportedON AS VARCHAR),'') AS [Exported On]
		, ISNULL(CAST(DL.UserName AS VARCHAR),'') AS [Exported By]
		, ADB.[ExportStatus] AS [ExportStatus]	
		, Colors
		, ISNULL(CAST([LastMdfOn] AS VARCHAR),'') AS [Last Modification On]
		, ISNULL(DA.UserName,'') AS [Last Modification By]
		, ROW_NUMBER() OVER( PARTITION BY [BussinessDate], _StoreID ORDER BY [BussinessDate] DESC, _StoreID, Type ASC, OrderNumber) AS [RowNumber1]
		, ROW_NUMBER() OVER( ORDER BY [BussinessDate] DESC , _StoreID ) AS RowNumber
	 FROM #TempStoreDetails AS DP
			--LEFT OUTER JOIN  Export.Log EL WITH (NOLOCK) ON DP._StoreID = EL.StoreID  AND DP.BussinessDate = EL.BusinessDate
			OUTER APPLY ( 
							SELECT TOP 1  ExportedOn
										, ExportStatus
										, ExportedBy 
							FROM  Archive.DetailsData ADBD	 WITH(NOLOCK) 
							WHERE ADBD.DataID = DP._DataID  AND Operation like 'Export%'
							ORDER BY ExportedOn DESC ) AS ADB		
			LEFT OUTER JOIN dbo.Logins DL WITH (NOLOCK) ON DL.LoginID = ADB.ExportedBy
			LEFT OUTER JOIN dbo.Logins DA WITH (NOLOCK) ON DA.LoginID = DP.[LastMdfBy]
	) AS My
	ORDER BY [BussinessDate] DESC, StoreName ASC
	FOR XML RAW ('T')
	
	-- =========  REMOVE TEMP TABLE  ====================================================
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDat', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDat
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDate', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDate
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDetails', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDetails
	END

	IF OBJECT_ID( N'tempdb.dbo.#TempStores', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStores
	END
	-- ==================================================================================
END