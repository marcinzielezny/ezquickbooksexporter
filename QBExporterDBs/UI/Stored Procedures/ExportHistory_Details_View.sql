﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/07/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [UI].[ExportHistory_Details_View]
(
	  @SessionID 		uniqueidentifier
	, @Rows				int = 50
	, @BussinessDay		nvarchar(50)
	, @StoreID			bigint
	, @PageNumber		int = 1 OUTPUT
	, @KeyID			uniqueidentifier = NULL OUTPUT
	, @TotalPages		int = 1 OUTPUT
	, @TotalRows		int = 1 OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
		, @Source				varchar(50)
		, @DateFrom 			datetime
		, @DateTo				datetime
		, @FirstRow				int
		, @LastRow				int
		, @XmlData				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'UI_Accounting_InitData_ExportDetails'

	IF ( ISNULL( @Rows, 0 ) = 0 )
	BEGIN
		SET @Rows = 50
	END
	
	SET @PageNumber = ISNULL( @PageNumber, 1 )
	-- ==================================================================================
 
	-- =========  SET FILTERS  ===========================================================

	SET @DateFrom =CAST(@BussinessDay AS Datetime)
	SET @DateTo = DATEADD(day,1, @DateFrom)
	-- ==================================================================================
		
	-- =========  CREATE TEMP TABLES  ===================================================
			
	CREATE TABLE #TempStoreDetails
	 (	  ArchiveID			bigint
		, _StoreID			bigint
		, [StoreName]		nvarchar(50)
		, [BussinessDate]	nvarchar(50)
		, [POS Field Name]	nvarchar(100)
		, [_PosFieldID]     bigint
		, [OrderNumber]		int
		, Debit				money
		, Credit			money
		, [Hours Worked]	nvarchar(50)
		, [Destination]		nvarchar(100)
		, [Operation]		nvarchar(100)
		, Colors			nvarchar(50)
		, [LastMdfOn]		datetime
		, [LastMdfBy]		bigint
		, [ExportedOn]		datetime
		, [ExportedBy]		bigint
		, [ExportStatus]	nvarchar(50)
		, RN				int
		, ExportNumber		nvarchar(50)
	 ) 
	-- ==================================================================================

	;WITH DateRange AS
	(
		SELECT CAST( @DateFrom AS date ) AS Val
		UNION ALL
		SELECT DATEADD( DAY, 1, Val )
		FROM DateRange
		WHERE Val < CAST( @DateTo AS date )
	)
	SELECT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , [Destination]
		    , [ExportedOn]
		    , ROW_NUMBER() OVER( ORDER BY Data.[BussinessDate] DESC, StoreName, ExportedOn DESC ) AS [RowNumber]
			, ExportNumber
	INTO #TempStoreDat
	FROM (
	SELECT 
		DISTINCT Data.BussinessDate
		, _StoreID
		, StoreName
		, IsDetails
		, Destination
		, CAST(ExportedOn as SMALLDATETIME) AS ExportedOn
		, AD.ExportNumber
	FROM (
	SELECT  @BussinessDay AS [BussinessDate]
		, StoreID AS _StoreID
		, Name AS [StoreName]
		, '' AS [IsDetails]
		, '' AS [Destination]
	FROM Data.Stores WHERE StoreID=@StoreID
		) AS Data INNER JOIN [Archive].[DetailsData] AS AD ON Data._StoreID=AD.StoreID AND Data.[BussinessDate]=AD.[BusinessDate] AND Operation='Export'
	) AS Data
	
		
	SET @TotalRows = ( SELECT COUNT(*) FROM #TempStoreDat)
	SET @TotalPages = CEILING(CAST(@TotalRows as money)/CAST(@Rows as money))
	
	IF (@PageNumber>@TotalPages)
	BEGIN
		SET @PageNumber=@TotalPages
	END
	
	-- ==================================================================================
	SET	@FirstRow = ( @PageNumber - 1 ) * @Rows + 1
	SET @LastRow = @PageNumber * @Rows
	-- ==================================================================================	
	
	SELECT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , [Destination]
		    , [ExportedOn]
		    , [RowNumber]
			,  CASE WHEN (RowNumber % 2)=1 THEN
			   'AliceBlue'
			  ELSE 
			   'Honeydew'
			  END AS Colors
			, [ExportNumber]
	INTO #TempStoreDate
	FROM #TempStoreDat
	WHERE RowNumber BETWEEN @FirstRow AND @LastRow
		
	-- =========  RESULTSET  ============================================================
	SELECT 'EXPORT ARCHIVE FROM ' + CONVERT( varchar(100 ), @DateFrom, 101 ) + ' TO ' + CONVERT( varchar(100 ), @DateTo, 101 )

	
	INSERT INTO #TempStoreDetails
	 (	 _StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [_PosFieldID]
		, [OrderNumber]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]	
		, [Operation]	
		, Colors
		, [LastMdfOn]
		, [LastMdfBy]
		, [ExportedOn]
		, [ExportedBy]
		, [ExportStatus]
		, [RN]
		, [ExportNumber]		
	 ) 
	SELECT 
			DISTINCT 
			   _StoreID
			, [StoreName]
			, [BussinessDate]
			, PF.Name [POS Field Name]
			, PF.PosFieldsID AS [_PosFieldID]
			, OrderNumber 
			, CASE WHEN PF.SpecialGroup = 'Debit' THEN  ISNULL( DD.Value, 0.0 ) ELSE 0.0 END AS Debit
			, CASE WHEN PF.SpecialGroup = 'Credit'  THEN  ISNULL( DD.Value, 0.0 ) ELSE 0.0 END AS Credit
			, [HoursWorked] AS [Hours Worked]
			, ExportedTo AS [Destination]
			, [Operation]
			, Colors
			, DD.[CreatedOn] AS [LastMdf]
			, DD.[CreatedBy] AS [LastMdfBy]
			, DP.[ExportedOn]
			, [ExportedBy]
			, [ExportStatus]
			, RN
			, DP.ExportNumber
	 FROM (
			SELECT 	
					_StoreID
					, [StoreName]
					, [BussinessDate]
					, Destination
					, Colors
					, ExportedOn
					, RowNumber AS RN
					, ExportNumber
			FROM   #TempStoreDate TSD 
			  ) AS DP
	LEFT OUTER JOIN Archive.DetailsData DD ON DP._StoreID = DD.StoreID  AND DP.BussinessDate = DD.BusinessDate  AND DP.ExportNumber=DD.ExportNumber
	LEFT OUTER JOIN Dict.POSFields PF ON DD.PosField=PF.PosFieldsID
	WHERE Operation IS NOT NULL
	ORDER BY  [BussinessDate], _StoreID

	SELECT DISTINCT _StoreID
			, ExportNumber			AS [HD_ExportNumber]
			, [StoreName]			AS [HD_StoreName]
			, [BussinessDate]		AS [HD_BussinessDate]
			, [Operation]			
			, [POS Field Name]		AS [HD_POSFieldName]
			, Debit					AS [HD_Debit]
			, Credit				AS [HD_Credit]
			, ISNULL([Hours Worked],'')		AS [HD_HrsWorked2]
			, [Destination]			AS [HD_Destination]
			, [Exported On]			AS [HD_ExportedOn]
			, [Exported By]			AS [HD_ExportedBy]
			, [ExportStatus]		AS [HD_ExportStatus]
			, _class
			, _KeyID
			, [Last Modification On]	AS [HD_LastModificationOn]
			, [Last Modification By]	AS [HD_LastModificationBy]
			,  CASE WHEN (RowNumber1 % 2)=1 THEN 
				 CASE WHEN (Colors='Honeydew') THEN  'LightGreen' ELSE 'LightBlue' END
			   ELSE Colors END AS _style
			, _ON
	FROM(
	SELECT _StoreID
	    , [ExportNumber]			
		, [StoreName]		
		, [BussinessDate]
		, Operation	
		, [POS Field Name]	    
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]	
		, 'CM_Accounting_ExportDetails' AS  _class
		, @KeyID AS _KeyID	
		, ISNULL(CAST(DP.ExportedON AS VARCHAR),'') AS [Exported On]
		, ISNULL(CAST(DL.UserName AS VARCHAR),'') AS [Exported By]
		, DP.[ExportStatus] AS [ExportStatus]	
		, Colors
		, ISNULL(CAST([LastMdfOn] AS VARCHAR),'') AS [Last Modification On]
		, DA.UserName AS [Last Modification By]
		, ROW_NUMBER() OVER( PARTITION BY [BussinessDate], _StoreID, ExportNumber ORDER BY [BussinessDate] DESC, _StoreID DESC, ExportNumber DESC, OrderNumber ASC) AS [RowNumber1]
		, ROW_NUMBER() OVER( ORDER BY [BussinessDate] DESC , _StoreID DESC, OrderNumber ASC ) AS RowNumber
		, OrderNumber AS _ON
	 FROM #TempStoreDetails AS DP
			-- LEFT OUTER JOIN  Export.Log EL ON DP._StoreID = EL.StoreID  AND DP.BussinessDate = EL.BusinessDate
			LEFT OUTER JOIN dbo.Logins DL ON DL.LoginID = DP.ExportedBy
			LEFT OUTER JOIN dbo.Logins DA ON DA.LoginID = DP.[LastMdfBy]
	) AS My
	ORDER BY [BussinessDate], _StoreID, ExportNumber, _ON
	FOR XML RAW ('T')
	
	-- =========  REMOVE TEMP TABLE  ====================================================
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDat', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDat
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDate', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDate
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDetails', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDetails
	END

	-- ==================================================================================
END