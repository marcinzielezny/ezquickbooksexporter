﻿
/* 2013  ezUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/06/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [UI].[PrepareExportSet] (
	  @SessionID UNIQUEIDENTIFIER
	, @OrganizationID UNIQUEIDENTIFIER
	, @StoreDay NVARCHAR(max)
	, @Message NVARCHAR(255) OUTPUT
	)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID BIGINT
		,@StoreD XML

	-- ==================================================================================
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID(@SessionID)

	IF (@UserID IS NULL)
	BEGIN
		PRINT 'UserID is NULL'

		RETURN
	END

	SET @StoreD = CAST(@StoreDay AS XML)

	-- ===================================================================================
	SELECT ESM.StoreID
		,BussinessDate
		,EC.ConfigID
		,IsZero
	INTO #TempStoreDay
	FROM (
		SELECT T.c.value('@_StoreID', 'bigint') AS StoreID
			,T.c.value('@EX_BussinessDate', 'date') AS BussinessDate
			,T.c.value('@EX_CheckStore', 'bit') AS Selected
			,T.c.value('@_IsZero', 'money') AS IsZero
		FROM @StoreD.nodes('/*') AS T(c)
		) AS Data
	INNER JOIN Export.StoreMatch ESM WITH (NOLOCK) ON Data.StoreID = ESM.StoreID
	INNER JOIN Export.Configuration EC WITH (NOLOCK) ON ESM.ConfigID = EC.ConfigID
	WHERE Data.Selected = 1

	IF (
			EXISTS (
				SELECT TOP 1 1
				FROM #TempStoreDay
				WHERE IsZero <> 0
				)
			)
	BEGIN
		SET @Message = 'Data can''t be exported one of selected stores has incorrect values of Debit/Credit'

		RETURN
	END;

	MERGE [Export].[Log] AS DDD
	USING (
		SELECT BussinessDate
			,StoreID
		FROM (
			SELECT DISTINCT StoreID
				,BussinessDate
			FROM #TempStoreDay
			) AS Data
		) AS T
		ON DDD.StoreID = T.StoreID
			AND DDD.[BusinessDate] = BussinessDate
	WHEN MATCHED
		THEN
			UPDATE
			SET [ExportedOn] = GETDATE()
				,[ExportedBy] = @UserID
				,[Status] = 'In Progress'
				,[ExportNumber] = [ExportNumber] + 1
	WHEN NOT MATCHED
		THEN
			INSERT (
				[StoreID]
				,[BusinessDate]
				,[ExportedOn]
				,[ExportedBy]
				,[Status]
				)
			VALUES (
				T.StoreID
				,BussinessDate
				,GETDATE()
				,@UserID
				,'In Progress'
				);

	-- ========= Return Store Data =======================================================
	SELECT EC.ConfigName
		,EC.ConfigPath
		,EC.ConfigID
		,CAST((
				SELECT   StoreID
					   , TS_D.BussinessDate
					,CAST((
							SELECT PFM.NAME AS FieldName
								,Description AS Description
								,CASE 
									WHEN DDD.SG = 'D'
										THEN (-1) * ABS(DDD.Value)
									WHEN PFM.Name = 'Closing Keep'
										THEN (-1) * ABS(DDD.Value)
									ELSE DDD.Value
									END AS Amount
								,CASE 
									WHEN NoQuantity = 1
										THEN ''
									ELSE '1.00'
									END AS Quantity
								,CASE 
									WHEN NoQuantity = 1
										THEN (- 1) * ABS(DDD.Value)
									ELSE 0
									END AS Rate
								,SM.[QBAStore] AS ClassListID
								,RIGHT('000000000000' + CAST(EL.[ExportLogID] AS VARCHAR(10)) + '/' + CAST(EL.ExportNumber AS VARCHAR(10)), 11) AS ExportID
								,CAST(DDD.BusinessDate AS DATETIME) AS BusinessDate
								,SM.QBAStore AS QBStore
								,TSD.StoreID
								,DDD.DataID
								,SM.ConfigID
								,DS.NAME AS StoreName
							FROM #TempStoreDay TSD
							INNER JOIN [Data].[DetailsData] DDD WITH (NOLOCK) ON TSD.StoreID = DDD.StoreID
								AND TSD.BussinessDate = DDD.BusinessDate
								AND EmployeeID IS NULL
								AND ISNULL(DDD.Value,0) <> 0 
							INNER JOIN [Export].[StoreMatch] SM WITH (NOLOCK) ON DDD.StoreID = SM.StoreID
							INNER JOIN [Data].[Stores] DS WITH (NOLOCK) ON DS.StoreID = SM.StoreID
							INNER JOIN [Dict].[PosFields] PFM WITH (NOLOCK) ON DDD.PosField = PFM.PosFieldsID
								AND (
									PFM.[DetailedSet] = SM.StoreType
									OR PFM.[SummarySet] = SM.StoreType
									)
							INNER JOIN [Export].[Log] EL WITH (NOLOCK) ON TSD.StoreID = EL.StoreID
								AND TSD.BussinessDate = EL.BusinessDate
							WHERE SM.ConfigID = C.ConfigID
								AND TSD.StoreID = TS_D.StoreID
								AND TSD.BussinessDate = TS_D.BussinessDate
							ORDER BY OrderNumber ASC
							FOR XML RAW('Stores')
							) AS XML)
				FROM #TempStoreDay TS_D
				ORDER BY TS_D.BussinessDate ASC
				FOR XML RAW('Store')
				) AS XML)
		,CAST((
				SELECT '' AS blank
					,[QBEmployee] AS QBEmployee
					,'' AS blank1
					,[QBPayType] AS PayRoll
					,[HoursWorked] AS HrsWrkd
					,TSD.BussinessDate
					,SM.[QBAStore] AS ClassListID
					,RIGHT('000000000000' + CAST(EL.[ExportLogID] AS VARCHAR(10)) + '/' + CAST(EL.ExportNumber AS VARCHAR(10)), 11) AS ExportID
					,DDD.DataID
					,SM.ConfigID
					,DDD.EmployeeID
					,DE.NAME AS [EmployeeName]
				FROM #TempStoreDay TSD
				INNER JOIN [Data].[DetailsData] DDD WITH (NOLOCK) ON TSD.StoreID = DDD.StoreID
					AND TSD.BussinessDate = DDD.BusinessDate
				INNER JOIN [Export].[StoreMatch] SM WITH (NOLOCK) ON DDD.StoreID = SM.StoreID
				INNER JOIN [Export].[EmployeeMatch] EM WITH (NOLOCK) ON DDD.EmployeeID = EM.EmployeeMatchID
				INNER JOIN Data.Employee DE WITH (NOLOCK) ON EM.EmployeeID = DE.EmployeeID
				INNER JOIN [Export].[Log] EL WITH (NOLOCK) ON TSD.StoreID = EL.StoreID
					AND TSD.BussinessDate = EL.BusinessDate
				WHERE SM.ConfigID = C.ConfigID
				FOR XML RAW('Employee')
				) AS XML)
	FROM Export.Configuration AS EC
	INNER JOIN (
		SELECT DISTINCT ConfigID
		FROM #TempStoreDay
		) AS C ON EC.ConfigID = C.ConfigID
	FOR XML RAW('Data')

	-- ===================================================================================
	IF OBJECT_ID(N'tempdb.dbo.#TempStoreDay', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDay
	END
END