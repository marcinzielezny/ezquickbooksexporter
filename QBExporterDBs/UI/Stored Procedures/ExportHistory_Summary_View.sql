﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/07/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [UI].[ExportHistory_Summary_View]
(
	  @SessionID 		uniqueidentifier
	, @Rows				int = 50
	, @Period    		varchar(max) = NULL
	, @Stores    		varchar(max) = NULL
	, @PageNumber		int = 1 OUTPUT
	, @KeyID			uniqueidentifier = NULL OUTPUT
	, @TotalPages		int = 1 OUTPUT
	, @TotalRows		int = 1 OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
		, @Source				varchar(50)
		, @DateFrom 			datetime
		, @DateTo				datetime
		, @FirstRow				int
		, @LastRow				int
		, @XmlData				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'ExportHistory_Summary_View'

	IF ( ISNULL( @Rows, 0 ) = 0 )
	BEGIN
		SET @Rows = 50
	END
	
	SET @PageNumber = ISNULL( @PageNumber, 1 )
	-- ==================================================================================
 
	-- =========  SET FILTERS  ===========================================================
	IF ( @KeyID IS NULL )
	BEGIN
		SET @KeyID = NEWID()

		SET @XmlData = CAST( (
				ISNULL( @Period, '' )
				+ ISNULL( @Stores, '' )
			) AS xml )

	EXEC V100.Filter_ByXpath
				@SessionID		= @SessionID
				, @XmlData  	= @XmlData 
				, @XPath		= @Source
				, @Procedure	= @Source
				, @KeyID 		= @KeyID
	END

	SET @DateFrom = V100.FILTER_DateFrom( @KeyID, @SessionID )
	SET @DateTo = V100.FILTER_DateTo( @KeyID, @SessionID )
	-- ==================================================================================
	

	
	-- =========  CREATE TEMP TABLES  ===================================================
	CREATE TABLE #TempStores(
		StoreID bigint
		, Location varchar(50)
		, IsDetails bit  
	)

	EXEC V100.PrepareTempStores
			@SessionID  = @SessionID
			, @UserID  = @UserID
			, @KeyID    = @KeyID
			
			
	CREATE TABLE #TempStoreDetails
	 (	  ArchiveID			bigint
		, _StoreID			bigint
		, [StoreName]		nvarchar(50)
		, [BussinessDate]	nvarchar(50)
		, [POS Field Name]	nvarchar(100)
		, [Finnancial Account] nvarchar(100)
		, [_PosFieldID]     bigint NULL
		, Debit				money NULL
		, Credit			money NULL
		, DebitRoyalty		money NULL
		, CreditRoyalty		money NULL
		, [Hours Worked]	nvarchar(50)
		, [Destination]		nvarchar(100)
		, [Operation]		nvarchar(100)
		, Colors			nvarchar(50)
		, [LastMdfOn]		datetime
		, [LastMdfBy]		bigint
		, [ExportedOn]		datetime
		, [ExportedBy]		bigint
		, [ExportStatus]	nvarchar(50)
		, RN				int
		, [ExportNumber]	nvarchar(50)
	 ) 
	-- ==================================================================================


	-- =========  PREPATE TEMP DATA  ====================================================
	IF DATEDIFF( DAY, @DateFrom, @DateTo ) > 30
	BEGIN
		SET @DateFrom = DATEADD( DAY, -30, @DateTo )
	END 
	ELSE IF DATEDIFF( DAY, @DateFrom, @DateTo ) < 0
	BEGIN
		SET @DateFrom = DATEADD( DAY, 0, @DateTo )
	END 

	;WITH DateRange AS
	(
		SELECT CAST( @DateFrom AS date ) AS Val
		UNION ALL
		SELECT DATEADD( DAY, 1, Val )
		FROM DateRange
		WHERE Val < CAST( @DateTo AS date )
	)
	SELECT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , [Destination]
		    , [ExportedOn]
		    , [ExportNumber]
		    , [RowNumber]
			,  CASE WHEN (RowNumber % 2)=1 THEN
			   'AliceBlue'
			  ELSE 
			   'Honeydew'
			  END AS Colors
	INTO #TempStoreDate
	FROM (
	SELECT 
		DISTINCT Data.BussinessDate
		, _StoreID
		, StoreName
		, IsDetails
		, Destination
		, CAST(ExportedOn as SMALLDATETIME) AS ExportedOn
		, ExportedBy
		, [ExportStatus]
		, AD.ExportNumber
		, [RowNumber]
		
	FROM (
	SELECT BussinessDate
		, _StoreID
		, StoreName
		, IsDetails
		, Destination
		, ROW_NUMBER() OVER( PARTITION BY [BussinessDate] ORDER BY [BussinessDate]) AS [RowNumber]
	FROM (
	SELECT  CONVERT( varchar(100), DR.Val, 101 ) AS [BussinessDate]
		, EX.StoreID AS _StoreID
		, EX.Location AS [StoreName]
		, EX.IsDetails AS [IsDetails]
		, '' AS [Destination]
	FROM DateRange DR
		CROSS JOIN #TempStores EX
		) AS M ) AS Data INNER JOIN 
			[Archive].[DetailsData] AS AD ON Data._StoreID=AD.StoreId AND Data.[BussinessDate]=AD.[BusinessDate]
			 WHERE AD.Operation='Export'
	) AS Data

	-- =========  RESULTSET  ============================================================
	SELECT 'EXPORT ARCHIVE FROM ' + CONVERT( varchar(100 ), @DateFrom, 101 ) + ' TO ' + CONVERT( varchar(100 ), @DateTo, 101 )

	
	INSERT INTO #TempStoreDetails
	 (	  ArchiveID
		,_StoreID			
		, [StoreName]		
		, [BussinessDate]	
		, [POS Field Name]	
		, [Finnancial Account] 
		, [_PosFieldID]     
		, Debit				
		, Credit
		, [Hours Worked]
		, [Destination]	
		, [Operation]	
		, Colors
		, [LastMdfOn]
		, [LastMdfBy]
		, [ExportedOn]
		, [ExportedBy]
		, [ExportStatus]
		, [RN]	
		, ExportNumber	
	 ) 
	SELECT 
			DISTINCT ArchiveDataID
			, _StoreID
			, [StoreName]
			, [BussinessDate]
			, PF.Name [POS Field Name]
			, [ExportAccountName] AS [Finnancial Account]
			, PF.PosFieldsID AS [_PosFieldID]
			, CASE WHEN PF.SpecialGroup = 'Debit' THEN  ISNULL( DD.Value, 0.0 ) ELSE 0.0 END AS Debit
			, CASE WHEN PF.SpecialGroup = 'Credit'  THEN  ISNULL( DD.Value, 0.0 ) ELSE 0.0 END AS Credit
			, [HoursWorked] AS [Hours Worked]
			, ExportedTo AS [Destination]
			, [Operation]
			, Colors
			, DD.[CreatedOn] AS [LastMdf]
			, DD.[CreatedBy] AS [LastMdfBy]
			, DD.[ExportedOn]
			, [ExportedBy]
			, [ExportStatus]
			, RN
			,DP.[ExportNumber]
	 FROM (
			SELECT 	
					_StoreID
					, [StoreName]
					, [BussinessDate]
					, Destination
					, Colors
					, ExportedOn
					, RowNumber AS RN
					, ExportNumber
			FROM   #TempStoreDate TSD 
			  ) AS DP
	LEFT OUTER JOIN Archive.DetailsData DD ON DD.ExportNumber = DP.ExportNumber
	LEFT OUTER JOIN Dict.POSFields PF ON DD.PosField=PF.PosFieldsID
	WHERE Operation IS NOT NULL
	ORDER BY  [BussinessDate] DESC
	
	-- ==================================================================================

	SET @TotalRows = ( SELECT COUNT(*) FROM  #TempStoreDate )
	SET @TotalPages = CEILING( CAST( @TotalRows AS float ) / @Rows )
	-- ==================================================================================

	-- =========  COUNT FIRST AND LAST ROW  =============================================
	IF ( @PageNumber > @TotalPages )
	BEGIN
		SET @PageNumber = @TotalPages
	END

	SET	@FirstRow = ( @PageNumber - 1 ) * @Rows + 1
	SET @LastRow = @PageNumber * @Rows
	-- ==================================================================================
	
	
	SELECT DISTINCT _StoreID
			, [ExportNumber]		AS [HD_ExportNumber]
			, [StoreName]			AS [HD_StoreName]
			, [BussinessDate]		AS [HD_BussinessDate]
			, Debit					AS [HD_Debit]
			, Credit				AS [HD_Credit]
			, DebitRoyalty			AS [HD_DebitRoyalty]
			, CreditRoyalty			AS [HD_CreditRoyalty]
			, [Hours Worked]		AS [HD_HrsWorked]
			, [Destination]			AS [HD_Destination]
			, [Exported On]			AS [HD_ExportedOn]
			, [Exported By]			AS [HD_ExportedBy]
			, [ExportStatus]		AS [HD_ExportStatus]
			--,  CASE WHEN (RowNumber1 % 2)=1 THEN 
			--	 CASE WHEN (Colors='Honeydew') THEN  'LightGreen' ELSE 'LightBlue' END
			--   ELSE Colors END AS _style
	FROM(
	SELECT _StoreID	
		, [ExportNumber]		
		, [StoreName]		
		, [BussinessDate]	
		, SUM(Debit) AS Debit			
		, SUM(Credit) AS Credit
		, SUM(DebitRoyalty) AS DebitRoyalty			
		, SUM(CreditRoyalty) AS CreditRoyalty
		, SUM( CASE WHEN ISNULL( [Hours Worked], '' ) !='' THEN  CAST(LEFT([Hours Worked],CHARINDEX(':', [Hours Worked]) - 1 ) AS decimal(10,2))+CAST(RIGHT(LEFT( [Hours Worked], CHARINDEX(':', [Hours Worked]) + 2 ),2) AS decimal(10,2))/60 ELSE 0 END ) AS [Hours Worked] 
		, [Destination]	
		, ISNULL(CAST(DP.ExportedOn AS VARCHAR),'') AS [Exported On]
		, ISNULL(CAST(DL.UserName AS VARCHAR),'') AS [Exported By]
		, [ExportStatus] AS [ExportStatus]	
		, Colors
		, ROW_NUMBER() OVER( PARTITION BY [BussinessDate], _StoreID ORDER BY [BussinessDate] DESC, _StoreID, ExportedOn DESC) AS [RowNumber1]
		, ROW_NUMBER() OVER( ORDER BY [BussinessDate] DESC , _StoreID, ExportedOn DESC ) AS RowNumber
	 FROM #TempStoreDetails AS DP
			LEFT OUTER JOIN dbo.Logins DL WITH (NOLOCK) ON DL.LoginID = ExportedBy
			LEFT OUTER JOIN dbo.Logins DA WITH (NOLOCK) ON DA.LoginID = DP.[LastMdfBy]
	GROUP BY _StoreID, [StoreName], [BussinessDate]	, [Destination], ExportedOn , DL.UserName , ExportStatus, Colors, [ExportNumber]
	) AS My
	WHERE RowNumber BETWEEN @FirstRow AND @LastRow
	FOR XML RAW ('T')
	
	-- =========  REMOVE TEMP TABLE  ====================================================
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDate', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDate
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDetails', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDetails
	END

	IF OBJECT_ID( N'tempdb.dbo.#TempStores', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStores
	END
	-- ==================================================================================
END