﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/06/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [UI].[ToExport_View]
(
	  @SessionID 		uniqueidentifier
	, @Rows				int = 50
	, @Period    		varchar(max) = NULL
	, @Stores    		varchar(max) = NULL
	, @ShowExported		bit = 0
	, @PageNumber		int = 1 OUTPUT
	, @KeyID			uniqueidentifier = NULL OUTPUT
	, @TotalPages		int = 1 OUTPUT
	, @TotalRows		int = 1 OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
		, @Source				varchar(50)
		, @DateFrom 			datetime
		, @DateTo				datetime
		, @FirstRow				int
		, @LastRow				int
		, @XmlData				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'UI_Accounting_InitData_ExportDetails'

	IF ( ISNULL( @Rows, 0 ) = 0 )
	BEGIN
		SET @Rows = 50
	END
	
	SET @PageNumber = ISNULL( @PageNumber, 1 )
	-- ==================================================================================

	-- =========  SET FILTERS  ===========================================================
	IF ( @KeyID IS NULL )
	BEGIN
		SET @KeyID = NEWID()

		SET @XmlData = CAST( (
				ISNULL( @Period, '' )
				+ ISNULL( @Stores, '' )
			) AS xml )

		EXEC V100.Filter_ByXpath
				@SessionID		= @SessionID
				, @XmlData  	= @XmlData 
				, @XPath		= @Source
				, @Procedure	= @Source
				, @KeyID 		= @KeyID
	END

	SET @DateFrom = V100.FILTER_DateFrom( @KeyID, @SessionID )
	SET @DateTo = V100.FILTER_DateTo( @KeyID, @SessionID )
	-- ==================================================================================

	-- =========  CREATE TEMP TABLES  ===================================================
	CREATE TABLE #TempStores(
		StoreID bigint
		, Location varchar(50)
		, IsDetails bit  
	)

	EXEC V100.PrepareTempStores
			@SessionID  = @SessionID
			, @UserID  = @UserID
			, @KeyID    = @KeyID
	-- ==================================================================================

	-- =========  PREPATE TEMP DATA  ====================================================
	IF DATEDIFF( DAY, @DateFrom, @DateTo ) > 30
	BEGIN
		SET @DateFrom = DATEADD( DAY, -30, @DateTo )
	END 
	ELSE IF DATEDIFF( DAY, @DateFrom, @DateTo ) < 0
	BEGIN
		SET @DateFrom = DATEADD( DAY, 0, @DateTo )
	END 

	;WITH DateRange AS
	(
		SELECT CAST( @DateFrom AS date ) AS Val
		UNION ALL
		SELECT DATEADD( DAY, 1, Val )
		FROM DateRange
		WHERE Val < CAST( @DateTo AS date )
	)
	SELECT [BussinessDate]
		    , _StoreID
		    , [StoreName]
		    , [IsDetails]
		    , [Destination]
		    , [ExportedOn]
		    , [ExportStatus]
		    , [RowNumber]
			,  CASE WHEN (RowNumber % 2)=1 THEN
			   'AliceBlue'
			  ELSE 
			   'Honeydew'
			  END AS Colors
	INTO #TempStoreDate
	FROM (
	SELECT  CONVERT( varchar(100), DR.Val, 101 ) AS [BussinessDate]
		, EX.StoreID AS _StoreID
		, EX.Location AS [StoreName]
		, EX.IsDetails AS [IsDetails]
		, '' AS [Destination]
		, '' AS [ExportedOn]
		, '' AS [ExportStatus]
		, ROW_NUMBER() OVER( ORDER BY DR.Val DESC, EX.Location ) AS [RowNumber]
	FROM DateRange DR
		CROSS JOIN #TempStores EX) AS Data

	-- ==================================================================================

	-- =========  RESULTSET  ============================================================
	SELECT 'CHECK STORES TO EXPORT FROM ' + CONVERT( varchar(100 ), @DateFrom, 101 ) + ' TO ' + CONVERT( varchar(100 ), @DateTo, 101 )
	
	SELECT DISTINCT 
			'false'													AS EX_CheckStore
			,_StoreID
			, [StoreName]											AS EX_StoreName
			, [BussinessDate]										AS EX_BussinessDate
			--, _class
			, @KeyID												AS _KeyID
			, ISNULL(CAST([ExportedOn] AS VARCHAR),'') 				AS EX_ExportedOn
			, ISNULL([ExportedBy],'')								AS EX_ExportedBy
			, CASE WHEN Debit = Credit THEN 'Data can be exported' ELSE 
			   'Data can''t be exported Debit/Credit not equal' END AS [EX_Information]
			, CASE WHEN Debit = Credit THEN 0 ELSE 1 END			AS _IsZero
			, CASE WHEN ExportedOn is NULL THEN 0 
			  ELSE 1 END											AS _Exported
			, Debit 
			, Credit
			, RowNumber1
	INTO #TempDataToPaging
	FROM(
		SELECT 
			 _StoreID
			, [StoreName]
			, [BussinessDate]
			, EL.ExportedOn		AS [ExportedOn]
			, DL.UserName		AS [ExportedBy]
			, SUM(CASE WHEN ((My.SpecialGroup = 'Debit' AND  DD.SG IS NULL ) OR DD.SG = 'D') THEN  ISNULL( DD.Value, 0.0 ) ELSE 0.0 END ) AS Debit
			, SUM(CASE WHEN ((My.SpecialGroup = 'Credit' AND  DD.SG IS NULL) OR DD.SG = 'C') THEN  ISNULL( DD.Value, 0.0 ) ELSE 0.0 END ) AS Credit
			, ROW_NUMBER() OVER( ORDER BY [BussinessDate] DESC, [StoreName]) AS [RowNumber1]
		FROM   #TempStoreDate TSD 
				INNER JOIN Data.DetailsData DD WITH (NOLOCK) ON TSD._StoreID = DD.StoreID  AND TSD.BussinessDate = DD.BusinessDate 
				INNER JOIN Export.StoreMatch SM WITH (NOLOCK) ON TSD._StoreID = SM.StoreID
				INNER JOIN  [Dict].[POSFields] My WITH (NOLOCK) ON  (My.PosFieldsID = DD.PosField AND ( My.[DetailedSet] = SM.StoreType OR My.[SummarySet] = SM.StoreType ) ) OR HoursWorked IS NOT NULL
				LEFT OUTER JOIN  Export.Log EL WITH (NOLOCK) ON TSD._StoreID = EL.StoreID  AND TSD.BussinessDate = EL.BusinessDate
				LEFT OUTER JOIN dbo.Logins DL WITH (NOLOCK) ON DL.LoginID = EL.ExportedBy
		WHERE (@ShowExported=1 OR (@ShowExported=0 AND EL.StoreID is NULL))
		GROUP BY _StoreID, [StoreName], [BussinessDate], EL.ExportedOn , DL.UserName
			
	) AS My

	SET @TotalRows = (SELECT COUNT(*) FROM #TempDataToPaging )
	SET @TotalPages = CEILING( CAST( @TotalRows AS float ) / @Rows )
	-- ==================================================================================

	-- =========  COUNT FIRST AND LAST ROW  =============================================
	IF ( @PageNumber > @TotalPages )
	BEGIN
		SET @PageNumber = @TotalPages
	END

	SET	@FirstRow = ( @PageNumber - 1 ) * @Rows + 1
	SET @LastRow = @PageNumber * @Rows
	-- ==================================================================================
	-- ==================================================================================

	SELECT   EX_CheckStore
			, _StoreID
			, EX_StoreName
			, EX_BussinessDate
			, _KeyID
			, EX_ExportedOn
			, EX_ExportedBy
			, [EX_Information]
			, _IsZero
			, _Exported
	FROM #TempDataToPaging
	--WHERE RowNumber1 BETWEEN @FirstRow AND @LastRow
	ORDER BY EX_BussinessDate DESC
	FOR XML RAW ('EXPORT')
	
	-- =========  REMOVE TEMP TABLE  ====================================================
	IF OBJECT_ID( N'tempdb.dbo.#TempStoreDate', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStoreDate
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempStores', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempStores
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TempDataToPaging', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TempDataToPaging
	END
	-- ==================================================================================
END