﻿/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/06/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [UI].[EditStoreDay_Save]
(
	  @SessionID 		uniqueidentifier
	, @XmlData			nvarchar(MAX)
	, @XmlEmployee		nvarchar(MAX)
	, @Message			nvarchar(250) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		  @UserID				bigint
		, @Source				varchar(50)
		, @Xml				xml
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	SET @Source = 'UI_Accounting_EditDay_SaveData'
	
	SET @Xml= CAST( @XmlData AS XML)
	-- ==================================================================================
	
	-- =========  GET Values from XML  ==================================================
	SELECT   _StoreID 
  			 , ED_StoreName
  			 , ED_BussinessDate
  			 , ED_POSFieldName
  			 , ED_FinnancialAccount
  			 , _PosFieldID
  			 , ED_Debit
  			 , ED_Credit
  			 , ED_HrsWorked 
  			 , Description
  			 , CASE WHEN ED_Debit !=0 AND ED_Credit != 0 THEN 1 ELSE 0 END UnValid 
	INTO #Temp
	FROM (
	SELECT
  		  T.c.value('@_StoreID','bigint')			AS _StoreID
  		 ,T.c.value('@ED_StoreName','nvarchar(50)')	AS ED_StoreName
  		 ,T.c.value('@ED_BussinessDate','date')		AS ED_BussinessDate
  		 ,T.c.value('@ED_POSFieldName','nvarchar(50)')	AS ED_POSFieldName
  		 ,T.c.value('@ED_FinnancialAccount','nvarchar(50)')	AS ED_FinnancialAccount
  		 ,T.c.value('@_PosFieldID','bigint')		AS _PosFieldID
  		 ,T.c.value('@ED_Debit','money')			AS ED_Debit
  		 ,T.c.value('@ED_Credit','money')			AS ED_Credit
  		 ,T.c.value('@ED_HrsWorked','money')		AS ED_HrsWorked 
  		 ,T.c.value('@Description', 'nvarchar(255)') AS Description
	FROM @Xml.nodes('/Data/*') as T(c) ) AS MY
	
	-- ==================================================================================
		
	IF (EXISTS(SELECT TOP 1 1 FROM #Temp WHERE UnValid=1))
	BEGIN
	
		SET @Message='Values are incorrect only debit or credit can be filled in each position!'
		RETURN 
	END
	
	-- ============Merge Account Data ============================================================
	;MERGE Data .DetailsData AS DDD
	USING (
		SELECT
  			   _StoreID AS StoreID
  			 , ED_StoreName
  			 , ED_BussinessDate
  			 , ED_POSFieldName
  			 , ED_FinnancialAccount
  			 , _PosFieldID
  			 , ED_Debit
  			 , ED_Credit
  			 , ED_HrsWorked 
  			 , Description
  		FROM #Temp
	) AS T 
	ON DDD.StoreID = T.StoreID
		AND DDD.[BusinessDate] = ED_BussinessDate
		AND DDD.PosField=T._PosFieldID
	WHEN MATCHED THEN
		UPDATE
		SET   LastModificationOn = GETDATE()
			, LastModificationBy = @UserID
			, Value = CASE WHEN ISNULL(ED_Credit, 0) <> 0 THEN ED_Credit ELSE ED_Debit END 
			, HoursWorked = ED_HrsWorked 
			, Description = T.Description
	WHEN NOT MATCHED THEN 
		INSERT ( StoreID, BusinessDate, PosField,   Value,  HoursWorked, CreatedBy, Description) 
		VALUES ( T.StoreID, ED_BussinessDate, T._PosFieldID,  CASE WHEN ISNULL(ED_Credit, 0) <> 0 THEN ED_Credit ELSE ED_Debit END ,  ED_HrsWorked, @UserID, T.Description  )
	;
	
	-- =========  GET Values from XML  ==================================================
	
	SET @Xml= CAST( @XmlEmployee AS XML)
	
	SELECT StoreID
		, ED_BussinessDate
		, _EmployeeID
		, ED_HrsWorked
	INTO #TemporaryData
	FROM (
	SELECT
  		  T.c.value('@_StoreID','bigint')			AS StoreID
  		 ,T.c.value('@ED_BussinessDate','date')		AS ED_BussinessDate
  		 ,T.c.value('@_EmployeeID','bigint')		AS _EmployeeID
  		 ,T.c.value('@ED_HrsWorked','nvarchar(50)')		AS ED_HrsWorked 
	FROM @Xml.nodes('/Data/*') as T(c)
	) AS N
	
	-- ==================================================================================
	
	-- ============Merge Employee Data ==================================================
	;

	MERGE Data.DetailsData AS DDD
	USING (
		SELECT StoreID
			,ED_BussinessDate
			,_EmployeeID
			,CAST(CAST(ED_HrsWorked AS TIME) AS VARCHAR(8)) AS ED_HrsWorked
		FROM (
			SELECT T.c.value('@_StoreID', 'bigint') AS StoreID
				,T.c.value('@ED_BussinessDate', 'date') AS ED_BussinessDate
				,T.c.value('@_EmployeeID', 'bigint') AS _EmployeeID
				,T.c.value('@ED_HrsWorked', 'nvarchar(50)') AS ED_HrsWorked
			FROM @Xml.nodes('/Data/*') AS T(c)
			) AS M
		) AS T
		ON DDD.StoreID = T.StoreID
			AND DDD.[BusinessDate] = ED_BussinessDate
			AND DDD.EmployeeID = T._EmployeeID
	WHEN MATCHED
		THEN
			UPDATE
			SET LastModificationOn = GETDATE()
				,LastModificationBy = @UserID
				,HoursWorked = ED_HrsWorked
	WHEN NOT MATCHED
		THEN
			INSERT (
				StoreID
				,BusinessDate
				,EmployeeID
				,HoursWorked
				,CreatedBy
				)
			VALUES (
				T.StoreID
				,ED_BussinessDate
				,T._EmployeeID
				,ED_HrsWorked
				,@UserID
				)

	;

	-- ==================================================================================
	
	IF OBJECT_ID( N'tempdb.dbo.#Temp', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #Temp
	END
	
	IF OBJECT_ID( N'tempdb.dbo.#TemporaryData', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #TemporaryData
	END
	
	
END