﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [UI].[Config_View]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END

	-- =========  RESULTSET  ============================================================
	SELECT 'EXPORT CONFIGURATION'

	SELECT ConfigID AS _ConfigID
		, [ConfigName] AS Name
		, [ConfigPath] AS FilePath
		, Convert(varchar(20),[CreatedOn],120)  AS [CO_CreatedOn]
		, CAST((
					SELECT DS.Name		AS Name
						  ,DS.StoreID	AS _StoreID
						  ,ES.ConfigID  AS _ConfigID
						  ,CAST((	
							SELECT 
								   EE.Name		 AS Name			
								 , CD.Name		 AS [QuickBooks Employee]
								 , EM.EXPayType  AS [Excel Paytype]
								 , CC.Name		AS [QuickBooks Paytype]
							FROM Export.EmployeeMatch EM WITH (NOLOCK) 
								INNER JOIN Data.Employee AS EE WITH (NOLOCK) ON EM.EmployeeID = EE.EmployeeID
								LEFT JOIN Dict.ConfigFields AS CC WITH (NOLOCK) ON CC.ConfigID = EC.ConfigID AND EM.QBPayType = CC.LongAccountNumber
								LEFT JOIN Dict.ConfigFields AS CD WITH (NOLOCK) ON CD.ConfigID = EC.ConfigID AND EM.QBEmployee = CD.LongAccountNumber
							WHERE ES.StoreID = EE.StoreID
							FOR XML RAW( 'E' )
							) AS XML)
					FROM Export.StoreMatch ES WITH (NOLOCK) 
						INNER JOIN Data.Stores AS DS WITH (NOLOCK)  
						ON ES.StoreID=DS.StoreID
					WHERE ES.ConfigID = EC.ConfigID
					FOR XML RAW( 'S' )) 
				AS XML)
		, 'CM_Accounting_ExportConfiguration' AS  _class
		, '0' AS _selected
	FROM Export.Configuration EC with (NOLOCK)
	WHERE OwnerID=@UserID
	FOR XML RAW( 'C' )
	-- ==================================================================================
END