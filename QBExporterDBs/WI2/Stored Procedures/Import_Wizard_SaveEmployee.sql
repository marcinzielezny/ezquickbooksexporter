﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Import_Wizard_SaveEmployee] (
	 @SessionID UNIQUEIDENTIFIER
	,@OrganizationID UNIQUEIDENTIFIER
	,@EmployeeData NVARCHAR(max)
	,@QBEmployee NVARCHAR(MAX)
	,@QBPayType NVARCHAR(MAX)
	)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID BIGINT
		,@EmployeeXML XML
		,@EmployQBXML XML 
		,@PayQBXML XML

	-- ==================================================================================
	SET @EmployeeXML = CAST(@EmployeeData AS XML)
	SET @EmployQBXML = CAST(@QBEmployee AS XML) 
	SET @PayQBXML	 = CAST(@QBPayType AS XML)

	CREATE TABLE #Diction (
		Value NVARCHAR(50)
		,ValueID NVARCHAR(50)
		,ShortNumber NVARCHAR(50)
		,Type NVARCHAR(50)
		,AccountType NVARCHAR(50)
		,ConfigID BIGINT
	)

	PRINT ('Employee')
	
	SELECT EmployeeID
		,QBEmployee
		,PayType
		,EXPayType
		,ConfigID
	INTO #Employee
	FROM (
		SELECT T.c.value('@_EmployeeID', 'bigint') AS EmployeeID
			,RTRIM(T.c.value('@AC_QBEmployee', 'nvarchar(50)')) AS QBEmployee
			,T.c.value('@AC_QBPayType', 'nvarchar(50)') AS PayType
			,T.c.value('@AC_ExcelPayType', 'nvarchar(20)') AS EXPayType
			,T.c.value('@_modified', 'bit') AS Modified
			,T.C.value('@_ConfigID', 'int') AS ConfigID
		FROM @EmployeeXML.nodes('/*') AS T(c)
		) AS MS
	WHERE ISNULL(QBEmployee, '') <> ''

		PRINT ('MERGE Employee')

	MERGE [Export].[EmployeeMatch] T
	USING (
		SELECT EmployeeID
			,QBEmployee
			,PayType
			,EXPayType
			,ConfigID
		FROM #Employee
		) AS S
		ON T.EmployeeID = S.EmployeeID
			AND T.EXPayType = S.EXPayType
			AND T.ConfigID = S.ConfigID
	WHEN MATCHED
		THEN
			UPDATE
			SET [QBEmployee] = S.QBEmployee
				,[QBPayType] = PayType
				,[ModifiedOn] = GETDATE()
				,[ModifiedBy] = @UserID
	WHEN NOT MATCHED BY TARGET
		THEN
			INSERT (
				[ConfigID]
				,[EmployeeID]
				,[QBEmployee]
				,[QBPayType]
				,[EXPayType]
				,[ModifiedOn]
				,[ModifiedBy]
				)
			VALUES (
				S.ConfigID
				,S.[EmployeeID]
				,S.QBEmployee
				,PayType
				,EXPayType
				,GETDATE()
				,@UserID
				);

	INSERT INTO #Diction (
		Value 
		,ValueID 
		,ShortNumber
		,Type 
		,ConfigID 
		)
	SELECT Value
		,ValueID
		,ShortNumber
		,Type
		,ConfigID
	FROM (
		SELECT T.c.value('@Value', 'nvarchar(50)') AS Value
			,T.c.value('@ValueID', 'nvarchar(50)') AS ValueID
			,T.c.value('@ShortNumber', 'nvarchar(50)') AS ShortNumber
			,'Employee' AS Type
		FROM @EmployQBXML.nodes('/*') AS T(c)
		
		UNION ALL
		
		SELECT T.c.value('@Value', 'nvarchar(50)') AS Value
			,T.c.value('@ValueID', 'nvarchar(50)') AS ValueID
			,T.c.value('@ShortNumber', 'nvarchar(50)') AS ShortNumber
			,'PayType' AS Type
		FROM @PayQBXML.nodes('/*') AS T(c)
		) AS M
		,#Employee E
	WHERE ISNULL(Value, '') <> ''
		AND (
			E.QBEmployee = M.ValueID
			OR E.PayType = M.ValueID
			)

	MERGE [Dict].[ConfigFields] AS T
	USING (
		SELECT DISTINCT Value
			,ValueID
			,Type
			,ShortNumber
			,ConfigID
		FROM #Diction
		) AS S
		ON T.LongAccountNumber = S.ValueID
			AND T.ConfigID = S.ConfigID
			AND T.[ListType] = S.[Type]
	WHEN MATCHED
		THEN
			UPDATE
			SET [Name] = Value
	WHEN NOT MATCHED BY TARGET
		THEN
			INSERT (
				[Name]
				,[ShortAccountNumber]
				,[LongAccountNumber]
				,[ConfigID]
				,[ListType]
				)
			VALUES (
				[Value]
				,[ShortNumber]
				,[ValueID]
				,[ConfigID]
				,[Type]
				);

	IF OBJECT_ID(N'tempdb.dbo.#Employee', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #Employee
	END

	IF OBJECT_ID(N'tempdb.dbo.#Diction', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #Diction
	END
	
END