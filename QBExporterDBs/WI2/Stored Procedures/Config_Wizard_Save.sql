﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Config_Wizard_Save] (
	@SessionID UNIQUEIDENTIFIER
	,@OrganizationID UNIQUEIDENTIFIER
	,@FilePath NVARCHAR(max)
	,@ConfigName NVARCHAR(max)
	,@ConfigID BIGINT
	,@Mode NVARCHAR(max)
	,@StoresDict NVARCHAR(max)
	,@StoresQBDict NVARCHAR(max)
	,@StoresMatch NVARCHAR(max)
	,@EmployeesDict NVARCHAR(max)
	,@EmployeesMatch NVARCHAR(max)
	,@EmployeesQBDict NVARCHAR(max)
	,@PayQBDict NVARCHAR(max)
	,@Message NVARCHAR(250) OUTPUT
	)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID BIGINT
		,@StoresEXXML XML
		,@StoresQBXML XML
		,@StoresData XML
		,@EmployEXXML XML
		,@EmployQBXML XML
		,@EmployData XML
		,@PayQBXML XML

	-- ==================================================================================
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID(@SessionID)

	IF (@UserID IS NULL)
	BEGIN
		PRINT 'UserID is NULL'

		RETURN
	END

	SET @Message = 'OK'
	SET @StoresEXXML = CAST(@StoresDict AS XML)
	SET @StoresQBXML = CAST(@StoresQBDict AS XML)
	SET @StoresData = CAST(@StoresMatch AS XML)
	SET @EmployEXXML = CAST(@EmployeesDict AS XML)
	SET @EmployQBXML = CAST(@EmployeesQBDict AS XML)
	SET @EmployData = CAST(@EmployeesMatch AS XML)
	SET @PayQBXML = CAST(@PayQBDict AS XML)

	CREATE TABLE #Diction (
		Value NVARCHAR(50)
		,ValueID NVARCHAR(50)
		,ShortNumber NVARCHAR(50)
		,Type NVARCHAR(50)
		,AccountType NVARCHAR(50)
		)

	INSERT INTO #Diction (
		Value
		,ValueID
		,Type
		,ShortNumber
		)
	SELECT Value
		,ValueID
		,Type
		,ShortNumber
	FROM (
		SELECT T.c.value('@Value', 'nvarchar(50)') AS Value
			,T.c.value('@ValueID', 'nvarchar(50)') AS ValueID
			,T.c.value('@ShortNumber', 'nvarchar(50)') AS ShortNumber
			,'Store' AS Type
		FROM @StoresQBXML.nodes('/*') AS T(c)
		
		UNION ALL
		
		SELECT T.c.value('@Value', 'nvarchar(50)') AS Value
			,T.c.value('@ValueID', 'nvarchar(50)') AS ValueID
			,T.c.value('@ShortNumber', 'nvarchar(50)') AS ShortNumber
			,'Employee' AS Type
		FROM @EmployQBXML.nodes('/*') AS T(c)
		
		UNION ALL
		
		SELECT T.c.value('@Value', 'nvarchar(50)') AS Value
			,T.c.value('@ValueID', 'nvarchar(50)') AS ValueID
			,T.c.value('@ShortNumber', 'nvarchar(50)') AS ShortNumber
			,'PayType' AS Type
		FROM @PayQBXML.nodes('/*') AS T(c)
		) AS M
	WHERE ISNULL( Value, '' ) <> ''

	SELECT T.c.value('@_StoreID', 'bigint') AS StoreID
		,T.c.value('@AC_StoreName', 'nvarchar(50)') AS StoreName
		,T.c.value('@AC_StoreSelected', 'bit') AS Selected
		,T.c.value('@AC_QBClass', 'nvarchar(50)') AS QBClass
		, ISNULL( T.c.value('@AC_IsPayrollOnly', 'bit'), 0 ) AS IsPayroll
		, ISNULL( T.c.value('@AC_SummaryControlsheet', 'bit'), 0 ) AS AC_SummaryControlsheet
		, ISNULL( T.c.value('@AC_DetailsControlsheet', 'bit'), 0 ) AS AC_DetailsControlsheet
	INTO #StoreMatch
	FROM @StoresData.nodes('/*') AS T(c)

	SELECT 
	     T.c.value('@AC_StoreName', 'nvarchar(50)') AS StoreName
		,NEWID() AS RecordID
	INTO #StoreEx
	FROM @StoresEXXML.nodes('/*') AS T(C)

	MERGE Data.Stores AS T
	USING (
		SELECT 
		     StoreName
			,RecordID
		FROM #StoreEx
		) AS S
		ON T.NAME = S.StoreName
	WHEN NOT MATCHED BY TARGET
		THEN
			INSERT (
				NAME
				,RecordID
				)
			VALUES (
				StoreName
				,RecordID
				);

	INSERT INTO [Data].[UsersPermissions] (
		 [UserID]
		,[Type]
		,[StoreID]
		,[Allow]
		)
	SELECT 
	     @UserID
		,'Manager'
		,ST.StoreID
		,1
	FROM #StoreEx EX
	INNER JOIN Data.Stores ST WITH (NOLOCK) ON EX.StoreName = ST.NAME
	LEFT JOIN [Data].[UsersPermissions] UP WITH (NOLOCK) ON UserID = @UserID
		AND ST.StoreID = UP.StoreID
	WHERE UP.UserID IS NULL

	-- ========= Validation ===========================================================	
	IF (
			NOT EXISTS (
				SELECT TOP 1 1
				FROM #StoreMatch
				WHERE Selected = 1
				)
			)
	BEGIN
		SET @Message = 'Please choose stores for configuration'

		RETURN
	END

	IF (
			EXISTS (
				SELECT ConfigID
				FROM Export.StoreMatch ESM
				INNER JOIN #StoreMatch SM ON ESM.StoreID = SM.StoreID
					AND ConfigID != @ConfigID
					AND SM.Selected = 1
				)
			)
	BEGIN
		SET @Message = 'Each store can only be configured to one QuickBooks file.  You are receiving this message because you are attempting to configure the same store to multiple QuickBooks files.  Please review your configuration.'

		RETURN
	END

	IF (@ConfigID = 0)
	BEGIN
		-- ========= ADD NEW CONFIGURATION ==================================================	
		INSERT INTO [Export].[Configuration] (
			[ConfigName]
			,[ConfigPath]
			,[OwnerID]
			,[IsActive]
			,[CreatedOn]
			,[CreatedBy]
			)
		VALUES (
			@ConfigName
			,@FilePath
			,@UserID
			,1
			,GETDATE()
			,@UserID
			)

		-- ==================================================================================	
		SET @ConfigID = @@IDENTITY

		-- =========  STORES  ===============================================================
		
		SELECT 
			 @ConfigID
			,ST.StoreID
			,QBClass
			,GETDATE()
			,@UserID
			,IsPayroll
			, AC_SummaryControlsheet 
			, AC_DetailsControlsheet 
			, CASE WHEN AC_SummaryControlsheet = 0 AND AC_DetailsControlsheet = 0 THEN 2
				   WHEN AC_SummaryControlsheet = 1 AND AC_DetailsControlsheet = 0 THEN 0  ELSE 1 END				   
		FROM #StoreMatch AS Data
		INNER JOIN Data.Stores AS ST WITH (NOLOCK) ON Data.StoreName = ST.NAME
		WHERE Selected = 1


		INSERT INTO Export.StoreMatch (
			[ConfigID]
			,[StoreID]
			,[QBAStore]
			,[ModifiedOn]
			,[ModifiedBy]
			,[IsPayroll]
			,[StoreType]
			)
		SELECT 
			 @ConfigID
			,ST.StoreID
			,QBClass
			,GETDATE()
			,@UserID
			,IsPayroll
			, CASE WHEN AC_SummaryControlsheet = 0 AND AC_DetailsControlsheet = 0 THEN 2
				   WHEN AC_SummaryControlsheet = 1 AND AC_DetailsControlsheet = 0 THEN 0  ELSE 1 END				   
		FROM #StoreMatch AS Data
		INNER JOIN Data.Stores AS ST WITH (NOLOCK) ON Data.StoreName = ST.NAME
		WHERE Selected = 1

		-- ==================================================================================
		-- ==================================================================================
		-- =========  EmployeesMatch  =======================================================
		SELECT 
		     T.c.value('@AC_StoreName', 'nvarchar(max)') AS StoreName
			,T.c.value('@AC_Employee', 'nvarchar(max)') AS EmployeeName
			,T.c.value('@_EmployeeID', 'bigint') AS EmployeeID
			,T.c.value('@AC_QBEmployee', 'nvarchar(50)') AS QBEmployee
			,T.c.value('@AC_ExcelPayType', 'nvarchar(20)') AS EXPayType
			,T.c.value('@AC_QBPayType', 'nvarchar(50)') AS PayType
			,T.c.value('@_modified', 'bit') AS Modified
			,DS.StoreID
		INTO #Employee
		FROM @EmployData.nodes('/*') AS T(c)
		INNER JOIN Data.Stores DS ON T.c.value('@AC_StoreName', 'nvarchar(max)') = DS.NAME 

		INSERT INTO Data.Employee (
			 [StoreID]
			,[Name]
			,[EXPayType]
			)
		SELECT DISTINCT 
			 EE.StoreID
			,EmployeeName
			,EE.EXPayType
		FROM (	
				SELECT 
					 T.c.value('@EX_StoreNumber', 'nvarchar(max)') AS StoreName
					,T.c.value('@EX_FirstName', 'nvarchar(max)') +' '+T.c.value('@EX_LastName', 'nvarchar(max)')  AS EmployeeName
					,T.c.value('@EX_PayType', 'nvarchar(20)') AS EXPayType
					,DS.StoreID
				FROM @EmployEXXML.nodes('/*') AS T(c)
				INNER JOIN Data.Stores DS ON T.c.value('@EX_StoreNumber', 'nvarchar(max)') = DS.NAME
		) AS EE
		LEFT JOIN Data.Employee DE ON DE.StoreID = EE.StoreID
			AND EE.EmployeeName = DE.NAME AND EE.EXPayType = DE.EXPayType
		WHERE DE.EmployeeID IS NULL

		INSERT INTO [Export].[EmployeeMatch] (
			 [ConfigID]
			,[EmployeeID]
			,[QBEmployee]
			,[QBPayType]
			,[EXPayType]
			,[ModifiedOn]
			,[ModifiedBy]
			)
		SELECT @ConfigID
			,DE.EmployeeID
			,EE.QBEmployee
			,PayType
			,EE.EXPayType
			,GETDATE()
			,@UserID
		FROM #Employee AS EE
		INNER JOIN Data.Employee DE ON DE.NAME = EE.EmployeeName AND EE.EXPayType = DE.EXPayType
			AND DE.StoreID = EE.StoreID
		WHERE EE.Modified = 1
			-- ==================================================================================
	END
	ELSE
	BEGIN
		-- ========= UPDATE CONFIGURATION ==================================================	
		UPDATE Export.Configuration
		SET [ConfigName] = @ConfigName
			,[ConfigPath] = @FilePath
			,[ModifiedOn] = GETDATE()
			,[ModifiedBy] = @UserID
		WHERE ConfigID = @ConfigID

		-- ========= UPDATE Stores ==================================================	
		MERGE Export.StoreMatch T
		USING (
			SELECT StoreID
				,Selected
				,QBClass
				,IsPayroll
				,CASE WHEN AC_SummaryControlsheet = 0 AND AC_DetailsControlsheet = 0 THEN 2
				   WHEN AC_SummaryControlsheet = 1 THEN 0  ELSE 1 END	AS StoreType
			FROM #StoreMatch
			WHERE Selected = 1
			) AS S
			ON T.StoreID = S.StoreID
				AND T.ConfigID = @ConfigID
		WHEN MATCHED
			THEN
				UPDATE
				SET [QBAStore] = S.QBClass
					,[ModifiedOn] = GETDATE()
					,[ModifiedBy] = @UserID
					,[IsPayroll] = S.IsPayroll
					,[StoreType] = S.StoreType
		WHEN NOT MATCHED BY TARGET
			THEN
				INSERT (
					[ConfigID]
					,[StoreID]
					,[QBAStore]
					,[ModifiedOn]
					,[ModifiedBy]
					,[IsPayroll]
					,[StoreType]
					)
				VALUES (
					@ConfigID
					,S.StoreID
					,S.QBClass
					,GETDATE()
					,@UserID
					,S.IsPayroll
					,S.StoreType
					)
		WHEN NOT MATCHED BY SOURCE
			AND T.ConfigID = @ConfigID
			THEN
				DELETE;

		-- =============================================================================
		-- =============================================================================
		-- ========= UPDATE EMPLOYEES ==================================================	
		MERGE [Export].[EmployeeMatch] T
		USING (
			SELECT 
				  EmployeeID
				, QBEmployee
				, PayType
				, EXPayType
			FROM (
				SELECT T.c.value('@_EmployeeID', 'bigint') AS EmployeeID
					,RTRIM( T.c.value('@AC_QBEmployee', 'nvarchar(50)') ) AS QBEmployee
					,T.c.value('@AC_QBPayType', 'nvarchar(50)') AS PayType
					,T.c.value('@AC_ExcelPayType', 'nvarchar(20)') AS EXPayType
					,T.c.value('@_modified', 'bit') AS Modified
				FROM @EmployData.nodes('/*') AS T(c)
				) AS MS
			WHERE ISNULL(QBEmployee,'') <> ''
			) AS S
			ON T.EmployeeID = S.EmployeeID
				AND T.EXPayType = S.EXPayType
				AND T.ConfigID = @ConfigID
		WHEN MATCHED
			THEN
				UPDATE
				SET [QBEmployee] = S.QBEmployee
					,[QBPayType] = PayType
					,[ModifiedOn] = GETDATE()
					,[ModifiedBy] = @UserID
		WHEN NOT MATCHED BY TARGET
			THEN
				INSERT (
					 [ConfigID]
					,[EmployeeID]
					,[QBEmployee]
					,[QBPayType]
					,[EXPayType]
					,[ModifiedOn]
					,[ModifiedBy]
					)
				VALUES (
					@ConfigID
					,S.[EmployeeID]
					,S.QBEmployee
					,PayType
					,EXPayType
					,GETDATE()
					,@UserID
					)
		WHEN NOT MATCHED BY SOURCE
			AND T.ConfigID = @ConfigID
			THEN
				DELETE;
	END

	MERGE [Dict].[ConfigFields] AS T
	USING (
		SELECT DISTINCT Value
			,ValueID
			,Type
			,ShortNumber
		FROM #Diction
		) AS S
		ON T.LongAccountNumber = S.ValueID
			AND T.ConfigID = @ConfigID
			AND T.[ShortAccountNumber] = S.ShortNumber
			AND T.[ListType] = S.[Type]
	WHEN MATCHED
		THEN
			UPDATE
			SET [Name] = Value
	WHEN NOT MATCHED BY TARGET
		THEN
			INSERT (
				[Name]
				,[ShortAccountNumber]
				,[LongAccountNumber]
				,[ConfigID]
				,[ListType]
				)
			VALUES (
				Value
				,ShortNumber
				,ValueID
				,@ConfigID
				,Type
				)
	WHEN NOT MATCHED BY SOURCE
		AND T.ConfigID = @ConfigID
		THEN
			DELETE;

	IF OBJECT_ID(N'tempdb.dbo.#DICTION', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #DICTION
	END

	IF OBJECT_ID(N'tempdb.dbo.#StoreMatch', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #StoreMatch
	END
	
	IF OBJECT_ID(N'tempdb.dbo.#StoreEx', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #StoreEx
	END

	IF OBJECT_ID(N'tempdb.dbo.#Employee', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #Employee
	END
END
