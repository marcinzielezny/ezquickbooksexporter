﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/02/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Config_Wizard_InitStoresMatch]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
	, @ExcelStoreFields		nvarchar(max)
	, @ExcelEmployeeFields  nvarchar(max)
	, @Mode					nvarchar(35)

)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE   @UserID		bigint
			, @Store		XML
			, @Employee		XML
			, @Ven			XML

	-- ==================================================================================
	
	SET @Store = CAST( @ExcelStoreFields  AS XML )
	SET @Employee = CAST( @ExcelEmployeeFields AS XML )
	
	SELECT 
		 T.c.value('@EX_StoreNumber','nvarchar(50)')	AS StoreName
	INTO #StoreName 
	FROM @Store.nodes('/T') as T(c)

	SELECT 
		 T.c.value('@EX_StoreNumber','nvarchar(50)')													AS StoreName 
	INTO #Employee 
	FROM @Employee.nodes('/T') as T(c)
	
	INSERT INTO #StoreName 
				(
					StoreName
				)
	SELECT DISTINCT EE.StoreName 
	FROM #Employee EE
		LEFT OUTER JOIN #StoreName SN ON EE.StoreName = SN.StoreName 
		  
	-- =========  SELECT VALUES  =========================================================
			
	SELECT DISTINCT
          'false'	 AS [AC_StoreSelected]
        , ISNULL(StoreName, '')		 AS [AC_StoreName]
        , ''		 AS [AC_QBClass]
		, '[Images/BgImage.00.4x18]' AS [EC_BLACK]
        , ''		 AS [_onMouseDownRow]
		, ''		 AS [_onMouseDownColumn]
		, ''		 AS [_ReloadRow]
        , ''		 AS [_VendorID]
		, 'false'	 AS [AC_IsPayrollOnly]
		, 'false'	 AS [AC_SummaryControlsheet]
		, 'false'    AS [AC_DetailsControlsheet]

		, CASE WHEN ISNULL(CM.ConfigName,'')='' THEN '' ELSE 'Store is in configuration:'+CM.ConfigName END AS Description 
		, CASE WHEN ISNULL(CM.ConfigName,'')='' THEN '' ELSE 'True' END AS [_disabled]
	FROM #StoreName SN 
		 LEFT OUTER JOIN Data.Stores AS DS WITH(nolock) ON DS.Name = SN.StoreName 
		 LEFT OUTER JOIN Export.StoreMatch AS SM WITH(nolock) ON DS.StoreID = SM.StoreID
		 LEFT OUTER JOIN Export.Configuration AS CM WITH(nolock) on SM.ConfigID = CM.ConfigID 
	WHERE StoreName IS NOT NULL
	FOR XML RAW( 'Store' )

	-- ==================================================================================

	-- =========  REMOVE TEMPORARY =======================================================
	
	IF OBJECT_ID( N'tempdb.dbo.#StoreName', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #StoreName 
	END

	IF OBJECT_ID( N'tempdb.dbo.#Employee', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #Employee 
	END
	
END