﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Config_Wizard_AddStore_Save] (
	 @SessionID UNIQUEIDENTIFIER
	,@OrganizationID UNIQUEIDENTIFIER
	,@ConfigID BIGINT
	,@Result NVARCHAR(MAX)
	)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID BIGINT

	-- ==================================================================================
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID(@SessionID)

	IF (@UserID IS NULL)
	BEGIN
		PRINT 'UserID is NULL'

		RETURN
	END


	IF (ISNULL(@Result, '')='')
	BEGIN 
	-- =========  SELECT Values  ===========================================================
	SELECT DS.StoreID AS '@_StoreID'
		,CASE 
			WHEN DM.StoreID IS NULL
				THEN 'false'
			ELSE 'true'
			END AS '@AC_StoreSelected'
		,NAME AS '@AC_StoreName'
		,ISNULL(DE.[QBAStore], '') AS '@AC_QBClass'
		,'[Images/BgImage.00.4x18]' AS [@EC_BLACK]
		,CASE 
			WHEN DM.[IsPayroll] = 1
				THEN 'true'
			ELSE 'false'
			END AS [@AC_IsPayrollOnly]
		,CASE 
			WHEN DM.StoreType IS NULL
				THEN 'false'
			WHEN DM.StoreType = 0
				THEN 'true'
			WHEN DM.StoreType = 1
				THEN 'false'
			ELSE 'false'
			END AS [@AC_SummaryControlsheet]
		,CASE 
			WHEN DM.StoreType IS NULL
				THEN 'false'
			WHEN DM.StoreType = 0 
				THEN 'false'
			WHEN DM.StoreType = 1
				THEN 'true'
			ELSE 'false'
			END AS [@AC_DetailsControlsheet]
		,'' AS [@_onMouseDownRow]
		,'0' AS [@_ReloadRow]
		,ISNULL(DM.VendorID, '') AS [@_VendorID]
		,CASE 
			WHEN ISNULL(CM.ConfigName, '') = ''
				OR CM.ConfigID = @ConfigID
				THEN ''
			ELSE 'Store is in configuration: ' + CM.ConfigName
			END AS [@AC_Description]
		,CASE 
			WHEN ISNULL(CM.ConfigName, '') = ''
				OR CM.ConfigID = @ConfigID
				THEN ''
			ELSE 'True'
			END AS [@_disabled]
	FROM Data.Stores DS
	INNER JOIN Data.UsersPermissions AS DU WITH (NOLOCK) ON DS.StoreID = DU.StoreID
	INNER JOIN Export.StoreMatch AS DM WITH (NOLOCK) ON DM.StoreID = DS.StoreID
		AND DM.ConfigID = @ConfigID
	LEFT OUTER JOIN Export.StoreMatch AS DE WITH (NOLOCK) ON DE.StoreID = DS.StoreID
	LEFT OUTER JOIN Export.Configuration AS CM WITH (NOLOCK) ON DE.ConfigID = CM.ConfigID
	WHERE DU.UserID = @UserID
		AND Allow = 1
	FOR XML PATH('Store')
	
	END
	ELSE 
	BEGIN 
	
	DECLARE @StoreDat XML 
	
	
	SET @StoreDat	= CAST( @Result AS XML )
	
	SELECT [@_StoreID]
		,  [@AC_StoreSelected]
		,  [@AC_StoreName]
		,  [@AC_QBClass]
		,  [@EC_BLACK]
		,  [@AC_IsPayrollOnly]
		,  [@AC_SummaryControlsheet]
		,  [@AC_DetailsControlsheet]
		,  [@_onMouseDownRow]
		,  [@_ReloadRow]
		,  [@_VendorID]
		,  [@AC_Description]
		,  [@_disabled]
	FROM (
		SELECT DS.StoreID AS '@_StoreID'
		,CASE 
			WHEN DM.StoreID IS NULL
				THEN 'false'
			ELSE 'true'
			END AS '@AC_StoreSelected'
		,NAME AS '@AC_StoreName'
		,ISNULL(DE.[QBAStore], '') AS '@AC_QBClass'
		,'[Images/BgImage.00.4x18]' AS [@EC_BLACK]
		,CASE 
			WHEN DM.[IsPayroll] = 1
				THEN 'true'
			ELSE 'false'
			END AS [@AC_IsPayrollOnly]
		,CASE 
			WHEN DM.StoreType IS NULL
				THEN 'false'
			WHEN DM.StoreType = 0
				THEN 'true'
			WHEN DM.StoreType = 1
				THEN 'false'
			ELSE 'false'
			END AS [@AC_SummaryControlsheet]
		,CASE 
			WHEN DM.StoreType IS NULL
				THEN 'false'
			WHEN DM.StoreType = 0 
				THEN 'false'
			WHEN DM.StoreType = 1
				THEN 'true'
			ELSE 'false'
			END AS [@AC_DetailsControlsheet]
		,'' AS [@_onMouseDownRow]
		,'0' AS [@_ReloadRow]
		,ISNULL(DM.VendorID, '') AS [@_VendorID]
		,CASE 
			WHEN ISNULL(CM.ConfigName, '') = ''
				OR CM.ConfigID = @ConfigID
				THEN ''
			ELSE 'Store is in configuration: ' + CM.ConfigName
			END AS [@AC_Description]
		,CASE 
			WHEN ISNULL(CM.ConfigName, '') = ''
				OR CM.ConfigID = @ConfigID
				THEN ''
			ELSE 'True'
			END AS [@_disabled]
	FROM Data.Stores DS
	INNER JOIN Data.UsersPermissions AS DU WITH (NOLOCK) ON DS.StoreID = DU.StoreID
	INNER JOIN Export.StoreMatch AS DM WITH (NOLOCK) ON DM.StoreID = DS.StoreID
		AND DM.ConfigID = @ConfigID
	LEFT OUTER JOIN Export.StoreMatch AS DE WITH (NOLOCK) ON DE.StoreID = DS.StoreID
	LEFT OUTER JOIN Export.Configuration AS CM WITH (NOLOCK) ON DE.ConfigID = CM.ConfigID
	WHERE DU.UserID = @UserID
		AND Allow = 1
		
	UNION ALL
	
	SELECT DS.StoreID AS '@_StoreID'
		,  'true' AS '@AC_StoreSelected'
		,NAME AS '@AC_StoreName'
		,'' AS '@AC_QBClass'
		,'[Images/BgImage.00.4x18]' AS [@EC_BLACK]
		, 'false' AS [@AC_IsPayrollOnly]
		, 'false' AS [@AC_SummaryControlsheet]
		, 'false' AS [@AC_DetailsControlsheet]
		, '' AS [@_onMouseDownRow]
		, '0' AS [@_ReloadRow]
		, '' AS [@_VendorID]
		, '' AS [@AC_Description]
		, 'false' AS [@_disabled]
	FROM Data.Stores DS
	INNER JOIN (
	SELECT	
			  T.c.value('@AC_StoreName', 'nvarchar(50)') AS [Store]
		   , T.c.value('@AC_StoreSelected', 'nvarchar(50)') AS [Selected]
	FROM @StoreDat.nodes('*') T(c)
	WHERE T.c.value('@AC_StoreSelected', 'nvarchar(50)') = 'true'
	) DF ON DS.Name = DF.Store
	) AS TMP
	FOR XML PATH('Store')
	
	END

END