﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/02/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Config_Wizard_InitEmployeesMatch]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
	, @StoreFields		nvarchar(max)
	, @ExcelEmployeeFields  nvarchar(max)
	, @Mode					nvarchar(35)
	, @ConfigID				bigint = NULL

)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE   @UserID		bigint
			, @Store		XML
			, @Employee		XML
			, @Ven			XML

	-- ==================================================================================
	
	SET @Store = CAST( @StoreFields  AS XML )
	SET @Employee = CAST( @ExcelEmployeeFields AS XML )
	
	SELECT T.c.value('@AC_StoreSelected','bit')	AS IsSelected
		, T.c.value('@AC_StoreName','nvarchar(50)')	AS StoreName
		, T.c.value('@AC_IsPayrollOnly','bit')	AS IsPayroll
	INTO #StoreName 
	FROM @Store.nodes('/Store') as T(c)

	SELECT 
		  T.c.value('@EX_EmployeeReference','nvarchar(50)')												AS EmployeeReference
		, T.c.value('@EX_FirstName','nvarchar(50)') + ' ' + T.c.value('@EX_LastName','nvarchar(50)')	AS EmployeeName
		, T.c.value('@EX_PayType','nvarchar(50)')														AS PayType
		, T.c.value('@EX_StoreNumber','nvarchar(50)')													AS StoreName 
	INTO #Employee 
	FROM @Employee.nodes('/T') as T(c)
	

	IF ( @ConfigID IS NOT NULL ) 
	BEGIN 

	SELECT   DE.[EmployeeID]			AS _EmployeeID
		   , DS.[StoreID]				AS _StoreID
		   , DS.Name					AS AC_StoreName
		   , DE.[Name]					AS AC_Employee
		   , [DE].[EXPayType]			AS AC_ExcelPayType
		   , ISNULL( QBEmployee, '')			AS AC_QBEmployee
		   , ISNULL( QBPayType, '' )			AS AC_QBPayType
	
	FROM  Data.Employee DE
		LEFT JOIN [Export].[EmployeeMatch] EM WITH (NOLOCK) ON EM.EmployeeID = DE.EmployeeID AND DE.EXPayType = EM.EXPayType AND EM.ConfigID=@ConfigID
		LEFT JOIN Data.Stores DS WITH (NOLOCK) ON DE.StoreID = DS.StoreID
		INNER JOIN #StoreName SN ON DS.Name = SN.StoreName
		INNER JOIN Data.UsersPermissions   AS DU WITH(nolock) ON DS.StoreID = DU.StoreID
	WHERE IsSelected = 1 AND IsPayroll = 1 
	FOR XML RAW( 'Employees' )

	END
	ELSE
	BEGIN
	SELECT 
		     EE.StoreName 			AS AC_StoreName
		   , EE.EmployeeName		AS AC_Employee
		   , EE.PayType				AS AC_ExcelPayType
		   , ''						AS AC_QBEmployee
		   , ''						AS AC_QBPayType
	FROM #Employee EE
		INNER JOIN #StoreName SN ON EE.StoreName = SN.StoreName
	WHERE EmployeeName IS NOT NULL AND IsSelected = 1 AND IsPayroll = 1
	FOR XML RAW( 'Employee' )
	END
	
	
	IF OBJECT_ID( N'tempdb.dbo.#StoreName', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #StoreName 
	END

	IF OBJECT_ID( N'tempdb.dbo.#Employee', N'U' ) IS NOT NULL
	BEGIN
		DROP TABLE #Employee 
	END
	
END