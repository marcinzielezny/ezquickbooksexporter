﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/02/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Import_Wizard_InitEmployeesMatch]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
	, @ExcelEmployeeFields  nvarchar(max)
	, @StoreExists			bit OUTPUT

)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE   @UserID		bigint
			, @Store		XML
			, @Employee		XML
			, @Ven			XML

	-- ==================================================================================
	
	SET @Employee = CAST( @ExcelEmployeeFields AS XML )
	
	SELECT 
		  T.c.value('@EX_EmployeeReference','nvarchar(50)')											AS EmployeeReference
		, T.c.value('@EX_FirstName','nvarchar(50)') + ' ' + T.c.value('@EX_LastName','nvarchar(50)')	AS EmployeeName
		, T.c.value('@EX_PayType','nvarchar(50)') 													AS PayType
		, T.c.value('@EX_StoreNumber','nvarchar(50)')						AS StoreName 
	INTO #Employee 
	FROM @Employee.nodes('/Employee') as T(c)
	

	INSERT INTO Data.Employee (
			 [StoreID]
			,[Name]
			,[EXPayType]
			)
	SELECT DISTINCT 
			 EE.StoreID
			,EmployeeName
			,EE.EXPayType
	FROM (	
				SELECT 
					 StoreName
					,EmployeeName
					,Paytype AS EXPayType
					,DS.StoreID
				FROM #Employee 
				INNER JOIN Data.Stores DS ON  StoreName = DS.NAME
		) AS EE
		LEFT JOIN Data.Employee DE ON DE.StoreID = EE.StoreID
			AND EE.EmployeeName = DE.NAME AND EE.EXPayType = DE.EXPayType
		WHERE DE.EmployeeID IS NULL
		
		
	SELECT DISTINCT
			 DE.[EmployeeID]			AS _EmployeeID
		   , DS.[StoreID]				AS _StoreID
		   , DS.Name					AS AC_StoreName
		   , DE.[Name]					AS AC_Employee
		   , [DE].[EXPayType]			AS AC_ExcelPayType
		   , ISNULL( QBEmployee, '')			AS AC_QBEmployee
		   , ISNULL( QBPayType, '' )			AS AC_QBPayType
		   , ESM.ConfigID					AS _ConfigID
	INTO #EMSec
	FROM  #Employee EE 
	INNER JOIN Data.Employee DE ON EE.EmployeeName = DE.Name
		LEFT JOIN [Export].[EmployeeMatch] EM WITH (NOLOCK) ON EM.EmployeeID = DE.EmployeeID AND DE.EXPayType = EM.EXPayType 
		LEFT JOIN Data.Stores DS WITH (NOLOCK) ON DE.StoreID = DS.StoreID
		INNER JOIN Data.Stores SN ON DE.StoreID = SN.StoreID
		INNER JOIN [Export].[StoreMatch] ESM ON  DS.StoreID = ESM.StoreID AND ESM.IsPayroll=1
		INNER JOIN Data.UsersPermissions   AS DU WITH(nolock) ON DS.StoreID = DU.StoreID	
	WHERE EM.EmployeeID IS NULL
	--FOR XML RAW ('Employee')

	SELECT DISTINCT
			  _EmployeeID
		   , _StoreID
		   , AC_StoreName
		   , AC_Employee
		   , AC_ExcelPayType
		   , AC_QBEmployee
		   , AC_QBPayType
		   , _ConfigID
	FROM #EMSec			
	FOR XML RAW ('Employee')

	SELECT   EC.ConfigName 
		   , EC.ConfigPath
		   , EC.ConfigID
	FROM  Export.Configuration AS EC 
	INNER JOIN (SELECT DISTINCT _ConfigID AS ConfigID 
				FROM #EMSec) AS C ON EC.ConfigID = C.ConfigID
	FOR XML RAW('Data')

	IF  ( EXISTS( SELECT TOP 1 1 
		  FROM #Employee EE 
			LEFT JOIN [Data].[Stores] DS ON EE.StoreName = DS.Name
			LEFT JOIN  [Export].[StoreMatch] SM ON SM.StoreID = DS.StoreID
		WHERE SM.StoreID IS NULL)) 
	BEGIN
		SET @StoreExists = 0
	END 
	ELSE 
	BEGIN
	    SET @StoreExists = 1
	END


	IF OBJECT_ID(N'tempdb.dbo.#Employee', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #Employee
	END
	
	IF OBJECT_ID(N'tempdb.dbo.#EMSec', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #EMSec
	END

END