﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Config_Wizard_EditGrids] (
	@SessionID UNIQUEIDENTIFIER
	,@OrganizationID UNIQUEIDENTIFIER
	,@ConfigID BIGINT
	)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID BIGINT

	-- ==================================================================================
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID(@SessionID)

	IF (@UserID IS NULL)
	BEGIN
		PRINT 'UserID is NULL'

		RETURN
	END

	-- =========  SELECT Values  ===========================================================
	SELECT DS.StoreID AS '@_StoreID'
		,CASE 
			WHEN DM.StoreID IS NULL
				THEN 'false'
			ELSE 'true'
			END AS '@AC_StoreSelected'
		,NAME AS '@AC_StoreName'
		,ISNULL(DE.[QBAStore], '') AS '@AC_QBClass'
		,'[Images/BgImage.00.4x18]' AS [@EC_BLACK]
		,CASE 
			WHEN DM.[IsPayroll] = 1
				THEN 'true'
			ELSE 'false'
			END AS [@AC_IsPayrollOnly]
		,CASE 
			WHEN DM.StoreType IS NULL
				THEN 'false'
			WHEN DM.StoreType = 0
				THEN 'true'
			WHEN DM.StoreType = 1
				THEN 'false'
			ELSE 'false'
			END AS [@AC_SummaryControlsheet]
		,CASE 
			WHEN DM.StoreType IS NULL
				THEN 'false'
			WHEN DM.StoreType = 0 
				THEN 'false'
			WHEN DM.StoreType = 1
				THEN 'true'
			ELSE 'false'
			END AS [@AC_DetailsControlsheet]
		,'' AS [@_onMouseDownRow]
		,'0' AS [@_ReloadRow]
		,ISNULL(DM.VendorID, '') AS [@_VendorID]
		,CASE 
			WHEN ISNULL(CM.ConfigName, '') = ''
				OR CM.ConfigID = @ConfigID
				THEN ''
			ELSE 'Store is in configuration: ' + CM.ConfigName
			END AS [@AC_Description]
		,CASE 
			WHEN ISNULL(CM.ConfigName, '') = ''
				OR CM.ConfigID = @ConfigID
				THEN ''
			ELSE 'True'
			END AS [@_disabled]
	FROM Data.Stores DS
	INNER JOIN Data.UsersPermissions AS DU WITH (NOLOCK) ON DS.StoreID = DU.StoreID
	INNER JOIN Export.StoreMatch AS DM WITH (NOLOCK) ON DM.StoreID = DS.StoreID
		AND DM.ConfigID = @ConfigID
	LEFT OUTER JOIN Export.StoreMatch AS DE WITH (NOLOCK) ON DE.StoreID = DS.StoreID
	LEFT OUTER JOIN Export.Configuration AS CM WITH (NOLOCK) ON DE.ConfigID = CM.ConfigID
	WHERE DU.UserID = @UserID
		AND Allow = 1
	FOR XML PATH('Store')

	SELECT DE.[EmployeeID] AS _EmployeeID
		,DS.[StoreID] AS _StoreID
		,DS.[Name] AS AC_StoreName
		,DE.[Name] AS AC_Employee
		,ISNULL([QBEmployee], '') AS AC_QBEmployee
		,ISNULL([QBPayType], '') AS AC_QBPayType
		,ISNULL([DE].[EXPayType], '') AS AC_ExcelPayType
	FROM Data.Employee DE
	LEFT JOIN [Export].[EmployeeMatch] EM WITH (NOLOCK) ON EM.EmployeeID = DE.EmployeeID
		AND EM.ConfigID = @ConfigID
	LEFT JOIN Data.Stores DS WITH (NOLOCK) ON DE.StoreID = DS.StoreID
	LEFT JOIN Export.StoreMatch ESM WITH (NOLOCK) ON ESM.StoreID = DS.StoreID
		AND EM.ConfigID = @ConfigID
	INNER JOIN Data.UsersPermissions AS DU WITH (NOLOCK) ON DS.StoreID = DU.StoreID
	WHERE DU.UserID = @UserID
		AND Allow = 1
	FOR XML RAW('Employees')

	-- ==================================================================================
	-- =========  SELECT Dictionaries  ==================================================
	SELECT NAME AS [Value]
		,LongAccountNumber AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK)
	WHERE ConfigID = @ConfigID
		AND ListType = 'PayType'
	FOR XML RAW('QBPayType')

	SELECT NAME AS [Value]
		,LongAccountNumber AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK)
	WHERE ConfigID = @ConfigID
		AND ListType = 'Employee'
	FOR XML RAW('QBEmployee')

	SELECT NAME AS [Value]
		,LongAccountNumber AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK)
	WHERE ConfigID = @ConfigID
		AND ListType = 'Store'
	FOR XML RAW('QBClass')
		-- ==================================================================================
END