﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 03/17/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[ImportData_Save] (
	@SessionID UNIQUEIDENTIFIER
	,@OrganizationID UNIQUEIDENTIFIER
	,@Type NVARCHAR(200)
	,@ImportData NVARCHAR(max)
	,@Message NVARCHAR(max) OUTPUT
	)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID BIGINT
		,@ImportEXData XML

	-- ==================================================================================
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID(@SessionID)

	IF (@UserID IS NULL)
	BEGIN
		PRINT 'UserID is NULL'

		RETURN
	END

	SET @Message = 'OK'
	SET @ImportEXData = CAST(@ImportData AS XML)

	IF (@Type = 'CONTROLSHEET')
	BEGIN
		CREATE TABLE #ImportEx (
			Store NVARCHAR(50)
			,Value MONEY
			,DATE DATETIME
			,[Column] NVARCHAR(50)
			,[SG] NVARCHAR(1)
			)

		INSERT INTO #ImportEx (
			Store
			,Value
			,[Date]
			,[Column]
			,[SG]
			)
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_ClosingKeep', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_ClosingKeep' AS [Column]
			,NULL AS [SG]
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_DepositInBank', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_DepositInBank'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Cashcards', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Cashcards'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Debit', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Debit'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Visaandmastercard', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Visaandmastercard'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Discover', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Discover'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_AmericanExp', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_AmericanExp'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Cateringcallcenter', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Cateringcallcenter'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Other', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Other'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,ABS(T.c.value('@EX_PaidOuts', 'money')) AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_PaidOuts'
			,CASE 
				WHEN T.c.value('@EX_PaidOuts', 'money') < 0
					THEN 'C'
				ELSE 'D'
				END
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,ABS(T.c.value('@EX_OverShort', 'money')) AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OverShort'
			,CASE 
				WHEN T.c.value('@EX_OverShort', 'money') < 0
					THEN 'D'
				ELSE 'C'
				END
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_UnitSales', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_UnitSales'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Footlong', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Footlong'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_SixInch', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_SixInch'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_ThreeInch', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_ThreeInch'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_MuffinMelt', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_MuffinMelt'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Salad', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Salad'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Pizza', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Pizza'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_OtherCarrier', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OtherCarrier'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_AddOn', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_AddOn'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Catering', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Catering'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_DrinkSales', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_DrinkSales'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Fountain', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Fountain'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_BottledBeverage', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_BottledBeverage'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_HotBeverage', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_HotBeverage'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_OtherBeverage', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OtherBeverage'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_MiscSales', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_MiscSales'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Chips', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Chips'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Cookies', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Cookies'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Soups', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Soups'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_OtherMisc', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OtherMisc'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_CashCardSales', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_CashCardSales'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_Salestax', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_Salestax'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_OtherReceipts', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OtherReceipts'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_UnitCouponsDisc', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_UnitCouponsDisc'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_UnitRefunds', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_UnitRefunds'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_UnitVoids', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_UnitVoids'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_DrinksCouponsDisc', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_DrinksCouponsDisc'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_DrinksRefunds', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_DrinksRefunds'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_DrinksVoids', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_DrinksVoids'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_OtherCouponsDisc', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OtherCouponsDisc'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_OtherRefunds', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OtherRefunds'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_OtherVoids', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OtherVoids'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
	
		UNION ALL
		
		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_OpeningCash', 'money') AS Value
			,T.c.value('@EX_Date', 'Datetime') AS [Date]
			,'EX_OpeningCash'
			,NULL
		FROM @ImportEXData.nodes('*') T(c)
		
		DECLARE @StoreMatch NVARCHAR(max)

		SET @StoreMatch = ''

		SELECT @StoreMatch = @StoreMatch + Store + ', '
		FROM (
			SELECT DISTINCT EX.Store
			FROM #ImportEx EX
			LEFT JOIN Data.Stores ST ON EX.Store = ST.NAME
			LEFT JOIN Export.StoreMatch SM ON ST.StoreID = SM.StoreID
			WHERE SM.StoreID IS NULL
				AND EX.Store IS NOT NULL
			) AS TMP

		IF (@StoreMatch <> '')
		BEGIN
			SET @Message = 'Stores: ' + @StoreMatch + 'was not listed in the configuration. The configuration for this store needs to be set-up. Click OK than CANCEL and go to QuickBooks export Configuration (top left of the home screen). Click Edit Selected Config if you want to add store to existing config or Add if you want to create new configuration.'

			INSERT INTO [Data].[Stores] (
				NAME
				,RecordID
				)
			SELECT Store
				,NEWID()
			FROM (
				SELECT DISTINCT EX.Store
				FROM #ImportEx EX
				LEFT JOIN Data.Stores ST ON EX.Store = ST.NAME
				LEFT JOIN Export.StoreMatch SM ON ST.StoreID = SM.StoreID
				WHERE ST.NAME IS NULL
					AND SM.StoreID IS NULL
					AND EX.Store IS NOT NULL
				) AS TMP

			INSERT INTO [Data].[UsersPermissions] (
				UserID
				,Type
				,StoreID
				,Allow
				)
			SELECT DISTINCT @UserID
				, 'Manager'
				,ST.StoreID
				,1
			FROM #ImportEx EX
			INNER JOIN Data.Stores ST ON EX.Store = ST.NAME
			LEFT JOIN [Data].[UsersPermissions] UP ON UP.StoreID = ST.StoreID
				AND UP.UserID = @UserID
			WHERE UP.PermissionID IS NULL

			RETURN
		END

		UPDATE [Data].[DetailsData]
		SET [Value] = EX.Value
			,[Description] = DI.[Name]
			,[SG] = DI.[SG]
		FROM #ImportEx EX
		INNER JOIN Data.Stores ST ON EX.Store = ST.NAME
		INNER JOIN Dict.POSFields DI ON EX.[Column] = DI.Ex_Name
		INNER JOIN [Data].[DetailsData] DD ON [DD].[PosField] = [DI].[PosFieldsID]
			AND [DD].[BusinessDate] = [EX].[Date]
			AND ST.StoreID = DD.StoreID

		INSERT INTO [Data].[DetailsData] (
			[StoreID]
			,[Value]
			,[DefaultValue]
			,[PosField]
			,[Description]
			,[BusinessDate]
			,[SG]
			)
		SELECT DISTINCT ST.StoreID
			,EX.Value
			,0.00
			,DI.PosFieldsID
			,DI.[Name]
			,[Date]
			,EX.[SG]
		FROM #ImportEx EX
		INNER JOIN Data.Stores ST ON EX.Store = ST.NAME
		INNER JOIN Dict.POSFields DI ON EX.[Column] = DI.Ex_Name
		INNER JOIN Export.StoreMatch SM ON ST.StoreID = SM.StoreID
		LEFT OUTER JOIN [Data].[DetailsData] DD ON [DD].[PosField] = [DI].[PosFieldsID]
			AND [DD].[BusinessDate] = [EX].[Date] AND ST.StoreID = DD.StoreID
		WHERE DD.PosField IS NULL
	END
	ELSE
	BEGIN
		SELECT T.c.value('@EX_Date', 'Datetime') AS [Date]
			,T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_FirstName', 'nvarchar(50)') AS [FirstName]
			,T.c.value('@EX_LastName', 'nvarchar(50)') AS [LastName]
			,T.c.value('@EX_Hours', 'decimal(19,6)') AS [Hours]
			,T.c.value('@EX_PayType', 'nvarchar(50)') AS [PayType]
		INTO #ImportEm
		FROM @ImportEXData.nodes('*') T(c)

		UPDATE [Data].[DetailsData]
		SET [HoursWorked] = [dbo].[floatToTime]([Hours])
			,[LastModificationOn] = GETDATE()
			,[LastModificationBy] = @UserID
		FROM #ImportEm EM
		INNER JOIN [Data].[Stores] DS ON EM.Store = DS.NAME
		INNER JOIN [Data].[Employee] EP ON EP.NAME = EM.FirstName + ' ' + EM.LastName
			AND DS.StoreID = EP.StoreID
		INNER JOIN [Export].[EmployeeMatch] EE ON EM.Store = DS.NAME
			AND EE.EmployeeID = EP.EmployeeID
			AND EM.PayType = EE.EXPayType
		INNER JOIN [Data].[DetailsData] DD ON DD.BusinessDate = [EM].[Date]
			AND EE.EmployeeMatchID = DD.EmployeeID

		INSERT INTO [Data].[DetailsData] (
			[BusinessDate]
			,[StoreID]
			,[EmployeeID]
			,[HoursWorked]
			,[LastModificationOn]
			,[LastModificationBy]
			)
		SELECT [EM].[Date]
			,[EP].[StoreID]
			,[EE].[EmployeeMatchID]
			,[dbo].[floatToTime]([EM].[Hours])
			,GETDATE()
			,@UserID
		FROM #ImportEm EM
		INNER JOIN [Data].[Stores] DS ON EM.Store = DS.NAME
		INNER JOIN [Data].[Employee] EP ON EP.NAME = EM.FirstName + ' ' + EM.LastName
			AND DS.StoreID = EP.StoreID
		INNER JOIN [Export].[EmployeeMatch] EE ON EM.Store = DS.NAME
			AND EE.EmployeeID = EP.EmployeeID
			AND EM.PayType = EE.EXPayType
		LEFT JOIN [Data].[DetailsData] DD ON DD.BusinessDate = [EM].[Date]
			AND EE.EmployeeMatchID = DD.EmployeeID
		WHERE DD.DataID IS NULL
	END

	IF OBJECT_ID(N'tempdb.dbo.#ImportEm', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #ImportEm
	END

	IF OBJECT_ID(N'tempdb.dbo.#ImportEx', N'U') IS NOT NULL
	BEGIN
		DROP TABLE #ImportEx
	END
END