﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 03/17/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Validate_Employee_Data] (
	@SessionID UNIQUEIDENTIFIER
	,@OrganizationID UNIQUEIDENTIFIER
	,@Type NVARCHAR(200)
	,@ImportData NVARCHAR(max)
	,@Validate BIT OUTPUT
	,@Message NVARCHAR(max) OUTPUT
	)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID BIGINT
		,@ImportEXData XML

	-- ==================================================================================
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID(@SessionID)

	IF (@UserID IS NULL)
	BEGIN
		PRINT 'UserID is NULL'

		RETURN
	END

	SET @Message = 'OK'
	SET @ImportEXData = CAST(@ImportData AS XML)

	IF (@Type = 'CONTROLSHEET')
	BEGIN
		IF (
				EXISTS (
					SELECT TOP 1 1
					FROM @ImportEXData.nodes('*') T(c)
					WHERE T.c.value('@EX_Date', 'Datetime') > GETDATE()
					)
				)
		BEGIN
			SET @Message = 'The data within your import sheet is for a future date.  This data cannot be imported.  Click OK and check the date on your import file.'

			RETURN
		END

		SELECT T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [EX_StoreNumber]
			,T.c.value('@EX_Date', 'Datetime') AS [EX_Date]
			,T.c.value('@EX_OpeningCash', 'money') AS [EX_OpeningCash]
			,T.c.value('@EX_ClosingKeep', 'money') AS [EX_ClosingKeep]
			,T.c.value('@EX_DepositInBank', 'money') AS [EX_DepositInBank]
			,T.c.value('@EX_Cashcards', 'money') AS [EX_Cashcards]
			,T.c.value('@EX_Debit', 'money') AS [EX_Debit]
			,T.c.value('@EX_Visaandmastercard', 'money') AS [EX_VisaAndMastercard]
			,T.c.value('@EX_Discover', 'money') AS [EX_Discover]
			,T.c.value('@EX_AmericanExp', 'money') AS [EX_AmericanEx]
			,T.c.value('@EX_Cateringcallcenter', 'money') AS [EX_CateringCallCenter]
			,T.c.value('@EX_Other', 'money') AS [EX_Other]
			,T.c.value('@EX_PaidOuts', 'money') AS [EX_Paidouts]
			,T.c.value('@EX_OverShort', 'money') AS [EX_OverShort]
			,T.c.value('@EX_UnitSales', 'money') AS [EX_UnitSales]
			,T.c.value('@EX_Footlong', 'money') AS [EX_Footlong]
			,T.c.value('@EX_SixInch', 'money') AS [EX_SixInch]
			,T.c.value('@EX_ThreeInch', 'money') AS [EX_ThreeInch]
			,T.c.value('@EX_MuffinMelt', 'money') AS [EX_Muffin]
			,T.c.value('@EX_Salad', 'money') AS [EX_Salad]
			,T.c.value('@EX_Pizza', 'money') AS [EX_Pizza]
			,T.c.value('@EX_OtherCarrier', 'money') AS [EX_OtherCarrier]
			,T.c.value('@EX_AddOn', 'money') AS [EX_AddOn]
			,T.c.value('@EX_Catering', 'money') AS [EX_Catering]
			,T.c.value('@EX_UnitCouponsDisc', 'money') AS [EX_UnitCouponsDisc]
			,T.c.value('@EX_UnitRefunds', 'money') AS [EX_UnitRefunds]
			,T.c.value('@EX_UnitVoids', 'money') AS [EX_UnitVoids]
			,T.c.value('@EX_DrinkSales', 'money') AS [EX_DrinkSales]
			,T.c.value('@EX_Fountain', 'money') AS [EX_Fountain]
			,T.c.value('@EX_BottledBeverage', 'money') AS [EX_BottledBeverage]
			,T.c.value('@EX_HotBeverage', 'money') AS [EX_HotBeverage]
			,T.c.value('@EX_OtherBeverage', 'money') AS [EX_OtherBeverage]
			,T.c.value('@EX_DrinksCouponsDisc', 'money') AS [EX_DrinksCouponsDisc]
			,T.c.value('@EX_DrinksRefunds', 'money') AS [EX_DrinksRefunds]
			,T.c.value('@EX_DrinksVoids', 'money') AS [EX_DrinksVoids]
			,T.c.value('@EX_MiscSales', 'money') AS [EX_MiscSales]
			,T.c.value('@EX_Chips', 'money') AS [EX_Chips]
			,T.c.value('@EX_Cookies', 'money') AS [EX_Cookies]
			,T.c.value('@EX_Soups', 'money') AS [EX_Soups]
			,T.c.value('@EX_OtherMisc', 'money') AS [EX_OtherMisc]
			,T.c.value('@EX_OtherCouponsDisc', 'money') AS [EX_OtherCouponsDisc]
			,T.c.value('@EX_OtherRefunds', 'money') AS [EX_OtherRefunds]
			,T.c.value('@EX_OtherVoids', 'money') AS [EX_OtherVoids]
			,T.c.value('@EX_CashCardSales', 'money') AS [EX_CashCardSales]
			,T.c.value('@EX_Salestax', 'money') AS [EX_SalesTax]
			,T.c.value('@EX_OtherReceipts', 'money') AS [EX_OtherReceipts]
		FROM @ImportEXData.nodes('*') T(c)
		WHERE T.c.value('@EX_StoreNumber', 'nvarchar(50)') IS NOT NULL
		FOR XML RAW('Controlsheet')
	END
	ELSE
	BEGIN
		SELECT T.c.value('@EX_Date', 'Datetime') AS [Date]
			,T.c.value('@EX_StoreNumber', 'nvarchar(50)') AS [Store]
			,T.c.value('@EX_FirstName', 'nvarchar(50)') AS [FirstName]
			,T.c.value('@EX_LastName', 'nvarchar(50)') AS [LastName]
			,T.c.value('@EX_Hours', 'decimal(19,2)') AS [Hours]
			,T.c.value('@EX_PayType', 'nvarchar(50)') AS [PayType]
			,CAST(' ' AS NVARCHAR(MAX)) AS [Description]
		INTO #ImportEm
		FROM @ImportEXData.nodes('*') T(c)
		WHERE T.c.value('@EX_Date', 'Datetime') IS NOT NULL

		UPDATE #ImportEm
		SET [Description] = CASE 
				WHEN EP.NAME IS NULL
					THEN 'Employee not configured'
				WHEN DS.StoreID IS NULL
					THEN 'Store not configured'
				WHEN EE.EmployeeMatchID IS NULL
					THEN 'Employee is not in expected configuration'
				ELSE ''
				END
		FROM #ImportEm EM
		LEFT JOIN [Data].[Stores] DS ON EM.Store = DS.NAME
		LEFT JOIN [Data].[Employee] EP ON EP.NAME = EM.FirstName + ' ' + EM.LastName
		LEFT JOIN [Export].[EmployeeMatch] EE ON EM.Store = DS.NAME
			AND EE.EmployeeID = EP.EmployeeID
			AND EM.PayType = EE.EXPayType

		UPDATE #ImportEm
		SET [Description] = CASE 
				WHEN EP.NAME IS NULL
					THEN 'Employee not configured'
				WHEN DS.StoreID IS NULL
					THEN 'Store not configured'
				WHEN EE.EmployeeMatchID IS NULL
					THEN 'Employee is not in expected configuration'
				ELSE ''
				END
		FROM #ImportEm EM
		LEFT JOIN [Data].[Stores] DS ON EM.Store = DS.NAME
		LEFT JOIN [Data].[Employee] EP ON EP.NAME = EM.FirstName + ' ' + EM.LastName
		LEFT JOIN [Export].[EmployeeMatch] EE ON EM.Store = DS.NAME
			AND EE.EmployeeID = EP.EmployeeID
			AND EM.PayType = EE.EXPayType

		UPDATE #ImportEm
		SET [Description] = 'Error: The restaurant you are trying to import has not been set up.  Click cancel and go back to the QuickBooks Export Configuration screen to configure the restaurant.'
		FROM #ImportEm EM
		LEFT JOIN [Data].[Stores] DS ON EM.Store = DS.NAME
		LEFT JOIN [Export].[StoreMatch] SM ON SM.StoreID = DS.StoreID
		WHERE SM.StoreID IS NULL

		IF (
				EXISTS (
					SELECT TOP 1 1
					FROM #ImportEm
					WHERE [Description] <> ''
					)
				)
		BEGIN
			SET @Validate = 1
		END
		ELSE
		BEGIN
			SET @Validate = 0
		END

		SELECT [Date] AS [CO_CreatedOn]
			,[Store] AS [EX_StoreNumber]
			,[FirstName] AS [EX_FirstName]
			,[LastName] AS [EX_LastName]
			,[Hours] AS [EX_Hours]
			,[PayType] AS [EX_PayType]
			,[Description] AS [AC_Description]
		FROM #ImportEm
		FOR XML RAW('Employee')
	END
END