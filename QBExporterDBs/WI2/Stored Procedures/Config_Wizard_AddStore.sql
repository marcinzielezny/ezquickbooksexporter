﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 09/08/2014
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Config_Wizard_AddStore] (
	@SessionID UNIQUEIDENTIFIER
	,@OrganizationID UNIQUEIDENTIFIER
	)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE @UserID BIGINT

	-- ==================================================================================
	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID(@SessionID)

	IF (@UserID IS NULL)
	BEGIN
		PRINT 'UserID is NULL'

		RETURN
	END

	-- =========  SELECT Values  ===========================================================
	SELECT DS.StoreID AS '@_StoreID'
		,CASE 
			WHEN DM.StoreID IS NULL
				THEN 'false'
			ELSE 'true'
			END AS '@AC_StoreSelected'
		,NAME AS '@AC_StoreName'
	FROM Data.Stores DS
	LEFT JOIN Data.UsersPermissions AS DU WITH (NOLOCK) ON DS.StoreID = DU.StoreID
	LEFT JOIN Export.StoreMatch AS DM WITH (NOLOCK) ON DM.StoreID = DS.StoreID
	LEFT OUTER JOIN Export.StoreMatch AS DE WITH (NOLOCK) ON DE.StoreID = DS.StoreID
	LEFT OUTER JOIN Export.Configuration AS CM WITH (NOLOCK) ON DE.ConfigID = CM.ConfigID
	WHERE DU.UserID = @UserID
		AND Allow = 1
		AND DM.StoreID IS NULL
	FOR XML PATH('Store')
		-- ==================================================================================
END