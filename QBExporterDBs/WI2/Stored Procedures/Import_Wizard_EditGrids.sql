﻿
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 08/05/2013
-- Description: 
-- =============================================
CREATE PROCEDURE [WI2].[Import_Wizard_EditGrids]
(
	  @SessionID 			uniqueidentifier
	, @OrganizationID		uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON

	-- =========  DECLARATIONS  =========================================================
	DECLARE  
		@UserID					bigint
	-- ==================================================================================

	-- =========  SET VALUES  ===========================================================
	SET @UserID = dbo.GetUserBySessionID( @SessionID )
	IF (@UserID IS NULL) 
	BEGIN
		PRINT 'UserID is NULL'
		RETURN
	END
	
	-- =========  SELECT Dictionaries  ==================================================
	SELECT	  Name AS [Value]
			, LongAccountNumber	AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK) 
	WHERE ListType='PayType'
	FOR XML RAW ('QBPayType')
	
	SELECT	  Name AS [Value]
			, LongAccountNumber	AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK) 
	WHERE ListType='Employee'
	ORDER BY Name ASC
	FOR XML RAW ('QBEmployee')
	
	SELECT	  Name AS [Value]
			, LongAccountNumber	AS [ValueID]
	FROM Dict.ConfigFields WITH (NOLOCK) 
	WHERE ListType='Store'
	FOR XML RAW ('QBClass')
	
	
	-- ==================================================================================
	

END