﻿CREATE TABLE [Dict].[ConfigFields] (
    [Name]               NVARCHAR (50)  NULL,
    [ShortAccountNumber] NVARCHAR (100) NULL,
    [LongAccountNumber]  NVARCHAR (100) NULL,
    [ConfigID]           BIGINT         NULL,
    [ListType]           NVARCHAR (20)  NULL,
    [ConfigFieldsID]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [AccountType]        NVARCHAR (100) NULL,
    [ExHeader]           NVARCHAR (100) NULL
);

