﻿CREATE TABLE [Dict].[POSFields] (
    [PosFieldsID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]                 NVARCHAR (50)  NULL,
    [DefaultAccountNumber] NVARCHAR (100) NULL,
    [DetailedSet]          BIT            NULL,
    [SummarySet]           BIT            NULL,
    [OrderNumber]          INT            NULL,
    [IsRoyaltyClass]       BIT            CONSTRAINT [DF_POSFields_IsRoyaltyClass] DEFAULT ((0)) NULL,
    [SpecialGroup]         NVARCHAR (20)  NULL,
    [IsPerStore]           BIT            CONSTRAINT [DF_POSFields_IsPerStore] DEFAULT ((0)) NULL,
    [EX_Name]              NVARCHAR (100) NULL,
    [NoQuantity]           BIT            CONSTRAINT [DF_POSFields_NoQuantity] DEFAULT ((0)) NULL,
    [Expense]              BIT            CONSTRAINT [DF_POSFields_Expense] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_POSFields] PRIMARY KEY CLUSTERED ([PosFieldsID] ASC)
);





