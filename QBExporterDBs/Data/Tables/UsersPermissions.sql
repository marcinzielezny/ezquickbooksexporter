﻿CREATE TABLE [Data].[UsersPermissions] (
    [PermissionID] BIGINT       IDENTITY (1, 1) NOT NULL,
    [UserID]       BIGINT       NOT NULL,
    [Type]         VARCHAR (10) NOT NULL,
    [StoreID]      BIGINT       NULL,
    [Allow]        BIT          DEFAULT ((0)) NULL,
    [CreatedOn]    DATETIME     CONSTRAINT [DF_UsersPermissions_CreatedOn] DEFAULT (getdate()) NULL,
    [CreatedBy]    BIGINT       NULL,
    [ModifiedOn]   DATETIME     NULL,
    [ModifiedBy]   BIGINT       NULL
);

