﻿CREATE TABLE [Data].[DetailsData] (
    [DataID]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [StoreID]            BIGINT         NULL,
    [Value]              MONEY          NULL,
    [DefaultValue]       MONEY          NULL,
    [PosField]           BIGINT         NULL,
    [EmployeeID]         BIGINT         NULL,
    [HoursWorked]        NVARCHAR (50)  NULL,
    [Description]        NVARCHAR (255) NULL,
    [BusinessDate]       DATE           NULL,
    [CreatedOn]          DATETIME       CONSTRAINT [DF_DetailsData_CreatedOn] DEFAULT (getdate()) NULL,
    [CreatedBy]          BIGINT         NULL,
    [LastModificationOn] DATETIME       NULL,
    [LastModificationBy] BIGINT         NULL,
    [SpecialGroup]       NVARCHAR (20)  NULL,
    [SG]                 NVARCHAR (1)   NULL,
    CONSTRAINT [PK_DetailsData] PRIMARY KEY CLUSTERED ([DataID] ASC)
);




GO
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 17/05/2013
-- Description: 
-- =============================================

CREATE TRIGGER [Data].ArchiveInsert
   ON  Data.DetailsData
   AFTER INSERT
AS 
BEGIN
	
	INSERT INTO Archive.DetailsData
	  ([DataID]
      ,[StoreID]
      ,[BusinessDate]
      ,[Value]
      ,[DefaultValue]
      ,[PosField]
      ,[EmployeeID]
      ,[HoursWorked]
      ,[Operation]
      ,[CreatedOn]
      ,[CreatedBy])
     SELECT 
	   [DataID]
      ,[StoreID]
      ,[BusinessDate]
      ,[Value]
      ,[DefaultValue]
      ,[PosField]
      ,[EmployeeID]
      ,[HoursWorked]
      ,'Insert data'
      ,[CreatedOn]
      ,[CreatedBy]
     FROM INSERTED I   

END
GO
/* 2013  EZUniverseINC */
/* All rights reserved */
-- =============================================
-- Author:		MZ
-- Create date: 17/05/2013
-- Description: 
-- =============================================

CREATE TRIGGER [Data].[ArchiveUpdate]
   ON  Data.DetailsData
   AFTER UPDATE
AS 
BEGIN
	
	INSERT INTO Archive.DetailsData
	  ([DataID]
      ,[StoreID]
      ,[BusinessDate]
      ,[Value]
      ,[DefaultValue]
      ,[PosField]
      ,[EmployeeID]
      ,[HoursWorked]
      ,[Operation]
      ,[CreatedOn]
      ,[CreatedBy])
     SELECT 
	   [DataID]
      ,[StoreID]
      ,[BusinessDate]
      ,[Value]
      ,[DefaultValue]
      ,[PosField]
      ,[EmployeeID]
      ,[HoursWorked]
      ,'Update data'
      ,[LastModificationOn]
      ,[LastModificationBy]
     FROM INSERTED I   

END