﻿CREATE TABLE [Data].[Employee] (
    [EmployeeID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [StoreID]    BIGINT         NULL,
    [Name]       NVARCHAR (100) NULL,
    [EXPayType]  NVARCHAR (20)  NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([EmployeeID] ASC)
);

