﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickbooksExport
{
    public class SessionResult
    {
        public bool success;
        public int code; 
        public String description;

        public SessionResult(bool success)
        {
            this.success = success;
            this.code = 0;
           
        }

        public SessionResult(bool success, String description)
        {
            this.success = success;
            this.description = description;
            this.code = 0;
        }

        public SessionResult(bool success, String description, int code)
        {
            this.success = success;
            this.code = code;
            this.description = description;
        }
    }
}
