﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickbooksExport
{
    public static class RequestWarn
    {
        public static String errorCodeToString(int i)
        {
            switch (i)
            {
                case 0	         : return "The QuickBooks server processed the request successfully.	Status OK";
                case 1	         : return "No match.	The filters used in the query request did not return any matching objects from QuickBooks.";
                case 500	     : return "    One or more objects cannot be found	The query request has not been fully completed. There was a required element(fieldValue) that could not be found in QuickBooks.";
                case 501	     : return "    Object not in this qbXML specification	Unable to represent objectName fieldValue in this version of the qbXML spec.";
                case 510	     : return "    Object cannot be returned	Unable to return object.";
                case 530	     : return "    Unsupported field	The field fieldName is not supported by this implementation.";
                case 531	     : return "    Unsupported enum value	The enum value fieldValue in the field fieldName is not supported by this implementation.";
                case 550	     : return "    Cannot save notes	The objectName object was saved successfully, but its corresponding Notes record could not be saved.";
                case 560	     : return "    Deprecated field used	This field will not always be supported.";
                case 570	     : return "    Cannot link to transaction	Unable to link to transaction fieldValue because it has already been closed.";
                case 600	     : return "    No cleared state to return	(For error recovery; no message is returned.)";
                case 1000	     : return "Internal error	There has been an internal error when processing the request.";
                case 1010	     : return "System not available	System not available";
                case 1030	     : return "Unsupported message	This request is not supported by this implementation.";
                case 1060	     : return "Invalid request ID	The request ID fieldValue is invalid, possibly too long, max 50 chars.";
                case 3000	     : return "Invalid object ID	The given object ID fieldValue in the field fieldName is invalid.";
                case 3010	     : return "Invalid Boolean	There was an error when converting the boolean value fieldValue in the field fieldName";
                case 3020	     : return "Invalid date	There was an error when converting the date value fieldValue in the field fieldName";
                case 3030	     : return "Invalid date range	Invalid date range: From date is greater than To date.";
                case 3031	     : return "Invalid string range	The From or To values in the provided fieldName are invalid.";
                case 3035	     : return "Invalid time interval	There was an error when converting the time interval fieldValue in the field fieldName";
                case 3040	     : return "Invalid amount	There was an error when converting the amount fieldValue in the field fieldName";
                case 3045	     : return "Invalid price	There was an error when converting the price fieldValue in the field fieldName";
                case 3050	     : return "Invalid percentage	There was an error when converting the percent fieldValue in the field fieldName";
                case 3060	     : return "Invalid quantity	There was an error when converting the quantity fieldValue in the field fieldName";
                case 3065	     : return "Invalid GUID	There was an error when converting the GUID value fieldValue in the field fieldName";
                case 3070	     : return "String too long	The string fieldValue in the field fieldName is too long.";
                case 3080	     : return "Invalid string	The string fieldValue is invalid.";
                case 3085	     : return "Invalid number	There was an error when converting the number fieldValue in the field fieldName";
                case 3090	     : return "Invalid object name	There was an error when storing fieldValue in the fieldName field.";
                case 3100	     : return "Name is not unique	The name fieldValue of the list element is already in use.";
                case 3101	     : return "Resulting amount too large	Multiplying the rate and the quantity results in an amount that exceeds the maximum allowable amount.";
                case 3110	     : return "Invalid enum value	The enumerated value fieldValue in the field fieldName is unknown.";
                case 3120	     : return "Object not found	Object fieldValue specified in the request cannot be found.";
                case 3121	     : return "OwnerID not found	Data Extension Definitions specified by OwnerID fieldValue not found for this object type.";
                case 3130	     : return "Parent reference not found	There is an invalid reference to a parent fieldValue in the objectName list.";
                case 3140	     : return "Reference not found	There is an invalid reference to QuickBooks fieldName fieldValue in the objectName.";
                case 3150	     : return "Missing required element	There is a missing element fieldName.";
                case 3151	     : return "Invalid element for request	Cannot use the element fieldName in this request.";
                case 3153	     : return "Element conflict in request	This error is returned whenever there is a conflict in the elements in the request. Each element has valid value, but their combination becomes invalid.";
                case 3160	     : return "Object cannot be deleted	Cannot delete the object specified by the id = fieldValue.";
                case 3161	     : return "Cannot delete before closing date	An attempt was made to delete a fieldValue with a date that is on or before the closing date of the company. If you are sure you really want to do this, please ask a user with Admin privileges to remove the password for editing transactions on or before to closing date (this setting is in the Accounting Company Preferences), then try again.";
                case 3162	     : return "Not allowed in multi-user mode	This operation is not allowed in multi-user mode.";
                case 3170	     : return "Object cannot be modified	There was an error when modifying a fieldValue.";
                case 3171	     : return "Cannot modify before closing date	An attempt was made to modify a fieldValue with a date that is on or before the closing date of the company. If you are sure you really want to do this, please ask a user with Admin privileges to remove the password for editing transactions on or before to closing date (this setting is in the Accounting Company Preferences), then try again.";
                case 3172	     : return "Cannot modify prior to last condense	An attempt was made to modify a fieldValue with a date that is on or before the last inventory condensed date.";
                case 3173	     : return "Related object deleted or modified	The related fieldName transaction object fieldValue was deleted or modified.";
                case 3175	     : return "Object is in use	There was an error adding, modifying or deleting fieldValue because it is already in use.";
                case 3176	     : return "Related object is in use	The related fieldName transaction object fieldValue is already in use. case  specified by the ID is appended to fieldValue if necessary.";
                case 3177	     : return "Duplicate AppliedToTxn IDs	The transaction object fieldValue may only be provided once in this request.";
              //  case 3180	     : return "Object cannot be added	There was an error when saving a fieldValue.";
                case 3185	     : return "Object cannot be voided	Cannot void the object specified by the id = fieldValue";
                case 3190	     : return "Cannot clear required element	Cannot clear the element in the fieldName field.";
                case 3200	     : return "Outdated edit sequence	The provided edit sequence fieldValue is out-of-date.";
                case 3205	     : return "Invalid address	There was an error when composing an address in fieldValue";
                case 3210	     : return "Other validation error	The fieldName field has an invalid value fieldValue";
                case 3220	     : return "Not authorized operation	There is no permission to perform this request, or the feature has been turned off in QuickBooks.";
                case 3230	     : return "Status rollback	The request has been rolled-back.";
              //  case 3231	     : return "Status unprocessed	The request has not been processed.";
                case 3240	     : return "Time creation mismatch	Object fieldValue specified in the request cannot be found.";
                case 3250	     : return "Feature not enabled	This feature is not enabled or not available in this version of QuickBooks.";
                case 3260	     : return "Insufficient permissions	Insufficient permission level to perform this action.";
                case 3261	     : return "Application has no sensitive data permission	The integrated application has no permission to access sensitive data.";
                case 3262	     : return "Requires payroll subscription	In order to complete this request, the company data file has to be subscribed to the Intuit Payroll Service.";
                case 3263	     : return "Not authorized for write access	This request cannot be completed because the integrated application had requested read-only access. Have the integrated application request read/write access, and have the QuickBooks administrator grant this access.";
                case 3270	     : return "Missing posting account	Missing posting account.";
                case 3280	     : return "Item type mismatch	The item fieldValue cannot be used in this line item. It does not have a correct type.";
                case 3290	     : return "Item line out of order	The line items in the request are in a different order than the line items in the transaction.";
                case 3300	     : return "Cannot open requested window	Could not open the requested objectName form or window.";
                case 3301	     : return "Not allowed in unattended mode	Cannot perform this request unless an interactive QuickBooks user is logged in.";
                case 3310	     : return "Unknown employee time status	Failed to save the Time Tracking transaction. The employee fieldValue provided in the TimeTrackingAdd request has the checkbox Use time data to create paychecks set to the Unknown state. Have your application ask the user whether or not to set time tracking for this employee. Then issue an EmployeeMod request to set this option to either True or False. If True, activities will be transferred to paychecks.";
                case 3320	     : return "Could not create report	The required report could not be generated.";
                case 3330	     : return "Invalid GUID for request	Cannot use the value fieldValue in the fieldName field in this request.";
                case 3340	     : return "Not allowed in data event callback	This request cannot be processed from within a data event callback procedure.";
                case 3350	     : return "Custom field list is full	Unable to define a new public data extension; the list of public extension definitions is full.";
                case 3351	     : return "Invalid type for custom field	The value or values provided for AssignToObject or RemoveFromObject may not be used for public data extension requests.";
                case 3352	     : return "Not allowed to reuse custom field	The data extension named fieldValue was previously defined with a different, incompatible AssignToObject. Unable to use the AssignToObject type in this request.";
                case 3360	     : return "Callback app cannot be verified	The callback application cannot be found from the CLSID or ProgID provided in the subscription request.";
                case 9001	     : return "Invalid checksum	(For error recovery; no message is returned.)";
                case 9002	     : return "No stored response found	(For error recovery; no message is returned.)";
                case 9003	     : return "Reinitialization problem	(For error recovery; no message is returned.)";
                case 9004	     : return "Invalid message ID	(For error recovery; no message is returned.)";
                case 9005	     : return "An error recovery record could not be saved	An attempt was made to save an error recovery record for a message set. The save operation failed and the record wasn't saved.";
                case 9100	     : return "Macro name not unique	The macro name fieldValue is already in use; it may only be defined once.";
                case 9101	     : return "Macro name too long	The macro name fieldValue is too long.";
                case 9102	     : return "Macro name invalid	The macro name fieldValue contains invalid characters.";
                case 9103	     : return "Macro substitution failure	The request was unable to use a macro value, probably due to an earlier error encountered when defining the macro.";
                //case 0x80040300  : return " Attempted to retrieve a value before it has been set.";
                //case 0x80040301  : return " Internal error interpreting the response.";
                //case 0x80040302  : return " The given enum value is invalid.";
                //case 0x80040303  : return " The given numeric value is out of range.";
                //case 0x80040304  : return " The given string is longer than the maximum length allowed. (Note that for many strings, the maximum length is different for desktop versions of QuickBooks than for QuickBooks Online Edition.)";
                //case 0x80040305  : return " The given value has an invalid format.";
                //case 0x80040306  : return " Attempted to retrieve a value that has been set to empty.";
                //case 0x80040307  : return " This message will indicate why the verification of the request set failed.";
                //case 0x80040308  : return " Could not communicate with the QuickBooks SDK.";
                //case 0x80040309  : return " The QuickBooks SDK is a pre-release version."
                //case 0x8004030A  : return " The given version of qbXML is not supported.";
                //case 0x8004030B  : return " This feature is not supported in the specified version of qbXML.";
                //case 0x8004030C  : return " This message set must be used with the QBSessionManager, not QBOESessionManager. -or- This message set must be used with the QBOESessionManager, not QBSessionManager.";
                //case 0x8004030D  : return " HTTP-specific error text.";
                //case 0x8004030E  : return " (Specific error-recovery messages will be sent with this HRESULT.)";
                //case 0x8004030F  : return " This function is supported by a newer version of Request Processor.";
                //case 0x80040310  : return " error recovery is enabled. Process or clear the saved Response status before issuing another request.";
                //case 0x80040311  : return " The ConnectionTicket must be filled in.";
                //case 0x80040312  : return " The Country value is invalid.";
                //case 0x80040400  : return " QuickBooks found an error when parsing the provided XML text stream.";
                //case 0x80040401  : return " Could not access QuickBooks.";
                //case 0x80040402  : return " Unexpected error. Check the qbsdklog.txt file for possible, additional information.";
                //case 0x80040403  : return " Could not open the specified QuickBooks company data file.";
                //case 0x80040404  : return " The version of QuickBooks currently running does not support qbXML.";
                //case 0x80040405  : return " qbXML components have not been installed.";
                //case 0x80040406  : return " Could not determine the version of the QuickBooks company data file, or the data file has been modified and requires a newer version of QuickBooks.";
                //case 0x80040407  : return " The installation of QuickBooks appears to be incomplete. Please reinstall QuickBooks.";
                //case 0x80040408  : return " Could not start QuickBooks.";
                //case 0x80040409  : return " The current version of QuickBooks cannot work with the specified company data file.";
                //case 0x8004040A  : return " QuickBooks company data file is already open and it is different from the one requested.";
                //case 0x8004040B  : return " Could not get the name of the current QuickBooks company data file.";
                //case 0x8004040C  : return " BeginSession method has not been called or it did not succeed.";
                //case 0x8004040D  : return " The ticket parameter is invalid.";
                //case 0x8004040E  : return " There is not enough memory to complete the request.";
                //case 0x8004040F  : return " The OpenConnection method has not been called.";
                //case 0x80040410  : return " The QuickBooks company data file is currently open in a mode other than the one specified by your application.";
                //case 0x80040411  : return " Before calling the BeginSession method, you must call the EndSession method to terminate the current session.";
                //case 0x80040412  : return " You cannot make multiple successive calls to the OpenConnection method. Call CloseConnection before calling OpenConnection again.";
                //case 0x80040413  : return " QuickBooks does not support the rollbackOnError value of the onError attribute.";
                //case 0x80040414  : return " A modal dialog box is showing in the QuickBooks user interface. Your application cannot access QuickBooks until the user dismisses the dialog box.";
                //case 0x80040415  : return " A call to the OpenConnection method must include the name of your application.";
                //case 0x80040416  : return " If QuickBooks is not running, a call to the BeginSession method must include the name of the QuickBooks company data file.";
                //case 0x80040417  : return " If the QuickBooks company data file is not open, a call to the BeginSession method must include the name of the data file.";
                //case 0x80040418  : return " This application has not accessed this QuickBooks company data file before. Only the QuickBooks administrator can grant an application permission to access a QuickBooks company data file for the first time.";
                //case 0x80040419  : return " This applications certificate is invalid. An application must have a valid certificate to access QuickBooks company data files.";
                //case 0x8004041A  : return " This application does not have permission to access this QuickBooks company data file. The QuickBooks administrator can grant access permission through the Integrated Application preferences.";
                //case 0x8004041B  : return " Unable to lock the necessary information to allow this application to access this company data file. Try again later.";
                //case 0x8004041C  : return " An internal QuickBooks error occurred while trying to access the QuickBooks company data file.";
                //case 0x8004041D  : return " This application is not allowed to log into this QuickBooks company data file automatically. The QuickBooks administrator can grant permission for automatic login through the Integrated Application preferences.";
                //case 0x8004041E  : return " This applications certificate is expired. If you want to allow the application to log into QuickBooks automatically, log into QuickBooks and try again. Then click Allow Always when you are notified that the certificate has expired.";
                //case 0x8004041F  : return " QuickBooks Basic cannot accept XML requests. Another product in the QuickBooks line, such as QuickBooks Pro or Premier, 2002 or later, is required.";
                //case 0x80040420  : return " The QuickBooks user has denied access.";
                //case 0x80040421  : return " The returned text is passed via the qbXML COM Request Processor directly from QuickBooks to your application and is not issued by the qbXML COM Request Processor itself. You may find it useful to copy the text verbatim to your message window.";
                //case 0x80040422  : return " This application requires Single User file access mode and there is already another application sharing data with this QuickBooks company data file.";
                //case 0x80040423  : return " The version of qbXML that was requested is not supported or is unknown.";
                //case 0x80040424  : return " QuickBooks did not finish its initialization. Please try again later.";
                //case 0x80040425  : return " Invalid parameter.";
                //case 0x80040426  : return " Scripts are not allowed to call QBXMLRP.";
                //case 0x80040427  : return " Unregistered QuickBooks.";
                //case 0x80040428  : return " The current request processor does not support the request.";
                //case 0x80040429  : return " The current messageset is not supported.";
                //case 0x8004042A  : return " Remote access is not allowed.";
                //case 0x8004042B  : return " Unsupported interface.";
                //case 0x8004042C  : return " Certificate has been revoked.";
                default:
                    return "unknown";
                }
        }


        public static String errorCodeToString(int i, String Message)
        {
            String errorMessage = errorCodeToString(i);

            if (errorMessage == "unknown")
            {
                errorMessage = Message;
            }


            return errorMessage; 

        }


    }
}

