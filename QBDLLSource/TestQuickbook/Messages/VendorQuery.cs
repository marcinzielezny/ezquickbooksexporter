﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class VendorQuery : IMessageHandler
    {
        List<Vendor> vendorList;

        public VendorQuery()
        {
            vendorList = new List<Vendor>();
        }

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            SessionResult result;
            if (responseMsgSet == null)
                return new SessionResult(false);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false);

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);

                if (response.StatusCode >= 0)
                {
                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtVendorQueryRs)
                        {
                            IVendorRetList VendorRet = (IVendorRetList)response.Detail;
                            result = WalkVendorRet(VendorRet);
                        }
                    }
                }
            }
            return new SessionResult(true);
        }

        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            IVendorQuery VendorQueryRq = requestMsgSet.AppendVendorQueryRq();

            VendorQueryRq.ORVendorListQuery.VendorListFilter.ActiveStatus.SetValue(ENActiveStatus.asAll);

  

            return new SessionResult(true);
        }

        private SessionResult WalkVendorRet(IVendorRetList VendorRet)
        {

            //Adding intentionaly blank vendor

            vendorList.Add(new Vendor("", ""));
            
            for (int i = 0; i < VendorRet.Count; i++)
            {
                IVendorRet ier = VendorRet.GetAt(i);

                if (ier != null)
                {
                    vendorList.Add(new Vendor((ier.Name != null) ? ier.Name.GetValue() : ""
                                                  , (ier.ListID != null) ? ier.ListID.GetValue() : ""));
                }   
            }

            return new SessionResult(true);
        }

        public List<Vendor> GetVendorList()
        {
            return vendorList;
        }
    }
}
