﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;
using System.Globalization;

namespace QuickbooksExport
{
    public class JournalEntryInsert : IMessageHandler
    {
        List<JournalEntry> journalEnrties;

        public JournalEntryInsert(List<JournalEntry> journalEnrties)
        {
            this.journalEnrties = new List<JournalEntry>();
            this.journalEnrties = journalEnrties;
        }

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {

            StringBuilder sb = new StringBuilder("");
            int code = 0; 

            if (responseMsgSet == null)
                return new SessionResult(false);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false);

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                if (response.StatusCode >= 0)
                {
                    sb.AppendLine(RequestWarn.errorCodeToString(response.StatusCode));
                    sb.AppendLine(response.StatusMessage);
                    code = response.StatusCode; 

                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtJournalEntryAddRs)
                        {
                            IJournalEntryRet JournalEntryRet = (IJournalEntryRet)response.Detail;
                        }
                    }
                }
            }

            return new SessionResult(true, sb.ToString(), code);
        }


        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            requestMsgSet.Attributes.OnError = ENRqOnError.roeContinue;
            IJournalEntryAdd JournalEntryAddRq = requestMsgSet.AppendJournalEntryAddRq();

            for(int i = 0; i<journalEnrties.Count; i++)
            {
                JournalEntryAddRq.TxnDate.SetValue(journalEnrties[i].BusinessDate);
                JournalEntryAddRq.RefNumber.SetValue(journalEnrties[i].ExportNo);

                if(journalEnrties[i].AccountType == EntryType.Debit)
                {
                    IORJournalLine ORJournalLineListElement130 = JournalEntryAddRq.ORJournalLineList.Append();
                
                    ORJournalLineListElement130.JournalDebitLine.AccountRef.ListID.SetValue(journalEnrties[i].AccountListID);
                    ORJournalLineListElement130.JournalDebitLine.AccountRef.FullName.SetValue(journalEnrties[i].AccountFullName);
                    ORJournalLineListElement130.JournalDebitLine.Memo.SetValue(journalEnrties[i].Description);
                    ORJournalLineListElement130.JournalDebitLine.Amount.SetValue(journalEnrties[i].Amount);

                    if (!String.IsNullOrEmpty(journalEnrties[i].ClassListID))
                        ORJournalLineListElement130.JournalDebitLine.ClassRef.ListID.SetValue(journalEnrties[i].ClassListID);

                    if (!String.IsNullOrEmpty(journalEnrties[i].VendorListID))
                     ORJournalLineListElement130.JournalDebitLine.EntityRef.ListID.SetValue(journalEnrties[i].VendorListID);
                }
                else if( journalEnrties[i].AccountType == EntryType.Credit)
                {
                 
                     IORJournalLine ORJournalLineListElement131 = JournalEntryAddRq.ORJournalLineList.Append();
                     ORJournalLineListElement131.JournalCreditLine.AccountRef.ListID.SetValue(journalEnrties[i].AccountListID);
                     ORJournalLineListElement131.JournalCreditLine.AccountRef.FullName.SetValue(journalEnrties[i].AccountFullName);
                     ORJournalLineListElement131.JournalCreditLine.Memo.SetValue(journalEnrties[i].Description);
                     ORJournalLineListElement131.JournalCreditLine.Amount.SetValue(journalEnrties[i].Amount);

                    if (!String.IsNullOrEmpty(journalEnrties[i].ClassListID))
                         ORJournalLineListElement131.JournalCreditLine.ClassRef.ListID.SetValue(journalEnrties[i].ClassListID);

                    if (!String.IsNullOrEmpty(journalEnrties[i].VendorListID))
                        ORJournalLineListElement131.JournalCreditLine.EntityRef.ListID.SetValue(journalEnrties[i].VendorListID);
                }
            }

            Console.WriteLine(requestMsgSet.ToXMLString());
            return new SessionResult(true);
        }
    }
}
