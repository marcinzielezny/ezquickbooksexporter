﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public interface IMessageHandler
    {
        SessionResult HandleResponse(IMsgSetResponse responseMsgSet);
        SessionResult BuildRequest(IMsgSetRequest requestMsgSet);
    }
}
