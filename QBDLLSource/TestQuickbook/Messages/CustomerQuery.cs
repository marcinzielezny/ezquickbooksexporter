﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class CustomerInsert : IMessageHandler
    {
         Customer myCust; 

        public CustomerInsert(Customer cust)
        {
            this.myCust = new Customer();
            this.myCust = cust;
        }

        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {

            requestMsgSet.Attributes.OnError = ENRqOnError.roeStop; 

            try
            {
                if (myCust!=null)
                {

                    ICustomerAdd CustomerAddRq = requestMsgSet.AppendCustomerAddRq();

                    CustomerAddRq.FirstName.SetValue("POS");
                    CustomerAddRq.LastName.SetValue("Exporter");
                    CustomerAddRq.Name.SetValue("POS Exporter");
                   
                }
             
            }
            catch (Exception ex)
            {
                return new SessionResult(false, ex.Message, -1);
            }

            Console.WriteLine(requestMsgSet.ToXMLString());

            return new SessionResult(true);
        }


        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            StringBuilder sb = new StringBuilder("");
            int code = 0;

            if (responseMsgSet == null)
                return new SessionResult(false, "Response message is null");

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false, "Response message list is null");

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                if (response.StatusCode >= 0)
                {
                    sb.AppendLine(RequestWarn.errorCodeToString(response.StatusCode, response.StatusMessage));
                    sb.AppendLine(response.StatusCode.ToString());
                    code += response.StatusCode;

                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtCustomerAddRs)
                        {
                            ICustomerRet CustomerRet = (ICustomerRet)response.Detail;                            
                        }
                    }
                }
            }
            return new SessionResult(true,sb.ToString(), code);
        }
    }
}
