﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class TimesheetInsert : IMessageHandler
    {
        List<TimesheetEntry> entryList;    

        public TimesheetInsert(List<TimesheetEntry> tsEntry)
        {
            this.entryList = new List<TimesheetEntry>();
            this.entryList = tsEntry;
        }

        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {

            requestMsgSet.Attributes.OnError = ENRqOnError.roeStop; 

            try
            {
                for (int i = 0; i < entryList.Count; i++)
                {

                    ITimeTrackingAdd TimeTrackingAddRq = requestMsgSet.AppendTimeTrackingAddRq();

                    TimeTrackingAddRq.EntityRef.FullName.SetValue(entryList[i].Employee.Name);
                    TimeTrackingAddRq.EntityRef.ListID.SetValue(entryList[i].Employee.ListID);

                    TimeTrackingAddRq.PayrollItemWageRef.FullName.SetValue(entryList[i].PItem.Name);
                    TimeTrackingAddRq.PayrollItemWageRef.ListID.SetValue(entryList[i].PItem.ListID);

                    TimeTrackingAddRq.Duration.SetValue((short)entryList[i].Duration.TotalHours, (short)entryList[i].Duration.Minutes, (short)entryList[i].Duration.Seconds, false);
                    TimeTrackingAddRq.TxnDate.SetValue(entryList[i].BusinessDate);

                    if (!String.IsNullOrEmpty(entryList[i].ClassID))
                    {
                        TimeTrackingAddRq.ClassRef.ListID.SetValue(entryList[i].ClassID);
                    }

                    TimeTrackingAddRq.IsBillable.SetValue(false);
                }
             
            }
            catch (Exception ex)
            {
                return new SessionResult(false, ex.Message, -1);
            }

            Console.WriteLine(requestMsgSet.ToXMLString());
            string st = requestMsgSet.ToXMLString(); 

            return new SessionResult(true);
        }


        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            StringBuilder sb = new StringBuilder("");
            int code = 0;

            if (responseMsgSet == null)
                return new SessionResult(false, "Response message is null");

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false, "Response message list is null");

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                if (response.StatusCode >= 0)
                {
                    sb.AppendLine(RequestWarn.errorCodeToString(response.StatusCode, response.StatusMessage));
                    sb.AppendLine(response.StatusCode.ToString());
                    code += response.StatusCode;

                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtTimeTrackingAddRs)
                        {
                            ITimeTrackingRet TimeTrackingRet = (ITimeTrackingRet)response.Detail;                            
                        }
                    }
                }
            }
            return new SessionResult(true,sb.ToString(), code);
        }
    }
}
