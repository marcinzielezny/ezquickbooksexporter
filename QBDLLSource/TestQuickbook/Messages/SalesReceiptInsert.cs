﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;
using System.Globalization;

namespace QuickbooksExport
{
    public class SalesReceiptInsert : IMessageHandler
    {
        List<SalesReceipt> salesReceipts;

        public SalesReceiptInsert(List<SalesReceipt> salesReceipts)
        {
            this.salesReceipts = new List<SalesReceipt>();
            this.salesReceipts = salesReceipts;
        }

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {

            StringBuilder sb = new StringBuilder("");
            int code = 0;

            if (responseMsgSet == null)
                return new SessionResult(false);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false);

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                if (response.StatusCode >= 0)
                {
                    sb.AppendLine(RequestWarn.errorCodeToString(response.StatusCode));
                    sb.AppendLine(response.StatusMessage);
                    code = response.StatusCode;

                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtJournalEntryAddRs)
                        {
                            IJournalEntryRet JournalEntryRet = (IJournalEntryRet)response.Detail;
                        }
                    }
                }
            }

            return new SessionResult(true, sb.ToString(), code);
        }


        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            if (salesReceipts.Count > 0)
            {
                requestMsgSet.Attributes.OnError = ENRqOnError.roeContinue;

                ISalesReceiptAdd salesReceiptAddRq = requestMsgSet.AppendSalesReceiptAddRq();


                salesReceiptAddRq.TxnDate.SetValue(salesReceipts[0].BusinessDate);
                salesReceiptAddRq.RefNumber.SetValue(salesReceipts[0].StoreName);
               
                salesReceiptAddRq.Memo.SetValue(salesReceipts[0].StoreName + "/" + salesReceipts[0].ExportNo);
                salesReceiptAddRq.BillAddress.Addr1.SetValue("POS Exporter");
                salesReceiptAddRq.BillAddress.Addr2.SetValue("");
                salesReceiptAddRq.ClassRef.ListID.SetValue(salesReceipts[0].ClassListID); 


                for (int i = 0; i < salesReceipts.Count; i++)
                {
                    IORSalesReceiptLineAdd ORSalesReceiptListElement130 = salesReceiptAddRq.ORSalesReceiptLineAddList.Append();

                    ORSalesReceiptListElement130.SalesReceiptLineAdd.ItemRef.FullName.SetValue(salesReceipts[i].Item);
                    ORSalesReceiptListElement130.SalesReceiptLineAdd.Desc.SetValue(salesReceipts[i].Description);
                    ORSalesReceiptListElement130.SalesReceiptLineAdd.Amount.SetValue(salesReceipts[i].Amount);

                    if (salesReceipts[i].Quantity != null)
                    {
                        ORSalesReceiptListElement130.SalesReceiptLineAdd.Quantity.SetValue((double)salesReceipts[i].Quantity);
                    }

                    ORSalesReceiptListElement130.SalesReceiptLineAdd.ORRatePriceLevel.Rate.SetValue(salesReceipts[i].Rate);
                    ORSalesReceiptListElement130.SalesReceiptLineAdd.ClassRef.ListID.SetValue(salesReceipts[i].ClassListID);

                }

                Console.WriteLine(requestMsgSet.ToXMLString());
            }

            return new SessionResult(true);
        }
    }
}
