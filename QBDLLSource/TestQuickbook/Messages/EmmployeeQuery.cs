﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class EmmployeeQuery : IMessageHandler
    {
        List<Employee> employeeList;

        public EmmployeeQuery()
        {
            employeeList = new List<Employee>();
        }

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            SessionResult result;
            if (responseMsgSet == null)
                return new SessionResult(false);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false);

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);

                if (response.StatusCode >= 0)
                {
                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtEmployeeQueryRs)
                        {
                            IEmployeeRetList EmployeeRet = (IEmployeeRetList)response.Detail;
                            result = WalkEmployeeRet(EmployeeRet);
                        }
                    }
                }
            }
            return new SessionResult(true);
        }

        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            IEmployeeQuery EmployeeQueryRq = requestMsgSet.AppendEmployeeQueryRq();

            EmployeeQueryRq.ORListQuery.ListFilter.ActiveStatus.SetValue(ENActiveStatus.asAll);

            //TODO: extend filters list

            return new SessionResult(true);
        }

        private SessionResult WalkEmployeeRet(IEmployeeRetList EmployeeRet)
        {
            for (int i = 0; i < EmployeeRet.Count; i++)
            {
                IEmployeeRet ier = EmployeeRet.GetAt(i);

                if (ier != null && ier.IsActive.GetValue() == true )
                {
                    employeeList.Add(new Employee((ier.Name != null) ? ier.Name.GetValue() : ""
                                                  , (ier.ListID != null) ? ier.ListID.GetValue() : "")
                                                  );
                }   
            }

            return new SessionResult(true);
        }

        public List<Employee> GetEmployeeList()
        {
            return employeeList;
        }
    }
}
