﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class PayrollItemQuery : IMessageHandler
    {
        List<PayrollItem> payrollList;

        public PayrollItemQuery()
        {
            payrollList = new List<PayrollItem>();
        }

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {

            if (responseMsgSet == null)
                return new SessionResult(true);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(true);

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                if (response.StatusCode >= 0)
                {
                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtPayrollItemWageQueryRs)
                        {
                            IPayrollItemWageRetList PayrollItemWageRet = (IPayrollItemWageRetList)response.Detail;
                            WalkPayrollItemWageRet(PayrollItemWageRet);
                        }
                    }
                }
            }

            return new SessionResult(true);
        }

        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            IPayrollItemWageQuery PayrollItemWageQueryRq = requestMsgSet.AppendPayrollItemWageQueryRq();
            //TODO: extend filters list

            return new SessionResult(true);
        }

        private void WalkPayrollItemWageRet(IPayrollItemWageRetList PayrollItemWageRet)
        {
            for (int i = 0; i < PayrollItemWageRet.Count; i++)
            {
                IPayrollItemWageRet ipiwr = PayrollItemWageRet.GetAt(i);
                if (ipiwr != null)
                {
                    payrollList.Add(new PayrollItem((ipiwr.Name != null) ? ipiwr.Name.GetValue() : ""
                                                    , (ipiwr.ListID != null) ? ipiwr.ListID.GetValue():""
                                                   ));
                }
            }
        }

        public List<PayrollItem> GetPayrollItemList()
        {
            return payrollList;
        }



    }
}
