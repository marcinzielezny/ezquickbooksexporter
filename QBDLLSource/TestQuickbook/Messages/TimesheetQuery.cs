﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace TestQuickbook
{
    public class TimesheetQuery : IMessageHandler
    {
        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false); 

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);

                if (response.StatusCode >= 0)
                {
                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtTimeTrackingQueryRs)
                        {
                            ITimeTrackingRetList TimeTrackingRet = (ITimeTrackingRetList)response.Detail;
                            WalkTimeTrackingRet(TimeTrackingRet);
                        }
                    }
                }
            }

            return new SessionResult(true);
        }

        private void WalkTimeTrackingRet(ITimeTrackingRetList TimeTrackingRetAll)
        {

            for (int i = 0; i < TimeTrackingRetAll.Count; i++)
            {
                ITimeTrackingRet TimeTrackingRet = TimeTrackingRetAll.GetAt(i);

                if (TimeTrackingRet != null)
                {
                    Console.WriteLine("{0} | ", (string)TimeTrackingRet.TxnID.GetValue());
                    Console.WriteLine("{0} | ", (DateTime)TimeTrackingRet.TimeCreated.GetValue());
                    Console.WriteLine("{0} | ", (DateTime)TimeTrackingRet.TimeModified.GetValue());
                    Console.WriteLine("{0} | ", (string)TimeTrackingRet.EditSequence.GetValue());
                    Console.WriteLine("{0} | ", (int)TimeTrackingRet.TxnNumber.GetValue());
                    Console.WriteLine("{0} | ", (DateTime)TimeTrackingRet.TxnDate.GetValue());
                    Console.WriteLine("{0} | ", (string)TimeTrackingRet.EntityRef.ListID.GetValue());
                    Console.WriteLine("{0} | ", (string)TimeTrackingRet.EntityRef.FullName.GetValue());
                    Console.WriteLine("{0} | ", (string)TimeTrackingRet.PayrollItemWageRef.ListID.GetValue());
                    Console.WriteLine("{0} | ", (string)TimeTrackingRet.PayrollItemWageRef.FullName.GetValue());
                    Console.WriteLine("\n \n", TimeTrackingRetAll.Count);
                }
            }

            Console.WriteLine("\n Items number: {0} \n", TimeTrackingRetAll.Count);
        }

        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            ITimeTrackingQuery TimeTrackingQueryRq = requestMsgSet.AppendTimeTrackingQueryRq();

            //TODO: extend filters list

            return new SessionResult(true);
        }
    }
}
