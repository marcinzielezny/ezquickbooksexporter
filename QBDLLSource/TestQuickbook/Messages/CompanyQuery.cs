﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class CompanyQuery : IMessageHandler
    {
        Company company;

        public CompanyQuery()
        {
            company = new Company();
        }

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            SessionResult result;

            if (responseMsgSet == null)
                return new SessionResult(false);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false);

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);

                if (response.StatusCode >= 0)
                {
                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtCompanyQueryRs)
                        {
                            ICompanyRet CompanyRet = (ICompanyRet)response.Detail;
                            result = WalkCompanyRet(CompanyRet);
                        }
                    }
                }
            }

            return new SessionResult(true);
        }

        
        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            ICompanyQuery AccountQueryRq = requestMsgSet.AppendCompanyQueryRq();

            return new SessionResult(true);
        }


        private SessionResult WalkCompanyRet(ICompanyRet CompanyRet)
        {
            if (CompanyRet == null) 
                return new SessionResult(false);


            if (CompanyRet.CompanyName != null)
            {
                company.Name = (string)CompanyRet.CompanyName.GetValue();
            }

            if (CompanyRet.LegalCompanyName != null)
            {
                company.ListID = (string)CompanyRet.LegalCompanyName.GetValue();
            }


            return new SessionResult(true);
        }


        public Company GetCompany()
        {
            return company;
        }

    }
}
