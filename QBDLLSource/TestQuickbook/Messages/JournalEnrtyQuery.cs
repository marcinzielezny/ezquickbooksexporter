﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class JournalEntryQuery : IMessageHandler
    {

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            if (responseMsgSet == null)
                return new SessionResult(false);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false);

            //if we sent only one request, there is only one response, we'll walk the list for this sample
            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                //check the status code of the response, 0=ok, >0 is warning
                if (response.StatusCode >= 0)
                {
                    //the request-specific response is in the details, make sure we have some
                    if (response.Detail != null)
                    {
                        //make sure the response is the type we're expecting
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtJournalEntryQueryRs)
                        {
                            //upcast to more specific type here, this is safe because we checked with response.Type check above
                            IJournalEntryRetList JournalEntryRet = (IJournalEntryRetList)response.Detail;
                            WalkJournalEntryRet(JournalEntryRet);
                        }
                    }
                }
            }


            return new SessionResult(true);
        }


        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            IJournalEntryQuery JournalEntryQueryRq = requestMsgSet.AppendJournalEntryQueryRq();

            return new SessionResult(true);
        }


        private void WalkJournalEntryRet(IJournalEntryRetList JournalEntryRetList)
        {
            for (int i = 0; i < JournalEntryRetList.Count; i++)
            {
                IJournalEntryRet JournalEntryRet = JournalEntryRetList.GetAt(i);

                string TxnID40 = (string)JournalEntryRet.TxnID.GetValue();
                Console.WriteLine("{0}", TxnID40);

                DateTime TimeCreated41 = (DateTime)JournalEntryRet.TimeCreated.GetValue();
                Console.WriteLine("{0}", TimeCreated41);

                DateTime TimeModified42 = (DateTime)JournalEntryRet.TimeModified.GetValue();
                Console.WriteLine("{0}", TimeModified42);

                string EditSequence43 = (string)JournalEntryRet.EditSequence.GetValue();
                Console.WriteLine("{0}", EditSequence43);
                Console.WriteLine("===================================================");

                if (JournalEntryRet.TxnNumber != null)
                {
                    int TxnNumber44 = (int)JournalEntryRet.TxnNumber.GetValue();
                    Console.WriteLine("{0}", TxnNumber44);

                }
                DateTime TxnDate45 = (DateTime)JournalEntryRet.TxnDate.GetValue();
                Console.WriteLine("{0}", TxnDate45);


                if (JournalEntryRet.RefNumber != null)
                {
                    string RefNumber46 = (string)JournalEntryRet.RefNumber.GetValue();
                    Console.WriteLine("{0}", RefNumber46);

                }
                if (JournalEntryRet.ORJournalLineList != null)
                {
                    for (int i47 = 0; i47 < JournalEntryRet.ORJournalLineList.Count; i47++)
                    {
                        IORJournalLine ORJournalLine = JournalEntryRet.ORJournalLineList.GetAt(i47);
                        if (ORJournalLine.JournalDebitLine != null)
                        {
                            if (ORJournalLine.JournalDebitLine != null)
                            {
                                if (ORJournalLine.JournalDebitLine.TxnLineID != null)
                                {
                                    string TxnLineID48 = (string)ORJournalLine.JournalDebitLine.TxnLineID.GetValue();
                                    Console.WriteLine("{0}", TxnLineID48);

                                }
                                if (ORJournalLine.JournalDebitLine.AccountRef != null)
                                {
                                    if (ORJournalLine.JournalDebitLine.AccountRef.ListID != null)
                                    {
                                        string ListID49 = (string)ORJournalLine.JournalDebitLine.AccountRef.ListID.GetValue();
                                        Console.WriteLine("{0}", ListID49);

                                    }
                                    if (ORJournalLine.JournalDebitLine.AccountRef.FullName != null)
                                    {
                                        string FullName50 = (string)ORJournalLine.JournalDebitLine.AccountRef.FullName.GetValue();
                                        Console.WriteLine("{0}", FullName50);

                                    }
                                }
                                if (ORJournalLine.JournalDebitLine.Amount != null)
                                {
                                    double Amount51 = (double)ORJournalLine.JournalDebitLine.Amount.GetValue();
                                    Console.WriteLine("{0}", Amount51);

                                }
                                if (ORJournalLine.JournalDebitLine.Memo != null)
                                {
                                    string Memo52 = (string)ORJournalLine.JournalDebitLine.Memo.GetValue();
                                    Console.WriteLine("{0}", Memo52);

                                }
                                if (ORJournalLine.JournalDebitLine.EntityRef != null)
                                {
                                    if (ORJournalLine.JournalDebitLine.EntityRef.ListID != null)
                                    {
                                        string ListID53 = (string)ORJournalLine.JournalDebitLine.EntityRef.ListID.GetValue();
                                        Console.WriteLine("{0}", ListID53);

                                    }
                                    if (ORJournalLine.JournalDebitLine.EntityRef.FullName != null)
                                    {
                                        string FullName54 = (string)ORJournalLine.JournalDebitLine.EntityRef.FullName.GetValue();
                                        Console.WriteLine("{0}", FullName54);

                                    }
                                }
                                if (ORJournalLine.JournalDebitLine.ClassRef != null)
                                {
                                    if (ORJournalLine.JournalDebitLine.ClassRef.ListID != null)
                                    {
                                        string ListID55 = (string)ORJournalLine.JournalDebitLine.ClassRef.ListID.GetValue();
                                        Console.WriteLine("{0}", ListID55);

                                    }
                                    if (ORJournalLine.JournalDebitLine.ClassRef.FullName != null)
                                    {
                                        string FullName56 = (string)ORJournalLine.JournalDebitLine.ClassRef.FullName.GetValue();
                                        Console.WriteLine("{0}", FullName56);

                                    }
                                }
                            }
                        }
                        if (ORJournalLine.JournalCreditLine != null)
                        {
                            if (ORJournalLine.JournalCreditLine != null)
                            {
                                if (ORJournalLine.JournalCreditLine.TxnLineID != null)
                                {
                                    string TxnLineID57 = (string)ORJournalLine.JournalCreditLine.TxnLineID.GetValue();
                                    Console.WriteLine("{0}", TxnLineID57);

                                }
                                if (ORJournalLine.JournalCreditLine.AccountRef != null)
                                {
                                    if (ORJournalLine.JournalCreditLine.AccountRef.ListID != null)
                                    {
                                        string ListID58 = (string)ORJournalLine.JournalCreditLine.AccountRef.ListID.GetValue();
                                        Console.WriteLine("{0}", ListID58);

                                    }
                                    if (ORJournalLine.JournalCreditLine.AccountRef.FullName != null)
                                    {
                                        string FullName59 = (string)ORJournalLine.JournalCreditLine.AccountRef.FullName.GetValue();
                                        Console.WriteLine("{0}", FullName59);

                                    }
                                }
                                if (ORJournalLine.JournalCreditLine.Amount != null)
                                {
                                    double Amount60 = (double)ORJournalLine.JournalCreditLine.Amount.GetValue();
                                    Console.WriteLine("{0}", Amount60);

                                }
                                if (ORJournalLine.JournalCreditLine.Memo != null)
                                {
                                    string Memo61 = (string)ORJournalLine.JournalCreditLine.Memo.GetValue();
                                    Console.WriteLine("{0}", Memo61);

                                }
                                if (ORJournalLine.JournalCreditLine.EntityRef != null)
                                {
                                    if (ORJournalLine.JournalCreditLine.EntityRef.ListID != null)
                                    {
                                        string ListID62 = (string)ORJournalLine.JournalCreditLine.EntityRef.ListID.GetValue();
                                        Console.WriteLine("{0}", ListID62);

                                    }
                                    if (ORJournalLine.JournalCreditLine.EntityRef.FullName != null)
                                    {
                                        string FullName63 = (string)ORJournalLine.JournalCreditLine.EntityRef.FullName.GetValue();
                                        Console.WriteLine("{0}", FullName63);

                                    }
                                }
                                if (ORJournalLine.JournalCreditLine.ClassRef != null)
                                {
                                    if (ORJournalLine.JournalCreditLine.ClassRef.ListID != null)
                                    {
                                        string ListID64 = (string)ORJournalLine.JournalCreditLine.ClassRef.ListID.GetValue();
                                        Console.WriteLine("{0}", ListID64);

                                    }
                                    if (ORJournalLine.JournalCreditLine.ClassRef.FullName != null)
                                    {
                                        string FullName65 = (string)ORJournalLine.JournalCreditLine.ClassRef.FullName.GetValue();
                                        Console.WriteLine("{0}", FullName65);

                                    }
                                }
                            }
                        }
                    }
                }
                if (JournalEntryRet.DataExtRetList != null)
                {
                    for (int i66 = 0; i66 < JournalEntryRet.DataExtRetList.Count; i66++)
                    {
                        IDataExtRet DataExtRet = JournalEntryRet.DataExtRetList.GetAt(i66);
                        if (DataExtRet.OwnerID != null)
                        {
                            string OwnerID67 = (string)DataExtRet.OwnerID.GetValue();
                            Console.WriteLine("{0}", OwnerID67);

                        }
                        string DataExtName68 = (string)DataExtRet.DataExtName.GetValue();
                        Console.WriteLine("{0}", DataExtName68);

                        ENDataExtType DataExtType69 = (ENDataExtType)DataExtRet.DataExtType.GetValue();
                        Console.WriteLine("{0}", DataExtType69);

                        string DataExtValue70 = (string)DataExtRet.DataExtValue.GetValue();
                        Console.WriteLine("{0}", DataExtValue70);

                    }
                }
                Console.WriteLine();
            }
        }
    }
}
