﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class StoreQuery: IMessageHandler
    {
         List<Store> storeList;

         public StoreQuery()
        {
            storeList = new List<Store>();
        }

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            SessionResult result;

            if (responseMsgSet == null)
                return new SessionResult(false);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false);

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                if (response.StatusCode >= 0)
                {
                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtClassQueryRs)
                        {
                            IClassRetList ClassRet = (IClassRetList)response.Detail;
                            result = WalkClassRet(ClassRet);
                        }
                    }
                }
            }

            return  new SessionResult(true);
        }


        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            IClassQuery ClassQueryRq = requestMsgSet.AppendClassQueryRq();

            ClassQueryRq.ORListQuery.ListFilter.ActiveStatus.SetValue(ENActiveStatus.asAll);
            //TODO: extend filters list

            return new SessionResult(true);
        }


        private SessionResult WalkClassRet(IClassRetList ClassRet)
        {
            for (int i = 0; i < ClassRet.Count; i++)
            {
                IClassRet icr=ClassRet.GetAt(i);

                if (icr != null)
                {
                    storeList.Add(new Store( (icr.Name != null) ? icr.FullName.GetValue():""
                                            , (icr.Name != null) ? icr.ListID.GetValue():""));
                }
            }

            return new SessionResult(true);
        }


        public List<Store> GetStoreList()
        {
            return storeList;
        }
    }
}
