﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;

namespace QuickbooksExport
{
    public class AccountQuery : IMessageHandler
    {
        List<Account> accountList;

        public AccountQuery()
        {
            accountList = new List<Account>();
        }

        public SessionResult HandleResponse(IMsgSetResponse responseMsgSet)
        {
            SessionResult result;

            if (responseMsgSet == null)
                return new SessionResult(false);

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null)
                return new SessionResult(false);

            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);

                if (response.StatusCode >= 0)
                {
                    if (response.Detail != null)
                    {
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtAccountQueryRs)
                        {
                            IAccountRetList AccountRet = (IAccountRetList)response.Detail;
                            result = WalkClassRet(AccountRet);
                        }
                    }
                }
            }

            return new SessionResult(true);
        }


        public SessionResult BuildRequest(IMsgSetRequest requestMsgSet)
        {
            IAccountQuery AccountQueryRq = requestMsgSet.AppendAccountQueryRq();

            return new SessionResult(true);
        }


        private SessionResult WalkClassRet(IAccountRetList AccountRetList)
        {
            //List<int> selectedNumbers = new List<int>(){
            //        1100, 1115, 1120, 1599, 2200, 2205, 4100, 4105, 4110
            //        , 4115, 4120, 4125, 4130, 4135, 4140, 4200, 4205, 4210
            //        , 4300, 4305, 4310, 4315, 4320, 4400, 4500, 7300, 7400, 7805 
            //};

            for (int i = 0; i < AccountRetList.Count; i++)
            {
                IAccountRet iar = AccountRetList.GetAt(i);

                if (iar != null)
                {
                    accountList.Add(new Account( (iar.Name != null) ? iar.Name.GetValue():""
                                                , (iar.FullName != null) ?  iar.FullName.GetValue():""
                                                , (iar.ListID != null) ? iar.ListID.GetValue():""
                                                , (iar.AccountNumber != null) ? iar.AccountNumber.GetValue():""
                                                , (iar.AccountType != null) ? iar.AccountType.GetAsString():""
                                                , (iar.BankNumber != null) ? iar.BankNumber.GetValue() : ""
                                                , (iar.TotalBalance != null) ? iar.TotalBalance.GetValue(): 0
                                                )
                                                );

                }
            }

            return new SessionResult(true);
        }


        public List<Account> GetAccountList()
        {
            return accountList;
        }

    }
}
