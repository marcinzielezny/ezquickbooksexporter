﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBFC12Lib;
using System.Runtime.InteropServices;
using System.Threading;

namespace QuickbooksExport
{
    public class Session : IDisposable
    {
        #region Members

        private volatile QBSessionManager manager;
        private volatile bool isActiveSession = false;
        private volatile bool isActiveConnection = false;
        Thread disposingThread = null;

        private List<Employee> employees;
        private List<Store> stores;
        private List<Account> accounts;
        private List<PayrollItem> payrollItems;
        private List<Vendor> vendors;
        private Company company;
        private List<TimesheetEntry> timesheetItems;
        private List<JournalEntry> journalEntries;
        private List<SalesReceipt> salesReceipts;

        private String appAlias;
        private short version;
        private short subversion;

        #endregion

        #region Constructors and destructors

        public Session(String appAlias, short version, short subversion)
        {
            manager = new QBSessionManager();
            timesheetItems = new List<TimesheetEntry>();
            journalEntries = new List<JournalEntry>();
            salesReceipts = new List<SalesReceipt>();

            this.appAlias = appAlias;
            this.version = version;
            this.subversion = subversion;
            this.isActiveSession = true;

            disposingThread = new Thread(this.DelayedDispose);
        }

        ~Session()
        {
            Disconnect();
        }

        #endregion

        #region Getters and Setters

        public void CloseSessionActivity()
        {
            this.isActiveSession = false;
        }

        public short GetVersion()
        {
            return this.version;
        }

        public Store GetStore(int index)
        {
            return this.stores[index];
        }

        public Employee GetEmployee(int index)
        {
            return this.employees[index];
        }

        public PayrollItem GetPayrollItem(int index)
        {
            return this.payrollItems[index];
        }

        public Account GetAccount(int index)
        {
            return this.accounts[index];
        }

        public Vendor GetVendor(int index)
        {
            return this.vendors[index];
        }

        public Store GetStore(String name)
        {
            for (int i = 0; i < stores.Count; i++)
            {
                if (stores[i].Name == name)
                {
                    return this.stores[i];
                }
            }
            return null;
        }

        public Employee GetEmployee(String name)
        {
            for (int i = 0; i < employees.Count; i++)
            {
                if (employees[i].Name == name)
                {
                    return this.employees[i];
                }
            }
            return null;
        }

        public PayrollItem GetPayrollItem(String name)
        {

            for (int i = 0; i < accounts.Count; i++)
            {
                if (payrollItems[i].Name == name)
                {
                    return this.payrollItems[i];
                }
            }
            return null;
        }

        public Account GetAccount(String number)
        {
            for (int i = 0; i < accounts.Count; i++)
            {
                if (accounts[i].Number == number)
                {
                    return this.accounts[i];
                }
            }
            return null;
        }

        public Vendor GetVendor(String name)
        {
            for (int i = 0; i < vendors.Count; i++)
            {
                if (vendors[i].Name == name)
                {
                    return this.vendors[i];
                }
            }
            return null;
        }


        public Company GetCompany()
        {
            return this.company;
        }

        public List<Store> GetStores()
        {
            return this.stores;
        }

        public List<Employee> GetEmployees()
        {
            return this.employees;
        }

        public List<Account> GetAccounts()
        {
            return this.accounts;
        }

        public List<PayrollItem> GetPayrollItems()
        {
            return this.payrollItems;
        }

        public List<Vendor> GetVendors()
        {
            return this.vendors;
        }

        private SessionResult FillEmployeeList(EmmployeeQuery emp)
        {
            employees = emp.GetEmployeeList();
            return new SessionResult(true);
        }

        private SessionResult FillStoreList(StoreQuery store)
        {
            stores = store.GetStoreList();
            return new SessionResult(true);
        }

        private SessionResult FillAccountList(AccountQuery account)
        {
            accounts = account.GetAccountList();
            return new SessionResult(true);
        }

        private SessionResult FillPayrollItemsList(PayrollItemQuery payrollItem)
        {
            payrollItems = payrollItem.GetPayrollItemList();
            return new SessionResult(true);
        }

        private SessionResult FillVendorList(VendorQuery vendor)
        {
            vendors = vendor.GetVendorList();
            return new SessionResult(true);
        }

        private SessionResult FillCompany(CompanyQuery cq)
        {
            company = cq.GetCompany();
            return new SessionResult(true);
        }

        #endregion

        #region Xml import/export operations

        public String ShortSerializeStores()
        {
            String result = "";

            for (int i = 0; i < stores.Count; i++)
            {
                result += stores[i].Serialize();
            }

            return result;
        }

        public String ShortSerializeEmployees()
        {
            String result = "";

            for (int i = 0; i < employees.Count; i++)
            {
                result += employees[i].Serialize();
            }

            return result;
        }

        public String ShortSerializeVendors()
        {
            String result = "";

            for (int i = 0; i < vendors.Count; i++)
            {
                result += vendors[i].Serialize();
            }

            return result;
        }

        public String ShortSerializeAccounts()
        {
            String result = "";

            for (int i = 0; i < accounts.Count; i++)
            {
                result += accounts[i].Serialize();
            }

            return result;
        }

        public String DetailedSerializeAccounts()
        {
            String result = "";

            for (int i = 0; i < accounts.Count; i++)
            {
                result += accounts[i].SerializeDetails();
            }

            return result;
        }

        public String ShortSerializePayrollItems()
        {
            String result = "";

            for (int i = 0; i < payrollItems.Count; i++)
            {
                result += payrollItems[i].Serialize();
            }

            return result;
        }

        public void NormalizeJournalEntry(String xmlInput)
        {
            string[] rows = xmlInput.Split('>');
            foreach (string row in rows)
            {
                if (row.Length > 1)
                {
                    JournalEntry tempEntry = new JournalEntry(row + ">");
                    journalEntries.Add(tempEntry);
                }

            }
        }

        public void NormalizeStoreReceipt(String xmlInput)
        {
            string[] rows = xmlInput.Split('>');
            foreach (string row in rows)
            {
                if (row.Length > 1)
                {
                    SalesReceipt tempReceipt = new SalesReceipt(row + ">");
                    salesReceipts.Add(tempReceipt);
                }

            }
        }

        public void NormalizeTimesheetItem(String xmlInput)
        {
            string[] rows = xmlInput.Split('>');
            foreach (string row in rows)
            {
                if (row.Length > 1)
                {
                    TimesheetEntry tempEntry = new TimesheetEntry(row + ">");
                    timesheetItems.Add(tempEntry);
                }

            }
        }

        #endregion

        public SessionResult Connect(String filepath)
        {
            this.isActiveConnection = true;
            disposingThread.Start();

            try
            {
                this.manager.OpenConnection2(filepath, this.appAlias, ENConnectionType.ctLocalQBD);
                this.manager.BeginSession(filepath, ENOpenMode.omDontCare);
            }
            catch (COMException cex)
            {
                this.manager.CloseConnection();
                this.manager.OpenConnection2("", this.appAlias, ENConnectionType.ctLocalQBD);
                this.manager.BeginSession(filepath, ENOpenMode.omDontCare);
            }


            this.isActiveConnection = false;

            return new SessionResult(true);
        }

        public SessionResult Disconnect()
        {
            if (manager != null)
            {
                try
                {
                    this.manager.EndSession();
                    this.manager.CloseConnection();
                }
                catch (Exception ex)
                {
                    return new SessionResult(false, "Connection closing error: " + ex.Message, -1);
                }
                finally
                {
                    Marshal.ReleaseComObject(manager);
                    manager = null;
                }
            }

            if (disposingThread.IsAlive)
            {
                disposingThread.Abort();
                disposingThread.Join();
            }

            return new SessionResult(true);
        }

        public void DelayedDispose()
        {
            try
            {
                DateTime endTime = DateTime.Now.AddMinutes(5);

                while (DateTime.Now < endTime && isActiveSession == true) { }

                try
                {
                    this.manager.EndSession();
                    this.manager.CloseConnection();
                }
                catch
                {
                    //  throw new Exception("Delayed dispose error: closing session error");
                }

                while (isActiveConnection == true && isActiveSession == true) { }

                try
                {
                    Marshal.ReleaseComObject(manager);
                    manager = null;
                }
                catch
                {
                    //  throw new Exception("Delayed dispose error: disposing com object error");
                }

                disposingThread.Abort();
                disposingThread.Join();
            }
            catch
            {
                // throw new Exception("Delayed dispose error: disposing com object error");
            }
        }

        public SessionResult Execute(IMessageHandler action)
        {
            SessionResult result;
            IMsgSetRequest requestMsgSet;
            IMsgSetResponse responseMsgSet;

            requestMsgSet = manager.CreateMsgSetRequest("US", this.version, this.subversion);
            result = action.BuildRequest(requestMsgSet);

            responseMsgSet = manager.DoRequests(requestMsgSet);
            result = action.HandleResponse(responseMsgSet);

            switch (action.ToString())
            {
                case "QuickbooksExport.EmmployeeQuery":
                    FillEmployeeList((EmmployeeQuery)action);
                    break;
                case "QuickbooksExport.StoreQuery":
                    FillStoreList((StoreQuery)action);
                    break;
                case "QuickbooksExport.AccountQuery":
                    FillAccountList((AccountQuery)action);
                    break;
                case "QuickbooksExport.PayrollItemQuery":
                    FillPayrollItemsList((PayrollItemQuery)action);
                    break;
                case "QuickbooksExport.VendorQuery":
                    FillVendorList((VendorQuery)action);
                    break;
                case "QuickbooksExport.CompanyQuery":
                    FillCompany((CompanyQuery)action);
                    break;
            }

            return new SessionResult(result.success, result.description, result.code);
        }


        public SessionResult InsertsPOSCustomer()
        {
            SessionResult result = new SessionResult(true);

            result = Execute(new CustomerInsert(new Customer()));

            return result; 
        }

        public SessionResult ExecuteInserts()
        {
            SessionResult result = new SessionResult(true);

            if (timesheetItems.Count > 0)
            {

                foreach (DateTime da in timesheetItems.Select(item => item.BusinessDate).Distinct())
                {
                    result = Execute(new TimesheetInsert(timesheetItems.Where(item => item.BusinessDate == da).ToList()));
                }
            }

            if (journalEntries.Count > 0)
            {
                foreach (DateTime da in journalEntries.Select(item => item.BusinessDate).Distinct())
                {
                    result = Execute(new JournalEntryInsert(journalEntries.Where(item => item.BusinessDate == da).ToList()));
                }
            }

            if (salesReceipts.Count > 0)
            {
                foreach (DateTime da in salesReceipts.Select(item => item.BusinessDate).Distinct())
                {
                    foreach (String sn in salesReceipts.Select(item => item.StoreName).Distinct())
                    {
                        if (salesReceipts.Where(item => item.BusinessDate == da && item.StoreName == sn).ToList() != null)
                        {
                            result = Execute(new SalesReceiptInsert(salesReceipts.Where(item => item.BusinessDate == da && item.StoreName == sn).ToList()));

                            if (result.code != 0)
                            {
                                break; 
                            }
                        }
                    }

                    if (result.code != 0)
                    {
                        break;
                    }
                }
            }

            return new SessionResult(result.success, result.description, result.code);
        }

        public void PrintInfo()
        {
            Console.WriteLine("\n=====================================================");
            Console.WriteLine("Employees:\n");

            for (int i = 0; i < employees.Count; i++)
            {
                Console.WriteLine("{0}", employees[i].ToString());
            }

            Console.WriteLine("\n=====================================================");
            Console.WriteLine("Stores:\n");

            for (int i = 0; i < stores.Count; i++)
            {
                Console.WriteLine("{0}", stores[i].ToString());
            }

            Console.WriteLine("\n=====================================================");
            Console.WriteLine("Payroll Items:\n");

            for (int i = 0; i < payrollItems.Count; i++)
            {
                Console.WriteLine("{0}", payrollItems[i].ToString());
            }

            Console.WriteLine("\n=====================================================");
            Console.WriteLine("Accounts:\n");

            for (int i = 0; i < accounts.Count; i++)
            {
                Console.Write("{0} \t", i);
                Console.WriteLine("{0}", accounts[i].ToString());
            }

            Console.WriteLine("{0}", ShortSerializeAccounts());
            Console.WriteLine();
            Console.WriteLine("{0}", ShortSerializeEmployees());
            Console.WriteLine();
            Console.WriteLine("{0}", ShortSerializePayrollItems());
            Console.WriteLine();
            Console.WriteLine("{0}", ShortSerializeStores());
            Console.WriteLine();
            Console.WriteLine("{0}", DetailedSerializeAccounts());
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
