﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace QuickbooksExport
{
    public class Account
    {
        private String name;
        private String fullName;
        private String number;
        private String type;
        private String bankNo;
        private double balance;
        private String listID;

        public Account(String name, String fullName, String listID, String number, String type, String bankNo, double balance)
        {
            this.name = name;
            this.fullName = fullName;
            this.listID = listID;
            this.number = number;
            this.type = type;
            this.bankNo = bankNo;
            this.balance = balance;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string FullName
        {
            get { return fullName; }
            set { fullName = value; }
        }

        public string ListID
        {
            get { return listID; }
            set { listID = value; }
        }

        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        public string BankNumber
        {
            get { return bankNo; }
            set { bankNo = value; }
        }

        public double Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        public override string ToString()
        {
            return String.Format("{0,6}", this.number)
                    + String.Format("{0,25}", this.name)
                    + String.Format("{0,11}", this.bankNo)
                    + String.Format("{0,25}", this.type)
                    + String.Format("{0,6}", this.balance);
        }

        public String Serialize()
        {
            XElement tmpElem = new XElement(new XElement("QBAccount",
                                                new XAttribute("ValueID", ListID),
                                                new XAttribute("Value", Number + '-' + Name + ' ' + BankNumber),
                                                new XAttribute("ShortNumber", Number),
                                                new XAttribute("Type", type)
                                              )
                                           );

            return tmpElem.ToString();
        }

        public String SerializeDetails()
        {
            XElement tmpElem = new XElement("QBAccount",
                                                new XAttribute("ValueID", ListID),
                                                  new XAttribute("Name", Name),
                                                  new XAttribute("Value", Number + FullName + BankNumber),
                                                  new XAttribute("Number", Number),
                                                  new XAttribute("BankNumber", BankNumber),
                                                  new XAttribute("Balance", Balance),
                                                  new XAttribute("Type", type)
                                           );

            return tmpElem.ToString();
        }

        
    }
}

