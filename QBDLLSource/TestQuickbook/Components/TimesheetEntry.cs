﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;

namespace QuickbooksExport
{
    public class TimesheetEntry
    {
        private Employee employee;
        private PayrollItem pItem;
        private TimeSpan duration;
        private DateTime businessDate;
        private String classID; 

        public TimesheetEntry(Employee emp,  PayrollItem pItem, TimeSpan duration, DateTime businessDate, String classID)
        {
            this.employee = emp;
            this.pItem = pItem;
            this.duration = duration;
            this.businessDate = businessDate;
            this.classID = classID; 
        }

        public TimesheetEntry(String xmlData)
        {
            this.Normalize(xmlData);
        }

        public Employee Employee
        {
            get { return employee; }
            set { employee = value; }
        }

        public PayrollItem PItem
        {
            get { return pItem; }
            set { pItem = value; }
        }

        public TimeSpan Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        public DateTime BusinessDate
        {
            get { return businessDate; }
            set { businessDate = value; }
        }

        public string ClassID
        {
            get { return classID; }
            set { classID = value; }
        }

        private void Normalize(String strElement)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(strElement);

            XmlElement xe = doc.DocumentElement;

            this.employee = new Employee(Convert.ToString(xe.Attributes[0].Value), Convert.ToString(xe.Attributes[1].Value));
            this.pItem = new PayrollItem(Convert.ToString(xe.Attributes[2].Value), Convert.ToString(xe.Attributes[3].Value));
            string[] timeSpanArray =  xe.Attributes[4].Value.ToString().Split(':');
            this.Duration = new TimeSpan(int.Parse(timeSpanArray[0]), int.Parse(timeSpanArray[1]), int.Parse(timeSpanArray[2]));
            this.businessDate = DateTime.Parse(xe.Attributes[5].Value);
            this.classID = Convert.ToString(xe.Attributes[6].Value);
        }

        public String Serialize()
        {
            XElement tmpElem = new XElement(new XElement("QBTimesheetEntry",
                                                            new XAttribute("EmployeeName", this.employee.Name),
                                                            new XAttribute("EmployeeID", this.employee.ListID),
                                                            new XAttribute("PayrollItemName", this.pItem.Name),
                                                            new XAttribute("PayrollItemID", this.pItem.ListID),
                                                            new XAttribute("Duration", this.duration),
                                                            new XAttribute("BusinessDate", this.businessDate),
                                                            new XAttribute("ClassID", this.classID)
                                                    )
                                           );


            return tmpElem.ToString();
        }
    }
}
