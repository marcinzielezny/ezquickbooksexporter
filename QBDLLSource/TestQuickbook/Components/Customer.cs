﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace QuickbooksExport
{
    public class  Customer
    {
        private String name;
        private String listID;

        public Customer()
        {
            this.name = "";
            this.listID = "";
        }


        public Customer(String name, String listID)
        {
            this.name = name;
            this.listID = listID;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string ListID
        {
            get { return listID; }
            set { listID = value; }
        }

        public override string ToString()
        {
            return this.name + "\t " + this.listID + "\t ";
        }

        public String Serialize()
        {
            XElement tmpElem = new XElement("QBCustomer",
                                            new XAttribute("ValueID", ListID),
                                            new XAttribute("Value", Name)  );


            return tmpElem.ToString();
        }
    }
}
