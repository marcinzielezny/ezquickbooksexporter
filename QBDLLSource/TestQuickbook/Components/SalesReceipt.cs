﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Globalization;

namespace QuickbooksExport
{
    public class SalesReceipt
    {

        private String item;

        private String description;
        private Double amount;
        private Double? quantity;

        private Double rate;
        private String classListID;
        private String exportNo;
        private DateTime businessDate;
        private String storeName;

        public SalesReceipt(String item, String description, Double amount, Double quantity, Double rate, String classListID, String exportNo, String storeName)
        {
            this.item = item;
            this.description = description;
            this.amount = amount;
            this.quantity = quantity;
            this.rate = rate; 
            this.classListID = classListID;
            this.exportNo = exportNo;
            this.storeName = storeName; 

        }

        public SalesReceipt(String xmlData)
        {
            this.Normalize(xmlData);
        }

        private void Normalize(String strElement)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(strElement);

            XmlElement xe = doc.DocumentElement;

            this.item = Convert.ToString(xe.Attributes[0].Value);
            this.description = Convert.ToString(xe.Attributes[1].Value);
            this.amount = Double.Parse(xe.Attributes[2].Value, NumberStyles.Currency, new System.Globalization.CultureInfo("en-US"));

            if (!String.IsNullOrEmpty(xe.Attributes[3].Value))
            {
                this.quantity =
                    Double.Parse(xe.Attributes[3].Value, NumberStyles.Currency, new System.Globalization.CultureInfo("en-US"));
            }

            this.rate = Double.Parse(xe.Attributes[4].Value, NumberStyles.Currency, new System.Globalization.CultureInfo("en-US"));
            this.classListID = Convert.ToString(xe.Attributes[5].Value);
            this.exportNo = Convert.ToString(xe.Attributes[6].Value);
            this.businessDate = DateTime.Parse(xe.Attributes[7].Value);
            this.storeName = Convert.ToString(xe.Attributes[12].Value);
         
        }

        public Double Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public String Description
        {
            get { return description; }
            set { description = value; }
        }


        public DateTime BusinessDate
        {
            get { return businessDate; }
            set { businessDate = value; }
        }
        public String ClassListID
        {
            get { return classListID; }
            set { classListID = value; }
        }

        public String Item
        {
            get { return item; }
            set { item = value; }
        }

        public Double Rate
        {
            get { return rate; }
            set { rate = value; }
        } 

        public String ExportNo
        {
            get { return exportNo; }
            set { exportNo = value; }
        }

        public Double? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public String StoreName
        {
            get { return storeName; }
            set { storeName = value; }
        }

        public String Serialize()
        {
            XElement tmpElem = new XElement(new XElement("QBSalesReceipt",
                                                            new XAttribute("Item", this.Item.ToString()),
                                                            new XAttribute("Description", this.Description),
                                                            new XAttribute("Amount", this.Amount.ToString().ToString().Replace(".", ",")),
                                                            new XAttribute("Quantity", this.Quantity.ToString().ToString().Replace(".", ",")),
                                                            new XAttribute("Rate", this.Rate.ToString().Replace(".", ",")),
                                                            new XAttribute("classListID", this.classListID),
                                                            new XAttribute("ExportNo", this.exportNo),
                                                            new XAttribute("BusinessDate", this.BusinessDate.ToString())
                                                        )
                                           );

            return tmpElem.ToString();
        }

    }
}
