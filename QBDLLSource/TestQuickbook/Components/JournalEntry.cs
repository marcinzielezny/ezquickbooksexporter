﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;

namespace QuickbooksExport
{
    public class JournalEntry
    {
        private Double amount;
        private String description;
        private String accountListID;
        private String accountFullName;
        private EntryType accountType;
        private DateTime businessDate;
        private String vendorListID;
        private String classListID;
        private String exportNo;

        public JournalEntry(Double amount, String description, Account account, EntryType accountType, DateTime businessDate, String vendorListID, String classListID, String exportNo)
        {
            this.amount = amount;
            this.description = description;
            this.accountFullName = account.FullName;
            this.accountListID = account.ListID;
            this.accountType = accountType;
            this.businessDate = businessDate;
            this.vendorListID = vendorListID;
            this.classListID = classListID;
        }

        public JournalEntry(String xmlData)
        {
            this.Normalize(xmlData);
        }

        private void Normalize(String strElement)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(strElement);

            XmlElement xe = doc.DocumentElement;

            this.amount = Convert.ToDouble(xe.Attributes[0].Value);
            this.description = Convert.ToString(xe.Attributes[1].Value);
            this.accountListID= Convert.ToString(xe.Attributes[2].Value);
            this.accountFullName = Convert.ToString(xe.Attributes[3].Value);

            if (Convert.ToString(xe.Attributes[4].Value) == "Credit")
                this.accountType = EntryType.Credit;
            else
                this.accountType = EntryType.Debit;

            this.businessDate = DateTime.Parse(xe.Attributes[5].Value);
            this.exportNo = Convert.ToString(xe.Attributes[7].Value);
            this.vendorListID = Convert.ToString(xe.Attributes[8].Value);
            this.classListID = Convert.ToString(xe.Attributes[9].Value);
        }

        public Double Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public String Description
        {
            get { return description; }
            set { description = value; }
        }

        public String AccountListID
        {
            get { return accountListID; }
            set { accountListID = value; }
        }

        public String AccountFullName
        {
            get { return accountFullName; }
            set { accountFullName = value; }
        }

        public EntryType AccountType
        {
            get { return accountType; }
            set { accountType = value; }
        }

        public DateTime BusinessDate
        {
            get { return businessDate; }
            set { businessDate = value; }
        }


        public String VendorListID
        {
            get { return vendorListID; }
            set { vendorListID = value; }
        }

        public String ClassListID
        {
            get { return classListID; }
            set { classListID = value; }
        }


        public String ExportNo
        {
            get { return exportNo; }
            set { exportNo = value; }
        } 


        public String Serialize()
        {
            XElement tmpElem = new XElement(new XElement("QBJournalEntry",
                                                            new XAttribute("Amount", this.Amount.ToString().Replace(".",",")),
                                                            new XAttribute("Description", this.Description),
                                                            new XAttribute("AccountListID", this.AccountListID),
                                                            new XAttribute("AccountFullName", this.AccountFullName),
                                                            new XAttribute("AccountType", this.AccountType.ToString()),
                                                            new XAttribute("BusinessDate", this.BusinessDate.ToString()),
                                                            new XAttribute("ExportNo", this.exportNo),
                                                            new XAttribute("vendorListID", this.vendorListID),
                                                            new XAttribute("classListID", this.vendorListID)
                                                        )
                                           );

            return tmpElem.ToString();
        }

    }
}
