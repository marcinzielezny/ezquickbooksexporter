﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickbooksExport;
using System.Windows.Forms;
using System.Threading;

namespace TestComponents
{
    class Program
    {
        static void Main(string[] args)
        {
            Session session = new Session("EZQuick", 3, 0);
            try
            {
                // Console.WriteLine("Enter company quickbook file path:");

                string path = @"C:\Users\michal.wrazen\Desktop\QB\Sample Subway.QBW";

                session.Connect(path);

                session.Execute(new StoreQuery());
                session.Execute(new EmmployeeQuery());
                session.Execute(new AccountQuery());
                session.Execute(new PayrollItemQuery());
                session.Execute(new CompanyQuery());
                session.Execute(new VendorQuery());

                //   session.PrintInfo();

                PayrollItem pItem = session.GetPayrollItem(0);
                Employee emp = session.GetEmployee("Employee 1");
                Store store = session.GetStore(0);
                Account creditAcnt = session.GetAccount("1100");
                Account debittAcnt = session.GetAccount("1115");

                Company comp = session.GetCompany();

                short ver = session.GetVersion();
                string description = "SubwayPOS export";

                DateTime businessDay = DateTime.Parse("03/25/2013");

                //JournalEntry jEntry1 = new JournalEntry(99.00, description, creditAcnt, EntryType.Credit, businessDay, "80000002-1362073081", "00000043/1" );
                //JournalEntry jEntry2 = new JournalEntry(99.00, description, debittAcnt, EntryType.Debit, businessDay, "80000002-1362073081", "00000043/1" );

                //List<JournalEntry> JEList = new List<JournalEntry>() { jEntry1, jEntry2 };
                String combo = "";
                //foreach(JournalEntry je in JEList)
                //{
                //    combo += je.Serialize();
                //}

                //TimesheetEntry tsEntry = new TimesheetEntry(emp, pItem, TimeSpan.Parse("5:23:31"), businessDay);

                //String combo2 = tsEntry.Serialize();
                TimeSpan ts = new TimeSpan(33, 13, 13);

                session.InsertsPOSCustomer();

                session.NormalizeTimesheetItem("<Employee blank=\"\" QBEmployee=\"80000001-1363012474\" blank1=\"\" PayRoll=\"80000008-1363029069\" HrsWrkd=\"8:14:19\" BussinessDate=\"2014-11-25\" ClassListID=\"80000002-1362073081\" ExportID=\"000000318/4\" DataID=\"13950\" ConfigID=\"4\" EmployeeID=\"62\" EmployeeName=\"Jane Doe\" />");

              /*  session.NormalizeStoreReceipt("<Stores FieldName=\"Closing Keep\" Description=\"Closing Keep\" Amount=\"-700.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14777\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Opening Cash\" Description=\"Opening Cash\" Amount=\"700.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14784\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Deposit in Bank\" Description=\"Deposit in Bank\" Amount=\"1256.5500\" Quantity=\"\" Rate=\"-1256.5500\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14791\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Cashcards\" Description=\"Cashcards\" Amount=\"64.9900\" Quantity=\"\" Rate=\"-64.9900\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14721\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Debit\" Description=\"Debit\" Amount=\"0.0000\" Quantity=\"\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14537\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Visa And Mastercard\" Description=\"Visa And Mastercard\" Amount=\"1539.7900\" Quantity=\"\" Rate=\"-1539.7900\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14806\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Discover\" Description=\"Discover\" Amount=\"7.1100\" Quantity=\"\" Rate=\"-7.1100\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14653\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"American Express\" Description=\"American Express\" Amount=\"37.4600\" Quantity=\"\" Rate=\"-37.4600\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14692\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Catering Call Center\" Description=\"Catering Call Center\" Amount=\"0.0000\" Quantity=\"\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14543\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Other\" Description=\"Other\" Amount=\"0.0000\" Quantity=\"\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14550\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Paidouts\" Description=\"Paidouts\" Amount=\"0.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14556\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Over Short\" Description=\"Over Short\" Amount=\"-8.4500\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14654\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Unit Sales\" Description=\"Unit Sales\" Amount=\"2229.9400\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14813\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Drinks Sales\" Description=\"Drinks Sales\" Amount=\"251.8400\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14758\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Misc Sales\" Description=\"Misc Sales\" Amount=\"245.0600\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14757\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Cash Card Sales\" Description=\"Cash Card Sales\" Amount=\"0.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14637\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Sales Tax\" Description=\"Sales Tax\" Amount=\"187.5100\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14742\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Other Receipts\" Description=\"Other Receipts\" Amount=\"0.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000003-1362073086\" ExportID=\"000000165/5\" BusinessDate=\"2014-11-04T00:00:00\" QBStore=\"80000003-1362073086\" StoreID=\"214\" DataID=\"14633\" ConfigID=\"131\" StoreName=\"10784\" />"
+ "<Stores FieldName=\"Closing Keep\" Description=\"Closing Keep\" Amount=\"-600.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14470\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Opening Cash\" Description=\"Opening Cash\" Amount=\"600.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14477\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Deposit in Bank\" Description=\"Deposit in Bank\" Amount=\"1369.9400\" Quantity=\"\" Rate=\"-1369.9400\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14491\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Cashcards\" Description=\"Cashcards\" Amount=\"89.1700\" Quantity=\"\" Rate=\"-89.1700\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14416\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Debit\" Description=\"Debit\" Amount=\"0.0000\" Quantity=\"\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14230\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Visa And Mastercard\" Description=\"Visa And Mastercard\" Amount=\"2198.9200\" Quantity=\"\" Rate=\"-2198.9200\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14505\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Discover\" Description=\"Discover\" Amount=\"0.0000\" Quantity=\"\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14232\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"American Express\" Description=\"American Express\" Amount=\"122.1900\" Quantity=\"\" Rate=\"-122.1900\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14424\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Catering Call Center\" Description=\"Catering Call Center\" Amount=\"0.0000\" Quantity=\"\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14237\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Other\" Description=\"Other\" Amount=\"0.0000\" Quantity=\"\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14244\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Paidouts\" Description=\"Paidouts\" Amount=\"0.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14250\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Over Short\" Description=\"Over Short\" Amount=\"-14.5900\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14360\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Unit Sales\" Description=\"Unit Sales\" Amount=\"2912.7800\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14509\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Drinks Sales\" Description=\"Drinks Sales\" Amount=\"289.8100\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14457\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Misc Sales\" Description=\"Misc Sales\" Amount=\"334.9600\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14460\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Cash Card Sales\" Description=\"Cash Card Sales\" Amount=\"0.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14332\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Sales Tax\" Description=\"Sales Tax\" Amount=\"257.2600\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14447\" ConfigID=\"131\" StoreName=\"10426\" />"
+ "<Stores FieldName=\"Other Receipts\" Description=\"Other Receipts\" Amount=\"0.0000\" Quantity=\"1.00\" Rate=\"0.0000\" ClassListID=\"80000002-1362073081\" ExportID=\"000000159/5\" BusinessDate=\"2014-02-17T00:00:00\" QBStore=\"80000002-1362073081\" StoreID=\"213\" DataID=\"14327\" ConfigID=\"131\" StoreName=\"10426\" />"
);*/

                SessionResult SR = session.ExecuteInserts();
                Console.WriteLine(SR.description);
                Console.ReadLine();

                session.CloseSessionActivity();

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}", ex.Message));
                session.CloseSessionActivity();
            }


        }
    }
}
