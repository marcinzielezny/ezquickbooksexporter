properties { 
  
 
  $reg_company = "EZUniverse Inc."
  $app_id = "{4B40DA48-B22B-4946-95E3-08816CA895C7}"
  $app_id33 = "{07279EBE-96CE-42FC-ADFF-D9F720081031B}"
  $app_id32 = "{07279EBE-96CE-42FC-ADFF-D9F720100719A}}"
  $app_name = "QuickBookExporter"
  $app_name_on_desktop = "QuickBookExporter"

  $ver = "1.1"
  $rev
  $sqlBit 
  $xrev=".$rev"
  $setup_name = "QuickBookExporter"

  $app_file_name = "$setup_name.Setup.$sqlBit.v.$ver$xrev.exe"
  $cert_File_Name= "IPC.2013.pfx"
  $cert_Password = "Ipc123"
  
  $base_dir         = resolve-path "..\."
  $setup_dir		= "$base_dir\Setup"
  $app_dir			= "$base_dir\Application"
  $build_dir        = "$base_dir\_build"     
  $db_dir 			= "$base_dir\QBExporterDBs"    
  
  $resources_dir      = "$base_dir\resources" 
  
  $tools_dir        = "$base_dir\tools"  

  $repository_dir   = "\\EZSRVGL-OFFICE\Applications\$app_name"
  
} 

include .\psake_ext.ps1

	
task default -depends Release

task Clean { 
  
  Write-Host "Cleaning build folder" -foregroundcolor black -backgroundcolor white 
  
  if (Test-Path  ("$build_dir")){
		Remove-Item -force -recurse $build_dir -ErrorAction SilentlyContinue 
  }
		
} 


task Init -depends  Clean { 	
    	
    new-item $build_dir -itemType directory 
	
	$xml = [xml](Get-Content "$app_dir\App\QuickBookExporter.v.1.1.xml" -Encoding UTF8)
	
	$xml.EditorApplication.Version=[string]$rev;
	
	$xml.EditorApplication.properties.XmlLog.LogConsole.log = "false"
	
	$xml.Save("$app_dir\App\QuickBookExporter.v.1.1.xml")
	
	$xml = [xml](Get-Content "$app_dir\bin\XmlRuntime.exe.Config" -Encoding UTF8)
	
	$xml.configuration.'system.serviceModel'.client.endpoint[0].address = [string]'http://SubwayController:10501/subupdtcentsvc';
	
	$xml.Save("$app_dir\bin\XmlRuntime.exe.Config")
	
    & "$tools_dir\EZCryptoTool\EZCryptoTool.exe" "Crypt" "$app_dir\App\QuickBookExporter.v.1.1.xml" "$app_dir\App\QuickBookExporter.v.1.1.apppc"
		
	Copy-Item  "$app_dir\App\QuickBookExporter.v.1.1.xml"	"$setup_dir\QuickBookExporter.v.1.1\" -force -recurse
	Copy-Item  "$db_dir\bin\Debug\QBExporterDBs.dacpac"	    "$setup_dir\_CommonSetup\ToDeploy\QB.dacpac" -force -recurse
	
	if ($sqlBit -eq "x86")
	{
		Copy-Item  "$tools_dir\SQLServer\SQLEXPR_x86_ENU.exe"   "$setup_dir\_CommonSetup\SQLEXPR_x86_ENU.exe" -force -recurse
	} else 
	{
		Copy-Item  "$tools_dir\SQLServer\SQLEXPR_x64_ENU.exe"   "$setup_dir\_CommonSetup\SQLEXPR_x86_ENU.exe" -force -recurse
	}
	Remove-Item -force -recurse "$app_dir\App\QuickBookExporter.v.1.1.xml"
	
	if ($lastExitCode -ne 0) {
		 throw "Error: Cannot crypt to Apppc"
	}
	
}

task CreatePDF {
	
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.Office.Interop.Word")
	$word_app = New-Object -ComObject Word.Application
	$word_appvisible = $false
	$document = $word_app.Documents.Open("$app_dir\Docs\QuickBookExporter.ReleaseManifest.docx") 
	$pdf_filename = "$app_dir\Docs\QuickBookExporter.ReleaseManifest.pdf"
	$document.SaveAs([ref] $pdf_filename, [ref] 17)
    $document.Close()
	$word_app.Quit()
	
}

task Release -depends SignBuild  {  
 	Copy-Item  	"$setup_dir\QuickBookExporter.v.1.1\$app_file_name" "$build_dir"  -force -recurse
    Remove-Item -force -recurse "$setup_dir\QuickBookExporter.v.1.1\$app_file_name" -ErrorAction SilentlyContinue 
}


task Build -depends Init { 
   
  $old = pwd
  cd $setup_dir
     
  Write-Host "Start of executing InnoSetup" -foregroundcolor black -backgroundcolor white           
  & "$tools_dir\inno\iscc.exe" "$setup_dir\QuickBookExporter.v.1.1.iss" /dRegCompany="$reg_company" /dAppName="$app_name"  /dSetupName="$setup_name"  /dRev="$xrev" /dAppID="$app_id" /dAppID32="$app_id32" /dAppID33="$app_id33" /dAppNameOnDesktop="$app_name_on_desktop" /dVersiu="$ver" /dSqlBit="$sqlBit"
  
	if ($lastExitCode -ne 0) {
        throw "Error: Failed to execute iscc."                              
    }
  
  	Copy-Item  "$setup_dir\QuickBookExporter.v.1.1\QuickBookExporter.v.1.1.xml" "$app_dir\App\"	 -force -recurse
	Remove-Item -force -recurse "$setup_dir\QuickBookExporter.v.1.1\QuickBookExporter.v.1.1.xml" -ErrorAction SilentlyContinue 
  
} 


task SignBuild -depends Build {

  $old = pwd
  cd $setup_dir
  
  Write-Host "Start of signing App" -foregroundcolor black -backgroundcolor white            
  
    & "$tools_dir\sign\signtool.exe" sign /d "QuickBookExporter" /f "$tools_dir\sign\$cert_File_Name" /p $cert_Password  "$setup_dir\QuickBookExporter.v.1.1\$app_file_name"  
	& "$tools_dir\sign\signtool.exe" timestamp /t "http://timestamp.verisign.com/scripts/timestamp.dll" "$setup_dir\QuickBookExporter.v.1.1\$app_file_name" 
    & "$tools_dir\sign\signtool.exe" verify /pa /v "$setup_dir\QuickBookExporter.v.1.1\$app_file_name" 
  
  Write-Host "End of signing App" -foregroundcolor black -backgroundcolor white      
  
    if ($lastExitCode -ne 0) {
        throw "Error: Build cannot be signed."                              
    }
  
  cd $old 

} 