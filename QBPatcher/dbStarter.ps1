Set-ExecutionPolicy -executionpolicy Unrestricted -ErrorAction "SilentlyContinue"
Clear-Host
Write-Host Start getting files paths


$path = 'Database\'
$serverName = "localhost\QBUniverse"
$DB = "QBExporterDB" 
$userName = "sa"
$pass = "asSA1#9$"


$sqlpsreg="HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.SqlServer.Management.PowerShell.sqlps"

if (Get-ChildItem $sqlpsreg -ErrorAction "SilentlyContinue")
{
    throw "SQL Server Provider for Windows PowerShell is not installed."
}
else
{
    $item = Get-ItemProperty $sqlpsreg
    $sqlpsPath = [System.IO.Path]::GetDirectoryName($item.Path)
}



$arr =  Get-ChildItem -Path $path
$arr = $arr | Sort-Object Name

Write-Host Sorted files 

Write-Host $arr

foreach ($file in $arr) 
{
	$fullpath = $path + $file.Name

	$fullpath = Resolve-Path -Path  $fullpath

	Write-Host $fullpath
	
	push-location
	
    cd $sqlpsPath
    
    if ( (Get-PSSnapin -Name SqlServerCmdletSnapin100 -ErrorAction SilentlyContinue) -eq $null )
    {
        Add-PSSnapin SqlServerCmdletSnapin100
    }
    
    if ( (Get-PSSnapin -Name SqlServerProviderSnapin100 -ErrorAction SilentlyContinue) -eq $null )
    {
        Add-PSSnapin SqlServerProviderSnapin100
    }
	
    Invoke-sqlcmd -InputFile $fullpath -database $DB -serverinstance $servername -username $username -password $pass
	
    pop-location

}