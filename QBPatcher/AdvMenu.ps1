$title = "Select Action"
$message = "Which action you would like to execute?"

$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Patch with scripts", `
    "Patch with scripts"

$no = New-Object System.Management.Automation.Host.ChoiceDescription "&Patch all database", `
    "Patch all database"

$options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)

$result = $host.ui.PromptForChoice($title, $message, $options, 0) 

switch ($result)
    {
        0 { }
        1 {"You selected No."}
    }